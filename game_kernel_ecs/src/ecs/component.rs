use game_kernel_utils::meta_struct::{MetaStruct, META_STRUCT_FACTORY, MetaStructCreationError, MetaStructReflect};
use game_kernel_utils::{self, meta_struct};
use lazy_static::lazy_static;

use evmap::shallow_copy::ShallowCopy;
use game_kernel_utils::KeyType;
use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Display;
use std::sync::Arc;

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct ComponentIndex {
    pub(super) index: KeyType,
}

impl ComponentIndex {
    pub(super) fn new(index: KeyType) -> Self {
        Self { index }
    }
}

impl From<ComponentIndex> for KeyType {
    fn from(ci: ComponentIndex) -> KeyType {
        ci.index
    }
}

pub type ComponentCreationError = MetaStructCreationError;

pub type ComponentField = meta_struct::Field;

pub trait Component: MetaStruct {
    fn dyn_clone(&self) -> Box<dyn Component>;
}

impl Clone for Box<dyn Component> {
    fn clone(&self) -> Self {
        Component::dyn_clone(self.as_ref())
    }
}

impl MetaStructReflect for dyn Component {
    fn get_field<T: 'static>(&self, name: &str) -> Option<&T> {
        self.get_field_any(name).and_then(|f| f.downcast_ref())
    }

    fn get_field_mut<T: 'static>(&mut self, name: &str) -> Option<&mut T> {
        self.get_field_any_mut(name).and_then(|f| f.downcast_mut())
    }
}

/*pub trait ComponentEq
{
    fn equals_a(&self, other: &'static ComponentEq) -> bool;
}

impl<S: 'static + Component + PartialEq> ComponentEq for S
{
    fn equals_a(&self, other: &ComponentEq) -> bool {
        // Do a type-safe casting. If the types are different,
        // return false, otherwise test the values for equality.
        (&other as &Any)
            .downcast_ref::<S>()
            .map_or(false, |a| self == a)
    }
}*/

use std::any::TypeId;
pub type ComponentId = TypeId;

/*pub struct ComponentBox
{
    boxed_component: Box<dyn Component>,
}

//TODO implement properly
impl PartialEq for ComponentBox
{
    fn eq(&self, other: &ComponentBox) -> bool {
        //self.boxed_component.equals_a(other.boxed_component as &ComponentEq)
        false
    }
}

impl Eq for ComponentBox{}

impl From<Box<dyn Component>> for ComponentBox
{
    fn from(boxed_component: Box<dyn Component>) -> Self{
        return Self{boxed_component}
    }
}

impl ShallowCopy for ComponentBox
{
    unsafe fn shallow_copy(&self) ->core::mem::ManuallyDrop<Self> {
        core::mem::ManuallyDrop::new(
                ComponentBox
                {
                    boxed_component: core::mem::ManuallyDrop::into_inner(self.boxed_component.shallow_copy())
                }
            )
    }
}

/*impl Deref for ComponentBox {
    type Target = Component;

    fn deref(&self) -> &Component {
        self.boxed_component.as_ref()
    }
}*/*/

pub struct ComponentFactory {
    casters: HashMap<String, fn(Box<dyn MetaStruct>) -> Result<Box<dyn Component>, ComponentCreationError>>,
    pub(crate) store_builders: HashMap<ComponentId, fn() -> Box<dyn ComponentStorage>>,
}

impl ComponentFactory {
    pub fn new() -> Self {
        Self {
            casters: HashMap::new(),
            store_builders: HashMap::new(),
        }
    }

    pub fn register<T>(&mut self)
    where
        T: Component,
    {
        let mut meta_lock = META_STRUCT_FACTORY.lock().unwrap();
        meta_lock.register::<T>();
        self.casters.insert(T::get_name(), |dm| dm.into_any().downcast::<T>().map(|v| v as _).map_err(|_| ComponentCreationError::NoSuchMetaStruct));
        self.store_builders
            .insert(TypeId::of::<T>(), || super::store_for_type::<T>());
    }

    pub fn instantiate(
        &mut self,
        name: &str,
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn Component>, ComponentCreationError> {
        let mut meta_lock = META_STRUCT_FACTORY.lock().unwrap();
        meta_lock.instantiate(name, args).and_then(|m| self.casters.get(name).ok_or(ComponentCreationError::NoSuchMetaStruct)?(m))
    }
}

use std::sync::Mutex;

use super::ComponentStorage;
lazy_static! {
    pub static ref COMPONENT_FACTORY: Mutex<ComponentFactory> = Mutex::new(ComponentFactory::new());
}

#[macro_export]
macro_rules! parse_component_arg {
    ($args:expr, $name:expr, $arg_type:ty) => {
        ($args
            .get($name)
            .ok_or(ComponentCreationError::MissingArgument($name.to_owned()))?
            .clone()
            .downcast::<$arg_type>()
            .map_err(|_| ComponentCreationError::MismatechedArgumentType($name.to_owned()))?)
    };
}

#[macro_export]
macro_rules! parse_component_arg_infer {
    ($args:expr, $name:expr) => {
        ($args
            .get($name)
            .ok_or(ComponentCreationError::MissingArgument($name.to_owned()))?
            .clone()
            .downcast()
            .map_err(|_| ComponentCreationError::MismatechedArgumentType($name.to_owned()))?)
    };
}

#[macro_export]
macro_rules! component_catch {
    ($in:block) => {
        (|| -> Result<_, game_kernel_ecs::ecs::component::ComponentCreationError> {
            Ok($in)
        })()
    };
}

use game_kernel_utils;
use lazy_static::lazy_static;

use evmap::shallow_copy::ShallowCopy;
use game_kernel_utils::KeyType;
use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Display;
use std::sync::Arc;

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct ComponentIndex {
    pub(super) index: KeyType,
}

impl ComponentIndex {
    pub(super) fn new(index: KeyType) -> Self {
        Self { index }
    }
}

impl From<ComponentIndex> for KeyType {
    fn from(ci: ComponentIndex) -> KeyType {
        ci.index
    }
}

#[derive(Clone, Debug)]
pub enum ComponentCreationError {
    NoSuchComponent,
    MissingArgument(String),
    WrongArgument(String),
    MismatechedArgumentType(String),
}

impl Display for ComponentCreationError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub struct ComponentField {
    pub type_id: TypeId,
    pub type_name: &'static str,
}

impl ComponentField {
    pub fn new(type_id: TypeId, type_name: &'static str) -> Self {
        Self {
            type_id,
            type_name,
        }
    }
}

pub trait Component: Any {
    fn get_name() -> String
    where
        Self: Sized;
    fn get_name_dyn(&self) -> String;
    fn get_builder() -> fn(
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn Component>, ComponentCreationError>
    where
        Self: Sized;
    fn get_field_any(&self, name: &str) -> Option<&dyn Any>;
    fn get_field_any_mut(&mut self, name: &str) -> Option<&mut dyn Any>;
    fn get_fields(&self) -> &'static [&'static str];
    fn get_fields_types_static() -> HashMap<String, ComponentField> where Self: Sized;
    fn into_any(self: Box<Self>) -> Box<dyn Any>;
    fn gk_as_any(&self) -> &dyn Any;
    fn gk_as_any_mut(&mut self) -> &mut dyn Any;

    fn new_field(type_id: TypeId, type_name: &'static str) -> ComponentField where Self: Sized{
        ComponentField {
            type_id,
            type_name,
        }
    }
}

pub trait ComponentReflect {
    fn get_field<T: 'static>(&self, name: &str) -> Option<&T>;
    fn get_field_mut<T: 'static>(&mut self, name: &str) -> Option<&mut T>;
}

impl<C: Component> ComponentReflect for C {
    fn get_field<T: 'static>(&self, name: &str) -> Option<&T> {
        self.get_field_any(name).and_then(|f| f.downcast_ref())
    }

    fn get_field_mut<T: 'static>(&mut self, name: &str) -> Option<&mut T> {
        self.get_field_any_mut(name).and_then(|f| f.downcast_mut())
    }
}

impl ComponentReflect for dyn Component {
    fn get_field<T: 'static>(&self, name: &str) -> Option<&T> {
        self.get_field_any(name).and_then(|f| f.downcast_ref())
    }

    fn get_field_mut<T: 'static>(&mut self, name: &str) -> Option<&mut T> {
        self.get_field_any_mut(name).and_then(|f| f.downcast_mut())
    }
}

/*pub trait ComponentEq
{
    fn equals_a(&self, other: &'static ComponentEq) -> bool;
}

impl<S: 'static + Component + PartialEq> ComponentEq for S
{
    fn equals_a(&self, other: &ComponentEq) -> bool {
        // Do a type-safe casting. If the types are different,
        // return false, otherwise test the values for equality.
        (&other as &Any)
            .downcast_ref::<S>()
            .map_or(false, |a| self == a)
    }
}*/

use std::any::TypeId;
pub type ComponentId = TypeId;

/*pub struct ComponentBox
{
    boxed_component: Box<dyn Component>,
}

//TODO implement properly
impl PartialEq for ComponentBox
{
    fn eq(&self, other: &ComponentBox) -> bool {
        //self.boxed_component.equals_a(other.boxed_component as &ComponentEq)
        false
    }
}

impl Eq for ComponentBox{}

impl From<Box<dyn Component>> for ComponentBox
{
    fn from(boxed_component: Box<dyn Component>) -> Self{
        return Self{boxed_component}
    }
}

impl ShallowCopy for ComponentBox
{
    unsafe fn shallow_copy(&self) ->core::mem::ManuallyDrop<Self> {
        core::mem::ManuallyDrop::new(
                ComponentBox
                {
                    boxed_component: core::mem::ManuallyDrop::into_inner(self.boxed_component.shallow_copy())
                }
            )
    }
}

/*impl Deref for ComponentBox {
    type Target = Component;

    fn deref(&self) -> &Component {
        self.boxed_component.as_ref()
    }
}*/*/

pub struct ComponentFactory {
    components_map: HashMap<
        String,
        fn(
            args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
        ) -> Result<Box<dyn Component>, ComponentCreationError>,
    >,
    ids_map: HashMap<ComponentId, String>,
    pub(crate) store_builders: HashMap<ComponentId, fn() -> Box<dyn ComponentStorage>>,
    fields_maps: HashMap<String, HashMap<String, ComponentField>>,
}

impl ComponentFactory {
    pub fn new() -> Self {
        Self {
            components_map: HashMap::new(),
            ids_map: HashMap::new(),
            store_builders: HashMap::new(),
            fields_maps: HashMap::new(),
        }
    }

    pub fn register<T>(&mut self)
    where
        T: Component,
    {
        self.components_map.insert(T::get_name(), T::get_builder());
        self.ids_map.insert(TypeId::of::<T>(), T::get_name());
        self.store_builders
            .insert(TypeId::of::<T>(), || super::store_for_type::<T>());
        self.fields_maps.insert(T::get_name(), T::get_fields_types_static());
    }

    pub fn instantiate(
        &mut self,
        name: &str,
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn Component>, ComponentCreationError> {
        match self.components_map.get(name) {
            Some(builder_fn) => builder_fn(args),
            None => Err(ComponentCreationError::NoSuchComponent),
        }
    }

    pub fn all_names<'a>(&'a self) -> impl Iterator<Item = String> + 'a {
        self.components_map.iter().map(|(n, _)| n.clone())
    }

    pub fn get_component_fields(&self, component_name: &str) -> Option<&HashMap<String, ComponentField>> {
        self.fields_maps.get(component_name)
    }

    pub fn get_name(&self, id: ComponentId) -> Option<&String> {
        self.ids_map.get(&id)
    }
}

use std::sync::Mutex;

use super::ComponentStorage;
lazy_static! {
    pub static ref COMPONENT_FACTORY: Mutex<ComponentFactory> = Mutex::new(ComponentFactory::new());
}

#[macro_export]
macro_rules! parse_component_arg {
    ($args:expr, $name:expr, $arg_type:ty) => {
        ($args
            .get($name)
            .ok_or(ComponentCreationError::MissingArgument($name.to_owned()))?
            .clone()
            .downcast::<$arg_type>()
            .map_err(|_| ComponentCreationError::MismatechedArgumentType($name.to_owned()))?)
    };
}

#[macro_export]
macro_rules! parse_component_arg_infer {
    ($args:expr, $name:expr) => {
        ($args
            .get($name)
            .ok_or(ComponentCreationError::MissingArgument($name.to_owned()))?
            .clone()
            .downcast()
            .map_err(|_| ComponentCreationError::MismatechedArgumentType($name.to_owned()))?)
    };
}

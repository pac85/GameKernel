//! ECS root module
//! =======
//! This module contains the word structure, the rest of the ecs is in the submodules
//! -----------

pub mod component;
pub mod entity;
pub mod system;

use game_kernel_utils::{hierarchy, KeyType, SparseSet};

use std::any::{Any, TypeId};
use std::borrow::Borrow;
use std::cell::{Ref, RefCell, RefMut};
use std::ops::DerefMut;
use std::slice::SliceIndex;
use std::sync::Arc;

use self::component::{Component, COMPONENT_FACTORY};

pub type EntitId = KeyType;

pub struct ComponentGuard {
    components: Arc<Vec<Arc<RefCell<dyn component::Component>>>>,
    //pub component_refcell: Option<Ref<'a, dyn component::Component>>,
    index: usize,
}

impl ComponentGuard {
    pub fn new(components: Arc<Vec<Arc<RefCell<dyn component::Component>>>>, index: usize) -> Self {
        Self {
            components,
            //component_refcell: None,
            index,
        }
    }

    pub fn get(&self) -> &Arc<RefCell<dyn component::Component>> {
        self.components.get(self.index).unwrap()
    }
    /*pub fn get(&'a mut self) -> &'a Ref<'a, dyn component::Component> {
        if self.component_refcell.is_none() {
            self.component_refcell = Some(self.components[self.index].borrow())
        }
        self.component_refcell.as_ref().unwrap()
    }*/
}

/*impl<'a> Deref for ComponentGuard<'a>
{
    type Target = &RefCell<Box<dyn component::Component>>;

    fn deref(&'a self) -> &'a Self::Target
    {
        &self.components[self.index]
    }
}*/

pub(crate) trait ComponentStorage: Any {
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
    fn remove(&mut self, k: usize);
    fn component_tid(&self) -> TypeId;
    fn add_dyn(&self, entity: KeyType, component: Box<dyn Component>);
    fn dyn_get(&self, index: usize) -> Option<Ref<'_, dyn Component>>;
    fn dyn_get_mut(&self, index: usize) -> Option<RefMut<'_, dyn Component>>;
}

impl<T: Component + 'static> ComponentStorage for RefCell<SparseSet<T>> {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn remove(&mut self, k: usize) {
        self.borrow_mut().remove(k);
    }

    fn component_tid(&self) -> TypeId {
        TypeId::of::<T>()
    }

    fn add_dyn(&self, entity: KeyType, component: Box<dyn Component>) {
        self.borrow_mut()
            .insert(entity as usize, *component.into_any().downcast().unwrap());
    }

    fn dyn_get(&self, index: usize) -> Option<Ref<'_, dyn Component>> {
        if self.borrow().get(index).is_some() {
            Some(Ref::map(self.borrow(), |c| c.get(index).unwrap()))
        } else {
            None
        }
    }

    fn dyn_get_mut(&self, index: usize) -> Option<RefMut<'_, dyn Component>> {
        if self.borrow().get(index).is_some() {
            Some(RefMut::map(self.borrow_mut(), |c| {
                c.get_mut(index).unwrap()
            }))
        } else {
            None
        }
    }
}

pub(crate) fn store_for_type<C: Component>() -> Box<dyn ComponentStorage> {
    Box::new(RefCell::new(SparseSet::<C>::new()))
}

pub struct World {
    hierarchy: entity::AdjHashMap,
    components: Vec<Box<dyn ComponentStorage>>,
}

impl World {
    pub fn new() -> Self {
        Self {
            hierarchy: entity::AdjHashMap::new(),
            components: vec![],
        }
    }

    pub fn get_root() -> KeyType {
        0
    }

    /// deletes all entities
    pub fn reset(&mut self) {
        //TODO
    }

    ///returns children
    pub fn get_children(&self, parent: KeyType) -> Option<impl Iterator<Item = KeyType>> {
        self.hierarchy.get_children(parent)
    }

    /// this function will create and add ann entity to the world as a child of the specified parent
    /// # Errors
    /// if the specified parent does not exist it will return an Error, otherwise an Ok containing the Entity index
    pub fn add_entity(&mut self, parent: u64) -> Result<u64, &str> {
        match self.hierarchy.insert_entity(&parent) {
            Some(index) => Ok(index),
            None => Err("unable to insert the entity in the hierarchy, parent not found"),
        }
    }

    /// this function will destroy the specified entity and assign it's children to the specified new parent.
    ///
    /// If you instead want to destroy all of the children use rem_entity_recursive
    /// # Errors
    /// if the specified entity does not exist it will return an Error, otherwise an empty Ok
    pub fn rem_entity(&mut self, entity: u64, new_parent: u64) -> Result<(), &str> {
        //moves all children
        if hierarchy::utils::move_children(&mut self.hierarchy, &entity, &new_parent) {
            for stores in self.components.iter_mut() {
                stores.remove(entity as usize);
            }
            Ok(())
        } else {
            Err("unable to find the entity to remove")
        }
    }

    /// this function will destroy the specified entity and it's children.
    ///
    /// If you instead want to assign all of the children to a specified entity use rem_entity
    /// # Errors
    /// if the specified entity does not exist it will return an Error, otherwise an empty Ok
    pub fn rem_entity_recursive(&mut self, entity: u64) -> Result<(), &str> {
        let mut removed = vec![];
        let mut on_removed = |entity_index: &hierarchy::Keytype| {
            removed.push(*entity_index);
        };
        if hierarchy::utils::recursive_delete(&mut self.hierarchy, &entity, &mut on_removed) {
            for entity in removed.into_iter() {
                for stores in self.components.iter_mut() {
                    stores.remove(entity as usize);
                }
            }
            return Ok(());
        } else {
            return Err("unable to find the entity to remove");
        }
    }

    pub fn add_component<C: AddComponent>(&mut self, entity: KeyType, component: C) {
        //check wether entity exists
        let _i = self.hierarchy.get_children(entity).unwrap();

        AddComponent::add_component(self, entity, component);
    }

    pub fn remove_component<C: Component>(&mut self, entity: KeyType) -> Result<(), &'static str> {
        let store = self
            .components
            .iter()
            .find(|c| c.component_tid() == TypeId::of::<C>())
            .ok_or("not found")?;
        let store = store
            .as_any()
            .downcast_ref::<RefCell<SparseSet<C>>>()
            .unwrap();
        store.borrow_mut().remove(entity as usize);
        Ok(())
    }

    /*fn get_store(&mut self, tid: TypeId) -> &Box<dyn ComponentStorage> {
        if let Some(store) = self
            .components
            .iter()
            .find(|c| c.component_tid() == tid)
        {
            store
        } else {
            self.components
                .push(COMPONENT_FACTORY.lock().unwrap().store_builders[&tid]());
            self.components.last().unwrap()
        }
    }*/

    pub(crate) fn add_component_dyn(&mut self, entity: KeyType, component: Box<dyn Component>) {
        let store = if let Some(store) = self
            .components
            .iter()
            .find(|c| c.component_tid() == component.gk_as_any().type_id())
        {
            store
        } else {
            self.components
                .push(COMPONENT_FACTORY.lock().unwrap().store_builders
                    [&component.gk_as_any().type_id()]());
            self.components.last().unwrap()
        };

        store.add_dyn(entity, component);
    }
    pub(crate) fn add_component_static<T: Component>(&mut self, entity: KeyType, component: T) {
        let store = if let Some(store) = self
            .components
            .iter()
            .find(|c| c.component_tid() == TypeId::of::<T>())
        {
            store
        } else {
            self.components
                .push(COMPONENT_FACTORY.lock().unwrap().store_builders[&TypeId::of::<T>()]());
            self.components.last().unwrap()
        };

        store
            .as_any()
            .downcast_ref::<RefCell<SparseSet<T>>>()
            .unwrap()
            .borrow_mut()
            .insert(entity as usize, component);
    }

    /// this function returns true whenever an entity contains a set of components
    pub fn has_components<'a, Q: Query<'a>>(&'a self, entity: u64) -> bool {
        Q::has_components(self, entity)
    }

    pub fn get_components<C: Component>(&self) -> Option<Ref<SparseSet<C>>> {
        let c = self
            .components
            .iter()
            .find(|c| c.component_tid() == TypeId::of::<C>())?;
        let store = c
            .as_any()
            .downcast_ref::<RefCell<SparseSet<C>>>()
            .unwrap()
            .borrow();
        Some(store)
    }

    pub fn get_components_mut<C: Component>(&self) -> Option<RefMut<SparseSet<C>>> {
        let c = self
            .components
            .iter()
            .find(|c| c.component_tid() == TypeId::of::<C>())?;
        let store = c
            .as_any()
            .downcast_ref::<RefCell<SparseSet<C>>>()
            .unwrap()
            .borrow_mut();
        Some(store)
    }

    pub fn get_entity_component<C: Component>(&self, entity: KeyType) -> Option<Ref<C>> {
        let store = self.get_components()?;
        store.get(entity as usize)?;
        let a = Ref::map(store, |v| v.get(entity as usize).unwrap());
        Some(a)
    }

    pub fn get_entity_component_mut<C: Component>(&self, entity: KeyType) -> Option<RefMut<C>> {
        let store = self.get_components_mut()?;
        store.get(entity as usize)?;
        let a = RefMut::map(store, |v| v.get_mut(entity as usize).unwrap());
        Some(a)
    }

    pub fn get_entity_component_dyn(&self, component_id: component::ComponentId, entity: KeyType) -> Option<Ref<dyn Component>> {
        self.components
            .iter()
            .find(|c| c.component_tid() == component_id)?
            .dyn_get(entity as usize)
    }

    pub fn get_entity_component_dyn_mut(&self, component_id: component::ComponentId, entity: KeyType) -> Option<RefMut<dyn Component>> {
        self.components
            .iter()
            .find(|c| c.component_tid() == component_id)?
            .dyn_get_mut(entity as usize)
    }

    pub fn get_entity_components<'a, Q: Query<'a>>(&'a self, entity: KeyType) -> Option<Q::SO> {
        Q::get_components(self, entity)
    }

    pub fn get_all_entity_components(
        &self,
        entity: KeyType,
    ) -> impl Iterator<Item = Ref<'_, dyn Component>> {
        self.components
            .iter()
            .map(move |c| c.dyn_get(entity as usize))
            .filter_map(|x| x)
    }

    pub fn get_all_entity_components_mut(
        &self,
        entity: KeyType,
    ) -> impl Iterator<Item = RefMut<'_, dyn Component>> {
        self.components
            .iter()
            .map(move |c| c.dyn_get_mut(entity as usize))
            .filter_map(|x| x)
    }

    pub fn query<'a, T: 'a + Query<'a>>(&'a self) -> impl Iterator<Item = T> + 'a {
        T::query(self)
    }

    pub fn iter_entities(&self) -> impl Iterator<Item = KeyType> {
        self.hierarchy.iter()
    }
}

pub trait AddComponent {
    fn add_component(world: &mut World, entity: KeyType, component: Self);
}

impl<C: Component> AddComponent for C {
    fn add_component(world: &mut World, entity: KeyType, component: Self) {
        world.add_component_static(entity, component);
    }
}

impl AddComponent for Box<dyn Component> {
    fn add_component(world: &mut World, entity: KeyType, component: Self) {
        world.add_component_dyn(entity, component);
    }
}

pub struct RefIter<'a, T> {
    r: Option<Ref<'a, SparseSet<T>>>,
    i: usize,
}

impl<'a, T> RefIter<'a, T> {
    pub fn new(r: Option<Ref<'a, SparseSet<T>>>) -> Self {
        Self { r, i: 0 }
    }
}

impl<'a, T> Iterator for RefIter<'a, T> {
    type Item = Option<&'a T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.i >= self.r.as_ref()?.len() {
            None
        } else {
            let fr = unsafe { std::mem::transmute(self.r.as_ref()?.get(self.i)) };
            self.i += 1;
            Some(fr)
        }
    }
}

pub struct RefMutIter<'a, T> {
    r: Option<RefMut<'a, SparseSet<T>>>,
    i: usize,
}

impl<'a, T> RefMutIter<'a, T> {
    pub fn new(r: Option<RefMut<'a, SparseSet<T>>>) -> Self {
        Self { r, i: 0 }
    }
}

impl<'a, T> Iterator for RefMutIter<'a, T> {
    type Item = Option<&'a mut T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.i >= self.r.as_mut()?.len() {
            None
        } else {
            let fr = unsafe { std::mem::transmute(self.r.as_mut()?.get_mut(self.i)) };
            self.i += 1;
            Some(fr)
        }
    }
}

pub trait Query<'a> {
    type O: Iterator<Item = Self>;
    type SO;
    fn query(world: &'a World) -> Self::O;
    fn has_components(world: &'a World, entity: EntitId) -> bool;
    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO>;
}

pub struct SingleQueryIter<T, I: Iterator<Item = Option<T>>> {
    i: I,
    pub(crate) entity: KeyType,
}

impl<T, I: Iterator<Item = Option<T>>> SingleQueryIter<T, I> {
    pub fn new(i: I) -> Self {
        Self { i, entity: 0 }
    }
}

impl<T, I: Iterator<Item = Option<T>>> Iterator for SingleQueryIter<T, I> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            self.entity += 1;
            if let Some(v) = self.i.next()? {
                return Some(v);
            }
        }
    }
}

pub struct SingleEnumerate<T, I: Iterator<Item = Option<T>>> {
    inner: SingleQueryIter<T, I>,
}

impl<T, I: Iterator<Item = Option<T>>> SingleEnumerate<T, I> {
    pub fn new(inner: SingleQueryIter<T, I>) -> Self {
        Self { inner }
    }
}

impl<T, I: Iterator<Item = Option<T>>> Iterator for SingleEnumerate<T, I> {
    type Item = (KeyType, T);

    fn next(&mut self) -> Option<Self::Item> {
        let a = self.inner.next()?;
        Some((self.inner.entity - 1, a))
    }
}

impl<'a, C: Component> Query<'a> for &'a C {
    //type O = RefIter<'a, C>;
    type O = SingleQueryIter<Self, RefIter<'a, C>>;
    type SO = Ref<'a, C>;
    fn query(world: &'a World) -> Self::O {
        SingleQueryIter::new(RefIter::new(world.get_components()))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        world.get_entity_component(entity)
    }
}

impl<'a, C: Component> Query<'a> for (KeyType, &'a C) {
    //type O = RefIter<'a, C>;
    type O = SingleEnumerate<&'a C, RefIter<'a, C>>;
    type SO = Ref<'a, C>;
    fn query(world: &'a World) -> Self::O {
        SingleEnumerate::new(SingleQueryIter::new(RefIter::new(world.get_components())))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        world.get_entity_component(entity)
    }
}

impl<'a, C: Component> Query<'a> for &'a mut C {
    //type O = RefIter<'a, C>;
    type O = SingleQueryIter<Self, RefMutIter<'a, C>>;
    type SO = RefMut<'a, C>;

    fn query(world: &'a World) -> Self::O {
        SingleQueryIter::new(RefMutIter::new(world.get_components_mut()))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        world.get_entity_component_mut(entity)
    }
}

impl<'a, C: Component> Query<'a> for (KeyType, &'a mut C) {
    //type O = RefIter<'a, C>;
    type O = SingleEnumerate<&'a mut C, RefMutIter<'a, C>>;
    type SO = RefMut<'a, C>;

    fn query(world: &'a World) -> Self::O {
        SingleEnumerate::new(SingleQueryIter::new(RefMutIter::new(
            world.get_components_mut(),
        )))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        world.get_entity_component_mut(entity)
    }
}

pub struct DoubleQueryIter<T1, I1: Iterator<Item = Option<T1>>, T2, I2: Iterator<Item = Option<T2>>>
{
    i1: I1,
    i2: I2,
    pub(crate) entity: KeyType,
}

impl<T1, I1: Iterator<Item = Option<T1>>, T2, I2: Iterator<Item = Option<T2>>>
    DoubleQueryIter<T1, I1, T2, I2>
{
    pub fn new(i1: I1, i2: I2) -> Self {
        Self { i1, i2, entity: 0 }
    }
}

impl<T1, I1: Iterator<Item = Option<T1>>, T2, I2: Iterator<Item = Option<T2>>> Iterator
    for DoubleQueryIter<T1, I1, T2, I2>
{
    type Item = (T1, T2);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            self.entity += 1;
            if let (Some(v1), Some(v2)) = (self.i1.next()?, self.i2.next()?) {
                return Some((v1, v2));
            }
        }
    }
}

pub struct DoubleEnumerate<T1, I1: Iterator<Item = Option<T1>>, T2, I2: Iterator<Item = Option<T2>>>
{
    inner: DoubleQueryIter<T1, I1, T2, I2>,
}

impl<T1, I1: Iterator<Item = Option<T1>>, T2, I2: Iterator<Item = Option<T2>>>
    DoubleEnumerate<T1, I1, T2, I2>
{
    pub fn new(inner: DoubleQueryIter<T1, I1, T2, I2>) -> Self {
        Self { inner }
    }
}

impl<T1, I1: Iterator<Item = Option<T1>>, T2, I2: Iterator<Item = Option<T2>>> Iterator
    for DoubleEnumerate<T1, I1, T2, I2>
{
    type Item = (KeyType, T1, T2);

    fn next(&mut self) -> Option<Self::Item> {
        let (a, b) = self.inner.next()?;
        Some((self.inner.entity - 1, a, b))
    }
}

impl<'a, C1: Component, C2: Component> Query<'a> for (&'a C1, &'a C2) {
    //type O = RefIter<'a, C>;
    type O = DoubleQueryIter<&'a C1, RefIter<'a, C1>, &'a C2, RefIter<'a, C2>>;
    type SO = (Ref<'a, C1>, Ref<'a, C2>);

    fn query(world: &'a World) -> Self::O {
        DoubleQueryIter::new(
            RefIter::new(world.get_components()),
            RefIter::new(world.get_components()),
        )
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component(entity)?,
            world.get_entity_component(entity)?,
        ))
    }
}

impl<'a, C1: Component, C2: Component> Query<'a> for (KeyType, &'a C1, &'a C2) {
    //type O = RefIter<'a, C>;
    type O = DoubleEnumerate<&'a C1, RefIter<'a, C1>, &'a C2, RefIter<'a, C2>>;
    type SO = (Ref<'a, C1>, Ref<'a, C2>);

    fn query(world: &'a World) -> Self::O {
        DoubleEnumerate::new(DoubleQueryIter::new(
            RefIter::new(world.get_components()),
            RefIter::new(world.get_components()),
        ))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component(entity)?,
            world.get_entity_component(entity)?,
        ))
    }
}

impl<'a, C1: Component, C2: Component> Query<'a> for (&'a mut C1, &'a mut C2) {
    //type O = RefIter<'a, C>;
    type O = DoubleQueryIter<&'a mut C1, RefMutIter<'a, C1>, &'a mut C2, RefMutIter<'a, C2>>;
    type SO = (RefMut<'a, C1>, RefMut<'a, C2>);

    fn query(world: &'a World) -> Self::O {
        DoubleQueryIter::new(
            RefMutIter::new(world.get_components_mut()),
            RefMutIter::new(world.get_components_mut()),
        )
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component_mut(entity)?,
            world.get_entity_component_mut(entity)?,
        ))
    }
}

impl<'a, C1: Component, C2: Component> Query<'a> for (KeyType, &'a mut C1, &'a mut C2) {
    //type O = RefIter<'a, C>;
    type O = DoubleEnumerate<&'a mut C1, RefMutIter<'a, C1>, &'a mut C2, RefMutIter<'a, C2>>;
    type SO = (RefMut<'a, C1>, RefMut<'a, C2>);

    fn query(world: &'a World) -> Self::O {
        DoubleEnumerate::new(DoubleQueryIter::new(
            RefMutIter::new(world.get_components_mut()),
            RefMutIter::new(world.get_components_mut()),
        ))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component_mut(entity)?,
            world.get_entity_component_mut(entity)?,
        ))
    }
}

//TODO macro
//triple
pub struct TripleQueryIter<
    T1,
    I1: Iterator<Item = Option<T1>>,
    T2,
    I2: Iterator<Item = Option<T2>>,
    T3,
    I3: Iterator<Item = Option<T3>>,
> {
    i1: I1,
    i2: I2,
    i3: I3,
    pub(crate) entity: KeyType,
}

impl<
        T1,
        I1: Iterator<Item = Option<T1>>,
        T2,
        I2: Iterator<Item = Option<T2>>,
        T3,
        I3: Iterator<Item = Option<T3>>,
    > TripleQueryIter<T1, I1, T2, I2, T3, I3>
{
    pub fn new(i1: I1, i2: I2, i3: I3) -> Self {
        Self {
            i1,
            i2,
            i3,
            entity: 0,
        }
    }
}

impl<
        T1,
        I1: Iterator<Item = Option<T1>>,
        T2,
        I2: Iterator<Item = Option<T2>>,
        T3,
        I3: Iterator<Item = Option<T3>>,
    > Iterator for TripleQueryIter<T1, I1, T2, I2, T3, I3>
{
    type Item = (T1, T2, T3);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            self.entity += 1;
            if let (Some(v1), Some(v2), Some(v3)) =
                (self.i1.next()?, self.i2.next()?, self.i3.next()?)
            {
                return Some((v1, v2, v3));
            }
        }
    }
}

pub struct TripleEnumerate<
    T1,
    I1: Iterator<Item = Option<T1>>,
    T2,
    I2: Iterator<Item = Option<T2>>,
    T3,
    I3: Iterator<Item = Option<T3>>,
> {
    inner: TripleQueryIter<T1, I1, T2, I2, T3, I3>,
}

impl<
        T1,
        I1: Iterator<Item = Option<T1>>,
        T2,
        I2: Iterator<Item = Option<T2>>,
        T3,
        I3: Iterator<Item = Option<T3>>,
    > TripleEnumerate<T1, I1, T2, I2, T3, I3>
{
    pub fn new(inner: TripleQueryIter<T1, I1, T2, I2, T3, I3>) -> Self {
        Self { inner }
    }
}

impl<
        T1,
        I1: Iterator<Item = Option<T1>>,
        T2,
        I2: Iterator<Item = Option<T2>>,
        T3,
        I3: Iterator<Item = Option<T3>>,
    > Iterator for TripleEnumerate<T1, I1, T2, I2, T3, I3>
{
    type Item = (KeyType, T1, T2, T3);

    fn next(&mut self) -> Option<Self::Item> {
        let (a, b, c) = self.inner.next()?;
        Some((self.inner.entity - 1, a, b, c))
    }
}

impl<'a, C1: Component, C2: Component, C3: Component> Query<'a> for (&'a C1, &'a C2, &'a C3) {
    //type O = RefIter<'a, C>;
    type O =
        TripleQueryIter<&'a C1, RefIter<'a, C1>, &'a C2, RefIter<'a, C2>, &'a C3, RefIter<'a, C3>>;
    type SO = (Ref<'a, C1>, Ref<'a, C2>, Ref<'a, C3>);

    fn query(world: &'a World) -> Self::O {
        TripleQueryIter::new(
            RefIter::new(world.get_components()),
            RefIter::new(world.get_components()),
            RefIter::new(world.get_components()),
        )
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
            && world
                .get_components::<C3>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component(entity)?,
            world.get_entity_component(entity)?,
            world.get_entity_component(entity)?,
        ))
    }
}

impl<'a, C1: Component, C2: Component, C3: Component> Query<'a>
    for (KeyType, &'a C1, &'a C2, &'a C3)
{
    //type O = RefIter<'a, C>;
    type O =
        TripleEnumerate<&'a C1, RefIter<'a, C1>, &'a C2, RefIter<'a, C2>, &'a C3, RefIter<'a, C3>>;
    type SO = (Ref<'a, C1>, Ref<'a, C2>, Ref<'a, C3>);

    fn query(world: &'a World) -> Self::O {
        TripleEnumerate::new(TripleQueryIter::new(
            RefIter::new(world.get_components()),
            RefIter::new(world.get_components()),
            RefIter::new(world.get_components()),
        ))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
            && world
                .get_components::<C3>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component(entity)?,
            world.get_entity_component(entity)?,
            world.get_entity_component(entity)?,
        ))
    }
}

impl<'a, C1: Component, C2: Component, C3: Component> Query<'a>
    for (&'a mut C1, &'a mut C2, &'a mut C3)
{
    //type O = RefIter<'a, C>;
    type O = TripleQueryIter<
        &'a mut C1,
        RefMutIter<'a, C1>,
        &'a mut C2,
        RefMutIter<'a, C2>,
        &'a mut C3,
        RefMutIter<'a, C3>,
    >;
    type SO = (RefMut<'a, C1>, RefMut<'a, C2>, RefMut<'a, C3>);

    fn query(world: &'a World) -> Self::O {
        TripleQueryIter::new(
            RefMutIter::new(world.get_components_mut()),
            RefMutIter::new(world.get_components_mut()),
            RefMutIter::new(world.get_components_mut()),
        )
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
            && world
                .get_components::<C3>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component_mut(entity)?,
            world.get_entity_component_mut(entity)?,
            world.get_entity_component_mut(entity)?,
        ))
    }
}

impl<'a, C1: Component, C2: Component, C3: Component> Query<'a>
    for (KeyType, &'a mut C1, &'a mut C2, &'a mut C3)
{
    //type O = RefIter<'a, C>;
    type O = TripleEnumerate<
        &'a mut C1,
        RefMutIter<'a, C1>,
        &'a mut C2,
        RefMutIter<'a, C2>,
        &'a mut C3,
        RefMutIter<'a, C3>,
    >;
    type SO = (RefMut<'a, C1>, RefMut<'a, C2>, RefMut<'a, C3>);

    fn query(world: &'a World) -> Self::O {
        TripleEnumerate::new(TripleQueryIter::new(
            RefMutIter::new(world.get_components_mut()),
            RefMutIter::new(world.get_components_mut()),
            RefMutIter::new(world.get_components_mut()),
        ))
    }

    fn has_components(world: &'a World, entity: EntitId) -> bool {
        world
            .get_components::<C1>()
            .and_then(|c| c.get(entity as usize).map(|_| ()))
            .is_some()
            && world
                .get_components::<C2>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
            && world
                .get_components::<C3>()
                .and_then(|c| c.get(entity as usize).map(|_| ()))
                .is_some()
    }

    fn get_components(world: &'a World, entity: EntitId) -> Option<Self::SO> {
        Some((
            world.get_entity_component_mut(entity)?,
            world.get_entity_component_mut(entity)?,
            world.get_entity_component_mut(entity)?,
        ))
    }
}

pub struct AddEntity<W: DerefMut<Target = World>> {
    world: W,
    pub entity: EntitId,
}

impl<W: DerefMut<Target = World>> AddEntity<W> {
    pub fn new(mut world: W, parent: Option<EntitId>) -> Result<Self, String> {
        let entity = world.add_entity(parent.unwrap_or(World::get_root()))?;
        Ok(Self { world, entity })
    }
    pub fn with_component<T: component::Component>(mut self, component: T) -> Self {
        self.world.add_component(self.entity, component);
        self
    }
}

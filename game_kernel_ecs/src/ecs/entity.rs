use evmap;
use evmap::ShallowCopy;
pub use game_kernel_utils::hierarchy::*;
use parking_lot::Mutex;
use std::collections::HashMap;
use std::collections::HashSet;
use std::ops::Deref;

use super::component;

pub struct Entities {}

use game_kernel_settings;

use game_kernel_settings::settings::Config;

use std::any::Any;
use std::any::TypeId;
use std::cell::RefMut;
use std::cell::{Ref, RefCell};
use std::collections::HashMap;
use std::fmt;
use std::ops::DerefMut;

#[derive(Debug)]
pub enum SysErr {
    NotFound,
    InitError,
}

impl fmt::Display for SysErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "")
    }
}

pub trait System: Any {
    fn init(&mut self, manager: &SystemManager, wolrd: &super::World) -> Result<(), SysErr>;
    fn update(
        &mut self,
        manager: &SystemManager,
        world: &mut super::World,
        delta: std::time::Duration,
    );
}

trait SystemDowncast {
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;

    fn init(&self, manager: &SystemManager, wolrd: &super::World) -> Result<(), SysErr>;
    fn update(&self, manager: &SystemManager, world: &mut super::World, delta: std::time::Duration);
}

impl<T: System> SystemDowncast for RefCell<T> {
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }

    fn init(&self, manager: &SystemManager, wolrd: &super::World) -> Result<(), SysErr> {
        self.borrow_mut().init(manager, wolrd)
    }
    fn update(
        &self,
        manager: &SystemManager,
        world: &mut super::World,
        delta: std::time::Duration,
    ) {
        self.borrow_mut().update(manager, world, delta)
    }
}

pub trait Resource: Any {
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

impl<T: Any> Resource for RefCell<T> {
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

pub struct SystemManager {
    systems_map: HashMap<TypeId, usize>,
    systems: Vec<Box<dyn SystemDowncast>>,
    resources: HashMap<TypeId, Box<dyn Resource>>,
    _config: Config,
}

impl SystemManager {
    pub fn new(config: Config) -> SystemManager {
        SystemManager {
            systems_map: HashMap::new(),
            systems: vec![],
            resources: HashMap::new(),
            _config: config,
        }
    }

    pub fn add_resource<R: Any>(&mut self, resource: R) {
        self.resources
            .insert(TypeId::of::<R>(), Box::new(RefCell::new(resource)));
    }

    pub fn get_resource<R: Resource>(&self) -> Ref<R> {
        let id = TypeId::of::<R>();

        self.resources
            .get(&id)
            .unwrap()
            .as_any()
            .downcast_ref::<RefCell<R>>()
            .unwrap()
            .borrow()
    }

    pub fn get_resource_mut<R: Resource>(&self) -> RefMut<R> {
        let id = TypeId::of::<R>();

        self.resources
            .get(&id)
            .unwrap()
            .as_any()
            .downcast_ref::<RefCell<R>>()
            .unwrap()
            .borrow_mut()
    }

    ///registers a system in a manager and returns an index tha identifies it
    ///#Errors
    /// In some cases it may not be possible to add the system(eg. if it creates a dependency
    /// loop).
    /// In those cases the function will return an Err
    pub fn add_system<T: System>(&mut self, system: T) -> Result<(), &str> {
        self.systems.push(Box::new(RefCell::new(system)));
        self.systems_map
            .insert(TypeId::of::<T>(), self.systems.len() - 1);

        Ok(())
    }

    pub fn init_system<S: System>(&self, world: &super::World) -> Result<(), SysErr> {
        self.get_system_mut::<S>().unwrap().init(self, world)
    }

    pub fn add_and_init<S: System>(
        &mut self,
        system: S,
        world: &super::World,
    ) -> Result<(), SysErr> {
        self.add_system(system).unwrap();
        self.init_system::<S>(world)
    }

    pub fn get_system<S: System>(&self) -> Option<Ref<S>> {
        let index = *self.systems_map.get(&TypeId::of::<S>())?;

        self.systems
            .get(index)
            .map(|s| s.as_any().downcast_ref::<RefCell<S>>().unwrap().borrow())
    }

    pub fn get_system_mut<S: System>(&self) -> Option<RefMut<S>> {
        let index = *self.systems_map.get(&TypeId::of::<S>())?;
        self.systems.get(index).map(|s| {
            s.as_any()
                .downcast_ref::<RefCell<S>>()
                .unwrap()
                .borrow_mut()
        })
    }

    pub fn update_systems<W: DerefMut<Target = super::World>>(
        &self,
        mut world: W,
        delta: std::time::Duration,
    ) {
        //TODO right order
        for system in self.systems.iter() {
            system.update(self, world.deref_mut(), delta);
        }
    }
}

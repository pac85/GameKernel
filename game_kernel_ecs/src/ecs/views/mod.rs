use super::entity;

use std::any::Any;
use std::rc::Rc;

use super::component::{Component, ComponentId};

pub trait View: Any {
    fn on_register(&mut self, world: &super::World);
    fn on_enity_added(&mut self, world: &super::World, entity: u64);
    fn on_enity_removed(&mut self, world: &super::World, entity: u64);
    fn on_component_added(
        &mut self,
        world: &super::World,
        entity: u64,
        component: std::any::TypeId,
    );
    fn on_component_removed(
        &mut self,
        world: &super::World,
        entity: u64,
        component: &Box<dyn Component>,
    );
    /*fn iter<T>(&self, world: &super::World) -> T
    where T: Iterator<Item=entity::Keytype>,
          Self: Sized;*/
}

pub trait ImmediateView: Iterator<Item = entity::Keytype> {
    fn new(world: &super::World) -> Box<ImmediateView<Item = entity::Keytype>>
    where
        Self: Sized;
}

pub struct MultiComponentView {
    components: Box<[ComponentId]>,
    selected_entities: HashSet<u64>,
}

use std::collections::HashSet;

impl MultiComponentView {
    pub fn new(components: Box<[ComponentId]>) -> Self {
        Self {
            components,
            selected_entities: HashSet::new(),
        }
    }

    pub fn iter(&self) -> std::collections::hash_set::Iter<u64> {
        self.selected_entities.iter()
    }

    pub fn drain(&mut self) -> std::collections::hash_set::Drain<u64> {
        self.selected_entities.drain()
    }
}

#[macro_export]
macro_rules! multi_component_view {
    ($($x:ty), *) => {MultiComponentView::new(Box::new([$(std::any::TypeId::of::<$x>(), )*]));}
}

impl View for MultiComponentView {
    fn on_register(&mut self, world: &super::World) {
        for entity in world.iter_entities() {
            if world
                .has_components(*entity, self.components.as_ref())
                .unwrap_or(false)
            {
                self.selected_entities.insert(*entity);
            }
        }
    }

    fn on_enity_added(&mut self, world: &super::World, entity: u64) {
        if world
            .has_components(entity, self.components.as_ref())
            .unwrap_or(false)
        {
            self.selected_entities.insert(entity);
        }
    }

    fn on_enity_removed(&mut self, world: &super::World, entity: u64) {
        //return type can be ignored, we expect that the enity may not be among the selected ones
        self.selected_entities.remove(&entity);
    }

    fn on_component_added(
        &mut self,
        world: &super::World,
        entity: u64,
        component_id: std::any::TypeId,
    ) {
        if self.components.contains(&component_id) {
            self.on_enity_added(world, entity);
        }
    }

    fn on_component_removed(
        &mut self,
        world: &super::World,
        entity: u64,
        component: &Box<dyn Component>,
    ) {
        if self.components.contains(&component.gk_as_any().type_id()) {
            self.on_enity_removed(world, entity);
        }
    }
}

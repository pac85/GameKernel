//extern crate game_kernel_ecs_derive;
pub mod ecs;

#[cfg(test)]
mod tests {
    use core::panic;
    use std::{any::Any, cell::RefCell, collections::HashMap, sync::Arc};

    use game_kernel_utils::KeyType;

    use crate::ecs::{
        component::{Component, ComponentCreationError, COMPONENT_FACTORY},
        World,
    };

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[derive(Debug)]
    struct TestComponent {
        pub a: u32,
    }
    impl Component for TestComponent {
        fn get_name() -> String
        where
            Self: Sized,
        {
            return "TestComponent".to_owned();
        }
        fn get_name_dyn(&self) -> String {
            return "TestComponent".to_owned();
        }
        fn get_builder() -> fn(
            args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
        ) -> Result<Box<dyn Component>, ComponentCreationError>
        where
            Self: Sized,
        {
            |args| Ok(Box::new(Self { a: 1 }))
        }

        fn into_any(self: Box<Self>) -> Box<dyn Any> {
            self
        }
        fn gk_as_any(&self) -> &dyn Any {
            self
        }
        fn gk_as_any_mut(&mut self) -> &mut dyn Any {
            self
        }
    }

    #[derive(Debug)]
    struct TestComponent2 {
        pub a: u32,
    }
    impl Component for TestComponent2 {
        fn get_name() -> String
        where
            Self: Sized,
        {
            return "TestComponent2".to_owned();
        }
        fn get_name_dyn(&self) -> String {
            return "TestComponent2".to_owned();
        }
        fn get_builder() -> fn(
            args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
        ) -> Result<Box<dyn Component>, ComponentCreationError>
        where
            Self: Sized,
        {
            |args| Ok(Box::new(Self { a: 1 }))
        }

        fn into_any(self: Box<Self>) -> Box<dyn Any> {
            self
        }
        fn gk_as_any(&self) -> &dyn Any {
            self
        }
        fn gk_as_any_mut(&mut self) -> &mut dyn Any {
            self
        }
    }

    fn register_components() {
        let mut cl = COMPONENT_FACTORY.lock().unwrap();
        cl.register::<TestComponent>();
        cl.register::<TestComponent2>();
    }

    #[test]
    fn ecs_single_query_test() {
        register_components();

        let mut world = crate::ecs::World::new();

        let entity = world.add_entity(World::get_root()).unwrap();
        world.add_component(entity, TestComponent { a: 42 });

        let entity = world.add_entity(World::get_root()).unwrap();
        world.add_component(entity, TestComponent { a: 43 });

        assert!(world
            .query::<&TestComponent>()
            .zip([42, 43])
            .all(|(a, b)| a.a == b));
        assert_eq!(world.query::<&TestComponent>().count(), 2);

        //mutate components
        world.query::<&mut TestComponent>().for_each(|c| c.a += 1);
        assert!(world
            .query::<&TestComponent>()
            .zip([43, 44])
            .all(|(a, b)| a.a == b));

        //mutate single component
        world
            .get_entity_component_mut::<TestComponent>(entity)
            .unwrap()
            .a += 1;
        assert!(world
            .query::<&TestComponent>()
            .zip([43, 45])
            .all(|(a, b)| a.a == b));
    }

    #[test]
    fn ecs_double_query_test() {
        register_components();

        let mut world = crate::ecs::World::new();

        let entity = world.add_entity(World::get_root()).unwrap();
        world.add_component(entity, TestComponent { a: 42 });
        world.add_component(entity, TestComponent2 { a: 43 });
        assert!(world.has_components::<(&TestComponent, &TestComponent2)>(entity));

        let entity2 = world.add_entity(World::get_root()).unwrap();
        world.add_component(entity2, TestComponent { a: 42 });
        assert!(world.has_components::<&TestComponent>(entity2));

        //only one enitty has both components
        assert_eq!(
            world.query::<(&TestComponent, &TestComponent2)>().count(),
            1
        );

        assert_eq!(
            world
                .query::<(KeyType, &TestComponent, &TestComponent2)>()
                .map(|(k, _, _)| k)
                .next()
                .unwrap(),
            entity
        );
        assert!(world
            .query::<(KeyType, &TestComponent)>()
            .zip([entity, entity2])
            .all(|((k, _), e)| k == e));

        let (t1, t2): (&TestComponent, &TestComponent2) = world.query().next().unwrap();
        assert_eq!((t1.a, t2.a), (42, 43));

        let (t1, t2) = world
            .get_entity_components::<(&TestComponent, &TestComponent2)>(entity)
            .unwrap();
        assert_eq!((t1.a, t2.a), (42, 43));
    }

    #[test]
    fn ecs_test_entity_delete() {
        register_components();
        let mut world = crate::ecs::World::new();

        let entity1 = world.add_entity(World::get_root()).unwrap();
        let entity2 = world.add_entity(entity1).unwrap();
        let entity3 = world.add_entity(entity2).unwrap();

        world.rem_entity_recursive(entity1).unwrap(); //should delete all of them

        let entities: Vec<_> = world.iter_entities().collect();
        assert_eq!(entities.len(), 1);
        assert_eq!(entities[0], 0);
    }
}

use std::iter::Iterator;
use std::str::Split;

#[derive(Clone)]
pub struct Path {
    path: String,
}

impl Path {
    pub fn new(path: String) -> Self {
        Self { path }
    }

    pub fn obj_name_iter<'a>(&'a self) -> Split<'a, char> {
        self.path.split('/').clone()
    }

    pub fn split_at_base(&self) -> (Path, String) {
        let tokens = self.path.split('/').collect::<Vec<&str>>();
        let base = Path::new(tokens[0..tokens.len() - 1].join("/"));
        let file_name = tokens[tokens.len() - 1];

        (base, file_name.to_owned())
    }
}

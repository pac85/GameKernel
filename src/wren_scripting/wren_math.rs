use ruwren::*;

use cgmath::{dot, Matrix, Matrix2, Matrix3, Matrix4, Vector2, Vector3, Vector4};

macro_rules! make_vec_wrapper{
    ($wrap_name:ident, $cs:ty, $wn:expr, ($($setter_name:ident, $getter_name:ident, $component_name:ident, $idx:expr, )*), $from_float:expr) => {
        pub struct $wrap_name{
            pub inner: $cs,
        }

        impl Class for $wrap_name {
            fn initialize(vm: &VM) -> Self {
                let mut inner = $from_float(0.0);
                $(
                    inner.$component_name = vm.get_slot_double($idx).unwrap_or(0.0);
                )*
                Self {
                    inner
                }
            }
        }

        impl $wrap_name {
            pub fn new(inner: $cs) -> Self {
                Self{
                    inner,
                }
            }

            $(
                pub fn $getter_name(&self, vm: &VM) {
                    vm.set_slot_double(0, self.inner.$component_name);
                }

                pub fn $setter_name(&mut self, vm: &VM) {
                    //TODO do nothing if None
                    self.inner.$component_name = vm.get_slot_double(1).unwrap_or(0.0);
                }
            )*

            fn get_other_operand(vm: &VM) -> Option<$cs> {
                if let Some(other) = vm.get_slot_double(1) {
                    Some($from_float(other))
                } else if let Some(Self {inner}) = vm.get_slot_foreign::<$wrap_name>(1) {
                    Some(inner.clone())
                } else
                {
                    None
                }
            }

            pub fn op_add(&self, vm: &VM) {
                if let Some(other) = Self::get_other_operand(vm) {
                    let res = self.inner + other;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                }
                //TODO handle other cases
            }

            pub fn op_sub(&self, vm: &VM) {
                if let Some(other) = Self::get_other_operand(vm) {
                    let res = self.inner - other;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                }
                //TODO handle other cases
            }

            pub fn op_mul(&self, vm: &VM) {
                if let Some(other) = vm.get_slot_double(1) {
                    let res = self.inner * other;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                }
                //TODO handle other cases
            }

            pub fn op_div(&self, vm: &VM) {
                if let Some(other) = vm.get_slot_double(1) {
                    let res = self.inner / other;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                }
                //TODO handle other cases
            }

            pub fn dot(&self, vm: &VM) {
                if let Some(other) = vm.get_slot_foreign::<$wrap_name>(1) {
                    let res = dot(self.inner, other.inner);
                    vm.set_slot_double(0, res);
                }
                //TODO handle other cases
            }

            pub fn to_string(&self, vm: &VM) {
                vm.set_slot_string(0, format!("{:?}", self.inner));
            }

        }
    }
}

make_vec_wrapper!(
    WrenVec2,
    Vector2<f64>,
    "Vec2",
    (set_x, get_x, x, 1, set_y, get_y, y, 2,),
    |f| Vector2::new(f, f)
);
make_vec_wrapper!(
    WrenVec3,
    Vector3<f64>,
    "Vec3",
    (set_x, get_x, x, 1, set_y, get_y, y, 2, set_z, get_z, z, 3,),
    |f| Vector3::new(f, f, f)
);
make_vec_wrapper!(
    WrenVec4,
    Vector4<f64>,
    "Vec4",
    (set_x, get_x, x, 1, set_y, get_y, y, 2, set_z, get_z, z, 3, set_w, get_w, w, 4,),
    |f| Vector4::new(f, f, f, f)
);

create_module! {
    class("Vec2") crate::wren_scripting::wren_math::WrenVec2 => wren_vec2 {
        //getters and setters
        instance(getter "x") get_x,
        instance(getter "y") get_y,
        instance(setter "x") set_x,
        instance(setter "y") set_y,

        //operators
        instance(fn "+", 1) op_add,
        instance(fn "-", 1) op_sub,
        instance(fn "*", 1) op_mul,
        instance(fn "/", 1) op_div,

        instance(fn "dot", 1) dot,

        //various
        instance(fn "toString", 0) to_string
    }

    class("Vec3") crate::wren_scripting::wren_math::WrenVec3 => wren_vec3 {
        //getters and setters
        instance(getter "x") get_x,
        instance(getter "y") get_y,
        instance(getter "z") get_z,
        instance(setter "x") set_x,
        instance(setter "y") set_y,
        instance(setter "z") set_z,

        //operators
        instance(fn "+", 1) op_add,
        instance(fn "-", 1) op_sub,
        instance(fn "*", 1) op_mul,
        instance(fn "/", 1) op_div,

        instance(fn "dot", 1) dot,

        //various
        instance(fn "toString", 0) to_string
    }

    class("Vec4") crate::wren_scripting::wren_math::WrenVec4 => wren_vec4 {
        //getters and setters
        instance(getter "x") get_x,
        instance(getter "y") get_y,
        instance(getter "z") get_z,
        instance(getter "w") get_w,
        instance(setter "x") set_x,
        instance(setter "y") set_y,
        instance(setter "z") set_z,
        instance(setter "w") set_w,

        //operators
        instance(fn "+", 1) op_add,
        instance(fn "-", 1) op_sub,
        instance(fn "*", 1) op_mul,
        instance(fn "/", 1) op_div,

        instance(fn "dot", 1) dot,

        //various
        instance(fn "toString", 0) to_string
    }

    class ("Mat2") crate::wren_scripting::wren_math::WrenMat2 => wren_mat2 {
        //getters and setters
        instance(getter "x") get_x,
        instance(getter "y") get_y,
        instance(setter "x") set_x,
        instance(setter "y") set_y,

        //operators
        instance(fn "+", 1) op_add,
        instance(fn "-", 1) op_sub,
        instance(fn "*", 1) op_mul,
        instance(fn "/", 1) op_div,
        //various
        instance(fn "toString", 0) to_string
    }

    class ("Mat3") crate::wren_scripting::wren_math::WrenMat3 => wren_mat3 {
        //getters and setters
        instance(getter "x") get_x,
        instance(getter "y") get_y,
        instance(getter "z") get_z,
        instance(setter "x") set_x,
        instance(setter "y") set_y,
        instance(setter "z") set_z,

        //operators
        instance(fn "+", 1) op_add,
        instance(fn "-", 1) op_sub,
        instance(fn "*", 1) op_mul,
        instance(fn "/", 1) op_div,
        //various
        instance(fn "toString", 0) to_string
    }

    class ("Mat4") crate::wren_scripting::wren_math::WrenMat4 => wren_mat4 {
        //getters and setters
        instance(getter "x") get_x,
        instance(getter "y") get_y,
        instance(getter "z") get_z,
        instance(getter "w") get_w,
        instance(setter "x") set_x,
        instance(setter "y") set_y,
        instance(setter "z") set_z,
        instance(setter "w") set_w,

        //operators
        instance(fn "+", 1) op_add,
        instance(fn "-", 1) op_sub,
        instance(fn "*", 1) op_mul,
        instance(fn "/", 1) op_div,
        //various
        instance(fn "toString", 0) to_string
    }

    module => math
}

pub fn publish(lib: &mut ModuleLibrary) {
    math::publish_module(lib);
}

pub static MATH_WREN_SRC: &'static str = r##"
    foreign class Vec2 {
        construct new(x, y) {}
        foreign x
        foreign y
        foreign x=(a)
        foreign y=(a)

        foreign +(a)
        foreign -(a)
        foreign *(a)
        foreign /(a)

        foreign dot(a)

        foreign toString()
    }

    foreign class Vec3 {
        construct new(x, y, z) {}
        foreign x
        foreign y
        foreign z
        foreign x=(a)
        foreign y=(a)
        foreign z=(a)

        foreign +(a)
        foreign -(a)
        foreign *(a)
        foreign /(a)

        foreign dot(a)

        foreign toString()
    }
    foreign class Vec4 {
        construct new(x, y, z, w) {}
        foreign x
        foreign y
        foreign z
        foreign w
        foreign x=(a)
        foreign y=(a)
        foreign z=(a)
        foreign w=(a)

        foreign +(a)
        foreign -(a)
        foreign *(a)
        foreign /(a)

        foreign dot(a)

        foreign toString()
    }

    foreign class Mat2 {
        construct new(x, y) {}
        foreign x
        foreign y
        foreign x=(a)
        foreign y=(a)

        foreign +(a)
        foreign -(a)
        foreign *(a)
        foreign /(a)

        foreign toString()
    }

    foreign class Mat3 {
        construct new(x, y, z) {}
        foreign x
        foreign y
        foreign z
        foreign x=(a)
        foreign y=(a)
        foreign z=(a)

        foreign +(a)
        foreign -(a)
        foreign *(a)
        foreign /(a)

        foreign toString()
    }

    foreign class Mat4 {
        construct new(x, y, z, w) {}
        foreign x
        foreign y
        foreign z
        foreign w
        foreign x=(a)
        foreign y=(a)
        foreign z=(a)
        foreign w=(a)

        foreign +(a)
        foreign -(a)
        foreign *(a)
        foreign /(a)

        foreign toString()
    }
    "##;

//matrices

macro_rules! make_mat_wrapper{
    (
        $wrap_name:ident,
        $cs:ty,
        $wn:expr,
        $component_wrapper_ty:ty,
        $component_wren_name:expr,
        ($(
                $setter_name:ident, $getter_name:ident, $component_name:ident, $idx:expr,
        )*),
        $component_from_float:expr,
        $from_float:expr
    ) => {
        pub struct $wrap_name{
            inner: $cs,
        }

        impl Class for $wrap_name {
            fn initialize(vm: &VM) -> Self {
                let mut inner = $from_float(0.0);
                $(
                    inner.$component_name = vm.get_slot_foreign::<$component_wrapper_ty>($idx).map(|a| a.inner).unwrap_or($component_from_float(0.0));
                )*
                Self {
                    inner
                }
            }
        }

        impl $wrap_name {
            pub fn new(inner: $cs) -> Self {
                Self{
                    inner,
                }
            }

            $(
                pub fn $getter_name(&self, vm: &VM) {
                    vm.set_slot_new_foreign("math", $component_wren_name, <$component_wrapper_ty>::new(self.inner.$component_name), 0);
                }

                pub fn $setter_name(&mut self, vm: &VM) {
                    //TODO do nothing if None
                    self.inner.$component_name = vm.get_slot_foreign::<$component_wrapper_ty>(1).map(|a|a.inner).unwrap_or($component_from_float(0.0));
                }
            )*

            pub fn op_add(&self, vm: &VM) {
                if let Some(other) = vm.get_slot_foreign::<$wrap_name>(1) {
                    let res = self.inner + other.inner;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                }
                //TODO handle other cases
            }

            pub fn op_sub(&self, vm: &VM) {
                if let Some(other) = vm.get_slot_foreign::<$wrap_name>(1) {
                    let res = self.inner - other.inner;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                }
                //TODO handle other cases
            }

            pub fn op_mul(&self, vm: &VM) {
                if let Some(other) = vm.get_slot_foreign::<$wrap_name>(1) {
                    let res = self.inner * other.inner;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                } else if let Some(other) = vm.get_slot_foreign::<$component_wrapper_ty>(1) {
                    let res = self.inner * other.inner;
                    vm.set_slot_new_foreign("math", $component_wren_name, <$component_wrapper_ty>::new(res), 0);
                }
                //TODO handle other cases
            }

            pub fn op_div(&self, vm: &VM) {
                if let Some(other) = vm.get_slot_double(1) {
                    let res = self.inner / other;
                    vm.set_slot_new_foreign("math", $wn, Self::new(res), 0);
                }
                //TODO handle other cases
            }

            /*pub fn transponse(&self, vm: &VM) {
                vm.set_slot_new_foreign("math", $wn, self.inner.transponse(), 0);
            }*/

            pub fn to_string(&self, vm: &VM) {
                vm.set_slot_string(0, format!("{:?}", self.inner));
            }

        }
    }
}

make_mat_wrapper!(
    WrenMat2,
    Matrix2<f64>,
    "Mat2",
    WrenVec2,
    "Vec2",
    (set_x, get_x, x, 1, set_y, get_y, y, 2,),
    |f| Vector2::new(f, f),
    |f| Matrix2::new(f, f, f, f)
);

make_mat_wrapper!(
    WrenMat3,
    Matrix3<f64>,
    "Mat3",
    WrenVec3,
    "Vec3",
    (set_x, get_x, x, 1, set_y, get_y, y, 2, set_z, get_z, z, 3,),
    |f| Vector3::new(f, f, f),
    |f| Matrix3::new(f, f, f, f, f, f, f, f, f)
);

make_mat_wrapper!(
    WrenMat4,
    Matrix4<f64>,
    "Mat4",
    WrenVec4,
    "Vec4",
    (set_x, get_x, x, 1, set_y, get_y, y, 2, set_z, get_z, z, 3, set_w, get_w, w, 4,),
    |f| Vector4::new(f, f, f, f),
    |f| Matrix4::new(f, f, f, f, f, f, f, f, f, f, f, f, f, f, f, f,)
);

use cgmath;
use evmap;
use game_kernel_utils;
use parking_lot;
use serde_derive;
use serde_json;
use toml;
use vulkano;
#[macro_use]
use game_kernel_ecs;
use downcast_rs;
use game_kernel_ecs_derive;
#[macro_use]
use game_kernel_logger;
pub use game_kernel_logger::{log_err, log_msg, log_warn};
use game_kernel_settings;
use game_kernel_vfs;
#[macro_use]
use paste;
#[macro_use]
use lazy_static;
#[macro_use]
use ruwren;

use winit;

#[macro_use]
use imgui;
use imgui_vulkano_renderer_unsafe;
use imgui_winit_support;

pub use rapier3d;

pub mod game_kernel {
    pub mod core_systems;
    pub mod subsystems;
    pub mod wren_scripting;

    pub type MainRenderer = subsystems::video::renderer::Renderer;
    pub type MainVulkanCommon = subsystems::video::common::vulkan::VulkanCommon;

    pub mod ecs {
        pub use game_kernel_ecs::ecs::{
            component::{
                Component, ComponentCreationError, ComponentField, ComponentId, ComponentReflect,
                COMPONENT_FACTORY,
            },
            system::*,
            AddEntity, EntitId, World,
        };
        pub use game_kernel_ecs::*; //for macros
        pub use game_kernel_ecs_derive::*;
    }

    pub mod engine_components {
        pub use super::core_systems::register_components;
        pub use super::core_systems::{
            base::transform::TransformComponent, camera_system::CameraComponent,
            light::LightComponent, static_mesh::StaticMeshComponent,
        };
    }
}

pub use game_kernel_settings::settings::Config;

pub use game_kernel::core_systems::{
    base::transform::TransformComponent, camera_system::*, light::*, renderer::*, static_mesh::*,
};

pub use game_kernel::subsystems::video::common::vulkan;
pub use game_kernel::subsystems::video::renderer;
pub use game_kernel::subsystems::video::renderer::{
    camera::Camera, material::Material, mesh::Mesh, model::Model, ui_module::DummyUiModule,
    ui_module::UiModule,
};

pub use game_kernel::subsystems::audio;

pub use game_kernel::subsystems::input;

pub use game_kernel_logger::logger;

pub mod utils {
    pub use game_kernel_utils::*;
}

pub use rodio;

pub use physfs_rs;

pub fn game_kernel_main() {}

/*#[cfg(test)]
mod tests {
    use ::game_kernel::logger;
    #[test]
    fn it_works() {
        logger::log_msg("this is a message");
        logger::log_warn("this is a message");
        logger::log_err("this is a message");
    }
}*/

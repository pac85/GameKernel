use crate::ecs::*;

#[derive(Clone, MetaStruct, Component)]
pub struct TagComponent {
    pub name: String,
}

impl TagComponent {
    pub fn new(name: impl AsRef<str>) -> Self {
        Self {
            name: name.as_ref().to_owned(),
        }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }
}

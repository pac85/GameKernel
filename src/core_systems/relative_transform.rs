use cgmath::VectorSpace;
use cgmath::{self, ElementWise, Euler, Quaternion, Rad, Rotation, Vector3};

use crate::core_systems::base::transform::TransformComponent;
use crate::ecs::*;
use game_kernel_ecs::ecs::component::*;
use std::any::Any;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::ops::Deref;
use std::sync::Arc;

#[derive(Debug, Clone, Copy)]
pub struct TransformInheritFlags {
    pub translation: bool,
    pub rotation: bool,
    pub scale: bool,
}

impl Default for TransformInheritFlags {
    fn default() -> Self {
        Self {
            translation: true,
            rotation: true,
            scale: true,
        }
    }
}

#[derive(Debug, Clone, MetaStruct, Component)]
pub struct RelativeTransformComponent {
    pub position: cgmath::Vector3<f32>,
    #[arg_conv(Self::quaternion_from_velurer)]
    pub rotation: Quaternion<f32>,
    pub scale: cgmath::Vector3<f32>,
    #[arg_default]
    pub inherit_flags: TransformInheritFlags,
}

impl RelativeTransformComponent {
    pub fn identity() -> Self {
        Self {
            position: cgmath::Vector3::new(0f32, 0f32, 0f32),
            rotation: Quaternion::new(1.0, 0.0, 0.0, 0.0),
            scale: cgmath::Vector3::new(1f32, 1f32, 1f32),
            inherit_flags: Default::default(),
        }
    }

    pub fn from_pos_rot_scale(
        position: cgmath::Vector3<f32>,
        rotation: cgmath::Vector3<f32>,
        scale: cgmath::Vector3<f32>,
    ) -> Self {
        Self {
            position,
            rotation: Self::quaternion_from_velurer(&rotation),
            scale,
            inherit_flags: Default::default(),
        }
    }

    pub fn translate(self, translation: cgmath::Vector3<f32>) -> Self {
        Self {
            position: self.position + translation,
            ..self
        }
    }

    pub fn rotate<R: Into<Quaternion<f32>>>(self, rotation: R) -> Self {
        Self {
            rotation: rotation.into(),
            ..self
        }
    }

    pub fn scale(self, scale: Vector3<f32>) -> Self {
        Self { scale, ..self }
    }

    pub fn with_inherit_flags(self, inherit_flags: TransformInheritFlags) -> Self {
        Self {
            inherit_flags,
            ..self
        }
    }

    pub fn get_matrix(&self) -> cgmath::Matrix4<f32> {
        cgmath::Matrix4::from_translation(self.position)
            * cgmath::Matrix4::from(self.rotation)
            * cgmath::Matrix4::from_nonuniform_scale(self.scale.x, self.scale.y, self.scale.z)
    }

    pub fn interpolate(&self, other: &Self, factor: f32) -> Self {
        Self {
            scale: self.scale.lerp(other.scale, factor),
            rotation: self.rotation.slerp(other.rotation, factor),
            position: self.position.lerp(other.position, factor),
            inherit_flags: self.inherit_flags,
        }
    }

    pub fn quaternion_from_velurer(v: &Vector3<f32>) -> Quaternion<f32> {
        Quaternion::from(Euler::new(Rad(v.x), Rad(v.y), Rad(v.z)))
    }
}

pub struct RelativeTransformSystem {}

impl RelativeTransformSystem {
    pub fn new() -> Self {
        Self {}
    }

    pub fn update_subtree(&self, world: &mut World, root: EntitId) {
        let mut q = VecDeque::new();

        q.push_back(root);

        while let Some(parent) = q.pop_front() {
            for child in world.get_children(parent).expect(&format!(
                "{} is returned as a child but it doesn't exist",
                parent
            )) {
                q.push_back(child);
            }

            let parent_transform = world
                .get_entity_components::<&TransformComponent>(parent)
                .as_deref()
                .cloned()
                .unwrap_or(TransformComponent::identity());

            for child in world.get_children(parent).unwrap() {
                //if the child had a relative transform component the tranform is propagated from
                //the parent
                if world.has_components::<&RelativeTransformComponent>(child) {
                    if !world.has_components::<&TransformComponent>(child) {
                        world.add_component(child, TransformComponent::identity());
                    }
                    let (child_relative, mut child_trasnform) = world.get_entity_components::<(&mut RelativeTransformComponent, &mut TransformComponent)>(child).unwrap();

                    if child_relative.inherit_flags.scale {
                        child_trasnform.scale = parent_transform
                            .scale
                            .mul_element_wise(child_relative.scale);
                    }
                    if child_relative.inherit_flags.translation {
                        let rotated_translation = if child_relative.inherit_flags.rotation {
                            parent_transform.rotation.rotate_vector(
                                child_relative
                                    .position
                                    .mul_element_wise(parent_transform.scale),
                            )
                        } else {
                            child_relative
                                .position
                                .mul_element_wise(parent_transform.scale)
                        };
                        child_trasnform.position = parent_transform.position + rotated_translation;
                    }
                    if child_relative.inherit_flags.rotation {
                        child_trasnform.rotation =
                            parent_transform.rotation * child_relative.rotation;
                    } else {
                        child_trasnform.rotation = child_relative.rotation;
                    }
                }
            }
        }
    }
}

use crate::ecs::World;
use game_kernel_ecs::ecs::system::{SysErr, System, SystemManager};

impl System for RelativeTransformSystem {
    fn init(&mut self, _manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(&mut self, _manager: &SystemManager, world: &mut World, _delta: std::time::Duration) {
        self.update_subtree(world, World::get_root());
    }
}

use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

use game_kernel_utils::{Framestamps, KeyType};

use crate::subsystems::video::renderer;
use crate::{ecs::*, RendererSystem};
use game_kernel_ecs::ecs::{
    component::*,
    system::{SysErr, System, SystemManager},
};
use game_kernel_ecs_derive::*;

#[derive(Clone, Copy, MetaStruct, Component)]
pub struct LightComponent {
    pub light: renderer::light::Light,
    #[arg_default]
    light_index: Option<KeyType>,
}

impl LightComponent {
    pub fn new(light: renderer::light::Light) -> Self {
        Self {
            light,
            light_index: None,
        }
    }
}

use std::cell::RefCell;

use super::base::transform::TransformComponent;

pub struct LightSystem {
    renderer: Option<Arc<RefCell<crate::MainRenderer>>>,
    frame_stamps: Framestamps,
}

impl LightSystem {
    pub fn new() -> Self {
        Self {
            renderer: None,
            frame_stamps: Framestamps::new(),
        }
    }
}

impl System for LightSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        self.renderer = Some(
            manager
                .get_system::<RendererSystem>()
                .unwrap()
                .get_renderer(),
        );

        Ok(())
    }

    fn update<'a>(
        &'a mut self,
        manager: &SystemManager,
        world: &'a mut World,
        _delta: std::time::Duration,
    ) {
        if self.renderer.is_none() {
            return;
        }
        let renderer = self.renderer.as_ref().unwrap();

        for (transform_component, mut light_component) in
            world.query::<(&mut TransformComponent, &mut LightComponent)>()
        {
            light_component
                .light
                .maybe_set_position(transform_component.position)
                .maybe_set_rotation(transform_component.rotation);

            if let Some(light_index) = light_component.light_index {
                renderer
                    .borrow_mut()
                    .update_light(light_index, light_component.light);
            } else {
                light_component.light_index =
                    Some(renderer.borrow_mut().add_light(light_component.light))
            }

            self.frame_stamps
                .set_last_seen(light_component.light_index.unwrap());
        }

        let mut renderer = renderer.borrow_mut();
        let old_indices: Vec<_> = renderer
            .get_lights()
            .filter(|(index, _)| self.frame_stamps.is_element_old(*index))
            .map(|(index, _)| index)
            .collect();
        for index in old_indices {
            if self.frame_stamps.is_element_old(index) {
                renderer.remove_light(index);
                self.frame_stamps.element_removed(index);
            }
        }

        self.frame_stamps.next_frame();
    }
}

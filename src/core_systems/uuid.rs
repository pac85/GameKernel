use crate::ecs::*;

#[derive(Copy, Clone)]
pub struct UuidGenrator {
    state: u64,
}

impl UuidGenrator {
    fn new_no_seed() -> Self {
        Self {
            state: 14695981039346656037,
        }
    }

    pub fn advance(mut self, value: u64) -> Self {
        self.state ^= value;
        (self.state, _) = cgmath::num_traits::ops::overflowing::OverflowingMul::overflowing_mul(
            &self.state,
            &1099511628211,
        );
        self
    }

    pub fn new(seed: u64) -> Self {
        UuidGenrator::new_no_seed().advance(seed)
    }
}

impl Iterator for UuidGenrator {
    type Item = UuidComponent;

    fn next(&mut self) -> Option<Self::Item> {
        *self = self.advance(0);
        Some(UuidComponent::new(self.state))
    }
}

#[derive(Clone, Copy, MetaStruct, Component)]
pub struct UuidComponent {
    uuid: u64,
}

impl UuidComponent {
    pub fn new(uuid: u64) -> Self {
        Self { uuid }
    }

    pub fn uuid(&self) -> u64 {
        self.uuid
    }
}

use game_kernel_utils::{Framestamps, KeyType};

use std::sync::Arc;

use game_kernel_ecs::ecs::{
    component::*,
    system::{SysErr, System, SystemManager},
};
use game_kernel_ecs_derive::*;

use crate::subsystems::video::renderer;
use crate::{ecs::*, renderer::decals::Decal};
use crate::{RendererSystem, TransformComponent};

#[derive(MetaStruct, Component, Clone)]
pub struct DecalComponent {
    decal: Decal,
    #[arg_default]
    decal_index: Option<KeyType>,
}

impl DecalComponent {
    pub fn new(decal: Decal) -> Self {
        Self {
            decal,
            decal_index: None,
        }
    }

    pub fn decal_index(&self) -> Option<KeyType> {
        self.decal_index
    }
}

use std::cell::RefCell;

pub struct DecalSystem {
    renderer: Option<Arc<RefCell<crate::MainRenderer>>>,
    frame_stamps: Framestamps,
}

impl DecalSystem {
    pub fn new() -> Self {
        Self {
            renderer: None,
            frame_stamps: Framestamps::new(),
        }
    }
}

impl System for DecalSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        self.renderer = Some(
            manager
                .get_system::<RendererSystem>()
                .unwrap()
                .get_renderer(),
        );

        Ok(())
    }

    fn update<'a>(
        &'a mut self,
        manager: &SystemManager,
        world: &'a mut World,
        _delta: std::time::Duration,
    ) {
        if self.renderer.is_none() {
            return;
        }
        let renderer = self.renderer.clone().unwrap();

        for (transform_component, mut decal_component) in
            world.query::<(&mut TransformComponent, &mut DecalComponent)>()
        {
            let decal_index = if let Some(decal_index) = decal_component.decal_index {
                renderer
                    .borrow_mut()
                    .update_decal_transform(&decal_index, transform_component.get_matrix());

                decal_index
            } else {
                let decal = Decal::new(
                    decal_component.decal.textures().clone(),
                    transform_component.get_matrix(),
                );
                decal_component.decal_index = Some(renderer.borrow_mut().add_decal(decal));

                decal_component.decal_index.unwrap()
            };

            self.frame_stamps.set_last_seen(decal_index);
        }

        //delete decals that have been seen in the past but not in this frame
        let mut renderer = renderer.borrow_mut();
        let old_indices = renderer
            .get_models()
            .map(|(k, _)| k)
            .filter(|k| self.frame_stamps.is_element_old(*k))
            .collect::<Vec<_>>();

        for decal_index in old_indices {
            renderer.remove_model(decal_index);
            self.frame_stamps.element_removed(decal_index);
        }

        self.frame_stamps.next_frame();
    }
}

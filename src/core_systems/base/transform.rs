use cgmath::*;

use crate::ecs::*;
use game_kernel_ecs_derive::*;
use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

#[derive(Clone, Debug, PartialEq, MetaStruct, Component)]
pub struct TransformComponent {
    pub position: Vector3<f32>,
    #[arg_conv(Self::quaternion_from_velurer)]
    pub rotation: Quaternion<f32>,
    pub scale: Vector3<f32>,
}

impl TransformComponent {
    pub fn component_new(
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn Component>, ComponentCreationError> {
        Ok(Box::new(Self {
            position: *parse_component_arg!(args, "position", Vector3<f32>),
            rotation: Self::quaternion_from_velurer(
                parse_component_arg!(args, "rotation", Vector3<f32>).as_ref(),
            ),
            scale: *parse_component_arg!(args, "scale", Vector3<f32>),
        }))
    }

    pub fn identity() -> Self {
        Self {
            position: Vector3::new(0f32, 0f32, 0f32),
            rotation: Quaternion::new(1.0, 0.0, 0.0, 0.0),
            scale: Vector3::new(1f32, 1f32, 1f32),
        }
    }

    pub fn from_pos_rot_scale(
        position: Vector3<f32>,
        rotation: Vector3<f32>,
        scale: Vector3<f32>,
    ) -> Self {
        Self {
            position,
            rotation: Self::quaternion_from_velurer(&rotation),
            scale,
        }
    }

    pub fn translate(self, translation: Vector3<f32>) -> Self {
        Self {
            position: self.position + translation,
            ..self
        }
    }

    pub fn rotate<R: Into<Quaternion<f32>>>(self, rotation: R) -> Self {
        Self {
            rotation: rotation.into(),
            ..self
        }
    }

    pub fn scale(self, scale: Vector3<f32>) -> Self {
        Self { scale, ..self }
    }

    pub fn get_matrix(&self) -> Matrix4<f32> {
        Matrix4::from_translation(self.position)
            * Matrix4::from(self.rotation)
            * Matrix4::from_nonuniform_scale(self.scale.x, self.scale.y, self.scale.z)
    }

    pub fn quaternion_from_velurer(v: &Vector3<f32>) -> Quaternion<f32> {
        Quaternion::from(Euler::new(Rad(v.x), Rad(v.y), Rad(v.z)))
    }
}

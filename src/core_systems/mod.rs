pub mod base;
pub mod camera_system;
pub mod decal;
pub mod light;
pub mod listener_system;
pub mod particle_system;
pub mod physics;
pub mod relative_transform;
pub mod renderer;
pub mod sound_source;
pub mod sound_system;
pub mod static_mesh;
pub mod tag;
pub mod uuid;

use crate::ecs::COMPONENT_FACTORY;

use super::subsystems::Subsystems;

pub fn register_components() {
    let mut factory = COMPONENT_FACTORY.lock().unwrap();

    factory.register::<relative_transform::RelativeTransformComponent>();
    factory.register::<tag::TagComponent>();
    factory.register::<base::transform::TransformComponent>();
    factory.register::<camera_system::CameraComponent>();
    factory.register::<light::LightComponent>();
    factory.register::<static_mesh::StaticMeshComponent>();
    factory.register::<decal::DecalComponent>();
    factory.register::<particle_system::ParticleSystemComponent>();
    //factory.register::<listener_system::ListenerComponent>();
    factory.register::<sound_source::SoundSourceComponent>();
    physics::register::register_components(&mut factory);
}

pub fn init_systems(sbsts: &mut Subsystems) {
    let manager = &mut sbsts.systems_manager;
    let world = &mut *sbsts.world.borrow_mut();

    manager
        .add_and_init(relative_transform::RelativeTransformSystem::new(), world)
        .unwrap();

    let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();
    manager
        .add_and_init(renderer::RendererSystem::new(renderer.clone()), world)
        .unwrap();
    manager
        .add_and_init(static_mesh::StaticMeshSystem::new(), world)
        .unwrap();
    manager
        .add_and_init(decal::DecalSystem::new(), world)
        .unwrap();
    manager
        .add_and_init(light::LightSystem::new(), world)
        .unwrap();
    manager
        .add_and_init(camera_system::CameraSystem::new(), world)
        .unwrap();
    manager
        .add_and_init(particle_system::PfxSystem::new(), world)
        .unwrap();
    manager
        .add_and_init(sound_system::SoundSystem::new(sbsts.audio.clone()), world)
        .unwrap();
    manager
        .add_and_init(listener_system::ListenerSystem::new(), world)
        .unwrap();
    manager
        .add_and_init(sound_source::SoundSourceSystem::new(), world)
        .unwrap();
    physics::register::init_systems(manager, world);
}

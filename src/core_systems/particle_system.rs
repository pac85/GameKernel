use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;
use std::sync::Arc;

use game_kernel_utils::{Framestamps, KeyType};

use crate::subsystems::video::renderer;
use crate::{ecs::*, RendererSystem};
use game_kernel_ecs::ecs::{
    component::*,
    system::{SysErr, System, SystemManager},
};
use game_kernel_ecs_derive::*;

use super::base::transform::TransformComponent;

#[derive(Clone, MetaStruct, Component)]
pub struct ParticleSystemComponent {
    particle_system: Arc<renderer::particle_fx::ParticleSystemBuilderWrapper>,
    #[arg_default]
    particle_system_index: Option<KeyType>,
    #[arg_default]
    emitter_position: bool,
}

impl ParticleSystemComponent {
    pub fn new(particle_system: Arc<renderer::particle_fx::ParticleSystemBuilderWrapper>) -> Self {
        Self {
            particle_system,
            particle_system_index: None,
            emitter_position: false,
        }
    }

    pub fn with_emitter_position(self, emitter_position: bool) -> Self {
        Self {
            emitter_position,
            ..self
        }
    }
}

pub struct PfxSystem {
    renderer: Option<Arc<RefCell<crate::MainRenderer>>>,
    frame_stamps: Framestamps,
}

impl PfxSystem {
    pub fn new() -> Self {
        Self {
            renderer: None,
            frame_stamps: Framestamps::new(),
        }
    }
}

impl System for PfxSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        self.renderer = Some(
            manager
                .get_system::<RendererSystem>()
                .unwrap()
                .get_renderer(),
        );

        Ok(())
    }

    fn update<'a>(
        &'a mut self,
        manager: &SystemManager,
        world: &'a mut World,
        _delta: std::time::Duration,
    ) {
        for (transform_component, mut ps_component) in
            world.query::<(&mut TransformComponent, &mut ParticleSystemComponent)>()
        {
            if ps_component.particle_system_index.is_none() {
                let a = ps_component.particle_system.builder.as_ref();
                ps_component.particle_system_index = Some(
                    self.renderer
                        .as_mut()
                        .unwrap()
                        .borrow_mut()
                        .build_particle_system(a),
                );
            } else {
                //TODO something?
                self.renderer
                    .as_mut()
                    .unwrap()
                    .borrow_mut()
                    .get_particle_system_mut(ps_component.particle_system_index.unwrap())
                    .and_then(|ps| {
                        if ps_component.emitter_position {
                            ps.get_emitter_mut()
                                .maybe_set_position(transform_component.position);
                        } else {
                            ps.set_transform(transform_component.get_matrix());
                        }

                        Some(0)
                    });
            }

            self.frame_stamps
                .set_last_seen(ps_component.particle_system_index.unwrap());
        }

        //delete meshes that have been seen in the past but not in this frame
        let mut renderer = self.renderer.as_ref().unwrap().borrow_mut();
        let old_indices = renderer
            .get_particle_systems()
            .map(|(k, _)| k)
            .filter(|k| self.frame_stamps.is_element_old(*k))
            .collect::<Vec<_>>();

        for ps_index in old_indices {
            renderer.remove_particle_system(ps_index);
            self.frame_stamps.element_removed(ps_index);
        }

        self.frame_stamps.next_frame();
    }
}

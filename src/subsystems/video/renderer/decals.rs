use cgmath::Matrix4;
use game_kernel_utils::{cast_range, AutoIndexMap, KeyType};
use vulkano::buffer::{
    BufferAccess, BufferContents, BufferUsage, DeviceLocalBuffer, TypedBufferAccess,
};
use vulkano::command_buffer::sys::UnsafeCommandBufferBuilder;
use vulkano::descriptor_set::allocator::{
    DescriptorSetAlloc, DescriptorSetAllocator, StandardDescriptorSetAlloc,
};
use vulkano::descriptor_set::layout::{
    DescriptorSetLayout, DescriptorSetLayoutBinding, DescriptorSetLayoutCreateInfo,
};
use vulkano::descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::image::ImageAccess;
use vulkano::pipeline::{ComputePipeline, Pipeline};
use vulkano::sampler::{Sampler, SamplerAddressMode, SamplerCreateInfo};
use vulkano::VulkanObject;

use crate::vulkan;
use crate::vulkan::{
    write_device_array, write_device_buffer, write_device_slice, ToImageView, VulkanBasic,
    VulkanCommon,
};

use super::texture::{PbrTextures, Texture};

use std::collections::BTreeMap;
use std::ops::Range;
use std::sync::Arc;
use vulkano::device::Device;
use vulkano::shader::{ShaderModule, ShaderStages};

use game_kernel_utils::UpdateRange;

mod shaders {
    pub mod classification_cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/decals/classification.glsl",
            spirv_version: "1.3",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/include/"],
            define: [("MAX_DECALS", "4096")]
        }
    }
    //pub use cs::ty::BVHTree as GpuBVHTree;
    pub use classification_cs::ty::Decal as GpuDecal;
    pub use classification_cs::ty::DecalsInfo as GpuDecalsInfo;
    pub use classification_cs::ty::ProjectionData as ClassificationProjectionData;

    use super::*;

    pub fn get_classification_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        classification_cs::load(device.clone()).unwrap()
    }
}

#[derive(Clone)]
pub struct Decal {
    textures: PbrTextures,
    projection_matrix: Matrix4<f32>,
}

impl Decal {
    pub fn new(textures: PbrTextures, projection_matrix: Matrix4<f32>) -> Self {
        Self {
            textures,
            projection_matrix,
        }
    }

    pub fn textures(&self) -> &PbrTextures {
        &self.textures
    }
}

struct PrivDecal {
    textures: [u32; 4],
    projection_matrix: Matrix4<f32>,
}

impl PrivDecal {
    pub fn as_gpu_decal(&self) -> shaders::GpuDecal {
        shaders::GpuDecal {
            textures: self.textures,
            world_matrix: self.projection_matrix.into(),
        }
    }
}

const ALBEDO_INDEX: usize = 0;
const NORMAL_MAP_INDEX: usize = 1;
const METALNESS_ROUGHNESS_ALPHA_INDEX: usize = 2;
const EMISSION_INDEX: usize = 3;

pub struct Decals {
    textures: [Vec<Texture>; 4],
    free_textures: [Vec<usize>; 4],
    decals: AutoIndexMap<PrivDecal>,
    decals_update_range: Range<u16>,
    decals_properties_update_range: Range<u16>,

    depth_buffer: Arc<dyn ImageAccess>,
    norm_rough_met: Arc<dyn ImageAccess>,
    reduced_depth: Arc<dyn ImageAccess>,

    buckets_buffer: Arc<DeviceLocalBuffer<[u32]>>,
    decals_buffer: Arc<DeviceLocalBuffer<[shaders::GpuDecal]>>,
    decals_info_buffer: Arc<DeviceLocalBuffer<shaders::GpuDecalsInfo>>,

    classification_pipeline: Arc<ComputePipeline>,
    decals_descriptor_set: Option<Arc<PersistentDescriptorSet>>,
    second_descriptor_set: Arc<PersistentDescriptorSet>,
    pub(crate) bucket_descriptor_set: Arc<PersistentDescriptorSet>,
    pub(crate) forward_pass_descriptor_set: Option<StandardDescriptorSetAlloc>,
    forward_pass_descriptor_set_layout: Arc<DescriptorSetLayout>,
    forward_pass_descriptor_set_objs: Vec<Arc<dyn std::any::Any>>,
    pub(crate) dummy_descriptor_set: Option<Arc<PersistentDescriptorSet>>,
}

impl Decals {
    pub fn get_2d_tile_count(size: [u32; 2], tile_size: [u32; 2]) -> u32 {
        ((size[0] as f32 / tile_size[0] as f32).ceil() as u32)
            * ((size[1] as f32 / tile_size[1] as f32).ceil() as u32)
    }

    pub fn get_2d_dispatch_size(size: [u32; 2], local_size: [u32; 2]) -> [u32; 3] {
        [
            (size[0] as f32 / local_size[0] as f32).ceil() as u32,
            (size[1] as f32 / local_size[1] as f32).ceil() as u32,
            1,
        ]
    }

    fn save_obj<T: std::any::Any>(
        forward_pass_descriptor_set_objs: &mut Vec<Arc<dyn std::any::Any>>,
        o: Arc<T>,
    ) -> Arc<T> {
        forward_pass_descriptor_set_objs.push(o.clone() as _);
        o
    }

    fn update_descriptor_set(&mut self, vulkan_common: &VulkanCommon) {
        let sampler = Sampler::new(
            vulkan_common.get_device(),
            SamplerCreateInfo {
                address_mode: [SamplerAddressMode::ClampToEdge; 3],
                border_color: vulkano::sampler::BorderColor::FloatTransparentBlack,
                ..SamplerCreateInfo::simple_repeat_linear()
            },
        )
        .unwrap();

        self.decals_descriptor_set = Some(
            PersistentDescriptorSet::new(
                vulkan_common.get_descriptor_set_allocator_ref(),
                self.classification_pipeline.layout().set_layouts()[0].clone(),
                [
                    WriteDescriptorSet::buffer(0, self.decals_buffer.clone()),
                    WriteDescriptorSet::buffer(1, self.decals_info_buffer.clone()),
                    WriteDescriptorSet::image_view_sampler_array(
                        2,
                        0,
                        self.textures[ALBEDO_INDEX]
                            .iter()
                            .map(|t| {
                                (
                                    Self::save_obj(
                                        &mut self.forward_pass_descriptor_set_objs,
                                        t.texture.clone().to_image_view(),
                                    ) as _,
                                    sampler.clone(),
                                )
                            })
                            .chain(std::iter::repeat((
                                self.textures[ALBEDO_INDEX][0]
                                    .texture
                                    .clone()
                                    .to_image_view() as _,
                                sampler.clone(),
                            )))
                            .take(4096),
                    ),
                    WriteDescriptorSet::image_view_sampler_array(
                        3,
                        0,
                        self.textures[NORMAL_MAP_INDEX]
                            .iter()
                            .map(|t| {
                                (
                                    Self::save_obj(
                                        &mut self.forward_pass_descriptor_set_objs,
                                        t.texture.clone().to_image_view(),
                                    ) as _,
                                    sampler.clone(),
                                )
                            })
                            .chain(std::iter::repeat((
                                self.textures[ALBEDO_INDEX][0]
                                    .texture
                                    .clone()
                                    .to_image_view() as _,
                                sampler.clone(),
                            )))
                            .take(4096),
                    ),
                    WriteDescriptorSet::image_view_sampler_array(
                        4,
                        0,
                        self.textures[METALNESS_ROUGHNESS_ALPHA_INDEX]
                            .iter()
                            .map(|t| {
                                (
                                    Self::save_obj(
                                        &mut self.forward_pass_descriptor_set_objs,
                                        t.texture.clone().to_image_view(),
                                    ) as _,
                                    sampler.clone(),
                                )
                            })
                            .chain(std::iter::repeat((
                                self.textures[ALBEDO_INDEX][0]
                                    .texture
                                    .clone()
                                    .to_image_view() as _,
                                sampler.clone(),
                            )))
                            .take(4096),
                    ),
                    WriteDescriptorSet::image_view_sampler_array(
                        5,
                        0,
                        self.textures[EMISSION_INDEX]
                            .iter()
                            .map(|t| {
                                (
                                    Self::save_obj(
                                        &mut self.forward_pass_descriptor_set_objs,
                                        t.texture.clone().to_image_view(),
                                    ) as _,
                                    sampler.clone(),
                                )
                            })
                            .chain(std::iter::repeat((
                                self.textures[ALBEDO_INDEX][0]
                                    .texture
                                    .clone()
                                    .to_image_view() as _,
                                sampler.clone(),
                            )))
                            .take(4096),
                    ),
                ],
            )
            .unwrap(),
        );

        let dummy_image = Self::save_obj(
            &mut self.forward_pass_descriptor_set_objs,
            self.textures[ALBEDO_INDEX][0]
                .texture
                .clone()
                .to_image_view(),
        );
        let forward_descriptor_set_writes = [
            WriteDescriptorSet::buffer(0, self.decals_buffer.clone()),
            WriteDescriptorSet::buffer(1, self.decals_info_buffer.clone()),
            WriteDescriptorSet::image_view_sampler_array(
                2,
                0,
                self.textures[ALBEDO_INDEX]
                    .iter()
                    .map(|t| {
                        (
                            Self::save_obj(
                                &mut self.forward_pass_descriptor_set_objs,
                                t.texture.clone().to_image_view(),
                            ) as _,
                            sampler.clone(),
                        )
                    })
                    .chain(std::iter::repeat((
                        dummy_image.clone() as _,
                        sampler.clone(),
                    )))
                    .take(4096),
            ),
            WriteDescriptorSet::image_view_sampler_array(
                3,
                0,
                self.textures[METALNESS_ROUGHNESS_ALPHA_INDEX]
                    .iter()
                    .map(|t| {
                        (
                            Self::save_obj(
                                &mut self.forward_pass_descriptor_set_objs,
                                t.texture.clone().to_image_view(),
                            ) as _,
                            sampler.clone(),
                        )
                    })
                    .chain(std::iter::repeat((
                        dummy_image.clone() as _,
                        sampler.clone(),
                    )))
                    .take(4096),
            ),
            WriteDescriptorSet::image_view_sampler_array(
                4,
                0,
                self.textures[EMISSION_INDEX]
                    .iter()
                    .map(|t| {
                        (
                            Self::save_obj(
                                &mut self.forward_pass_descriptor_set_objs,
                                t.texture.clone().to_image_view(),
                            ) as _,
                            sampler.clone(),
                        )
                    })
                    .chain(std::iter::repeat((dummy_image as _, sampler.clone())))
                    .take(4096),
            ),
        ];

        if self.forward_pass_descriptor_set.is_none() {
            self.forward_pass_descriptor_set = Some(
                vulkan_common
                    .get_descriptor_set_allocator_ref()
                    .allocate(&self.forward_pass_descriptor_set_layout, 0)
                    .unwrap(),
            );
        }

        unsafe {
            self.forward_pass_descriptor_set
                .as_mut()
                .unwrap()
                .inner_mut()
                .write(
                    self.forward_pass_descriptor_set_layout.as_ref(),
                    forward_descriptor_set_writes.iter(),
                );
        }

        //TODO move elsewhere
        /*let dummy_descriptor_set_layout = {
            let mut bindings = BTreeMap::new();
            DescriptorSetLayout::new(
                vulkan_common.get_device(),
                DescriptorSetLayoutCreateInfo {
                    bindings,
                    ..Default::default()
                },
            )
            .unwrap()
        };

        self.dummy_descriptor_set = Some(
            PersistentDescriptorSet::new(
                vulkan_common.get_descriptor_set_allocator_ref(),
                dummy_descriptor_set_layout,
                [],
            )
            .unwrap(),
        );*/
        self.decals_update_range.update_range_reset();
    }

    fn allocate_decals_buffer(
        vulkan_common: &VulkanCommon,
        size: u64,
    ) -> Arc<DeviceLocalBuffer<[shaders::GpuDecal]>> {
        DeviceLocalBuffer::array(
            vulkan_common.get_memory_allocator_ref(),
            size,
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            [vulkan_common.get_graphics_queue().queue_family_index()]
                .iter()
                .cloned(),
        )
        .unwrap()
    }

    pub fn new(
        vulkan_common: &VulkanCommon,
        depth_buffer: Arc<dyn ImageAccess>,
        norm_rough_met: Arc<dyn ImageAccess>,
        reduced_depth: Arc<dyn ImageAccess>,
        res_buffer: Arc<dyn BufferAccess>,
    ) -> Self {
        let textures = [(); 4].map(|_| vec![]);
        let free_textures = [(); 4].map(|_| vec![]);
        let decals = AutoIndexMap::new();
        let decals_update_range = 0..0;
        let decals_properties_update_range = 0..0;

        let buckets_buffer = DeviceLocalBuffer::array(
            vulkan_common.get_memory_allocator_ref(),
            (Self::get_2d_tile_count(vulkan_common.config.resolution, [8; 2]) * 128).into(),
            BufferUsage {
                storage_buffer: true,
                ..Default::default()
            },
            [vulkan_common.get_graphics_queue().queue_family_index()]
                .iter()
                .cloned(),
        )
        .unwrap();

        let decals_buffer = Self::allocate_decals_buffer(vulkan_common, 64);

        let decals_info_buffer = DeviceLocalBuffer::new(
            vulkan_common.get_memory_allocator_ref(),
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            [vulkan_common.get_graphics_queue().queue_family_index()]
                .iter()
                .cloned(),
        )
        .unwrap();

        let classification_pipeline = ComputePipeline::new(
            vulkan_common.get_device(),
            shaders::get_classification_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let decals_descriptor_set = None;

        let second_descriptor_set = PersistentDescriptorSet::new(
            vulkan_common.get_descriptor_set_allocator_ref(),
            classification_pipeline.layout().set_layouts()[1].clone(),
            [
                WriteDescriptorSet::buffer(0, buckets_buffer.clone()),
                WriteDescriptorSet::image_view_sampler(
                    1,
                    depth_buffer.clone().to_image_view(),
                    Sampler::new(vulkan_common.get_device(), SamplerCreateInfo::default()).unwrap(),
                ),
                WriteDescriptorSet::image_view(2, norm_rough_met.clone().to_image_view()),
                WriteDescriptorSet::image_view_sampler(
                    3,
                    reduced_depth.clone().to_image_view(),
                    Sampler::new(vulkan_common.get_device(), SamplerCreateInfo::default()).unwrap(),
                ),
                WriteDescriptorSet::buffer(4, res_buffer),
            ],
        )
        .unwrap();

        let third_buckets_descriptor_set_layout = {
            let mut bindings = BTreeMap::new();
            bindings.insert(
                0,
                DescriptorSetLayoutBinding {
                    descriptor_count: 1,
                    stages: ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    variable_descriptor_count: false,
                    immutable_samplers: vec![],
                    ..DescriptorSetLayoutBinding::descriptor_type(
                        vulkano::descriptor_set::layout::DescriptorType::StorageBuffer,
                    )
                },
            );
            DescriptorSetLayout::new(
                vulkan_common.get_device(),
                DescriptorSetLayoutCreateInfo {
                    bindings,
                    ..Default::default()
                },
            )
            .unwrap()
        };

        let bucket_descriptor_set = PersistentDescriptorSet::new(
            vulkan_common.get_descriptor_set_allocator_ref(),
            third_buckets_descriptor_set_layout,
            [WriteDescriptorSet::buffer(0, buckets_buffer.clone())],
        )
        .unwrap();

        let forward_pass_descriptor_set_layout = {
            let mut bindings = BTreeMap::new();
            bindings.insert(
                0,
                DescriptorSetLayoutBinding {
                    descriptor_count: 1,
                    stages: ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    variable_descriptor_count: false,
                    immutable_samplers: vec![],
                    ..DescriptorSetLayoutBinding::descriptor_type(
                        vulkano::descriptor_set::layout::DescriptorType::StorageBuffer,
                    )
                },
            );
            bindings.insert(
                1,
                DescriptorSetLayoutBinding {
                    descriptor_count: 1,
                    stages: ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    variable_descriptor_count: false,
                    immutable_samplers: vec![],
                    ..DescriptorSetLayoutBinding::descriptor_type(
                        vulkano::descriptor_set::layout::DescriptorType::StorageBuffer,
                    )
                },
            );
            bindings.insert(
                2,
                DescriptorSetLayoutBinding {
                    descriptor_count: 4096,
                    stages: ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    variable_descriptor_count: false,
                    immutable_samplers: vec![],
                    ..DescriptorSetLayoutBinding::descriptor_type(
                        vulkano::descriptor_set::layout::DescriptorType::CombinedImageSampler,
                    )
                },
            );
            bindings.insert(
                3,
                DescriptorSetLayoutBinding {
                    descriptor_count: 4096,
                    stages: ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    variable_descriptor_count: false,
                    immutable_samplers: vec![],
                    ..DescriptorSetLayoutBinding::descriptor_type(
                        vulkano::descriptor_set::layout::DescriptorType::CombinedImageSampler,
                    )
                },
            );
            bindings.insert(
                4,
                DescriptorSetLayoutBinding {
                    descriptor_count: 4096,
                    stages: ShaderStages {
                        fragment: true,
                        ..Default::default()
                    },
                    variable_descriptor_count: false,
                    immutable_samplers: vec![],
                    ..DescriptorSetLayoutBinding::descriptor_type(
                        vulkano::descriptor_set::layout::DescriptorType::CombinedImageSampler,
                    )
                },
            );

            DescriptorSetLayout::new(
                vulkan_common.get_device(),
                DescriptorSetLayoutCreateInfo {
                    bindings,
                    ..Default::default()
                },
            )
            .unwrap()
        };

        let forward_pass_descriptor_set = Some(
            vulkan_common
                .get_descriptor_set_allocator_ref()
                .allocate(&forward_pass_descriptor_set_layout, 0)
                .unwrap(),
        );

        Self {
            textures,
            free_textures,
            decals,
            decals_update_range,
            decals_properties_update_range,

            depth_buffer,
            norm_rough_met,
            reduced_depth,

            buckets_buffer,
            decals_buffer,
            decals_info_buffer,

            classification_pipeline,
            decals_descriptor_set,
            second_descriptor_set,
            bucket_descriptor_set,
            forward_pass_descriptor_set_layout,
            forward_pass_descriptor_set,
            forward_pass_descriptor_set_objs: vec![],
            dummy_descriptor_set: None,
        }
    }

    pub fn allocate_textures(&mut self, textures: PbrTextures) -> [u32; 4] {
        let PbrTextures {
            color,
            normal_map,
            metalness_roughness_map,
            emissive_map,
        } = textures;
        let indices: Vec<_> = self.textures.iter().map(|t| t.len() as u32).collect();
        [color, normal_map, metalness_roughness_map, emissive_map]
            .into_iter()
            .enumerate()
            .for_each(|(i, t)| self.textures[i].push(t));
        indices.try_into().unwrap()
    }

    pub fn add_decal(&mut self, decal: Decal) -> KeyType {
        let Decal {
            textures,
            projection_matrix,
        } = decal;
        let idxs = self.allocate_textures(textures);
        let priv_decal = PrivDecal {
            textures: idxs,
            projection_matrix,
        };
        let key = self.decals.insert(priv_decal);
        self.decals_update_range.update_range_add(key as u16);
        self.decals_properties_update_range
            .update_range_add(key as u16);
        key
    }

    pub fn remove_decal(&mut self, k: KeyType) {
        self.decals.remove(k);
        self.decals_update_range.update_range_add(k as _);
        self.decals_properties_update_range.update_range_add(k as _);
    }

    pub fn update_decal_transform(&mut self, k: &KeyType, tranform: Matrix4<f32>) {
        self.decals.get_mut(*k as _).unwrap().projection_matrix = tranform;
        self.decals_properties_update_range
            .update_range_add(*k as _);
    }

    fn update_decals_buffer(&mut self, vulkan_common: &VulkanCommon) -> bool {
        if self.decals_properties_update_range.is_empty() {
            false
        } else {
            let current_buffer_size = self.decals_buffer.len();
            let update_range = if (self.decals.len() as u64) > current_buffer_size {
                self.decals_buffer =
                    Self::allocate_decals_buffer(vulkan_common, current_buffer_size * 2);
                0..(self.decals.len() as u16)
            } else {
                self.decals_properties_update_range.clone()
            };

            let gpu_decals: Vec<_> = update_range
                .clone()
                .map(|el| self.decals[&(el as _)].as_gpu_decal())
                .collect();

            write_device_slice(
                vulkan_common.clone(),
                gpu_decals.into_iter(),
                self.decals_buffer.slice(cast_range(&update_range)).unwrap(),
            )
            .unwrap();

            self.decals_properties_update_range.update_range_reset();

            true
        }
    }

    pub fn decals_classification_cbb(
        &mut self,
        vulkan_common: &VulkanCommon,
        cbb: &mut UnsafeCommandBufferBuilder,
        camera: &super::camera::Camera,
        aspect_ratio: f32,
    ) {
        let decal_buffer_updated = self.update_decals_buffer(vulkan_common); //todo don't use a separate command buffer
        if !self.decals_update_range.is_empty() {
            self.update_descriptor_set(vulkan_common);
        }
        if self.decals_descriptor_set.is_none() {
            return;
        }

        let ash_device = vulkan_common.get_ash_device();
        let ash_command_buffer = cbb.handle();
        unsafe {
            if decal_buffer_updated {
                let data = shaders::GpuDecalsInfo {
                    n_decals: self.decals.len() as _,
                };
                vulkan::command_buffer::cmd_update_buffer(
                    &ash_device,
                    ash_command_buffer,
                    self.decals_info_buffer.inner().buffer.handle(),
                    self.decals_info_buffer.inner().offset,
                    &data,
                );
            }

            ash_device.cmd_bind_pipeline(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.classification_pipeline.handle(),
            );
            let pdata = shaders::ClassificationProjectionData {
                mv: camera.get_view().into(),
                p: {
                    let mut p = camera.get_proj(aspect_ratio);
                    let v = camera.get_eye().extend(0.0);
                    p[0][3] = v.x;
                    p[1][3] = v.y;
                    p[2][3] = v.z;
                    p.into()
                },
                //view_pos: camera.get_position().into(),
            };
            ash_device.cmd_push_constants(
                ash_command_buffer,
                self.classification_pipeline.layout().handle(),
                ash::vk::ShaderStageFlags::COMPUTE,
                0,
                pdata.as_bytes(),
            );
            ash_device.cmd_bind_descriptor_sets(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.classification_pipeline.layout().handle(),
                0,
                &[
                    self.decals_descriptor_set
                        .as_ref()
                        .unwrap()
                        .inner()
                        .handle(),
                    self.second_descriptor_set.inner().handle(),
                ],
                &[],
            );
            {
                let [group_count_x, group_count_y, group_count_z] = Self::get_2d_dispatch_size(
                    self.depth_buffer.dimensions().width_height(),
                    [8; 2],
                );
                ash_device.cmd_dispatch(
                    ash_command_buffer,
                    group_count_x,
                    group_count_y,
                    group_count_z,
                );
            }
        }
    }
}

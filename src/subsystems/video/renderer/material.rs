use super::shader;
use super::texture;

use std::sync::Arc;

use vulkano::device::Device;

mod test_shaders {
    pub mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "data/shaders/third_pbr/vert.glsl"
        }
    }

    pub mod fs {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "data/shaders/third_pbr/frag.glsl",
            include: ["data/shaders/include"]
        }
    }

    pub mod combined_fs {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "data/shaders/pre_pass/combined_frag.glsl",
            include: ["data/shaders/include"]
        }
    }
}

#[derive(Copy, Clone)]
pub enum BlendMode {
    PassThrough,
    AlphaBlending,
}

pub struct Material {
    pub custom_vertex_shader: Option<Arc<shader::RuntimeShader>>,
    pub fragment_shader: Arc<shader::RuntimeShader>,
    pub pre_fragment: Option<Arc<shader::RuntimeShader>>,
    pub blend_mode: BlendMode,
    pub textures: Vec<Arc<texture::Texture>>,
}

impl Material {
    pub unsafe fn test_new(
        device: Arc<Device>,
        tex: Arc<texture::Texture>,
        normal_map: Arc<texture::Texture>,
        roughness_map: Arc<texture::Texture>,
        metalness_map: Arc<texture::Texture>,
        emissive_map: Arc<texture::Texture>,
    ) -> Self {
        assert_eq!(tex.texture_usage, texture::TextureUsage::Color);
        assert_eq!(normal_map.texture_usage, texture::TextureUsage::Normal);
        assert_eq!(
            roughness_map.texture_usage,
            texture::TextureUsage::Roughness
        );
        assert_eq!(
            metalness_map.texture_usage,
            texture::TextureUsage::Metalness
        );
        let vertex = test_shaders::vs::load(device.clone()).unwrap();
        let fragment = test_shaders::fs::load(device.clone()).unwrap();
        Self {
            custom_vertex_shader: Some(shader::get_cached_or_build_shader(
                device.clone(),
                0,
                || {
                    Arc::new(shader::RuntimeShader::from_entry_point(
                        vertex.entry_point("main").unwrap(),
                        vertex.clone(),
                    ))
                },
            )),
            fragment_shader: shader::get_cached_or_build_shader(device, 1, || {
                Arc::new(shader::RuntimeShader::from_entry_point(
                    fragment.entry_point("main").unwrap(),
                    fragment.clone(),
                ))
            }),
            pre_fragment: None,
            blend_mode: BlendMode::PassThrough,
            textures: vec![tex, normal_map, roughness_map, metalness_map, emissive_map],
        }
    }

    pub unsafe fn test_new_combined(
        device: Arc<Device>,
        tex: Arc<texture::Texture>,
        normal_map: Arc<texture::Texture>,
        metalness_roughness_map: Arc<texture::Texture>,
        emissive_map: Arc<texture::Texture>,
    ) -> Self {
        assert_eq!(tex.texture_usage, texture::TextureUsage::Color);
        assert_eq!(normal_map.texture_usage, texture::TextureUsage::Normal);
        assert_eq!(
            metalness_roughness_map.texture_usage,
            texture::TextureUsage::MetalnessRoughness
        );
        let vertex = test_shaders::vs::load(device.clone()).unwrap();
        let fragment = test_shaders::fs::load(device.clone()).unwrap();
        let pre_fragment = test_shaders::combined_fs::load(device.clone()).unwrap();
        Self {
            custom_vertex_shader: Some(shader::get_cached_or_build_shader(
                device.clone(),
                0,
                || {
                    Arc::new(shader::RuntimeShader::from_entry_point(
                        vertex.entry_point("main").unwrap(),
                        vertex.clone(),
                    ))
                },
            )),
            fragment_shader: shader::get_cached_or_build_shader(device.clone(), 1, || {
                Arc::new(shader::RuntimeShader::from_entry_point(
                    fragment.entry_point("main").unwrap(),
                    fragment.clone(),
                ))
            }),
            pre_fragment: Some(shader::get_cached_or_build_shader(device, 2, || {
                Arc::new(shader::RuntimeShader::from_entry_point(
                    pre_fragment.entry_point("main").unwrap(),
                    pre_fragment.clone(),
                ))
            })),
            blend_mode: BlendMode::PassThrough,
            textures: vec![tex, normal_map, metalness_roughness_map, emissive_map],
        }
    }

    pub fn get_texture_by_usage(
        &self,
        usage: texture::TextureUsage,
    ) -> Option<Arc<texture::Texture>> {
        self.textures
            .iter()
            .find(|t| t.texture_usage == usage)
            .cloned()
    }

    pub fn get_normal_map(&self) -> Option<Arc<texture::Texture>> {
        self.get_texture_by_usage(texture::TextureUsage::Normal)
    }

    pub fn get_roughness_map(&self) -> Option<Arc<texture::Texture>> {
        self.get_texture_by_usage(texture::TextureUsage::Roughness)
    }

    pub fn get_metalness_map(&self) -> Option<Arc<texture::Texture>> {
        match self.get_texture_by_usage(texture::TextureUsage::Metalness) {
            Some(met) => Some(met),
            None => self.get_texture_by_usage(texture::TextureUsage::MetalnessRoughness),
        }
    }

    pub fn get_emissive_map(&self) -> Option<Arc<texture::Texture>> {
        self.get_texture_by_usage(texture::TextureUsage::Emission)
    }

    pub fn get_metalness_roughness_map(&self) -> Option<Arc<texture::Texture>> {
        self.get_texture_by_usage(texture::TextureUsage::MetalnessRoughness)
    }
}

use ash::vk::PresentInfoKHR;
use vulkano::device::Queue;
use vulkano::format::Format;
use vulkano::image::{AttachmentImage, ImageAccess, ImageUsage, SwapchainImage};
use vulkano::VulkanObject;

use vulkano::swapchain::{PresentInfo, SwapchainPresentInfo};
use vulkano::sync::{GpuFuture, Semaphore};

use std::sync::Arc;

use crate::vulkan::{SwapChain, VulkanBasic, VulkanCommon};

pub trait RenderOutput {
    /*fn output(
        &mut self,
        command_buffer_builder: &mut UnsafeCommandBufferBuilder,
        queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + Send + Sync + 'static>,
    );*/
    fn format(&self) -> Format;
    fn images(&self) -> Vec<Arc<dyn ImageAccess>>;
    fn next_idx(&mut self, acquire_semaphore: Option<&Semaphore>) -> usize;
    fn present(&self, present_semaphore: Option<&Semaphore>, queue: &Queue, idx: usize);
    fn present_image_layout(&self) -> vulkano::image::ImageLayout;
    fn signals_acquire_semaphore(&self) -> bool;
}

pub struct SwapchainOutout {
    pub swapchain: SwapChain,
    vulkan_common: VulkanCommon,
}

impl Clone for SwapchainOutout {
    fn clone(&self) -> Self {
        Self {
            swapchain: self.swapchain.clone(),
            vulkan_common: self.vulkan_common.clone(),
        }
    }
}

impl SwapchainOutout {
    pub fn new(swapchain: SwapChain, vulkan_common: VulkanCommon) -> Self {
        Self {
            swapchain,
            vulkan_common,
        }
    }
}

impl RenderOutput for SwapchainOutout
where
    SwapchainImage: ImageAccess,
{
    fn format(&self) -> Format {
        self.swapchain.get_swapchain_images()[0].format()
    }

    fn images(&self) -> Vec<Arc<dyn ImageAccess>> {
        self.swapchain
            .get_swapchain_images()
            .iter()
            .map(|image| image.clone() as _)
            .collect()
    }

    fn next_idx(&mut self, acquire_semaphore: Option<&Semaphore>) -> usize {
        use vulkano::swapchain::{acquire_next_image_raw, AcquiredImage};

        let AcquiredImage {
            image_index: idx,
            suboptimal: _,
        } = unsafe {
            acquire_next_image_raw(
                &self.swapchain.get_swapchain(),
                None,
                acquire_semaphore,
                None,
            )
            .unwrap()
        };

        idx as _
    }

    fn present(&self, present_semaphore: Option<&Semaphore>, queue: &Queue, idx: usize) {
        let swapchain = self.swapchain.get_swapchain();
        let mut idxs: smallvec::SmallVec<[u32; 1]> = smallvec::SmallVec::with_capacity(1);
        idxs.push(idx as u32);
        let mut semaphores: smallvec::SmallVec<[_; 1]> = smallvec::SmallVec::with_capacity(1);
        if let Some(sm) = present_semaphore {
            semaphores.push(sm.handle());
        }
        unsafe {
            let mut present_result = std::mem::MaybeUninit::uninit();
            let present_info = PresentInfoKHR {
                wait_semaphore_count: semaphores.len() as _,
                p_wait_semaphores: semaphores.as_ptr(),
                swapchain_count: 1,
                p_swapchains: &swapchain.handle() as *const _,
                p_image_indices: idxs.as_ptr(),
                p_results: present_result.as_mut_ptr(),
                ..Default::default()
            };
            (self
                .vulkan_common
                .get_device()
                .fns()
                .khr_swapchain
                .queue_present_khr)(
                self.vulkan_common.get_graphics_queue().handle(),
                &present_info as _,
            )
            .result()
            .unwrap();
        }
    }

    fn present_image_layout(&self) -> vulkano::image::ImageLayout {
        vulkano::image::ImageLayout::PresentSrc
    }

    fn signals_acquire_semaphore(&self) -> bool {
        true
    }
}

#[derive(Clone)]
pub struct OffscreenOutput {
    pub image: Arc<AttachmentImage>,
    vulkan_common: VulkanCommon,
}

impl OffscreenOutput {
    pub fn new(format: Format, vulkan_common: VulkanCommon) -> Self {
        Self::new_with_image(
            AttachmentImage::with_usage(
                vulkan_common.get_memory_allocator_ref(),
                vulkan_common.get_config().resolution,
                format,
                ImageUsage {
                    sampled: true,
                    ..ImageUsage::empty()
                },
            )
            .unwrap(),
            vulkan_common,
        )
    }

    pub fn new_with_image(image: Arc<AttachmentImage>, vulkan_common: VulkanCommon) -> Self {
        Self {
            image,
            vulkan_common,
        }
    }
}

impl RenderOutput for OffscreenOutput {
    fn format(&self) -> Format {
        self.image.format()
    }

    fn images(&self) -> Vec<Arc<dyn ImageAccess>> {
        vec![self.image.clone()]
    }

    fn next_idx(&mut self, acquire_semaphore: Option<&Semaphore>) -> usize {
        0
    }

    fn present(&self, present_semaphore: Option<&Semaphore>, queue: &Queue, idx: usize) {}

    fn present_image_layout(&self) -> vulkano::image::ImageLayout {
        vulkano::image::ImageLayout::ColorAttachmentOptimal
    }

    fn signals_acquire_semaphore(&self) -> bool {
        false
    }
}

use std::{ffi::CString, sync::Arc};

use ash::vk::{
    DescriptorSetLayout, DescriptorSetLayoutCreateFlags, DescriptorSetLayoutCreateInfo,
    PipelineShaderStageCreateFlags,
};
use vulkano::{
    buffer::BufferContents,
    command_buffer::{sys::UnsafeCommandBufferBuilder, RenderPassBeginInfo, SubpassContents},
    descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet},
    image::{
        view::{ImageView, ImageViewCreateInfo},
        ImageAccess, ImageAspects, ImageCreateFlags, ImageDimensions, ImageLayout,
        ImageSubresourceRange, ImageUsage, ImageViewAbstract,
    },
    pipeline::{
        graphics::{
            color_blend::{AttachmentBlend, BlendFactor, BlendOp},
            vertex_input::BuffersDefinition,
            viewport::Viewport,
        },
        ComputePipeline, GraphicsPipeline, Pipeline, PipelineBindPoint,
    },
    render_pass::{Framebuffer, FramebufferCreateInfo, RenderPass, Subpass},
    sampler::{Sampler, SamplerCreateInfo},
    shader::{DescriptorRequirements, ShaderStages},
    single_pass_renderpass,
    sync::{AccessFlags, DependencyInfo, ImageMemoryBarrier, PipelineStages},
    VulkanObject,
};

use crate::vulkan::{storage_image::StorageImage, ToImageView, VulkanBasic, VulkanCommon};

mod shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/post_processing/bloom.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/raytracing/", "data/shaders/include/"]
        }
    }

    pub mod cs_upsample {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/post_processing/bloom.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/raytracing/", "data/shaders/include/"],
            define: [("UPSAMPLE", "1")]
        }
    }

    pub mod blend_vs {
        vulkano_shaders::shader! {
            ty:  "vertex",
            path: "data/shaders/raytracing/vertex.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }

    pub mod blend_fs {
        vulkano_shaders::shader! {
            ty:  "fragment",
            path: "data/shaders/post_processing/bloom_blending_frag.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }

    use vulkano::{device::Device, shader::ShaderModule};

    use super::*;
    pub fn get_downsample_compute_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }

    pub fn get_upsample_compute_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        cs_upsample::load(device.clone()).unwrap()
    }

    pub fn get_blend_vertex_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        blend_vs::load(device.clone()).unwrap()
    }

    pub fn get_blend_fragment_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        blend_fs::load(device.clone()).unwrap()
    }
}

pub struct Bloom {
    vulkan_common: VulkanCommon,

    blurred_image: Arc<StorageImage>,
    bloom_pipeline: Arc<ComputePipeline>,
    upsample_pipeline: crate::subsystems::video::common::vulkan::pipeline::Pipeline,
    bloom_descriptor_set: Arc<dyn DescriptorSet>,
    upsample_descriptor_set: ash::vk::DescriptorSet,

    blend_render_pass: Arc<RenderPass>,
    blend_pipeline: Arc<GraphicsPipeline>,
    blend_framebuffer: Arc<Framebuffer>,
    blend_descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
}

impl Bloom {
    pub fn new(vulkan_common: &VulkanCommon, input_image: Arc<dyn ImageAccess>) -> Self {
        let ash_device = vulkan_common.get_ash_device();
        let blurred_image_dims = ImageDimensions::Dim2d {
            width: input_image.dimensions().width() / 2,
            height: input_image.dimensions().height() / 2,
            array_layers: 1,
        };
        let blurred_image = StorageImage::with_usage(
            vulkan_common.get_memory_allocator_ref(),
            blurred_image_dims,
            super::HDR_FORMAT,
            ImageUsage {
                sampled: true,
                storage: true,
                ..Default::default()
            },
            ImageCreateFlags {
                ..Default::default()
            },
            [vulkan_common.get_graphics_queue_family()],
            blurred_image_dims.max_mip_levels(),
        )
        .unwrap();

        let bloom_pipeline = ComputePipeline::new(
            vulkan_common.get_device(),
            shaders::get_downsample_compute_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let upsample_pipeline = unsafe {
            let mut ash_pipeline = crate::subsystems::video::common::vulkan::pipeline::Pipeline {
                descriptor_requirements: vec![],
                descriptor_set_layouts: vec![],
                pipeline_layout: ash::vk::PipelineLayout::null(),
                pipeline: ash::vk::Pipeline::null(),
            };

            let entry_point_name = CString::new("main".to_string()).unwrap();
            let shader = shaders::get_upsample_compute_shader(vulkan_common.get_device());
            let entry_point = shader.entry_point("main").unwrap();
            let descriptor_set_requirements = entry_point.descriptor_requirements();
            ash_pipeline.descriptor_requirements =
                descriptor_set_requirements.fold(Vec::new(), |v, ((set, binding), reqs)| {
                    let mut v: Vec<Vec<Option<(u32, DescriptorRequirements)>>> = v;
                    if v.len() <= set as _ {
                        v.resize(1 + set as usize, Vec::new());
                    }
                    if v[set as usize].len() <= binding as _ {
                        v[set as usize].resize(1 + binding as usize, None);
                    }
                    v[set as usize][binding as usize] = Some((binding, reqs.clone()));
                    v
                });
            ash_pipeline.descriptor_set_layouts = ash_pipeline
                .descriptor_requirements
                .iter()
                .map(|bindings| {
                    let ash_bindings = bindings
                        .into_iter()
                        .filter_map(|b| b.clone())
                        .map(|(binding, reqs)| ash::vk::DescriptorSetLayoutBinding {
                            binding,
                            descriptor_type: reqs.descriptor_types[0].into(),
                            descriptor_count: reqs.descriptor_count.unwrap_or(0),
                            stage_flags: ash::vk::ShaderStageFlags::COMPUTE,
                            p_immutable_samplers: std::ptr::null(),
                        })
                        .collect::<Vec<_>>();
                    ash_device
                        .create_descriptor_set_layout(
                            &ash::vk::DescriptorSetLayoutCreateInfo {
                                flags: DescriptorSetLayoutCreateFlags::default(),
                                binding_count: ash_bindings.len() as _,
                                p_bindings: ash_bindings.as_ptr(),
                                ..Default::default()
                            },
                            None,
                        )
                        .unwrap()
                })
                .collect::<Vec<_>>();
            let push_constant_requirements =
                entry_point
                    .push_constant_requirements()
                    .map(|reqs| ash::vk::PushConstantRange {
                        stage_flags: ash::vk::ShaderStageFlags::COMPUTE,
                        offset: reqs.offset,
                        size: reqs.size,
                    });

            ash_pipeline.pipeline_layout = ash_device
                .create_pipeline_layout(
                    &ash::vk::PipelineLayoutCreateInfo {
                        flags: ash::vk::PipelineLayoutCreateFlags::empty(),
                        set_layout_count: ash_pipeline.descriptor_set_layouts.len() as _,
                        p_set_layouts: ash_pipeline.descriptor_set_layouts.as_ptr(),
                        push_constant_range_count: if push_constant_requirements.is_some() {
                            1
                        } else {
                            0
                        },
                        p_push_constant_ranges: push_constant_requirements
                            .as_ref()
                            .map(|r| r as *const _)
                            .unwrap_or(std::ptr::null()),
                        ..Default::default()
                    },
                    None,
                )
                .unwrap();
            ash_pipeline.pipeline = ash_device
                .create_compute_pipelines(
                    ash::vk::PipelineCache::null(),
                    &[ash::vk::ComputePipelineCreateInfo {
                        flags: ash::vk::PipelineCreateFlags::empty(),
                        stage: ash::vk::PipelineShaderStageCreateInfo {
                            flags: PipelineShaderStageCreateFlags::empty(),
                            stage: ash::vk::ShaderStageFlags::COMPUTE,
                            module: shader.handle(),
                            p_name: entry_point_name.as_ptr(),
                            p_specialization_info: &ash::vk::SpecializationInfo::default()
                                as *const _,
                            ..Default::default()
                        },
                        layout: ash_pipeline.pipeline_layout,
                        base_pipeline_handle: ash::vk::Pipeline::null(),
                        base_pipeline_index: 0,
                        ..Default::default()
                    }],
                    None,
                )
                .unwrap()[0];

            ash_pipeline
        };

        let blurred_mip_levels = blurred_image_dims.max_mip_levels();
        let bloom_descriptor_set = PersistentDescriptorSet::new(
            vulkan_common.get_descriptor_set_allocator_ref(),
            bloom_pipeline.layout().set_layouts()[0].clone(),
            [
                WriteDescriptorSet::image_view_array(
                    0,
                    0,
                    (0..blurred_mip_levels)
                        .chain(
                            std::iter::repeat(blurred_mip_levels - 1)
                                .take((12 - blurred_mip_levels).max(0) as usize),
                        )
                        .map(|mip_level| {
                            ImageView::new(
                                blurred_image.clone(),
                                ImageViewCreateInfo {
                                    view_type: vulkano::image::ImageViewType::Dim2d,
                                    format: Some(blurred_image.format()),
                                    subresource_range: ImageSubresourceRange {
                                        aspects: ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        },
                                        mip_levels: mip_level..mip_level + 1,
                                        array_layers: 0..1,
                                    },
                                    usage: *blurred_image.usage(),
                                    ..Default::default()
                                },
                            )
                            .unwrap() as Arc<dyn ImageViewAbstract>
                        }),
                ),
                WriteDescriptorSet::image_view_sampler(
                    1,
                    input_image.clone().to_image_view(),
                    Sampler::new(
                        vulkan_common.get_device(),
                        SamplerCreateInfo::simple_repeat_linear(),
                    )
                    .unwrap(),
                ),
                WriteDescriptorSet::image_view_sampler(
                    2,
                    blurred_image.clone().to_image_view(),
                    Sampler::new(
                        vulkan_common.get_device(),
                        SamplerCreateInfo::simple_repeat_linear(),
                    )
                    .unwrap(),
                ),
            ],
        )
        .unwrap();
        let upsample_descriptor_pool = vulkan_common.get_ash_descriptor_pools().pool(
            &crate::vulkan::descriptor_set::PoolSizes::from_reqs(
                &upsample_pipeline.descriptor_requirements[0],
            ),
            8,
        );
        let upsample_descriptor_set = unsafe {
            let ds = ash_device
                .allocate_descriptor_sets(&ash::vk::DescriptorSetAllocateInfo {
                    descriptor_pool: upsample_descriptor_pool,
                    descriptor_set_count: 1,
                    p_set_layouts: &upsample_pipeline.descriptor_set_layouts[0] as *const _,
                    ..Default::default()
                })
                .unwrap()[0];

            /*fn create_image_view(ash_device: &ash::Device, image: ash::vk::Image) {
                ash_device.create_image_view(
                    &ash::vk::ImageViewCreateInfo {
                        flags: ash::vk::ImageViewCreateFlags::empty(),
                        image: image,
                        view_type: ash::vk::ImageViewType::TYPE_2D,
                        format: image.format().into(),
                        components: ash::vk::ComponentMapping {
                            r: ash::vk::ComponentSwizzle::R,
                            g: ash::vk::ComponentSwizzle::G,
                            b: ash::vk::ComponentSwizzle::B,
                            a: ash::vk::ComponentSwizzle::A,
                        },
                        subresource_range: ash::vk::ImageSubresourceRange {
                            aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                            base_mip_level: mip_level,
                            level_count: 1,
                            base_array_layer: 0,
                            layer_count: 1,
                        },
                        .. Default::default()
                    },
                    None
                ).unwrap();
            }*/

            let sampler = ash_device
                .create_sampler(
                    &ash::vk::SamplerCreateInfo {
                        flags: ash::vk::SamplerCreateFlags::empty(),
                        mag_filter: ash::vk::Filter::LINEAR,
                        min_filter: ash::vk::Filter::LINEAR,
                        mipmap_mode: ash::vk::SamplerMipmapMode::NEAREST,
                        address_mode_u: ash::vk::SamplerAddressMode::MIRROR_CLAMP_TO_EDGE,
                        address_mode_v: ash::vk::SamplerAddressMode::MIRROR_CLAMP_TO_EDGE,
                        address_mode_w: ash::vk::SamplerAddressMode::MIRROR_CLAMP_TO_EDGE,
                        mip_lod_bias: 0.0,
                        anisotropy_enable: 0,
                        compare_enable: 0,
                        min_lod: 0.0,
                        max_lod: ash::vk::LOD_CLAMP_NONE,
                        ..Default::default()
                    },
                    None,
                )
                .unwrap();

            let views: Vec<_> = (0..12)
                .map(|v| v.min(blurred_mip_levels - 1))
                .map(|mip_level| {
                    let image_view = ash_device
                        .create_image_view(
                            &ash::vk::ImageViewCreateInfo {
                                flags: ash::vk::ImageViewCreateFlags::empty(),
                                image: blurred_image.handle(),
                                view_type: ash::vk::ImageViewType::TYPE_2D,
                                format: blurred_image.format().into(),
                                components: ash::vk::ComponentMapping {
                                    r: ash::vk::ComponentSwizzle::R,
                                    g: ash::vk::ComponentSwizzle::G,
                                    b: ash::vk::ComponentSwizzle::B,
                                    a: ash::vk::ComponentSwizzle::A,
                                },
                                subresource_range: ash::vk::ImageSubresourceRange {
                                    aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                                    base_mip_level: mip_level,
                                    level_count: 1,
                                    base_array_layer: 0,
                                    layer_count: 1,
                                },
                                ..Default::default()
                            },
                            None,
                        )
                        .unwrap();

                    ash::vk::DescriptorImageInfo {
                        sampler,
                        image_view,
                        image_layout: ash::vk::ImageLayout::GENERAL,
                    }
                })
                .collect();

            let sampler_image_view = {
                let image_view = ash_device
                    .create_image_view(
                        &ash::vk::ImageViewCreateInfo {
                            flags: ash::vk::ImageViewCreateFlags::empty(),
                            image: blurred_image.handle(),
                            view_type: ash::vk::ImageViewType::TYPE_2D,
                            format: blurred_image.format().into(),
                            components: ash::vk::ComponentMapping {
                                r: ash::vk::ComponentSwizzle::R,
                                g: ash::vk::ComponentSwizzle::G,
                                b: ash::vk::ComponentSwizzle::B,
                                a: ash::vk::ComponentSwizzle::A,
                            },
                            subresource_range: ash::vk::ImageSubresourceRange {
                                aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                                base_mip_level: 0,
                                level_count: blurred_mip_levels,
                                base_array_layer: 0,
                                layer_count: 1,
                            },
                            ..Default::default()
                        },
                        None,
                    )
                    .unwrap();

                ash::vk::DescriptorImageInfo {
                    sampler,
                    image_view,
                    image_layout: ash::vk::ImageLayout::GENERAL,
                }
            };

            ash_device.update_descriptor_sets(
                &[
                    ash::vk::WriteDescriptorSet {
                        dst_set: ds,
                        dst_binding: 0,
                        dst_array_element: 0,
                        descriptor_count: blurred_mip_levels,
                        descriptor_type: ash::vk::DescriptorType::STORAGE_IMAGE,
                        p_image_info: views.as_ptr(),
                        ..Default::default()
                    },
                    ash::vk::WriteDescriptorSet {
                        dst_set: ds,
                        dst_binding: 2,
                        dst_array_element: 0,
                        descriptor_count: 1,
                        descriptor_type: ash::vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                        p_image_info: &sampler_image_view as *const _,
                        ..Default::default()
                    },
                ],
                &[],
            );

            ds
        };

        let blend_render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: Load,
                    store: Store,
                    format: super::HDR_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::ColorAttachmentOptimal,
                    final_layout: ImageLayout::ColorAttachmentOptimal,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        }
        .unwrap();

        let blend_pipeline = GraphicsPipeline::start()
            .vertex_input_state(BuffersDefinition::new())
            .cull_mode_disabled()
            .viewports_dynamic_scissors_irrelevant(1)
            .viewports(std::iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: vulkan_common.get_config().resolution.map(|x| x as f32),
                depth_range: 0.0..1.0,
            }))
            .vertex_shader(
                shaders::get_blend_vertex_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .fragment_shader(
                shaders::get_blend_fragment_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .render_pass(Subpass::from(blend_render_pass.clone(), 0).unwrap())
            .blend_collective(AttachmentBlend {
                color_op: BlendOp::Add,
                color_source: BlendFactor::SrcAlpha,
                color_destination: BlendFactor::OneMinusSrcAlpha,
                alpha_op: BlendOp::Add,
                alpha_source: BlendFactor::SrcAlpha,
                alpha_destination: BlendFactor::OneMinusSrcAlpha,
            })
            .build(vulkan_common.get_device())
            .unwrap();

        let blend_framebuffer = Framebuffer::new(
            blend_render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![input_image.clone().to_image_view()],
                ..Default::default()
            },
        )
        .unwrap();

        let blend_descriptor_set = PersistentDescriptorSet::new(
            vulkan_common.get_descriptor_set_allocator_ref(),
            blend_pipeline.layout().set_layouts()[0].clone(),
            [WriteDescriptorSet::image_view_sampler(
                0,
                blurred_image.clone().to_image_view(),
                Sampler::new(
                    vulkan_common.get_device(),
                    SamplerCreateInfo::simple_repeat_linear(),
                )
                .unwrap(),
            )],
        )
        .unwrap();

        Self {
            vulkan_common: vulkan_common.clone(),

            blurred_image,
            bloom_pipeline,
            upsample_pipeline,
            bloom_descriptor_set,
            upsample_descriptor_set,

            blend_render_pass,
            blend_pipeline,
            blend_framebuffer,
            blend_descriptor_set,
        }
    }

    pub fn process(&self, cbb: &mut UnsafeCommandBufferBuilder) {
        let ash_device = self.vulkan_common.get_ash_device();
        let ash_command_buffer = cbb.handle();
        unsafe {
            ash_device.cmd_bind_pipeline(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.bloom_pipeline.handle(),
            );
            ash_device.cmd_bind_descriptor_sets(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.bloom_pipeline.layout().handle(),
                0,
                &[self.bloom_descriptor_set.inner().handle()],
                &[],
            );
            let dims = self.blurred_image.dimensions();
            for mip_level in (0..(dims.max_mip_levels()))
            /*.take(7)*/
            {
                ash_device.cmd_push_constants(
                    ash_command_buffer,
                    self.bloom_pipeline.layout().handle(),
                    ash::vk::ShaderStageFlags::COMPUTE,
                    0,
                    shaders::cs::ty::MipLevel {
                        mip_level: mip_level as i32,
                    }
                    .as_bytes(),
                );
                let mip_dims = dims.mip_level_dimensions(mip_level).unwrap();
                ash_device.cmd_dispatch(
                    ash_command_buffer,
                    (mip_dims.width() as f32 / 16.0).ceil() as u32,
                    (mip_dims.height() as f32 / 16.0).ceil() as u32,
                    1,
                );

                ash_device.cmd_pipeline_barrier(
                    ash_command_buffer,
                    ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                    ash::vk::PipelineStageFlags::COMPUTE_SHADER
                        | ash::vk::PipelineStageFlags::FRAGMENT_SHADER,
                    ash::vk::DependencyFlags::empty(),
                    &[],
                    &[],
                    &[ash::vk::ImageMemoryBarrier {
                        src_access_mask: ash::vk::AccessFlags::SHADER_WRITE,
                        dst_access_mask: ash::vk::AccessFlags::SHADER_READ,
                        old_layout: ash::vk::ImageLayout::GENERAL,
                        new_layout: ash::vk::ImageLayout::GENERAL,
                        src_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                        dst_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                        image: self.blurred_image.inner().image.handle(),
                        subresource_range: ash::vk::ImageSubresourceRange {
                            aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                            base_mip_level: mip_level,
                            level_count: 1,
                            base_array_layer: 0,
                            layer_count: 1,
                        },
                        ..Default::default()
                    }],
                );
            }

            ash_device.cmd_bind_pipeline(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.upsample_pipeline.pipeline,
            );
            ash_device.cmd_bind_descriptor_sets(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.upsample_pipeline.pipeline_layout,
                0,
                &[self.upsample_descriptor_set],
                &[],
            );
            for mip_level in (0..(dims.max_mip_levels() - 1)) /*.take(6)*/
                .rev()
            {
                ash_device.cmd_push_constants(
                    ash_command_buffer,
                    self.bloom_pipeline.layout().handle(),
                    ash::vk::ShaderStageFlags::COMPUTE,
                    0,
                    shaders::cs::ty::MipLevel {
                        mip_level: mip_level as i32,
                    }
                    .as_bytes(),
                );
                let mip_dims = dims.mip_level_dimensions(mip_level).unwrap();
                ash_device.cmd_dispatch(
                    ash_command_buffer,
                    (mip_dims.width() as f32 / 16.0).ceil() as u32,
                    (mip_dims.height() as f32 / 16.0).ceil() as u32,
                    1,
                );

                ash_device.cmd_pipeline_barrier(
                    ash_command_buffer,
                    ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                    ash::vk::PipelineStageFlags::FRAGMENT_SHADER,
                    ash::vk::DependencyFlags::empty(),
                    &[],
                    &[],
                    &[ash::vk::ImageMemoryBarrier {
                        src_access_mask: ash::vk::AccessFlags::SHADER_WRITE,
                        dst_access_mask: ash::vk::AccessFlags::SHADER_READ,
                        old_layout: ash::vk::ImageLayout::GENERAL,
                        new_layout: ash::vk::ImageLayout::GENERAL,
                        src_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                        dst_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                        image: self.blurred_image.inner().image.handle(),
                        subresource_range: ash::vk::ImageSubresourceRange {
                            aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                            base_mip_level: mip_level,
                            level_count: 1,
                            base_array_layer: 0,
                            layer_count: 1,
                        },
                        ..Default::default()
                    }],
                );
            }

            //blend
            let [width, height] = self.blend_framebuffer.extent();
            ash_device.cmd_begin_render_pass(
                ash_command_buffer,
                &ash::vk::RenderPassBeginInfo {
                    render_pass: self.blend_render_pass.handle(),
                    framebuffer: self.blend_framebuffer.handle(),
                    render_area: ash::vk::Rect2D {
                        extent: ash::vk::Extent2D { width, height },
                        ..Default::default()
                    },
                    clear_value_count: 0,
                    p_clear_values: std::ptr::null(),
                    ..Default::default()
                },
                ash::vk::SubpassContents::INLINE,
            );

            ash_device.cmd_bind_pipeline(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::GRAPHICS,
                self.blend_pipeline.handle(),
            );
            ash_device.cmd_bind_descriptor_sets(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::GRAPHICS,
                self.blend_pipeline.layout().handle(),
                0,
                &[self.blend_descriptor_set.inner().handle()],
                &[],
            );
            ash_device.cmd_draw(ash_command_buffer, 3, 1, 0, 0);
            ash_device.cmd_end_render_pass(ash_command_buffer);
        }
    }
}

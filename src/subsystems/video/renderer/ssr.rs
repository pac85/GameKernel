use super::camera;
use crate::vulkan::*;
use vulkano::buffer::{BufferContents, BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::{CommandBufferLevel, CommandBufferUsage};
use vulkano::descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::Device;
use vulkano::image::{AttachmentImage, ImageAccess, ImageLayout};
use vulkano::pipeline::{ComputePipeline, Pipeline, PipelineBindPoint};
use vulkano::sampler::{
    Filter, Sampler, SamplerAddressMode, SamplerCreateInfo, SamplerMipmapMode, LOD_CLAMP_NONE,
};
use vulkano::shader::{ShaderModule, ShaderStages};
use vulkano::sync::{AccessFlags, DependencyInfo, MemoryBarrier, PipelineStages, Semaphore};
use vulkano::VulkanObject;

use std::sync::Arc;

pub mod ssr_shader {
    vulkano_shaders::shader! {
        ty: "compute",
        path: "data/shaders/ssr/compute.glsl",
        types_meta: {
            use bytemuck::{Pod, Zeroable};

            #[derive(Clone, Copy, Zeroable, Pod)]
        },
        include: ["data/shaders/include"]
    }

    use super::*;
    pub fn get_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        load(device.clone()).unwrap()
    }
}

use self::ssr_shader::ty::ProjectionData as SsrProjectionData;
use self::ssr_shader::ty::SsrFrameInfo;

pub struct Ssr {
    half_res_specular: [Arc<AttachmentImage>; 2],
    //  screen space reflections
    ssr_pipeline: Arc<ComputePipeline>,
    ssr_descriptor_sets: [Arc<dyn DescriptorSet>; 2],
    ssr_semaphore: Arc<Semaphore>,
    ssr_frame_info_buffer: Arc<CpuAccessibleBuffer<SsrFrameInfo>>,
    ssr_frame_info_descriptor_set: Arc<dyn DescriptorSet>,
}

impl Ssr {
    pub fn new(
        vulkan_common: impl VulkanBasic,
        depth_image: Arc<dyn ImageAccess>,
        norm_rough_image: Arc<AttachmentImage>,
        half_res_specular: [Arc<AttachmentImage>; 2],
        unprocessed_hdr: Arc<AttachmentImage>,
        mesh_index_image: Arc<AttachmentImage>,
        velocity_buffer_image: Arc<AttachmentImage>,
        res_buffer: Arc<DeviceLocalBuffer<super::Res>>,
    ) -> Self {
        let nearest_sampler =
            Sampler::new(vulkan_common.get_device(), SamplerCreateInfo::default()).unwrap();

        let linear_sampler = Sampler::new(
            vulkan_common.get_device(),
            SamplerCreateInfo::simple_repeat_linear_no_mipmap(),
        )
        .unwrap();

        //  screen space reflections
        let ssr_pipeline: Arc<ComputePipeline> = ComputePipeline::new(
            vulkan_common.get_device(),
            ssr_shader::get_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let build_ssr_descriptor_set = |is_first| {
            PersistentDescriptorSet::new(
                vulkan_common.get_descriptor_set_allocator_ref(),
                ssr_pipeline.clone().layout().set_layouts()[0].clone(),
                [
                    WriteDescriptorSet::image_view_sampler(
                        0,
                        norm_rough_image.clone().to_image_view(),
                        linear_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        1,
                        depth_image.clone().to_image_view(),
                        Sampler::new(
                            vulkan_common.get_device(),
                            SamplerCreateInfo {
                                lod: 0.0..=LOD_CLAMP_NONE,
                                ..SamplerCreateInfo::default()
                            },
                        )
                        .unwrap(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        2,
                        unprocessed_hdr.clone().to_image_view(),
                        linear_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        3,
                        mesh_index_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        4,
                        velocity_buffer_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        5,
                        half_res_specular[if is_first { 1 } else { 0 }]
                            .clone()
                            .to_image_view(),
                        linear_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view(
                        6,
                        half_res_specular[if is_first { 0 } else { 1 }]
                            .clone()
                            .to_image_view(),
                    ),
                    WriteDescriptorSet::buffer(7, res_buffer.clone()),
                ],
            )
            .unwrap() as Arc<dyn DescriptorSet>
        };

        let ssr_descriptor_sets = [
            build_ssr_descriptor_set(true),
            build_ssr_descriptor_set(false),
        ];

        let ssr_semaphore = Arc::new(Semaphore::from_pool(vulkan_common.get_device()).unwrap());

        let ssr_frame_info_buffer = CpuAccessibleBuffer::from_data(
            vulkan_common.get_memory_allocator_ref(),
            BufferUsage {
                uniform_buffer: true,
                storage_buffer: true,
                ..BufferUsage::none()
            },
            false,
            SsrFrameInfo { frame_hash: 0f32 },
        )
        .unwrap();

        let ssr_frame_info_descriptor_set = {
            PersistentDescriptorSet::new(
                vulkan_common.get_descriptor_set_allocator_ref(),
                ssr_pipeline.clone().layout().set_layouts()[1].clone(),
                [WriteDescriptorSet::buffer(0, ssr_frame_info_buffer.clone())],
            )
            .unwrap()
        };

        Self {
            half_res_specular,
            //  screen space reflections
            ssr_pipeline,
            ssr_descriptor_sets,
            ssr_semaphore,
            ssr_frame_info_buffer,
            ssr_frame_info_descriptor_set,
        }
    }

    pub fn ssr_cbb(
        &self,
        vulkan_common: impl VulkanBasic,
        lbuilder: &mut LifetimedCommandBufferBuilder,
        half_res: [u32; 2],
        camera: &camera::Camera,
        frames_count: u64,
        current_frame_idx: usize,
    ) {
        let ash_command_buffer = lbuilder.builder.handle();
        let ash_device = vulkan_common.get_ash_device();
        //updates frame seed
        *self.ssr_frame_info_buffer.write().unwrap() = SsrFrameInfo {
            frame_hash: (frames_count % 10000) as f32 / 10000.0,
        };
        unsafe {
            /*let mut lbuilder = LifetimedCommandBufferBuilder::new(
                vulkan_common.get_graphics_command_pool().clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap();*/

            let builder = &mut lbuilder.builder;

            /*let barrier = {
                let mut barrier_builder = UnsafeCommandBufferBuilderPipelineBarrier::new();
                barrier_builder.add_image_memory_barrier(
                    &target,
                    0..1,
                    0..1,
                    PipelineStages {
                        top_of_pipe: true,
                        ..PipelineStages::none()
                    },
                    AccessFlags::none(),
                    PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    false,
                    None,
                    ImageLayout::PresentSrc,
                    ImageLayout::ShaderReadOnlyOptimal,
                );
                barrier_builder
            };
            builder.pipeline_barrier(&barrier);*/

            ash_device.cmd_bind_descriptor_sets(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.ssr_pipeline.layout().handle(),
                0,
                &[
                    self.ssr_descriptor_sets[current_frame_idx].inner().handle(),
                    self.ssr_frame_info_descriptor_set.inner().handle(),
                ],
                &[],
            );
            ash_device.cmd_bind_pipeline(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.ssr_pipeline.handle(),
            );

            let aspect_ratio = vulkan_common.get_config().resolution[0] as f32
                / vulkan_common.get_config().resolution[1] as f32;
            let push_constants_value = SsrProjectionData {
                mv: camera.get_view().into(),
                p: {
                    let mut p = camera.get_proj(aspect_ratio);
                    let v = camera.get_eye().extend(0.0);
                    p[0][3] = v.x;
                    p[1][3] = v.y;
                    p[2][3] = v.z;
                    p.into()
                },
                //view_pos: camera.get_position().into(),
            };
            ash_device.cmd_push_constants(
                ash_command_buffer,
                self.ssr_pipeline.layout().handle(),
                ash::vk::ShaderStageFlags::COMPUTE,
                0,
                push_constants_value.as_bytes(),
            );
            ash_device.cmd_dispatch(
                ash_command_buffer,
                (half_res[0] as f32 / 16 as f32).ceil() as u32,
                (half_res[1] as f32 / 16 as f32).ceil() as u32,
                1,
            );

            ash_device.cmd_pipeline_barrier(
                ash_command_buffer,
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::PipelineStageFlags::ALL_COMMANDS,
                ash::vk::DependencyFlags::empty(),
                &[],
                &[],
                &[ash::vk::ImageMemoryBarrier {
                    src_access_mask: ash::vk::AccessFlags::SHADER_WRITE,
                    dst_access_mask: ash::vk::AccessFlags::SHADER_READ,
                    old_layout: ash::vk::ImageLayout::GENERAL,
                    new_layout: ash::vk::ImageLayout::GENERAL,
                    src_queue_family_index: vulkan_common.get_graphics_queue_family(),
                    dst_queue_family_index: vulkan_common.get_graphics_queue_family(),
                    image: self.half_res_specular[current_frame_idx]
                        .inner()
                        .image
                        .handle(),
                    subresource_range: ash::vk::ImageSubresourceRange {
                        aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    ..Default::default()
                }],
            );
        }
    }
}

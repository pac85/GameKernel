use std::sync::Arc;

use vulkano::buffer::{BufferUsage, DeviceLocalBuffer};
use vulkano::command_buffer::{sys::*, CopyImageInfo};
use vulkano::descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::Device;
use vulkano::format::Format;
use vulkano::image::view::{ImageView, ImageViewCreateInfo};
use vulkano::image::{
    AttachmentImage, ImageAccess, ImageAspects, ImageCreateFlags, ImageDimensions,
    ImageViewAbstract,
};
use vulkano::image::{ImageLayout, ImageSubresourceRange, ImageUsage};
use vulkano::pipeline::{ComputePipeline, Pipeline, PipelineBindPoint};
use vulkano::sampler::{Sampler, SamplerCreateInfo};
use vulkano::shader::{EntryPoint, ShaderModule};
use vulkano::sync::{AccessFlags, DependencyInfo, ImageMemoryBarrier, PipelineStages};
use vulkano::VulkanObject;

use crate::vulkan::storage_image::StorageImage;

use super::super::common::vulkan::*;

pub const DEPTH_BOUNDS_FORMAT: Format = Format::R16G16_UNORM;

mod depth_bounds_reduction_shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/depth_bounds_reduction_pass/compute.glsl"
        }
    }

    use super::*;
    pub fn get_shader<'a>(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }
}

mod depth_mimpmap_reduction_shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/depth_mipmap_reduction/compute.glsl",
            spirv_version: "1.3"
        }
    }

    use super::*;
    pub fn get_shader<'a>(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }
}

#[derive(Clone)]
pub struct DepthBoundsReductor {
    vulkan_common: VulkanCommon,
    tile_size: [u32; 2],

    output: Arc<AttachmentImage>,
    pipeline: Arc<ComputePipeline>,
    descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    //command_buffer: Arc<CommandBuffer<PoolAlloc=StandardCommandPoolAlloc>>,
}

impl DepthBoundsReductor {
    pub fn new(
        vulkan_common: &VulkanCommon,
        tile_size: [u32; 2],
        input: Arc<dyn ImageAccess>,
    ) -> Self {
        let vulkan_common = (*vulkan_common).clone();

        let output = AttachmentImage::with_usage(
            vulkan_common.get_memory_allocator_ref(),
            {
                let [width, height] = vulkan_common.get_config().resolution;
                [
                    (width as f32 / tile_size[0] as f32).ceil() as u32,
                    (height as f32 / tile_size[1] as f32).ceil() as u32,
                ]
            },
            DEPTH_BOUNDS_FORMAT,
            ImageUsage {
                sampled: true,
                storage: true,
                ..ImageUsage::none()
            },
        )
        .unwrap();

        let pipeline = ComputePipeline::new(
            vulkan_common.get_device(),
            depth_bounds_reduction_shaders::get_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let descriptor_set = PersistentDescriptorSet::new(
            vulkan_common.get_descriptor_set_allocator_ref(),
            pipeline.layout().set_layouts()[0].clone(),
            [
                WriteDescriptorSet::image_view_sampler(
                    0,
                    input.to_image_view(),
                    Sampler::new(
                        vulkan_common.get_device(),
                        SamplerCreateInfo::simple_repeat_linear(),
                    )
                    .unwrap(),
                ),
                WriteDescriptorSet::image_view(1, output.clone().to_image_view()),
            ],
        )
        .unwrap();

        /*let command_buffer = Arc::new(AutoCommandBufferBuilder::primary(vulkan_common.get_device(), vulkan_common.get_compute_queue().family()).unwrap()
        .dispatch().unwrap()
        .build().unwrap());*/

        Self {
            vulkan_common,
            tile_size,
            output,
            pipeline,
            descriptor_set,
        }
    }

    pub fn udpate_resolution<I>(&mut self, input: I) -> Option<[u32; 2]>
    where
        I: ImageAccess,
    {
        if self.output.dimensions() != input.dimensions() {
            let res = if let ImageDimensions::Dim2d { width, height, .. } = input.dimensions() {
                [
                    (width as f32 / self.tile_size[0] as f32).ceil() as u32,
                    (height as f32 / self.tile_size[1] as f32).ceil() as u32,
                ]
            } else {
                return None;
            };

            self.output = AttachmentImage::new(
                self.vulkan_common.get_memory_allocator_ref(),
                res,
                DEPTH_BOUNDS_FORMAT,
            )
            .unwrap();
            Some(res)
        } else {
            None
        }
    }

    //resolution of the texture where each textel is a tile
    fn get_resolution(&self) -> [u32; 2] {
        if let ImageDimensions::Dim2d { width, height, .. } = self.output.dimensions() {
            [width, height]
        } else {
            //should never get there
            panic!("")
        }
    }

    pub fn reduce(
        &mut self,
        input: Arc<dyn ImageAccess>,
        builder: &mut UnsafeCommandBufferBuilder,
    ) {
        self.udpate_resolution(&input);
        let res = if let ImageDimensions::Dim2d { width, height, .. } = input.dimensions() {
            [width, height]
        } else {
            return;
        };

        /*let command_buffer = Arc::new({
            let mut builder = AutoCommandBufferBuilder::primary(
                self.vulkan_common.get_device(),
                self.vulkan_common.get_compute_queue().family(),
            )
            .unwrap();
            builder
                .dispatch(
                    [res[0] / 16, res[1] / 16, 1],
                    self.pipeline.clone(),
                    self.descriptor_set.clone(),
                    (),
                )
                .unwrap();
            builder.build().unwrap()
        });*/

        let ash_device = self.vulkan_common.get_ash_device();
        let ash_command_buffer = builder.handle();
        unsafe {
            ash_device.cmd_bind_descriptor_sets(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.pipeline.layout().handle(),
                0,
                &[self.descriptor_set.inner().handle()],
                &[],
            );
            ash_device.cmd_bind_pipeline(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.pipeline.handle(),
            );

            ash_device.cmd_pipeline_barrier(
                ash_command_buffer,
                ash::vk::PipelineStageFlags::ALL_GRAPHICS,
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::DependencyFlags::empty(),
                &[],
                &[],
                &[ash::vk::ImageMemoryBarrier {
                    src_access_mask: ash::vk::AccessFlags::SHADER_READ,
                    dst_access_mask: ash::vk::AccessFlags::SHADER_WRITE,
                    old_layout: ash::vk::ImageLayout::UNDEFINED,
                    new_layout: ash::vk::ImageLayout::GENERAL,
                    src_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    dst_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    image: self.output.inner().image.handle(),
                    subresource_range: ash::vk::ImageSubresourceRange {
                        aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    ..Default::default()
                }],
            );

            let [width, height] = res;
            ash_device.cmd_dispatch(
                ash_command_buffer,
                (width as f32 / self.tile_size[0] as f32).ceil() as u32,
                (height as f32 / self.tile_size[1] as f32).ceil() as u32,
                1,
            );

            ash_device.cmd_pipeline_barrier(
                ash_command_buffer,
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::DependencyFlags::empty(),
                &[],
                &[],
                &[ash::vk::ImageMemoryBarrier {
                    src_access_mask: ash::vk::AccessFlags::SHADER_WRITE,
                    dst_access_mask: ash::vk::AccessFlags::SHADER_READ,
                    old_layout: ash::vk::ImageLayout::GENERAL,
                    new_layout: ash::vk::ImageLayout::GENERAL,
                    src_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    dst_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    image: self.output.inner().image.handle(),
                    subresource_range: ash::vk::ImageSubresourceRange {
                        aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    ..Default::default()
                }],
            );
        }
    }

    pub fn get_output_texture(&self) -> Arc<AttachmentImage> {
        self.output.clone()
    }
}

//compute mipmap gen
#[derive(Clone)]
pub struct DepthMipMapReductor {
    vulkan_common: VulkanCommon,

    src_image: Arc<dyn ImageAccess>,
    pipeline: Arc<ComputePipeline>,
    atomic_buffer:
        Arc<DeviceLocalBuffer</*depth_mimpmap_reduction_shaders::cs::ty::AtomicBuffer*/ u128>>,
    pub dst_image: Arc<dyn ImageAccess>,
    descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    //command_buffer: Arc<CommandBuffer<PoolAlloc=StandardCommandPoolAlloc>>,
}

impl DepthMipMapReductor {
    pub fn new(vulkan_common: &VulkanCommon, src_image: Arc<dyn ImageAccess>) -> Self {
        let vulkan_common = (*vulkan_common).clone();

        let pipeline = ComputePipeline::new(
            vulkan_common.get_device(),
            depth_mimpmap_reduction_shaders::get_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let atomic_buffer = DeviceLocalBuffer::new(
            vulkan_common.get_memory_allocator_ref(),
            BufferUsage {
                storage_buffer: true,
                ..Default::default()
            },
            Some(vulkan_common.get_graphics_queue_family()),
        )
        .unwrap();

        let input_mip_levels = src_image.dimensions().max_mip_levels();
        let dst_image = StorageImage::with_usage(
            vulkan_common.get_memory_allocator_ref(),
            src_image.dimensions(),
            Format::R32_SFLOAT,
            ImageUsage {
                sampled: true,
                storage: true,
                transfer_dst: true,
                ..Default::default()
            },
            ImageCreateFlags {
                ..Default::default()
            },
            [vulkan_common.get_graphics_queue_family()],
            input_mip_levels,
        )
        .unwrap();
        let descriptor_set = PersistentDescriptorSet::new(
            vulkan_common.get_descriptor_set_allocator_ref(),
            pipeline.layout().set_layouts()[0].clone(),
            [
                /*WriteDescriptorSet::image_view_sampler(
                    0,
                    input.clone().to_image_view(),
                    Sampler::new(
                        vulkan_common.get_device(),
                        SamplerCreateInfo::simple_repeat_linear(),
                    )
                    .unwrap(),
                ),*/
                WriteDescriptorSet::image_view_array(
                    0,
                    0,
                    (0..input_mip_levels)
                        //repeat last mip level if there are less than 12
                        .chain(
                            std::iter::repeat(input_mip_levels - 1)
                                .take((12 - input_mip_levels).max(0) as usize),
                        )
                        .map(|mip_level| {
                            ImageView::new(
                                dst_image.clone(),
                                ImageViewCreateInfo {
                                    view_type: vulkano::image::ImageViewType::Dim2d,
                                    format: Some(dst_image.format()),
                                    subresource_range: ImageSubresourceRange {
                                        aspects: ImageAspects {
                                            color: true,
                                            ..Default::default()
                                        },
                                        mip_levels: mip_level..mip_level + 1,
                                        array_layers: 0..1,
                                    },
                                    usage: *dst_image.usage(),
                                    ..Default::default()
                                },
                            )
                            .unwrap() as Arc<dyn ImageViewAbstract>
                        }),
                ),
                WriteDescriptorSet::buffer(1, atomic_buffer.clone()),
            ],
        )
        .unwrap();

        /*let command_buffer = Arc::new(AutoCommandBufferBuilder::primary(vulkan_common.get_device(), vulkan_common.get_compute_queue().family()).unwrap()
        .dispatch().unwrap()
        .build().unwrap());*/

        Self {
            vulkan_common,

            src_image,
            dst_image,
            pipeline,
            atomic_buffer,
            descriptor_set,
        }
    }

    pub fn reduce(&mut self, builder: &mut UnsafeCommandBufferBuilder) {
        let res = if let ImageDimensions::Dim2d { width, height, .. } = self.src_image.dimensions()
        {
            [width, height]
        } else {
            return;
        };

        let ash_device = self.vulkan_common.get_ash_device();
        let ash_command_buffer = builder.handle();
        unsafe {
            let [width, height] = self.src_image.dimensions().width_height();
            ash_device.cmd_copy_image(
                ash_command_buffer,
                self.src_image.inner().image.handle(),
                ash::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                self.dst_image.inner().image.handle(),
                ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                &[ash::vk::ImageCopy {
                    src_subresource: ash::vk::ImageSubresourceLayers {
                        aspect_mask: ash::vk::ImageAspectFlags::DEPTH,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    src_offset: ash::vk::Offset3D { x: 0, y: 0, z: 0 },
                    dst_subresource: ash::vk::ImageSubresourceLayers {
                        aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    dst_offset: ash::vk::Offset3D { x: 0, y: 0, z: 0 },
                    extent: ash::vk::Extent3D {
                        width,
                        height,
                        depth: 1,
                    },
                }],
            );
            ash_device.cmd_pipeline_barrier(
                ash_command_buffer,
                ash::vk::PipelineStageFlags::TRANSFER,
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::DependencyFlags::empty(),
                &[],
                &[],
                &[ash::vk::ImageMemoryBarrier {
                    src_access_mask: ash::vk::AccessFlags::TRANSFER_WRITE,
                    dst_access_mask: ash::vk::AccessFlags::SHADER_READ,
                    old_layout: ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                    new_layout: ash::vk::ImageLayout::GENERAL,
                    src_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    dst_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    image: self.dst_image.inner().image.handle(),
                    subresource_range: ash::vk::ImageSubresourceRange {
                        aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    ..Default::default()
                }],
            );

            ash_device.cmd_bind_descriptor_sets(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.pipeline.layout().handle(),
                0,
                &[self.descriptor_set.inner().handle()],
                &[],
            );
            ash_device.cmd_bind_pipeline(
                ash_command_buffer,
                ash::vk::PipelineBindPoint::COMPUTE,
                self.pipeline.handle(),
            );

            let [width, height] = res;
            ash_device.cmd_dispatch(
                ash_command_buffer,
                (width as f32 / 64 as f32).ceil() as u32,
                (height as f32 / 64 as f32).ceil() as u32,
                1,
            );

            ash_device.cmd_pipeline_barrier(
                ash_command_buffer,
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::PipelineStageFlags::COMPUTE_SHADER,
                ash::vk::DependencyFlags::empty(),
                &[],
                &[],
                &[ash::vk::ImageMemoryBarrier {
                    src_access_mask: ash::vk::AccessFlags::SHADER_WRITE,
                    dst_access_mask: ash::vk::AccessFlags::SHADER_READ,
                    old_layout: ash::vk::ImageLayout::GENERAL,
                    new_layout: ash::vk::ImageLayout::GENERAL,
                    src_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    dst_queue_family_index: self.vulkan_common.get_graphics_queue_family(),
                    image: self.dst_image.inner().image.handle(),
                    subresource_range: ash::vk::ImageSubresourceRange {
                        aspect_mask: ash::vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: self.dst_image.mip_levels(),
                        base_array_layer: 0,
                        layer_count: 1,
                    },
                    ..Default::default()
                }],
            );
        }
    }
}

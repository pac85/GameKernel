use super::{material::Material, mesh::Mesh};
use cgmath;
use std::sync::Arc;

#[derive(Clone)]
pub struct Model {
    pub material: Arc<Material>,
    pub mesh: Arc<Mesh>,
    pub transform: cgmath::Matrix4<f32>,
}

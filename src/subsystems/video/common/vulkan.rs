pub mod command_buffer;
pub mod descriptor_set;
pub mod pipeline;

use gltf::buffer::Target;
use vulkano;
use vulkano::buffer::BufferAccess;
use vulkano::buffer::BufferSlice;
use vulkano::command_buffer::allocator::CommandBufferAllocator;
use vulkano::command_buffer::allocator::CommandBufferBuilderAlloc;
use vulkano::command_buffer::allocator::StandardCommandBufferAlloc;
use vulkano::command_buffer::allocator::StandardCommandBufferAllocator;
use vulkano::command_buffer::allocator::StandardCommandBufferBuilderAlloc;
use vulkano::command_buffer::pool::CommandBufferAllocateInfo;
use vulkano::command_buffer::pool::CommandPoolAlloc;
use vulkano::command_buffer::pool::CommandPoolCreateInfo;
use vulkano::command_buffer::sys::CommandBufferBeginInfo;
use vulkano::command_buffer::CopyBufferInfo;
use vulkano::command_buffer::PrimaryCommandBufferAbstract;
use vulkano::descriptor_set::allocator::StandardDescriptorSetAllocator;
use vulkano::device::DeviceCreateInfo;
use vulkano::device::QueueCreateInfo;
use vulkano::image::view::ImageViewCreateInfo;
use vulkano::image::ImageAspects;
use vulkano::image::ImageSubresourceRange;
use vulkano::instance::InstanceCreateInfo;
use vulkano::memory::allocator::AllocationCreationError;
use vulkano::memory::allocator::StandardMemoryAllocator;
use vulkano::memory::DeviceMemoryError;
use vulkano::swapchain::SwapchainCreateInfo;
use vulkano::VulkanLibrary;
use vulkano::VulkanObject;
use winit;

use crate::log_msg;
use crate::log_warn;

use self::winit::window::Window;

use self::vulkano::device::{physical::PhysicalDevice, Device, DeviceExtensions, Queue};
use self::vulkano::image::{ImageUsage, SwapchainImage};
use self::vulkano::instance::{Instance, InstanceExtensions, Version};
use self::vulkano::swapchain;
use self::vulkano::swapchain::{
    AcquireError, PresentMode, Surface, Swapchain, SwapchainAcquireFuture,
};
use vulkano::format::Format;
use vulkano::impl_vertex;

use bytemuck::{Pod, Zeroable};

use serde::Deserialize;

use std::collections::HashMap;
use std::sync::Arc;

const DESIDERED_FORMAT: Format = Format::B8G8R8A8_SRGB;

//hack, UnsafeCommandPoolCreateInfo is erroneusly not exposed
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash)]
pub struct NonExhaustive(());
#[derive(Clone, Debug)]
pub struct UnsafeCommandPoolCreateInfo {
    pub queue_family_index: u32,
    pub transient: bool,
    pub reset_command_buffer: bool,
    pub _ne: NonExhaustive,
}

impl Default for UnsafeCommandPoolCreateInfo {
    #[inline]
    fn default() -> Self {
        Self {
            queue_family_index: u32::MAX,
            transient: false,
            reset_command_buffer: false,
            _ne: NonExhaustive(()),
        }
    }
}

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Vertex2 {
    position: [f32; 2],
}

impl_vertex!(Vertex2, position);

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Vertex3 {
    position: [f32; 3],
}

impl_vertex!(Vertex3, position);

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Vertex4 {
    position: [f32; 4],
}

impl_vertex!(Vertex4, position);

#[derive(Clone, Deserialize)]
pub struct VulkanConfig {
    device_index: usize,
    device_name: String,

    pub resolution: [u32; 2],
}

pub trait VulkanBasic: Clone {
    fn get_config(&self) -> &VulkanConfig;
    fn get_instance(&self) -> Arc<Instance>;
    fn get_device(&self) -> Arc<Device>;
    fn get_ash_device(&self) -> ash::Device;
    fn get_ash_descriptor_pools(&self) -> &descriptor_set::DescriptorPools;
    fn get_memory_allocator(&self) -> Arc<StandardMemoryAllocator>;
    fn get_memory_allocator_ref(&self) -> &Arc<StandardMemoryAllocator>;
    fn get_descriptor_set_allocator(&self) -> Arc<StandardDescriptorSetAllocator>;
    fn get_descriptor_set_allocator_ref(&self) -> &Arc<StandardDescriptorSetAllocator>;
    fn get_command_buffer_allocator(&self) -> Arc<StandardCommandBufferAllocator>;
    fn get_command_buffer_allocator_ref(&self) -> &Arc<StandardCommandBufferAllocator>;
    fn get_graphics_queue(&self) -> Arc<Queue>;
    fn get_transfers_queue(&self) -> Arc<Queue>;
    fn get_compute_queue(&self) -> Arc<Queue>;
    fn get_graphics_queue_family(&self) -> u32;
    fn get_compute_queue_family(&self) -> u32;
    fn get_transfer_queue_family(&self) -> u32;
    fn get_graphics_command_pool(&self) -> Arc<CommandPool>;
    fn get_transfer_command_pool(&self) -> Arc<CommandPool>;
    fn get_compute_command_pool(&self) -> Arc<CommandPool>;
}

/*pub trait AbstractVulkanCommon: VulkanBasic {
    fn get_swapchain(&self) -> Arc<Swapchain<W>>;
    fn get_swapchain_images(&self) -> Vec<Arc<SwapchainImage<W>>>;
    fn update_resolution_from_window(&mut self, window: &Window) -> Option<&VulkanConfig>;
    fn update_resolution(&mut self, res: (u32, u32)) -> Option<&VulkanConfig>;
    fn get_next_swapchain_image_index(&self) -> Option<(SwapchainAcquireFuture<W>, usize)>;
    fn get_next_swapchain_image(
        &self,
    ) -> Option<(SwapchainAcquireFuture<W>, Arc<SwapchainImage<W>>)>;
}*/

pub struct SwapChain {
    pub swapchain: Arc<Swapchain>,
    pub swapchain_images: Vec<Arc<SwapchainImage>>,
    pub surface: Arc<Surface>,
    pub swapchain_format: Format,
}

impl Clone for SwapChain {
    fn clone(&self) -> Self {
        Self {
            swapchain: self.swapchain.clone(),
            swapchain_images: self.swapchain_images.clone(),
            surface: self.surface.clone(),
            swapchain_format: self.swapchain_format,
        }
    }
}

impl SwapChain {
    pub fn new(vk: VulkanCommon, surface: Arc<Surface>) -> Self {
        let format;
        let (swapchain, images) = {
            let caps = vk
                .device
                .physical_device()
                .surface_capabilities(&surface, Default::default())
                .unwrap();

            let usage = ImageUsage {
                color_attachment: true,
                ..ImageUsage::none()
            };

            let alpha = caps.supported_composite_alpha.iter().next().unwrap();

            let supported_formats = vk
                .device
                .physical_device()
                .surface_formats(&surface, Default::default())
                .unwrap();

            format = supported_formats
                .iter()
                .find(|f| f.0 == DESIDERED_FORMAT)
                .unwrap_or_else(|| {
                    log_warn!(
                        "format {:?} not available, output may look wrong",
                        DESIDERED_FORMAT
                    );
                    &supported_formats[0]
                })
                .0;
            log_msg!("choosing format {:?}", format);

            Swapchain::new(
                vk.device.clone(),
                surface.clone(),
                SwapchainCreateInfo {
                    min_image_count: caps.min_image_count,
                    image_format: Some(format),
                    image_extent: vk.config.resolution,
                    image_usage: usage,
                    composite_alpha: alpha,
                    present_mode: PresentMode::Immediate,
                    ..Default::default()
                },
            )
            /*.num_images(caps.min_image_count)
            .format(format)
            .dimensions(vk.config.resolution)
            .usage(usage)
            .sharing_mode(&vk.graphics_queue)
            .present_mode(PresentMode::Immediate)
            .build()*/
            .unwrap()
        };

        Self {
            swapchain,
            swapchain_images: images,
            surface,
            swapchain_format: format,
        }
    }

    pub fn get_swapchain(&self) -> Arc<Swapchain> {
        self.swapchain.clone()
    }

    pub fn get_swapchain_images(&self) -> Vec<Arc<SwapchainImage>> {
        self.swapchain_images.clone()
    }
}

//#[derive(Clone)]
pub struct VulkanCommon {
    pub instance: Arc<Instance>,
    pub device: Arc<Device>,
    ash_device: ash::Device,
    ash_descriptor_pools: descriptor_set::DescriptorPools,
    pub physical_device: Arc<PhysicalDevice>,
    pub memory_allocator: Arc<StandardMemoryAllocator>,
    pub descriptor_set_allocator: Arc<StandardDescriptorSetAllocator>,
    pub command_buffer_allocator: Arc<StandardCommandBufferAllocator>,
    pub graphics_queue: Arc<Queue>,
    pub transfer_queue: Arc<Queue>,
    pub compute_queue: Arc<Queue>,
    pub graphics_queue_family_id: u32,
    pub compute_queue_family_id: u32,
    pub transfer_queue_family_id: u32,
    pub graphics_command_pool: Arc<CommandPool>,
    pub transfer_command_pool: Arc<CommandPool>,
    pub compute_command_pool: Arc<CommandPool>,

    pub config: VulkanConfig,
}

impl Clone for VulkanCommon {
    fn clone(&self) -> Self {
        Self {
            instance: self.instance.clone(),
            device: self.device.clone(),
            ash_device: self.ash_device.clone(),
            ash_descriptor_pools: self.ash_descriptor_pools.clone(),
            physical_device: self.physical_device.clone(),
            memory_allocator: self.memory_allocator.clone(),
            descriptor_set_allocator: self.descriptor_set_allocator.clone(),
            command_buffer_allocator: self.command_buffer_allocator.clone(),
            graphics_queue: self.graphics_queue.clone(),
            transfer_queue: self.transfer_queue.clone(),
            compute_queue: self.compute_queue.clone(),
            graphics_queue_family_id: self.graphics_queue_family_id,
            compute_queue_family_id: self.compute_queue_family_id,
            transfer_queue_family_id: self.transfer_queue_family_id,
            graphics_command_pool: self.graphics_command_pool.clone(),
            transfer_command_pool: self.transfer_command_pool.clone(),
            compute_command_pool: self.compute_command_pool.clone(),
            config: self.config.clone(),
        }
    }
}

impl VulkanBasic for VulkanCommon {
    fn get_config(&self) -> &VulkanConfig {
        &self.config
    }

    fn get_instance(&self) -> Arc<Instance> {
        self.instance.clone()
    }

    fn get_device(&self) -> Arc<Device> {
        self.device.clone()
    }

    fn get_ash_device(&self) -> ash::Device {
        self.ash_device.clone()
    }

    fn get_ash_descriptor_pools(&self) -> &descriptor_set::DescriptorPools {
        &self.ash_descriptor_pools
    }

    fn get_memory_allocator_ref(&self) -> &Arc<StandardMemoryAllocator> {
        &self.memory_allocator
    }

    fn get_memory_allocator(&self) -> Arc<StandardMemoryAllocator> {
        self.memory_allocator.clone()
    }

    fn get_descriptor_set_allocator(&self) -> Arc<StandardDescriptorSetAllocator> {
        self.descriptor_set_allocator.clone()
    }

    fn get_descriptor_set_allocator_ref(&self) -> &Arc<StandardDescriptorSetAllocator> {
        &self.descriptor_set_allocator
    }

    fn get_command_buffer_allocator(&self) -> Arc<StandardCommandBufferAllocator> {
        self.command_buffer_allocator.clone()
    }

    fn get_command_buffer_allocator_ref(&self) -> &Arc<StandardCommandBufferAllocator> {
        &self.command_buffer_allocator
    }

    fn get_graphics_queue(&self) -> Arc<Queue> {
        self.graphics_queue.clone()
    }

    fn get_transfers_queue(&self) -> Arc<Queue> {
        self.transfer_queue.clone()
    }

    fn get_compute_queue(&self) -> Arc<Queue> {
        self.compute_queue.clone()
    }

    fn get_graphics_queue_family(&self) -> u32 {
        self.graphics_queue.queue_family_index()
    }

    fn get_transfer_queue_family(&self) -> u32 {
        self.transfer_queue.queue_family_index()
    }

    fn get_compute_queue_family(&self) -> u32 {
        self.compute_queue.queue_family_index()
    }

    fn get_graphics_command_pool(&self) -> Arc<CommandPool> {
        self.graphics_command_pool.clone()
    }

    fn get_transfer_command_pool(&self) -> Arc<CommandPool> {
        self.transfer_command_pool.clone()
    }

    fn get_compute_command_pool(&self) -> Arc<CommandPool> {
        self.compute_command_pool.clone()
    }
}

/*impl AbstractVulkanCommon for VulkanCommon {
    fn get_swapchain(&self) -> Arc<Swapchain<W>> {
        self.swapchain.clone()
    }

    fn get_swapchain_images(&self) -> Vec<Arc<SwapchainImage<W>>> {
        self.swapchain_images.clone()
    }

    fn update_resolution_from_window(&mut self, window: &Window) -> Option<&VulkanConfig> {
        self.config.resolution = {
            let resolution: (u32, u32) = window.inner_size().into();
            [resolution.0, resolution.1]
        };
        Some(self.get_config())
    }

    fn update_resolution(&mut self, res: (u32, u32)) -> Option<&VulkanConfig> {
        self.config.resolution = {
            let resolution: (u32, u32) = res;
            [resolution.0, resolution.1]
        };
        Some(self.get_config())
    }

    fn get_next_swapchain_image_index(&self) -> Option<(SwapchainAcquireFuture<W>, usize)> {
        match swapchain::acquire_next_image(self.get_swapchain(), None) {
            Ok((image_index, _, acquire_future)) => Some((acquire_future, image_index)),
            Err(AcquireError::OutOfDate) => None,
            Err(err) =>
            /*TODO proper handling*/
            {
                panic!("Error while acquiring swapchain image: {:?}", err)
            }
        }
    }

    fn get_next_swapchain_image(
        &self,
    ) -> Option<(SwapchainAcquireFuture<W>, Arc<SwapchainImage<W>>)> {
        match swapchain::acquire_next_image(self.get_swapchain(), None) {
            Ok((image_index, _, acquire_future)) => {
                Some((acquire_future, self.swapchain_images[image_index].clone()))
            }
            Err(AcquireError::OutOfDate) => None,
            Err(err) =>
            /*TODO proper handling*/
            {
                panic!("Error while acquiring swapchain image: {:?}", err)
            }
        }
    }
}*/

trait DeviceName {
    fn name(&self) -> String;
}

impl DeviceName for PhysicalDevice {
    fn name(&self) -> String {
        self.properties().device_name.clone()
    }
}

impl VulkanCommon {
    pub fn new(instance: Arc<Instance>, config: &mut VulkanConfig, surface: Arc<Surface>) -> Self
//where Ext: Into<RawInstanceExtensions>,
    {
        /*let mut physical_device =
            PhysicalDevice::from_index(&instance, config.device_index).unwrap();
        if physical_device.name() != config.device_name {
            //if an hw change is
            physical_device = instance.enumerate_physical_devices().unwrap().next().unwrap();
            config.device_index = physical_device.index();
            config.device_name = physical_device.name();
        }*/

        let physical_device = instance
            .enumerate_physical_devices()
            .unwrap()
            .next()
            .unwrap();
        config.device_name = physical_device.name();
        log_msg!("using physical device {}", physical_device.name());

        for (id, family) in physical_device.queue_family_properties().iter().enumerate() {
            log_msg!(
                "family {} supports graphics:{} transfers:{} compute:{} and max {}",
                id,
                family.queue_flags.graphics,
                family.queue_flags.transfer,
                family.queue_flags.compute,
                family.queue_count
            );
        }

        //contains remaining queues for used queue family
        let mut remaining_queues = HashMap::new();

        //finds queue families
        let (graphics_queue_family_id, graphics_queue_family_props) = physical_device
            .queue_family_properties()
            .iter()
            .enumerate()
            .find(|(id, queue)| {
                queue.queue_flags.graphics
                    && physical_device
                        .surface_support(*id as u32, &surface)
                        .unwrap_or(false)
            })
            .unwrap();

        remaining_queues.insert(
            graphics_queue_family_id,
            graphics_queue_family_props.queue_count - 1,
        );

        let transfer_queue_family = physical_device
            .queue_family_properties()
            .iter()
            .enumerate()
            .find(|(id, queue)| {
                queue.queue_flags.transfer
                    && !queue.queue_flags.graphics
                    && remaining_queues.get(id).cloned().unwrap_or(1) > 0
            })
            .map(|x| Some(x))
            .unwrap_or(
                physical_device
                    .queue_family_properties()
                    .iter()
                    .enumerate()
                    .find(|(id, queue)| {
                        queue.queue_flags.transfer
                            && remaining_queues.get(id).cloned().unwrap_or(1) > 0
                    }),
            );

        if let Some((transfer_queue_family_id, transfer_queue_family_props)) = transfer_queue_family
        {
            remaining_queues.insert(
                transfer_queue_family_id,
                transfer_queue_family_props.queue_count - 1,
            );
        }

        let compute_queue_family = physical_device
            .queue_family_properties()
            .iter()
            .enumerate()
            .find(|(id, queue)| {
                queue.queue_flags.compute
                    && !queue.queue_flags.graphics
                    && remaining_queues.get(id).cloned().unwrap_or(1) > 0
            })
            .map(|x| Some(x))
            .unwrap_or(
                physical_device
                    .queue_family_properties()
                    .iter()
                    .enumerate()
                    .find(|(id, queue)| {
                        queue.queue_flags.compute
                            && remaining_queues.get(id).cloned().unwrap_or(1) > 0
                    }),
            );

        if let Some((compute_queue_family_id, compute_queue_family_props)) = compute_queue_family {
            remaining_queues.insert(
                compute_queue_family_id,
                compute_queue_family_props.queue_count - 1,
            );
        }

        let device_ext = DeviceExtensions {
            khr_swapchain: true,
            khr_storage_buffer_storage_class: true,
            ..DeviceExtensions::none()
        };
        let (device, mut queues) =
            if let (Some((transfer_queue_family_id, _)), Some((compute_queue_family_id, _))) =
                (transfer_queue_family, compute_queue_family)
            {
                log_msg!(
                "graphics_queue_family: {}, compute_queue_family: {}, transfer_queue_family: {}",
                graphics_queue_family_id,
                compute_queue_family_id,
                transfer_queue_family_id
            );

                let mut queues = vec![
                    graphics_queue_family_id,
                    transfer_queue_family_id,
                    compute_queue_family_id,
                ];
                queues.sort();
                queues.dedup();

                Device::new(
                    physical_device.clone(),
                    DeviceCreateInfo {
                        enabled_extensions: device_ext,
                        enabled_features: physical_device.supported_features().clone(),
                        queue_create_infos: queues
                            .iter()
                            .map(|qf| QueueCreateInfo {
                                queue_family_index: *qf as u32,
                                ..Default::default()
                            })
                            .collect(),

                        ..Default::default()
                    },
                )
                .unwrap()
            } else {
                log_msg!("using single family: {}", graphics_queue_family_id,);

                Device::new(
                    physical_device.clone(),
                    DeviceCreateInfo {
                        enabled_extensions: device_ext,
                        enabled_features: physical_device.supported_features().clone(),
                        queue_create_infos: vec![QueueCreateInfo {
                            queue_family_index: graphics_queue_family_id as u32,
                            ..Default::default()
                        }],

                        ..Default::default()
                    },
                )
                .unwrap()
            };

        //TODO: proper handling
        let graphics_queue = queues
            .find(|q| q.queue_family_index() == graphics_queue_family_id as u32)
            .unwrap();
        let transfer_queue = if let Some((transfer_queue_family_id, _)) = transfer_queue_family {
            queues
                .find(|q| q.queue_family_index() == transfer_queue_family_id as u32)
                .unwrap_or(graphics_queue.clone())
        } else {
            graphics_queue.clone()
        };
        let transfer_queue_family_id = transfer_queue_family
            .map(|(id, _)| id)
            .unwrap_or(graphics_queue_family_id);
        let compute_queue = if let Some((compute_queue_family_id, _)) = compute_queue_family {
            queues
                .find(|q| q.queue_family_index() == compute_queue_family_id as u32)
                .unwrap_or(graphics_queue.clone())
        } else {
            graphics_queue.clone()
        };
        let compute_queue_family = compute_queue_family
            .map(|(id, _)| id)
            .unwrap_or(graphics_queue_family_id);

        let graphics_command_pool = Arc::new(
            CommandPool::new(
                device.clone(),
                CommandPoolCreateInfo {
                    queue_family_index: graphics_queue.queue_family_index(),
                    ..Default::default()
                },
            )
            .unwrap(),
        );

        let transfer_command_pool = Arc::new(
            CommandPool::new(
                device.clone(),
                CommandPoolCreateInfo {
                    queue_family_index: transfer_queue.queue_family_index(),
                    ..Default::default()
                },
            )
            .unwrap(),
        );

        let compute_command_pool = Arc::new(
            CommandPool::new(
                device.clone(),
                CommandPoolCreateInfo {
                    queue_family_index: compute_queue.queue_family_index(),
                    ..Default::default()
                },
            )
            .unwrap(),
        );

        let memory_allocator = Arc::new(StandardMemoryAllocator::new_default(device.clone()));
        let descriptor_set_allocator =
            Arc::new(StandardDescriptorSetAllocator::new(device.clone()));
        let command_buffer_allocator = Arc::new(StandardCommandBufferAllocator::new(
            device.clone(),
            Default::default(),
        ));

        let ash_device: ash::Device = {
            struct AshDevice {
                handle: ash::vk::Device,

                device_fn_1_0: ash::vk::DeviceFnV1_0,
                device_fn_1_1: ash::vk::DeviceFnV1_1,
                device_fn_1_2: ash::vk::DeviceFnV1_2,
                device_fn_1_3: ash::vk::DeviceFnV1_3,
            }

            let dev = AshDevice {
                handle: device.handle(),

                device_fn_1_0: device.fns().v1_0.clone(),
                device_fn_1_1: device.fns().v1_1.clone(),
                device_fn_1_2: device.fns().v1_2.clone(),
                device_fn_1_3: device.fns().v1_3.clone(),
            };

            unsafe { std::mem::transmute(dev) }
        };

        let ash_descriptor_pools = descriptor_set::DescriptorPools::new(ash_device.clone());

        VulkanCommon {
            instance: instance.clone(),
            device,
            ash_device,
            ash_descriptor_pools,
            physical_device,
            memory_allocator,
            descriptor_set_allocator,
            command_buffer_allocator,
            graphics_queue,
            transfer_queue,
            compute_queue,
            graphics_queue_family_id: graphics_queue_family_id as u32,
            compute_queue_family_id: graphics_queue_family_id as u32, //compute_queue_family.id(),
            transfer_queue_family_id: graphics_queue_family_id as u32, //compute_queue_family.id(),
            graphics_command_pool,
            transfer_command_pool,
            compute_command_pool,
            config: config.clone(),
        }
    }

    pub fn get_instance() -> Arc<Instance> {
        let library = VulkanLibrary::new().unwrap();
        let extensions = vulkano_win::required_extensions(&library);

        let should_validate = std::env::var("GK_VULKAN_VALIDATION")
            .map(|v| v.trim() == "1")
            .unwrap_or(false);
        library
            .layer_properties()
            .unwrap()
            .for_each(|l| crate::log_msg!("available layer {}", l.name()));
        let layers: Vec<_> = library
            .layer_properties()
            .unwrap()
            .filter(|l| l.name().contains("validation") && should_validate)
            .collect();
        layers
            .iter()
            .for_each(|l| crate::log_msg!("using layer {}", l.name()));
        Instance::new(
            library,
            InstanceCreateInfo {
                max_api_version: Some(Version::V1_3),
                enabled_extensions: extensions,
                enabled_layers: layers.into_iter().map(|s| s.name().to_owned()).collect(),
                ..Default::default()
            },
        )
        .unwrap()
    }
}

use vulkano::image::view::ImageView;
use vulkano::image::ImageAccess;

pub trait ToImageView: ImageAccess + Sized {
    fn to_image_view(self) -> Arc<ImageView<Self>> {
        ImageView::new_default(Arc::new(self)).unwrap()
    }

    fn to_image_view_nomips(self) -> Arc<ImageView<Self>> {
        let format = self.format();
        let usage = *self.usage();
        ImageView::new(
            Arc::new(self),
            ImageViewCreateInfo {
                view_type: vulkano::image::ImageViewType::Dim2d,
                format: Some(format),
                subresource_range: ImageSubresourceRange {
                    aspects: ImageAspects {
                        depth: true,
                        ..Default::default()
                    },
                    mip_levels: 0..1,
                    array_layers: 0..1,
                },
                usage,
                ..Default::default()
            },
        )
        .unwrap()
    }
}

impl<T> ToImageView for T where T: ImageAccess + Sized {}

use vulkano::command_buffer::pool::CommandPool;
use vulkano::command_buffer::sys::{UnsafeCommandBuffer, UnsafeCommandBufferBuilder};
use vulkano::command_buffer::{CommandBufferLevel, CommandBufferUsage};
use vulkano::OomError;

pub struct LifetimedCommandBufferBuilder {
    pub builder: UnsafeCommandBufferBuilder,
    alloc: StandardCommandBufferBuilderAlloc,
}

impl LifetimedCommandBufferBuilder {
    pub unsafe fn new(
        pool: &StandardCommandBufferAllocator,
        queue_family_index: u32,
        level: CommandBufferLevel,
        usage: CommandBufferUsage,
    ) -> Result<Self, OomError> {
        let alloc = pool
            .allocate(queue_family_index, level, 1)
            .unwrap()
            .next()
            .unwrap();

        let builder = UnsafeCommandBufferBuilder::new(
            &alloc.inner(),
            CommandBufferBeginInfo {
                usage,
                ..Default::default()
            },
        )?;

        Ok(Self { builder, alloc })
    }

    pub fn build(self) -> Result<LifetimedCommandBuffer, OomError> {
        Ok(LifetimedCommandBuffer {
            cb: self.builder.build()?,
            alloc: Some(self.alloc.into_alloc()),
        })
    }
}

pub struct LifetimedCommandBuffer {
    pub cb: UnsafeCommandBuffer,
    alloc: Option<StandardCommandBufferAlloc>,
}

use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::sync::GpuFuture;

pub fn write_device_buffer<T, V: VulkanBasic>(
    vk: V,
    data: T,
    device_buffer: Arc<DeviceLocalBuffer<T>>,
) -> Result<(), AllocationCreationError>
where
    T: Send + Sync + Copy + Pod + 'static,
{
    let staging = CpuAccessibleBuffer::from_data(
        vk.get_memory_allocator_ref(),
        BufferUsage {
            transfer_src: true,
            ..BufferUsage::none()
        },
        false,
        data,
    )?;

    let mut copy_cbb = AutoCommandBufferBuilder::primary(
        vk.get_command_buffer_allocator_ref(),
        vk.get_transfer_queue_family(),
        CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap();

    copy_cbb
        .copy_buffer(CopyBufferInfo::buffers(staging, device_buffer.clone()))
        .unwrap();

    let copy_cb = copy_cbb.build().unwrap();

    copy_cb
        .execute(vk.get_transfers_queue())
        .unwrap()
        .flush()
        .unwrap();

    Ok(())
}

pub fn create_filled_device_buffer<'a, T, V: VulkanBasic, I>(
    vk: V,
    usage: BufferUsage,
    families: I,
    data: T,
) -> Result<Arc<DeviceLocalBuffer<T>>, AllocationCreationError>
where
    I: IntoIterator<Item = u32>,
    T: Send + Sync + Copy + Pod + 'static,
{
    //make sure transfers queue is always present
    let families = {
        let mut families: Vec<_> = families.into_iter().collect();
        let already_has_transfer = families
            .iter()
            .any(|q| *q == vk.get_transfer_queue_family());

        if !already_has_transfer {
            families.push(vk.get_transfer_queue_family());
        }

        families.into_iter()
    };

    let device_buffer = DeviceLocalBuffer::new(
        vk.get_memory_allocator_ref(),
        BufferUsage {
            transfer_dst: true,
            ..usage
        },
        families,
    )?;

    write_device_buffer(vk, data, device_buffer.clone());

    Ok(device_buffer)
}

pub fn write_device_slice<T, I, V: VulkanBasic>(
    vk: V,
    data: I,
    device_buffer: Arc<BufferSlice<[T], DeviceLocalBuffer<[T]>>>,
) -> Result<(), AllocationCreationError>
where
    T: Send + Sync + Copy + Pod + 'static,
    I: IntoIterator<Item = T>,
    I::IntoIter: ExactSizeIterator,
{
    let staging = CpuAccessibleBuffer::from_iter(
        vk.get_memory_allocator_ref(),
        BufferUsage {
            transfer_src: true,
            ..BufferUsage::none()
        },
        false,
        data,
    )?;

    let mut copy_cbb = AutoCommandBufferBuilder::primary(
        vk.get_command_buffer_allocator_ref(),
        vk.get_transfer_queue_family(),
        CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap();

    copy_cbb
        .copy_buffer(CopyBufferInfo::buffers(staging, device_buffer.clone()))
        .unwrap();

    let copy_cb = copy_cbb.build().unwrap();

    copy_cb
        .execute(vk.get_transfers_queue())
        .unwrap()
        .flush()
        .unwrap();

    Ok(())
}

pub fn write_device_array<T, I, V: VulkanBasic>(
    vk: V,
    data: I,
    device_buffer: Arc<DeviceLocalBuffer<[T]>>,
) -> Result<(), AllocationCreationError>
where
    T: Send + Sync + Copy + Pod + 'static,
    I: IntoIterator<Item = T>,
    I::IntoIter: ExactSizeIterator,
{
    write_device_slice(vk, data, device_buffer.into_buffer_slice())
}

pub fn create_filled_device_array<'a, T, I, V: VulkanBasic, F>(
    vk: V,
    usage: BufferUsage,
    families: F,
    data: I,
) -> Result<Arc<DeviceLocalBuffer<[T]>>, AllocationCreationError>
where
    F: IntoIterator<Item = u32>,
    T: Send + Sync + Copy + Pod + 'static,
    I: IntoIterator<Item = T>,
    I::IntoIter: ExactSizeIterator,
{
    //make sure transfers queue is always present
    let families = {
        let mut families: Vec<_> = families.into_iter().collect();
        let already_has_transfer = families
            .iter()
            .any(|q| *q == vk.get_transfer_queue_family());

        if !already_has_transfer {
            families.push(vk.get_transfer_queue_family());
        }

        families.into_iter()
    };

    //TODO: avoid copying
    let data: Vec<_> = data.into_iter().collect();
    let device_buffer = DeviceLocalBuffer::array(
        &vk.get_memory_allocator(),
        data.len() as u64,
        BufferUsage {
            transfer_dst: true,
            ..usage
        },
        families,
    )?;

    write_device_array(vk, data, device_buffer.clone());

    Ok(device_buffer)
}

//this is temorary
pub mod storage_image {
    use smallvec::SmallVec;
    use std::{
        fs::File,
        hash::{Hash, Hasher},
        sync::Arc,
    };
    use vulkano::image::{
        sys::{Image, ImageMemory, RawImage},
        traits::ImageContent,
        ImageAccess, ImageCreateFlags, ImageDescriptorLayouts, ImageDimensions, ImageError,
        ImageInner, ImageLayout, ImageUsage,
    };
    use vulkano::{
        device::{Device, DeviceOwned, Queue},
        format::Format,
        image::{sys::ImageCreateInfo, view::ImageView, ImageFormatInfo},
        memory::{
            allocator::{
                AllocationCreateInfo, AllocationType, MemoryAllocatePreference, MemoryAllocator,
                MemoryUsage,
            },
            DedicatedAllocation, DeviceMemoryError, ExternalMemoryHandleType,
            ExternalMemoryHandleTypes,
        },
        sync::Sharing,
        DeviceSize,
    };

    /// General-purpose image in device memory. Can be used for any usage, but will be slower than a
    /// specialized image.
    #[derive(Debug)]
    pub struct StorageImage {
        inner: Arc<Image>,
    }

    impl StorageImage {
        /// Creates a new image with the given dimensions and format.
        pub fn new(
            allocator: &(impl MemoryAllocator + ?Sized),
            dimensions: ImageDimensions,
            format: Format,
            queue_family_indices: impl IntoIterator<Item = u32>,
        ) -> Result<Arc<StorageImage>, ImageError> {
            let aspects = format.aspects();
            let is_depth = aspects.depth || aspects.stencil;

            if format.compression().is_some() {
                panic!() // TODO: message?
            }

            let usage = ImageUsage {
                transfer_src: true,
                transfer_dst: true,
                sampled: true,
                storage: true,
                color_attachment: !is_depth,
                depth_stencil_attachment: is_depth,
                input_attachment: true,
                ..ImageUsage::empty()
            };
            let flags = ImageCreateFlags::empty();

            StorageImage::with_usage(
                allocator,
                dimensions,
                format,
                usage,
                flags,
                queue_family_indices,
                1,
            )
        }

        /// Same as `new`, but allows specifying the usage.
        pub fn with_usage(
            allocator: &(impl MemoryAllocator + ?Sized),
            dimensions: ImageDimensions,
            format: Format,
            usage: ImageUsage,
            flags: ImageCreateFlags,
            queue_family_indices: impl IntoIterator<Item = u32>,
            mip_levels: u32,
        ) -> Result<Arc<StorageImage>, ImageError> {
            let queue_family_indices: SmallVec<[_; 4]> = queue_family_indices.into_iter().collect();
            assert!(!flags.disjoint); // TODO: adjust the code below to make this safe

            let raw_image = RawImage::new(
                allocator.device().clone(),
                ImageCreateInfo {
                    flags,
                    dimensions,
                    format: Some(format),
                    usage,
                    sharing: if queue_family_indices.len() >= 2 {
                        Sharing::Concurrent(queue_family_indices)
                    } else {
                        Sharing::Exclusive
                    },
                    mip_levels,
                    ..Default::default()
                },
            )?;
            let requirements = raw_image.memory_requirements()[0];
            let create_info = AllocationCreateInfo {
                requirements,
                allocation_type: AllocationType::NonLinear,
                usage: MemoryUsage::GpuOnly,
                allocate_preference: MemoryAllocatePreference::Unknown,
                dedicated_allocation: Some(DedicatedAllocation::Image(&raw_image)),
                ..Default::default()
            };

            match unsafe { allocator.allocate_unchecked(create_info) } {
                Ok(alloc) => {
                    debug_assert!(alloc.offset() % requirements.alignment == 0);
                    debug_assert!(alloc.size() == requirements.size);
                    let inner = Arc::new(unsafe {
                        raw_image
                            .bind_memory_unchecked([alloc])
                            .map_err(|(err, _, _)| err)?
                    });

                    Ok(Arc::new(StorageImage { inner }))
                }
                Err(err) => Err(err.into()),
            }
        }

        pub fn new_with_exportable_fd(
            allocator: &(impl MemoryAllocator + ?Sized),
            dimensions: ImageDimensions,
            format: Format,
            usage: ImageUsage,
            flags: ImageCreateFlags,
            queue_family_indices: impl IntoIterator<Item = u32>,
        ) -> Result<Arc<StorageImage>, ImageError> {
            let queue_family_indices: SmallVec<[_; 4]> = queue_family_indices.into_iter().collect();
            assert!(!flags.disjoint); // TODO: adjust the code below to make this safe

            let external_memory_properties = allocator
                .device()
                .physical_device()
                .image_format_properties(ImageFormatInfo {
                    flags,
                    format: Some(format),
                    image_type: dimensions.image_type(),
                    usage,
                    external_memory_handle_type: Some(ExternalMemoryHandleType::OpaqueFd),
                    ..Default::default()
                })
                .unwrap()
                .unwrap()
                .external_memory_properties;
            // VUID-VkExportMemoryAllocateInfo-handleTypes-00656
            assert!(external_memory_properties.exportable);

            // VUID-VkMemoryAllocateInfo-pNext-00639
            // Guaranteed because we always create a dedicated allocation

            let external_memory_handle_types = ExternalMemoryHandleTypes {
                opaque_fd: true,
                ..ExternalMemoryHandleTypes::empty()
            };
            let raw_image = RawImage::new(
                allocator.device().clone(),
                ImageCreateInfo {
                    flags,
                    dimensions,
                    format: Some(format),
                    usage,
                    sharing: if queue_family_indices.len() >= 2 {
                        Sharing::Concurrent(queue_family_indices)
                    } else {
                        Sharing::Exclusive
                    },
                    external_memory_handle_types,
                    ..Default::default()
                },
            )?;
            let requirements = raw_image.memory_requirements()[0];
            let memory_type_index = allocator
                .find_memory_type_index(requirements.memory_type_bits, MemoryUsage::GpuOnly.into())
                .expect("failed to find a suitable memory type");

            match unsafe {
                allocator.allocate_dedicated_unchecked(
                    memory_type_index,
                    requirements.size,
                    Some(DedicatedAllocation::Image(&raw_image)),
                    external_memory_handle_types,
                )
            } {
                Ok(alloc) => {
                    debug_assert!(alloc.offset() % requirements.alignment == 0);
                    debug_assert!(alloc.size() == requirements.size);
                    let inner = Arc::new(unsafe {
                        raw_image
                            .bind_memory_unchecked([alloc])
                            .map_err(|(err, _, _)| err)?
                    });

                    Ok(Arc::new(StorageImage { inner }))
                }
                Err(err) => Err(err.into()),
            }
        }

        /// Allows the creation of a simple 2D general purpose image view from `StorageImage`.
        #[inline]
        pub fn general_purpose_image_view(
            allocator: &(impl MemoryAllocator + ?Sized),
            queue: Arc<Queue>,
            size: [u32; 2],
            format: Format,
            usage: ImageUsage,
        ) -> Result<Arc<ImageView<StorageImage>>, ImageError> {
            let dims = ImageDimensions::Dim2d {
                width: size[0],
                height: size[1],
                array_layers: 1,
            };
            let flags = ImageCreateFlags::empty();
            let image_result = StorageImage::with_usage(
                allocator,
                dims,
                format,
                usage,
                flags,
                Some(queue.queue_family_index()),
                1,
            );

            match image_result {
                Ok(image) => {
                    let image_view = ImageView::new_default(image);
                    match image_view {
                        Ok(view) => Ok(view),
                        Err(e) => Err(ImageError::DirectImageViewCreationFailed(e)),
                    }
                }
                Err(e) => Err(e),
            }
        }

        /// Exports posix file descriptor for the allocated memory.
        /// Requires `khr_external_memory_fd` and `khr_external_memory` extensions to be loaded.
        #[inline]
        pub fn export_posix_fd(&self) -> Result<File, DeviceMemoryError> {
            let allocation = match self.inner.memory() {
                ImageMemory::Normal(a) => &a[0],
                _ => unreachable!(),
            };

            allocation
                .device_memory()
                .export_fd(ExternalMemoryHandleType::OpaqueFd)
        }

        /// Return the size of the allocated memory (used e.g. with cuda).
        #[inline]
        pub fn mem_size(&self) -> DeviceSize {
            let allocation = match self.inner.memory() {
                ImageMemory::Normal(a) => &a[0],
                _ => unreachable!(),
            };

            allocation.device_memory().allocation_size()
        }

        pub fn handle(&self) -> ash::vk::Image {
            vulkano::VulkanObject::handle(&self.inner)
        }
    }

    unsafe impl DeviceOwned for StorageImage {
        #[inline]
        fn device(&self) -> &Arc<Device> {
            self.inner.device()
        }
    }

    unsafe impl ImageAccess for StorageImage {
        #[inline]
        fn inner(&self) -> ImageInner<'_> {
            ImageInner {
                image: &self.inner,
                first_layer: 0,
                num_layers: self.inner.dimensions().array_layers(),
                first_mipmap_level: 0,
                num_mipmap_levels: self.inner.mip_levels(),
            }
        }

        #[inline]
        fn initial_layout_requirement(&self) -> ImageLayout {
            ImageLayout::General
        }

        #[inline]
        fn final_layout_requirement(&self) -> ImageLayout {
            ImageLayout::General
        }

        #[inline]
        fn descriptor_layouts(&self) -> Option<ImageDescriptorLayouts> {
            Some(ImageDescriptorLayouts {
                storage_image: ImageLayout::General,
                combined_image_sampler: ImageLayout::General,
                sampled_image: ImageLayout::General,
                input_attachment: ImageLayout::General,
            })
        }
    }

    unsafe impl<P> ImageContent<P> for StorageImage {
        fn matches_format(&self) -> bool {
            true // FIXME:
        }
    }

    impl PartialEq for StorageImage {
        #[inline]
        fn eq(&self, other: &Self) -> bool {
            self.inner() == other.inner()
        }
    }

    impl Eq for StorageImage {}

    impl Hash for StorageImage {
        fn hash<H: Hasher>(&self, state: &mut H) {
            self.inner().hash(state);
        }
    }
}

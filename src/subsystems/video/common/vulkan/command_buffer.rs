use ash::vk::{Buffer, CommandBuffer};

pub type DeviceSize = u64;

pub unsafe fn cmd_update_buffer<T>(
    ash_device: &ash::Device,
    command_buffer: CommandBuffer,
    dst_buffer: Buffer,
    dst_offset: DeviceSize,
    data: &T,
) {
    unsafe {
        (ash_device.fp_v1_0().cmd_update_buffer)(
            command_buffer,
            dst_buffer,
            dst_offset,
            std::mem::size_of_val(data) as u64,
            (data as *const T) as _,
        );
    }
}

use std::{cell::RefCell, collections::HashMap, sync::Arc};

use ash::vk::{
    DescriptorPool, DescriptorPoolCreateFlags, DescriptorPoolCreateInfo, DescriptorPoolSize,
    DescriptorType,
};
use vulkano::shader::DescriptorRequirements;

#[repr(C)]
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct DescriptorPoolSizeKey {
    pub ty: DescriptorType,
    pub descriptor_count: u32,
}

impl DescriptorPoolSizeKey {
    pub fn from_deescriptor_pool_size(d: DescriptorPoolSize) -> Self {
        todo!()
    }
}

#[derive(Clone, Hash, Eq, PartialEq)]
pub struct PoolSizes {
    sizes: Vec<DescriptorPoolSizeKey>,
}

impl PoolSizes {
    pub fn from_reqs(reqs: &Vec<Option<(u32, DescriptorRequirements)>>) -> Self {
        let sizes = reqs
            .iter()
            .filter_map(|r| r.as_ref())
            .fold(HashMap::new(), |v, (_, req)| {
                let count = req.descriptor_count.unwrap_or(0);
                if count == 0 {
                    return v;
                }
                let mut v = v;
                let t = req.descriptor_types[0];
                let t = DescriptorType::from_raw(unsafe { std::mem::transmute(t) });
                let size = match v.get_mut(&t) {
                    Some(s) => s,
                    None => {
                        let s = DescriptorPoolSizeKey {
                            ty: t,
                            descriptor_count: 0,
                        };
                        v.insert(t, s);
                        v.get_mut(&t).unwrap()
                    }
                };
                size.descriptor_count += count;

                v
            });

        Self {
            sizes: sizes.into_values().collect(),
        }
    }
    // Vec<Option<(u32, DescriptorRequirements)>>
}

#[derive(Clone)]
pub struct DescriptorPools {
    device: ash::Device,
    descriptor_pools: Arc<RefCell<HashMap<PoolSizes, DescriptorPool>>>,
}

impl DescriptorPools {
    pub fn new(device: ash::Device) -> Self {
        Self {
            device,
            descriptor_pools: Arc::new(RefCell::new(HashMap::new())),
        }
    }

    pub fn pool(&self, k: &PoolSizes, max_sets: u32) -> DescriptorPool {
        let mut descriptor_pools = self.descriptor_pools.borrow_mut();
        match descriptor_pools.get(&k) {
            Some(pool) => *pool,
            None => unsafe {
                let p_pool_sizes: *const DescriptorPoolSizeKey = k.sizes.as_ptr();
                println!("{:?}", k.sizes);
                let p_pool_sizes: *const DescriptorPoolSize = p_pool_sizes as _;
                let pool = self
                    .device
                    .create_descriptor_pool(
                        &DescriptorPoolCreateInfo {
                            max_sets,
                            flags: DescriptorPoolCreateFlags::empty(),
                            pool_size_count: k.sizes.len() as _,
                            p_pool_sizes,
                            ..Default::default()
                        },
                        None,
                    )
                    .unwrap();
                descriptor_pools.insert(k.clone(), pool);

                pool
            },
        }
    }
}

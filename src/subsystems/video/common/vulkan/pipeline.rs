use vulkano::shader::DescriptorRequirements;

pub struct Pipeline {
    pub descriptor_requirements: Vec<Vec<Option<(u32, DescriptorRequirements)>>>,
    pub descriptor_set_layouts: Vec<ash::vk::DescriptorSetLayout>,
    pub pipeline_layout: ash::vk::PipelineLayout,
    pub pipeline: ash::vk::Pipeline,
}

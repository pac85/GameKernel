pub mod egui;
pub mod imgui;

use std::sync::Arc;

use vulkano::command_buffer::sys::UnsafeCommandBufferBuilder;
use vulkano::device::Queue;
use vulkano::image::ImageViewAbstract;

//TODO macro that generates the implementation for an arbitrary number of modules
impl<A, B> crate::UiModule for (A, B)
where
    A: crate::UiModule,
    B: crate::UiModule,
{
    fn render(
        &mut self,
        command_buffer_builder: &mut UnsafeCommandBufferBuilder,
        queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + Send + Sync + 'static>,
    ) {
        let (a, b) = self;
        a.render(command_buffer_builder, queue.clone(), target.clone());
        b.render(command_buffer_builder, queue, target);
    }
}

use std::convert::TryFrom;
use std::default::Default;
use std::sync::Arc;

use crate::vulkan::{self, SwapChain};
use crate::vulkan::{VulkanBasic, VulkanCommon};
use crate::winit::window::Window;

use vulkano::command_buffer::sys::UnsafeCommandBufferBuilder;
use vulkano::command_buffer::{RenderPassBeginInfo, SubpassContents};
use vulkano::device::Queue;
use vulkano::format::ClearValue;
use vulkano::image::ImageViewAbstract;
use vulkano::pipeline::graphics::viewport::{Scissor, Viewport};
use vulkano::render_pass::{Framebuffer, FramebufferCreateInfo, RenderPass, Subpass};

use egui;
use egui_vulkano::{self, ToVkObjs, VkObjs};
use egui_winit;
use epaint;
use winit::event::{Event, WindowEvent};
use winit::event_loop::EventLoopWindowTarget;

impl ToVkObjs for VulkanCommon {
    fn to_vk_objs(self) -> VkObjs {
        VkObjs {
            device: self.get_device(),
            memory_allocator: self.get_memory_allocator(),
            descriptor_set_allocator: self.get_descriptor_set_allocator(),
            command_buffer_allocator: self.get_command_buffer_allocator(),
            queue: self.get_graphics_queue(),
        }
    }
}

pub struct GkEgui {
    pub context: egui::Context,
    pub platform: egui_winit::State,
    pub painter: egui_vulkano::Painter,
    render_pass: Arc<RenderPass>,
    dims: [f32; 2],
}

impl GkEgui {
    pub fn init<T>(
        window: &Window,
        event_loop: &EventLoopWindowTarget<T>,
        vulkan_common: vulkan::VulkanCommon,
        swapchain: SwapChain,
        scale_factor_override: Option<f64>,
    ) -> Result<Self, ()> {
        let size = window.inner_size();
        let context = egui::Context::default();
        let mut platform = egui_winit::State::new(event_loop);
        platform.set_pixels_per_point(window.scale_factor() as f32);
        context.set_pixels_per_point(window.scale_factor() as f32);

        let render_pass = vulkano::single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: Load,
                    store: Store,
                    format: swapchain.swapchain_format,
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        }
        .unwrap();

        let painter = egui_vulkano::Painter::new(
            Subpass::from(render_pass.clone(), 0).unwrap(),
            vulkan_common.clone(),
        )
        .unwrap();

        Ok(Self {
            context,
            platform,
            painter,
            render_pass,
            dims: [
                vulkan_common.get_config().resolution[0] as f32,
                vulkan_common.get_config().resolution[1] as f32,
            ],
        })
    }

    pub fn on_event(&mut self, event: &Event<'_, ()>) -> bool {
        match event {
            Event::WindowEvent {
                window_id: _,
                event: event,
            } => self.platform.on_event(&self.context, event),
            _ => false,
        }
    }

    pub fn context(&self) -> egui::Context {
        self.context.clone()
    }

    pub fn get_mod<'a>(&'a mut self) -> EguiModule<'a> {
        let egui::output::FullOutput {
            platform_output,
            textures_delta,
            shapes,
            repaint_after,
        } = self.context.end_frame();

        let dims = [
            self.dims[0] / self.context.pixels_per_point(),
            self.dims[1] / self.context.pixels_per_point(),
        ];
        EguiModule {
            ctx: self.context.clone(),
            painter: &mut self.painter,
            output: platform_output,
            clippped_shapes: Some(shapes),
            textures_delta,
            dims_pixels: self.dims,
            dims,
            render_pass: self.render_pass.clone(),
            frame_buffer: None,
        }
    }
}

pub struct EguiModule<'a> {
    ctx: egui::Context,
    painter: &'a mut egui_vulkano::Painter,
    output: egui::output::PlatformOutput,
    clippped_shapes: Option<Vec<epaint::ClippedShape>>,
    textures_delta: egui::TexturesDelta,
    dims_pixels: [f32; 2],
    dims: [f32; 2],

    render_pass: Arc<RenderPass>,
    frame_buffer: Option<Arc<dyn Drop>>,
}

impl<'a> crate::UiModule for EguiModule<'a> {
    fn render(
        &mut self,
        command_buffer_builder: &mut UnsafeCommandBufferBuilder,
        queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + Send + Sync + 'static>,
    ) {
        let scissors = Some(vec![Scissor::default()]);

        let framebuffer = Framebuffer::new(
            self.render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![target],
                ..Default::default()
            },
        )
        .unwrap();
        self.frame_buffer = Some(framebuffer.clone());

        unsafe {
            command_buffer_builder.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![None],
                    ..RenderPassBeginInfo::framebuffer(framebuffer.clone())
                },
                SubpassContents::Inline,
            );
        }

        self.painter
            .unsafe_draw(
                command_buffer_builder,
                self.dims_pixels,
                self.dims,
                &self.ctx,
                self.clippped_shapes.take().unwrap(),
                &std::mem::take(&mut self.textures_delta),
            )
            .unwrap();
        unsafe {
            command_buffer_builder.end_render_pass();
        }
    }
}

pub mod common;
pub mod renderer;
pub mod ui;

use vulkano::command_buffer::sys::UnsafeCommandBufferBuilder;
use vulkano::device::Queue;
use vulkano::image::ImageViewAbstract;
use vulkano::swapchain::Surface;

use std::cell::RefCell;
use std::sync::Arc;

use crate::vulkan::SwapChain;

use self::common::vulkan::VulkanCommon;

use winit::window::Window;

pub struct Video {
    pub renderer: Option<Arc<RefCell<renderer::Renderer>>>,
    pub swapchain: Option<SwapChain>,
}

impl Video {
    pub fn new() -> Self {
        Self {
            renderer: None,
            swapchain: None,
        }
    }

    pub fn init_renderer(
        &mut self,
        vulkan_common: &VulkanCommon,
        surface: Arc<Surface>,
    ) -> Result<(), String> {
        let swapchain = SwapChain::new(vulkan_common.clone(), surface);
        self.swapchain = Some(swapchain.clone());
        let renderer = renderer::Renderer::init(vulkan_common, &swapchain);
        self.renderer = if let Some(r) = renderer {
            Some(Arc::new(RefCell::new(r)))
        } else {
            None
        };
        Ok(())
    }

    pub fn get_swapchain(&self) -> SwapChain {
        self.swapchain.clone().unwrap()
    }
}

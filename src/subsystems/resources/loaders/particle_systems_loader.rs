use super::super::{CreationArg, LoadError};

use std::collections::HashMap;

use crate::renderer::particle_fx::*;

pub fn load_emitter(
    manager: &mut super::ResourceManager,
    creation_args: &HashMap<String, CreationArg>,
    fs: &physfs_rs::PhysFs,
) -> Result<Box<dyn DynCloneEmitter + Send + Sync>, LoadError> {
    let emitter_type = creation_args
        .get("type")
        .ok_or(LoadError::LoaderError("Missing type argument"))?;

    let emitter_type = if let CreationArg::Str(s) = emitter_type {
        s.clone()
    } else {
        return Err(LoadError::LoaderError("Wrong argument type"));
    };

    let emitter_factory = EMITTER_FACTORY.lock().unwrap();
    Ok(emitter_factory
        .instantiate(&emitter_type, manager.make_args_map(creation_args, fs)?)
        .unwrap())
}

use game_kernel_utils::{Framestamps, KeyType};

use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

use game_kernel_ecs::ecs::{
    component::*,
    system::{SysErr, System, SystemManager},
};
use game_kernel_ecs_derive::*;

use crate::game_kernel::ecs::*;
use crate::game_kernel::subsystems::video::renderer;
use crate::{RendererSystem, TransformComponent};

#[derive(Component, Clone)]
pub struct StaticMeshComponent {
    material: Arc<renderer::material::Material>,
    mesh: Arc<renderer::mesh::Mesh>,
    #[arg_default]
    mesh_index: Option<KeyType>,
}

impl StaticMeshComponent {
    pub fn new(
        material: Arc<renderer::material::Material>,
        mesh: Arc<renderer::mesh::Mesh>,
    ) -> Self {
        Self {
            material,
            mesh,
            mesh_index: None,
        }
    }
}

use std::cell::RefCell;

pub struct StaticMeshSystem {
    renderer: Option<Arc<RefCell<crate::game_kernel::MainRenderer>>>,
    frame_stamps: Framestamps,
}

impl StaticMeshSystem {
    pub fn new() -> Self {
        Self {
            renderer: None,
            frame_stamps: Framestamps::new(),
        }
    }
}

impl System for StaticMeshSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        self.renderer = Some(
            manager
                .get_system::<RendererSystem>()
                .unwrap()
                .get_renderer(),
        );

        Ok(())
    }

    fn update<'a>(
        &'a mut self,
        manager: &SystemManager,
        world: &'a mut World,
        _delta: std::time::Duration,
    ) {
        if self.renderer.is_none() {
            return;
        }
        let renderer = self.renderer.clone().unwrap();

        for (transform_component, mut static_mesh_component) in
            world.query::<(&mut TransformComponent, &mut StaticMeshComponent)>()
        {
            let mesh_index = if let Some(mesh_index) = static_mesh_component.mesh_index {
                if let Some(model) = (renderer.borrow_mut()).get_model_mut(mesh_index) {
                    model.transform = transform_component.get_matrix();
                } else {
                    unreachable!();
                }

                mesh_index
            } else {
                let model = crate::Model {
                    mesh: static_mesh_component.mesh.clone(),
                    material: static_mesh_component.material.clone(),
                    transform: transform_component.get_matrix(),
                };
                static_mesh_component.mesh_index = Some(renderer.borrow_mut().add_model(model));

                static_mesh_component.mesh_index.unwrap()
            };

            self.frame_stamps.set_last_seen(mesh_index);
        }

        //delete meshes that have been seen in the past but not in this frame
        let mut renderer = renderer.borrow_mut();
        let old_indices = renderer
            .get_models()
            .map(|(k, _)| k)
            .filter(|k| self.frame_stamps.is_element_old(*k))
            .collect::<Vec<_>>();

        for mesh_index in old_indices {
            renderer.remove_model(mesh_index);
            self.frame_stamps.element_removed(mesh_index);
        }

        self.frame_stamps.next_frame();
    }
}

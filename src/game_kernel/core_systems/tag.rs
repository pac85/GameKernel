use game_kernel_ecs::ecs::component::*;
use game_kernel_ecs::parse_component_arg;
use game_kernel_ecs_derive::*;

use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

#[derive(Clone, Component)]
pub struct TagComponent {
    pub name: String,
}

impl TagComponent {
    pub fn new(name: impl AsRef<str>) -> Self {
        Self {
            name: name.as_ref().to_owned(),
        }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }
}

use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;
use std::sync::Arc;

use game_kernel_utils::KeyType;

use crate::game_kernel::ecs::*;
use game_kernel_ecs::ecs::{
    component::*,
    system::{SysErr, System, SystemManager},
};
use game_kernel_ecs_derive::*;

use crate::audio::SpatialSourceBuilderWrapper;

#[derive(Component, Clone)]
pub struct SoundSourceComponent {
    source: Arc<SpatialSourceBuilderWrapper>,
    #[arg_default]
    source_index: Option<KeyType>,
}

impl SoundSourceComponent {
    pub fn new(source: Arc<SpatialSourceBuilderWrapper>) -> Self {
        Self {
            source,
            source_index: None,
        }
    }
}

use super::base::transform::TransformComponent;

pub struct SoundSourceSystem {
    ctx: Option<Arc<RefCell<crate::audio::AudioContext>>>,
}

impl SoundSourceSystem {
    pub fn new() -> Self {
        Self { ctx: None }
    }
}

impl System for SoundSourceSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        self.ctx = Some(
            manager
                .get_system::<super::sound_system::SoundSystem>()
                .unwrap()
                .get_ctx(),
        );

        Ok(())
    }

    fn update<'a>(
        &'a mut self,
        manager: &SystemManager,
        world: &'a mut World,
        _delta: std::time::Duration,
    ) {
        if self.ctx.is_none() {
            return;
        }
        let ctx = self.ctx.as_ref().unwrap();

        for (transform_component, mut sound_component) in
            world.query::<(&mut TransformComponent, &mut SoundSourceComponent)>()
        {
            if let Some(sound_index) = sound_component.source_index {
                ctx.borrow_mut()
                    .renderer
                    .0
                    .lock()
                    .unwrap()
                    .mutate_source(sound_index, |source| {
                        source.properties.set_position(transform_component.position)
                    })
                    .unwrap();
            } else {
                sound_component.source_index = Some(
                    ctx.borrow_mut()
                        .renderer
                        .0
                        .lock()
                        .unwrap()
                        .add_source(sound_component.source.builder.build()),
                );
            }
        }
    }
}

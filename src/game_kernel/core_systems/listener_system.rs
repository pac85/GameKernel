use std::cell::RefCell;
use std::sync::Arc;

use game_kernel_utils::KeyType;

use super::camera_system::CameraComponent;
use crate::game_kernel::ecs::*;

use crate::audio::Listener;

pub struct ListenerSystem {
    ctx: Option<Arc<RefCell<crate::audio::AudioContext>>>,
    active_listener: Option<EntitId>,
}

impl ListenerSystem {
    pub fn new() -> Self {
        Self {
            ctx: None,
            active_listener: None,
        }
    }

    pub fn get_active_listener(&self, world: &World) -> Option<EntitId> {
        self.active_listener.and_then(|entity| {
            if world.has_components::<&CameraComponent>(entity) {
                Some(entity)
            } else {
                None
            }
        })
    }

    pub fn set_active_listener(&mut self, entity: EntitId) {
        self.active_listener = Some(entity);
    }
}

use crate::game_kernel::ecs::World;
use crate::game_kernel_ecs::ecs::system::{SysErr, System, SystemManager};

impl System for ListenerSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        self.ctx = Some(
            manager
                .get_system::<super::sound_system::SoundSystem>()
                .unwrap()
                .get_ctx(),
        );

        Ok(())
    }

    fn update(&mut self, manager: &SystemManager, world: &mut World, _delta: std::time::Duration) {
        match self.active_listener {
            Some(camera_entity) if world.has_components::<&CameraComponent>(camera_entity) => {}

            _ => {
                self.active_listener = world
                    .query::<(KeyType, &CameraComponent)>()
                    .map(|(k, _)| k)
                    .next();
            }
        }

        if let Some(l_entity) = self.get_active_listener(world) {
            let ccomp = world
                .get_entity_component::<CameraComponent>(l_entity)
                .unwrap();
            let listener = Listener::from_camera(&ccomp.camera);
            self.ctx
                .as_ref()
                .unwrap()
                .borrow_mut()
                .renderer
                .0
                .lock()
                .unwrap()
                .set_listener(listener);
        }
    }
}

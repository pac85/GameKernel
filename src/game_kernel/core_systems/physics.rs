use core::panic;
use std::any::Any;
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, RwLock};

use crate::game_kernel::ecs::*;
use crate::utils::HashFramestamps;
use crate::TransformComponent;
use rapier3d::prelude::*;

pub mod register {
    use super::*;

    use crate::game_kernel::ecs::COMPONENT_FACTORY;
    use game_kernel_ecs::ecs::{component::ComponentFactory, system::*, World};
    use std::sync::MutexGuard;

    pub fn register_components(component_factory: &mut MutexGuard<ComponentFactory>) {
        component_factory.register::<RigidBodyComponent>();
        component_factory.register::<ColliderComponent>();
    }

    pub fn init_systems(manager: &mut SystemManager, world: &mut World) {
        manager.add_and_init(PhysicsSystem::new(), world).unwrap();
    }
}

#[derive(Debug, Clone)]
pub enum RigidBodyOrHandle {
    NotInserted(RigidBody),
    Inserted(RigidBodyHandle),
    Inserting(),
}

impl RigidBodyOrHandle {
    fn unwrap_body(self) -> RigidBody {
        if let Self::NotInserted(body) = self {
            body
        } else {
            panic!("unwrapped");
        }
    }

    pub fn unwrap_handle(&self) -> RigidBodyHandle {
        if let Self::Inserted(handle) = self {
            *handle
        } else {
            panic!("unwrapped");
        }
    }

    pub fn get_handle(&self) -> Option<RigidBodyHandle> {
        if let Self::Inserted(handle) = self {
            Some(*handle)
        } else {
            None
        }
    }

    pub fn is_inserted(&self) -> bool {
        match self {
            Self::Inserted(_) => true,
            _ => false,
        }
    }

    pub fn replace(&mut self, mut f: impl FnMut(RigidBody) -> RigidBodyHandle) {
        let body = std::mem::replace(self, Self::Inserting());
        *self = Self::Inserted(f(body.unwrap_body()));
    }
}

#[derive(Clone)]
pub enum ColliderOrHandle {
    NotInserted(Collider),
    Inserted(ColliderHandle),
    Inserting(),
}

impl ColliderOrHandle {
    fn unwrap_collider(self) -> Collider {
        if let Self::NotInserted(body) = self {
            body
        } else {
            panic!("unwrapped");
        }
    }

    pub fn unwrap_handle(&self) -> ColliderHandle {
        if let Self::Inserted(handle) = self {
            *handle
        } else {
            panic!("unwrapped");
        }
    }

    pub fn get_handle(&self) -> Option<ColliderHandle> {
        if let Self::Inserted(handle) = self {
            Some(*handle)
        } else {
            None
        }
    }

    pub fn replace(&mut self, mut f: impl FnMut(Collider) -> ColliderHandle) {
        let body = std::mem::replace(self, Self::Inserting());
        *self = Self::Inserted(f(body.unwrap_collider()));
    }
}

#[derive(Clone, Component)]
pub struct RigidBodyComponent {
    pub rigid_body: RigidBodyOrHandle,
}

impl RigidBodyComponent {
    pub fn from_rigid_body(rigid_body: RigidBody) -> Self {
        Self {
            rigid_body: RigidBodyOrHandle::NotInserted(rigid_body),
        }
    }
}

#[derive(Clone, Component)]
pub struct ColliderComponent {
    pub collider: ColliderOrHandle,
}

impl ColliderComponent {
    pub fn from_collider(collider: Collider) -> Self {
        Self {
            collider: ColliderOrHandle::NotInserted(collider),
        }
    }
}

//----------------------------------Systems-------------------------------------

pub struct Contact {
    counter: u32,
    others: HashSet<ColliderHandle>,
    points: Vec<rapier3d::math::Point<f32>>,
}

impl Contact {
    pub fn new(counter: u32) -> Self {
        Self {
            counter,
            others: HashSet::new(),
            points: vec![],
        }
    }
}

pub struct ContactTracker {
    contacting_map: RwLock<HashMap<ColliderHandle, Contact>>,
}

impl ContactTracker {
    pub fn new() -> Self {
        Self {
            contacting_map: RwLock::new(HashMap::new()),
        }
    }

    pub fn is_touching(&self, handle: ColliderHandle) -> bool {
        self.contacting_map
            .read()
            .unwrap()
            .get(&handle)
            .map(|x| x.counter)
            .unwrap_or(0)
            != 0
    }

    pub fn touching(&self, handle: ColliderHandle) -> HashSet<ColliderHandle> {
        self.contacting_map
            .read()
            .unwrap()
            .get(&handle)
            .map(|x| x.others.clone())
            .unwrap_or(HashSet::new())
    }

    pub fn contact_points(
        &self,
        handle: ColliderHandle,
    ) -> Option<Vec<rapier3d::math::Point<f32>>> {
        self.contacting_map
            .read()
            .unwrap()
            .get(&handle)
            .map(|x| x.points.clone())
    }
}

impl EventHandler for ContactTracker {
    /*fn handle_intersection_event(&self, event: IntersectionEvent) {
        let mut map = self.contacting_map.write().unwrap();
        match event {
            IntersectionEvent {
                collider1: a,
                collider2: b,
                intersecting: true,
            } => {
                if !map.contains_key(&a) {
                    map.insert(a, Contact::new(0));
                }
                if !map.contains_key(&b) {
                    map.insert(b, Contact::new(0));
                }
                map.get_mut(&a).unwrap().counter += 1;
                map.get_mut(&a).unwrap().others.insert(b);
                map.get_mut(&b).unwrap().counter += 1;
                map.get_mut(&b).unwrap().others.insert(a);
            }
            IntersectionEvent {
                collider1: a,
                collider2: b,
                intersecting: false,
            } => {
                map.get_mut(&a).unwrap().counter -= 1;
                map.get_mut(&a).unwrap().others.remove(&b);
                map.get_mut(&b).unwrap().counter -= 1;
                map.get_mut(&b).unwrap().others.remove(&a);
            }
        }
    }
    fn handle_contact_event(&self, event: ContactEvent, contact_pair: &ContactPair) {
        let mut map = self.contacting_map.write().unwrap();
        match event {
            ContactEvent::Started(a, b) => {
                if !map.contains_key(&a) {
                    map.insert(a, Contact::new(0));
                }
                if !map.contains_key(&b) {
                    map.insert(b, Contact::new(0));
                }
                map.get_mut(&a).unwrap().counter += 1;
                map.get_mut(&b).unwrap().counter += 1;

                //add all contact points
                contact_pair
                    .manifolds
                    .iter()
                    .flat_map(|m| m.data.solver_contacts.iter().map(|c| c.point))
                    .for_each(|p| {
                        map.get_mut(&a).unwrap().points.push(p);
                        map.get_mut(&b).unwrap().points.push(p);
                    });
            }
            ContactEvent::Stopped(a, b) => {
                map.get_mut(&a).unwrap().counter -= 1;
                map.get_mut(&b).unwrap().counter -= 1;

                if map.get_mut(&a).unwrap().counter == 0 {
                    map.get_mut(&a).unwrap().points.clear();
                }

                if map.get_mut(&b).unwrap().counter == 0 {
                    map.get_mut(&b).unwrap().points.clear();
                }
            }
        }
    }*/

    fn handle_collision_event(
        &self,
        bodies: &RigidBodySet,
        colliders: &ColliderSet,
        event: CollisionEvent,
        contact_pair: Option<&ContactPair>,
    ) {
    }

    fn handle_contact_force_event(
        &self,
        dt: Real,
        bodies: &RigidBodySet,
        colliders: &ColliderSet,
        contact_pair: &ContactPair,
        total_force_magnitude: Real,
    ) {
    }
}

pub struct PhysicsSystem {
    pub rigid_body_set: RigidBodySet,
    pub collider_set: ColliderSet,

    pub query_pipeline: QueryPipeline,
    pub integration_parameters: IntegrationParameters,
    pub physics_pipeline: PhysicsPipeline,
    pub island_manager: IslandManager,
    pub broad_phase: BroadPhase,
    pub narrow_phase: NarrowPhase,
    pub impulse_joint_set: ImpulseJointSet,
    pub multibody_joint_set: MultibodyJointSet,
    pub ccd_solver: CCDSolver,

    pub contact_tracker: ContactTracker,

    collider_stamps: HashFramestamps<(u32, u32)>,
    rigid_body_stamps: HashFramestamps<(u32, u32)>,
}

impl PhysicsSystem {
    pub fn new() -> Self {
        Self {
            rigid_body_set: RigidBodySet::new(),
            collider_set: ColliderSet::new(),

            query_pipeline: QueryPipeline::new(),
            integration_parameters: IntegrationParameters::default(),
            physics_pipeline: PhysicsPipeline::new(),
            island_manager: IslandManager::new(),
            broad_phase: BroadPhase::new(),
            narrow_phase: NarrowPhase::new(),
            impulse_joint_set: ImpulseJointSet::new(),
            multibody_joint_set: MultibodyJointSet::new(),
            ccd_solver: CCDSolver::new(),

            contact_tracker: ContactTracker::new(),

            collider_stamps: HashFramestamps::new(),
            rigid_body_stamps: HashFramestamps::new(),
        }
    }

    pub fn cast_ray(
        &self,
        ray: &Ray,
        max_toi: f32,
        solid: bool,
        query_groups: InteractionGroups,
        predicate: Option<&dyn Fn(ColliderHandle, &Collider) -> bool>,
    ) -> Option<(ColliderHandle, f32)> {
        let mut filter = QueryFilter::new().groups(query_groups);
        if let Some(predicate) = predicate.as_ref() {
            filter = filter.predicate(predicate);
        }
        self.query_pipeline.cast_ray(
            &self.rigid_body_set,
            &self.collider_set,
            ray,
            max_toi,
            solid,
            filter,
        )
    }

    pub fn cast_ray_and_get_normal(
        &self,
        ray: &Ray,
        max_toi: f32,
        solid: bool,
        query_groups: InteractionGroups,
        predicate: Option<&dyn Fn(ColliderHandle, &Collider) -> bool>,
    ) -> Option<(ColliderHandle, RayIntersection)> {
        let mut filter = QueryFilter::new().groups(query_groups);
        if let Some(predicate) = predicate.as_ref() {
            filter = filter.predicate(predicate);
        }
        self.query_pipeline.cast_ray_and_get_normal(
            &self.rigid_body_set,
            &self.collider_set,
            ray,
            max_toi,
            solid,
            filter,
        )
    }
}

pub fn nacg_v3(v: &Vector<Real>) -> cgmath::Vector3<f32> {
    return cgmath::Vector3::new(v.x, v.y, v.z);
}

pub fn cgna_v3(v: cgmath::Vector3<f32>) -> Vector<Real> {
    vector![v.x, v.y, v.z]
}

pub fn nacg_quat(r: &rapier3d::math::Rotation<Real>) -> cgmath::Quaternion<f32> {
    let im = r.imag();
    cgmath::Quaternion::new(r.scalar(), im.x, im.y, im.z)
}

pub fn cgna_quat(r: cgmath::Quaternion<f32>) -> rapier3d::math::Rotation<Real> {
    rapier3d::math::Rotation::new_normalize(rapier3d::na::Quaternion::new(r.s, r.v.x, r.v.y, r.v.z))
}

use crate::game_kernel::ecs::World;
use game_kernel_ecs::ecs::system::{SysErr, System, SystemManager};

impl System for PhysicsSystem {
    fn init(&mut self, _manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(
        &mut self,
        manager: &SystemManager,
        world: &mut World,
        delta_time: std::time::Duration,
    ) {
        self.query_pipeline.update(
            &self.island_manager,
            &self.rigid_body_set,
            &self.collider_set,
        );

        self.integration_parameters.dt = (delta_time.as_micros() as f32 / 1000000.0)
            .min(1.0 / 30.0)
            .max(1.0 / 200.0);
        self.physics_pipeline.step(
            &vector![0.0, -9.81, 0.0],
            &self.integration_parameters,
            &mut self.island_manager,
            &mut self.broad_phase,
            &mut self.narrow_phase,
            &mut self.rigid_body_set,
            &mut self.collider_set,
            &mut self.impulse_joint_set,
            &mut self.multibody_joint_set,
            &mut self.ccd_solver,
            &(),
            &self.contact_tracker,
        );

        for (rigid_body_entity, rigid_body_component) in
            world.query::<(u64, &mut RigidBodyComponent)>()
        {
            let transform_component =
                world.get_entity_components::<&mut TransformComponent>(rigid_body_entity);

            match (&rigid_body_component.rigid_body, transform_component) {
                (RigidBodyOrHandle::NotInserted(_), _) => rigid_body_component
                    .rigid_body
                    .replace(|r| self.rigid_body_set.insert(r)),
                (RigidBodyOrHandle::Inserted(handle), Some(mut transform_component)) => {
                    self.rigid_body_stamps
                        .set_last_seen(handle.into_raw_parts());

                    let body = &self.rigid_body_set[*handle];

                    transform_component.position = nacg_v3(body.translation());
                    transform_component.rotation = nacg_quat(body.rotation());
                }
                _ => (),
            }
        }
        for (collider_entity, collider_component) in world.query::<(u64, &mut ColliderComponent)>()
        {
            let rigid_body_component = world
                .get_entity_components::<&RigidBodyComponent>(collider_entity)
                .map(|c| c.rigid_body.unwrap_handle());

            let transform_component =
                world.get_entity_components::<&TransformComponent>(collider_entity);

            match (
                &collider_component.collider,
                rigid_body_component,
                transform_component,
            ) {
                (ColliderOrHandle::NotInserted(_), None, Some(transform_component)) => {
                    collider_component.collider.replace(|mut c| {
                        let axis_angle = cgna_quat(transform_component.rotation).scaled_axis();
                        c.set_position(Isometry::new(
                            cgna_v3(transform_component.position),
                            axis_angle,
                        ));
                        self.collider_set.insert(c)
                    })
                }
                (ColliderOrHandle::NotInserted(_), None, _) => collider_component
                    .collider
                    .replace(|c| self.collider_set.insert(c)),
                (ColliderOrHandle::NotInserted(_), Some(rigid_body_component), _) => {
                    collider_component.collider.replace(|c| {
                        self.collider_set.insert_with_parent(
                            c,
                            rigid_body_component,
                            &mut self.rigid_body_set,
                        )
                    })
                }
                (ColliderOrHandle::Inserted(handle), None, Some(transform_component)) => {
                    self.collider_stamps.set_last_seen(handle.into_raw_parts());

                    let collider = &mut self.collider_set[*handle];

                    let axis_angle = cgna_quat(transform_component.rotation).scaled_axis();
                    collider.set_position(Isometry::new(
                        cgna_v3(transform_component.position),
                        axis_angle,
                    ));
                }
                _ => (),
            }
        }

        let old_rigid_bodies = self
            .rigid_body_set
            .iter()
            .map(|(x, _)| x)
            .filter(|handle| {
                self.rigid_body_stamps
                    .is_element_old(&handle.into_raw_parts())
            })
            .collect::<Vec<_>>();

        let old_colliders = self
            .collider_set
            .iter()
            .map(|(x, _)| x)
            .filter(|handle| {
                self.collider_stamps
                    .is_element_old(&handle.into_raw_parts())
            })
            .collect::<Vec<_>>();

        for old_collider in old_colliders {
            self.collider_stamps
                .element_removed(&old_collider.into_raw_parts());
            self.collider_set.remove(
                old_collider,
                &mut self.island_manager,
                &mut self.rigid_body_set,
                false,
            );
        }

        for old_rigid_body in old_rigid_bodies {
            self.rigid_body_stamps
                .element_removed(&old_rigid_body.into_raw_parts());
            self.rigid_body_set.remove(
                old_rigid_body,
                &mut self.island_manager,
                &mut self.collider_set,
                &mut self.impulse_joint_set,
                &mut self.multibody_joint_set,
                false,
            );
        }

        self.rigid_body_stamps.next_frame();
        self.collider_stamps.next_frame();
    }
}

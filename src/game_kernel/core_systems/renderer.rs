use game_kernel_ecs::ecs::system::{SysErr, System, SystemManager};

use std::cell::RefCell;
use std::sync::Arc;

pub struct RendererSystem {
    renderer: Arc<RefCell<crate::game_kernel::MainRenderer>>,
}

impl RendererSystem {
    pub fn new(renderer: Arc<RefCell<crate::game_kernel::MainRenderer>>) -> Self {
        Self { renderer }
    }

    pub fn get_renderer(&self) -> Arc<RefCell<crate::game_kernel::MainRenderer>> {
        self.renderer.clone()
    }
}

use crate::game_kernel::ecs::World;

impl System for RendererSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(&mut self, manager: &SystemManager, world: &mut World, delta: std::time::Duration) {}
}

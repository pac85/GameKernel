use game_kernel_ecs::ecs::system::{SysErr, System, SystemManager};

use std::cell::RefCell;
use std::sync::Arc;

pub struct SoundSystem {
    ctx: Arc<RefCell<crate::audio::AudioContext>>,
}

impl SoundSystem {
    pub fn new(ctx: Arc<RefCell<crate::audio::AudioContext>>) -> Self {
        Self { ctx }
    }

    pub fn get_ctx(&self) -> Arc<RefCell<crate::audio::AudioContext>> {
        self.ctx.clone()
    }
}

use crate::game_kernel::ecs::World;

impl System for SoundSystem {
    fn init(&mut self, manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(&mut self, manager: &SystemManager, world: &mut World, delta: std::time::Duration) {}
}

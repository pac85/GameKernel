use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

use super::base::transform::TransformComponent;
use crate::game_kernel::ecs::*;
use crate::game_kernel::subsystems::video::renderer;

use cgmath::Rotation;
use game_kernel_utils::KeyType;
use renderer::camera::Camera;

#[derive(Component, Clone)]
pub struct CameraComponent {
    pub camera: Camera,
}

impl CameraComponent {
    pub fn new(camera: Camera) -> Self {
        Self { camera }
    }
}

pub struct CameraSystem {
    active_camera: Option<EntitId>,
}

impl CameraSystem {
    pub fn new() -> Self {
        Self {
            active_camera: None,
        }
    }

    pub fn get_active_camera(&self, world: &World) -> Option<EntitId> {
        self.active_camera.and_then(|entity| {
            if world.has_components::<&CameraComponent>(entity) {
                Some(entity)
            } else {
                None
            }
        })
    }

    pub fn set_active_camera(&mut self, entity: EntitId) {
        self.active_camera = Some(entity);
    }
}

use crate::game_kernel_ecs::ecs::system::{SysErr, System, SystemManager};

impl System for CameraSystem {
    fn init(&mut self, _manager: &SystemManager, _world: &World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(&mut self, manager: &SystemManager, world: &mut World, _delta: std::time::Duration) {
        match self.active_camera {
            Some(camera_entity) if world.has_components::<&CameraComponent>(camera_entity) => {
                if let Some((transform, mut camera)) = world
                    .get_entity_components::<(&mut TransformComponent, &mut CameraComponent)>(
                        camera_entity,
                    )
                {
                    camera.camera.eye = cgmath::Point3::new(0.0, 0.0, 0.0) + transform.position;
                    camera.camera.dir = transform
                        .rotation
                        .rotate_vector(cgmath::vec3(1.0, 0.0, 0.0));
                    camera.camera.up = transform
                        .rotation
                        .rotate_vector(cgmath::vec3(0.0, -1.0, 0.0));
                }
            }

            _ => {
                self.active_camera = world
                    .query::<(KeyType, &TransformComponent, &CameraComponent)>()
                    .map(|(k, _, _)| k)
                    .next();
            }
        }
    }
}

use std::sync::Arc;

use vulkano::command_buffer::sys::*;
use vulkano::descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::Device;
use vulkano::format::Format;
use vulkano::image::{ImageUsage, ImageLayout, ImageSubresourceRange};
use vulkano::image::{AttachmentImage, ImageAccess, ImageDimensions};
use vulkano::pipeline::{ComputePipeline, Pipeline, PipelineBindPoint};
use vulkano::sampler::{Sampler, SamplerCreateInfo};
use vulkano::shader::{EntryPoint, ShaderModule};
use vulkano::sync::{DependencyInfo, ImageMemoryBarrier, PipelineStages, AccessFlags};

use super::super::common::vulkan::*;

pub const DEPTH_BOUNDS_FORMAT: Format = Format::R16G16_UNORM;

mod depth_bounds_reduction_shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/depth_bounds_reduction_pass/compute.glsl"
        }
    }

    use super::*;
    pub fn get_shader<'a>(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }
}

#[derive(Clone)]
pub struct DepthBoundsReductor {
    vulkan_common: VulkanCommon,
    tile_size: [u32; 2],

    output: Arc<AttachmentImage>,
    pipeline: Arc<ComputePipeline>,
    descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    //command_buffer: Arc<CommandBuffer<PoolAlloc=StandardCommandPoolAlloc>>,
}

impl DepthBoundsReductor {
    pub fn new(
        vulkan_common: &VulkanCommon,
        tile_size: [u32; 2],
        input: Arc<AttachmentImage>,
    ) -> Self {
        let vulkan_common = (*vulkan_common).clone();

        let output = AttachmentImage::with_usage(
            vulkan_common.get_device(),
            {
                let [width, height] = vulkan_common.get_config().resolution;
                [
                    (width as f32 / tile_size[0] as f32).ceil() as u32,
                    (height as f32 / tile_size[1] as f32).ceil() as u32,
                ]
            },
            DEPTH_BOUNDS_FORMAT,
            ImageUsage {
                sampled: true,
                storage: true,
                ..ImageUsage::none()
            },
        )
        .unwrap();

        let pipeline = ComputePipeline::new(
            vulkan_common.get_device(),
            depth_bounds_reduction_shaders::get_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let descriptor_set = PersistentDescriptorSet::new(
            pipeline.layout().set_layouts()[0].clone(),
            [
                WriteDescriptorSet::image_view_sampler(
                    0,
                    input.to_image_view(),
                    Sampler::new(
                        vulkan_common.get_device(),
                        SamplerCreateInfo::simple_repeat_linear(),
                    )
                    .unwrap(),
                ),
                WriteDescriptorSet::image_view(1, output.clone().to_image_view()),
            ],
        )
        .unwrap();

        /*let command_buffer = Arc::new(AutoCommandBufferBuilder::primary(vulkan_common.get_device(), vulkan_common.get_compute_queue().family()).unwrap()
        .dispatch().unwrap()
        .build().unwrap());*/

        Self {
            vulkan_common,
            tile_size,
            output,
            pipeline,
            descriptor_set,
        }
    }

    pub fn udpate_resolution<I>(&mut self, input: I) -> Option<[u32; 2]>
    where
        I: ImageAccess,
    {
        if self.output.dimensions() != input.dimensions() {
            let res = if let ImageDimensions::Dim2d { width, height, .. } = input.dimensions() {
                [
                    (width as f32 / self.tile_size[0] as f32).ceil() as u32,
                    (height as f32 / self.tile_size[1] as f32).ceil() as u32,
                ]
            } else {
                return None;
            };

            self.output =
                AttachmentImage::new(self.vulkan_common.get_device(), res, DEPTH_BOUNDS_FORMAT)
                    .unwrap();
            Some(res)
        } else {
            None
        }
    }

    //resolution of the texture where each textel is a tile
    fn get_resolution(&self) -> [u32; 2] {
        if let ImageDimensions::Dim2d { width, height, .. } = self.output.dimensions() {
            [width, height]
        } else {
            //should never get there
            panic!("")
        }
    }

    pub fn reduce(
        &mut self,
        input: Arc<AttachmentImage>,
        builder: &mut UnsafeCommandBufferBuilder,
    ) {
        self.udpate_resolution(&input);
        let res = if let ImageDimensions::Dim2d { width, height, .. } = input.dimensions() {
            [width, height]
        } else {
            return;
        };

        /*let command_buffer = Arc::new({
            let mut builder = AutoCommandBufferBuilder::primary(
                self.vulkan_common.get_device(),
                self.vulkan_common.get_compute_queue().family(),
            )
            .unwrap();
            builder
                .dispatch(
                    [res[0] / 16, res[1] / 16, 1],
                    self.pipeline.clone(),
                    self.descriptor_set.clone(),
                    (),
                )
                .unwrap();
            builder.build().unwrap()
        });*/

        unsafe {
            builder.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                self.pipeline.layout(),
                0,
                [self.descriptor_set.inner()].iter().map(|s| *s),
                [].iter().map(|s: &u32| (*s).clone()),
            );
            builder.bind_pipeline_compute(&self.pipeline);

            let third_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        all_graphics: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        ..AccessFlags::all()
                    },
                    destination_stages: PipelineStages{
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_write: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::Undefined,
                    new_layout: ImageLayout::General,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.output.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.output.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            builder.pipeline_barrier(&third_barrier);

            let [width, height] = res;
            builder.dispatch([
                (width as f32 / self.tile_size[0] as f32).ceil() as u32,
                (height as f32 / self.tile_size[1] as f32).ceil() as u32,
                1,
            ]);

            let third_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        shader_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages{
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::General,
                    new_layout: ImageLayout::General,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.output.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.output.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            builder.pipeline_barrier(&third_barrier);
        }
    }

    pub fn get_output_texture(&self) -> Arc<AttachmentImage> {
        self.output.clone()
    }
}

use gltf::accessor::Item;
use vulkano;
use vulkano::command_buffer::RenderPassBeginInfo;
use vulkano::descriptor_set::WriteDescriptorSet;
use vulkano::pipeline::graphics::rasterization::RasterizationState;
use vulkano::pipeline::StateMode;
use vulkano::render_pass::FramebufferCreateInfo;
use vulkano::sampler::SamplerMipmapMode::{self, Nearest};
use vulkano::swapchain::Swapchain;
use vulkano_shaders::shader;

use std::any::Any;
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::iter;
use std::iter::Iterator;
use std::marker::{Send, Sync};
use std::ops::Bound::*;
use std::ops::Range;
use std::ops::RangeBounds;
use std::sync::Arc;

use self::particle_fx::ParticleSystem;
use self::vulkano::device::Device;
use self::vulkano::format;
use self::vulkano::format::ClearValue;
use self::vulkano::image::attachment::AttachmentImage;
use self::vulkano::memory::DeviceMemoryAllocationError;
use self::vulkano::pipeline::graphics::{
    depth_stencil, input_assembly::IndexType, vertex_input::BuffersDefinition, viewport::Viewport,
    GraphicsPipeline,
};
use self::vulkano::pipeline::{ComputePipeline, Pipeline, PipelineBindPoint};
use self::vulkano::render_pass::{Framebuffer, RenderPass, Subpass};
use self::vulkano::sampler::Sampler;
use super::common::vulkan::*;
use crate::game_kernel::core_systems::static_mesh;
use game_kernel_utils::{mat3_extend, mat4_transponse, submat3, AutoIndexMap, KeyType};
use vulkano::buffer::{BufferAccess, BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::submit::{SubmitCommandBufferBuilder, SubmitPresentBuilder};
use vulkano::command_buffer::sys::UnsafeCommandBufferBuilderBindVertexBuffer;
use vulkano::command_buffer::{
    pool::standard::StandardCommandPool, sys::UnsafeCommandBufferBuilder, AutoCommandBufferBuilder,
    CommandBufferLevel, CommandBufferUsage, PrimaryCommandBuffer, SubpassContents,
};
use vulkano::descriptor_set::{
    layout::DescriptorSetLayout, DescriptorSet, PersistentDescriptorSet,
};
use vulkano::image::{ImageAccess, ImageLayout};
use vulkano::image::{ImageSubresourceRange, ImageUsage};
use vulkano::pipeline::graphics::depth_stencil::{CompareOp, DepthState};
use vulkano::sampler::{Filter, SamplerAddressMode, SamplerCreateInfo};
use vulkano::shader::EntryPoint;
use vulkano::shader::ShaderModule;
use vulkano::shader::ShaderStages;
use vulkano::single_pass_renderpass;
use vulkano::sync::GpuFuture;
use vulkano::sync::{self, DependencyInfo, ImageMemoryBarrier, MemoryBarrier};
use vulkano::sync::{AccessFlags, PipelineStages};
use vulkano::sync::{Event, Fence, Semaphore};

use bytemuck::{Pod, Zeroable};

//use self::vulkano_shaders;
use crate::game_kernel::subsystems::video::renderer::shader::AbstractShader;
use crate::log_msg;
use cgmath::*;

pub mod camera;
pub mod light;
pub mod material;
pub mod mesh;
pub mod model;
pub mod output;
pub mod particle_fx;
pub mod post_processing;
pub mod probe;
pub mod raytracing;
pub mod shader;
pub mod ssr;
pub mod texture;
pub mod ui_module;
pub mod utils;

mod reduction;

const MAX_NO_PARTICLES: u32 = 131072;

const DEPTH_FORMAT: format::Format = format::Format::D32_SFLOAT;
const NOR_ROUGH_FORMAT: format::Format = format::Format::A2B10G10R10_UNORM_PACK32;
const MESH_INDEX_FORMAT: format::Format = format::Format::R32_UINT;
const VELOCITY_BUFFER_FORMAT: format::Format = format::Format::R16G16_UNORM;
const HDR_FORMAT: format::Format = format::Format::B10G11R11_UFLOAT_PACK32;
const SHADOW_MAP_FORMAT: format::Format = format::Format::D16_UNORM;
const SHADOW_MAP_ATLAS_SIZE: u32 = 8192;
const DEFERRED_PROBES_ATLAS_SIZE: u32 = 8192;
const DEFERRED_PROBE_SIZE: u32 = 128;
const DEFERRED_PROBES_ALBEDO_FORMAT: format::Format = format::Format::R8G8B8A8_UNORM;
const DEFERRED_PROBES_DEPTH_FORMAT: format::Format = format::Format::D32_SFLOAT;
const UI_FORMAT: format::Format = format::Format::R8G8B8A8_UNORM;

mod pre_pass_shaders {
    pub mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "data/shaders/pre_pass/vert.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }

    pub mod fs {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "data/shaders/pre_pass/frag.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/include"]
        }
    }
}

mod skeletal_pre_pass_shaders {
    vulkano_shaders::shader! {
        ty: "vertex",
        path: "data/shaders/pre_pass/skeletal.vert",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        include: ["data/shaders/include"]
    }
}

mod third_pass_shaders {
    pub mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "data/shaders/third_pbr/vert.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }

    pub mod fs {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "data/shaders/third_pbr/frag.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }
}

mod skeletal_third_pass_shaders {
    pub mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "data/shaders/third_pbr/skeletal.vert",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/include"]
        }
    }
}

mod light_pass_shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/light_pass/compute.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/include"]
        }
    }

    use super::*;
    pub fn get_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }
}

mod tiling_pass_shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/tiling_pass/compute.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/include"]
        }
    }

    use super::*;
    pub fn get_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }
}

mod final_pass_shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty:  "compute",
            path: "data/shaders/final_pass/compute.glsl",
            include: ["data/shaders/final_pass/"]
        }
    }

    pub mod vs {
        vulkano_shaders::shader! {
            ty:  "vertex",
            path: "data/shaders/final_pass/vertex.glsl",
            include: ["data/shaders/final_pass/"]
        }
    }

    pub mod fs {
        vulkano_shaders::shader! {
            ty:  "fragment",
            path: "data/shaders/final_pass/fragment.glsl",
            include: ["data/shaders/final_pass/"]
        }
    }

    use super::*;
    pub fn get_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }

    pub fn get_vertex_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        vs::load(device.clone()).unwrap()
    }

    pub fn get_fragment_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        fs::load(device.clone()).unwrap()
    }
}

mod shadow_mapping_offscreen_shaders {
    pub mod fs {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "data/shaders/shadow_mapping/offscreen.frag",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/include"]
        }
    }

    pub mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "data/shaders/shadow_mapping/offscreen.vert",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/include"]
        }
    }
}

/*struct ProjectionData
{
    model: cgmath::Matrix4<f32>,
    view: cgmath::Matrix4<f32>,
    proj: cgmath::Matrix4<f32>,
}*/

type ProjectionData = self::third_pass_shaders::vs::ty::ProjectionData;

//use self::pre_pass_shaders::fs::ty::PrevMvps;
use self::light_pass_shaders::cs::ty::{DirectionalLight, DirectionalLightData};
use self::pre_pass_shaders::vs::ty::{
    Projection as PreProjection, ProjectionData as PreProjectionData,
};
use self::tiling_pass_shaders::cs::ty::NLightsBlock;
use self::tiling_pass_shaders::cs::ty::ProjectionData as TilingProjectionData;
use self::tiling_pass_shaders::cs::ty::TileData;
use self::tiling_pass_shaders::cs::ty::{AreaLight, AreaLightData};
use self::tiling_pass_shaders::cs::ty::{PointLight, PointLightData};
use self::tiling_pass_shaders::cs::ty::{ReflectionProbe, ReflectionProbeData};
use self::tiling_pass_shaders::cs::ty::{SpotLight, SpotLightData};

use self::light_pass_shaders::cs::ty::ShadowData;
use self::shadow_mapping_offscreen_shaders::vs::ty::ProjectionData as ShadowProjectionData;

use self::light_pass_shaders::cs::ty::Res;

//    layout(set = 0, binding = 7) buffer TilingInfo
//   {
//       ivec2 tile_size;
//       int max_items;
//       int max_items_tile;
//   } tiling_info;
use self::tiling_pass_shaders::cs::ty::TilingInfo;

fn vec2_to_array<T>(v: cgmath::Vector2<T>) -> [T; 2] {
    [v.x, v.y]
}

fn vec3_to_array<T>(v: cgmath::Vector3<T>) -> [T; 3] {
    [v.x, v.y, v.z]
}

fn vec4_to_array<T>(v: cgmath::Vector4<T>) -> [T; 4] {
    [v.x, v.y, v.z, v.w]
}

impl DirectionalLight {
    pub fn new(light: &light::DirectionalLight, shadows: u32) -> Self {
        Self {
            color: vec3_to_array(light.color),
            direction: vec3_to_array(light.direction),
            angle: light.angle,
            shadows,
            n_cascades: light.n_cascades,
            cascade_scale: light.cascade_scale,

            _dummy0: [0u8; 4],
            _dummy1: [0u8; 4],
        }
    }

    pub fn zero() -> Self {
        Self {
            color: [0f32; 3],
            direction: [0f32; 3],
            angle: 0f32,
            shadows: 0,
            n_cascades: 0,
            cascade_scale: 0f32,

            _dummy0: [0u8; 4],
            _dummy1: [0u8; 4],
        }
    }
}

impl PointLight {
    pub fn new(light: &light::PointLight, shadows: u32) -> Self {
        Self {
            color: vec4_to_array(light.color.extend(0f32)),
            position: vec4_to_array(light.position.extend(0f32)),

            size: light.size,
            influence_radius: light.influence_radius,
            shadows,

            _dummy0: [0u8; 4],
        }
    }

    pub fn zero() -> Self {
        Self {
            color: [0f32; 4],
            position: [0f32; 4],

            size: 0f32,
            influence_radius: 0f32,
            shadows: 0,

            _dummy0: [0u8; 4],
        }
    }
}

impl SpotLight {
    pub fn new(light: &light::SpotLight, shadows: u32) -> Self {
        Self {
            color: vec3_to_array(light.color),
            position: vec3_to_array(light.position),
            direction: vec3_to_array(light.direction),

            size: light.size,
            cone_length: light.cone_length,
            cone_angle: (light.cone_angle / 360f32 * 3.1415), //.tan(),
            shadows,

            _dummy0: [0; 4],
            _dummy1: [0; 4],
            _dummy2: [0; 4],
        }
    }

    pub fn zero() -> Self {
        Self {
            color: [0f32; 3],
            position: [0f32; 3],
            direction: [0f32; 3],

            size: 0f32,
            cone_length: 0f32,
            cone_angle: 0f32,
            shadows: 0,

            _dummy0: [0; 4],
            _dummy1: [0; 4],
            _dummy2: [0; 4],
        }
    }
}

impl AreaLight {
    pub fn new(light: &light::AreaLight, shadows: u32) -> Self {
        Self {
            color: vec3_to_array(light.color),
            position: vec3_to_array(light.position),
            normal: vec3_to_array(light.normal),
            tangent: vec3_to_array(light.tangent),
            size: vec2_to_array(light.size),
            influence_radius: light.influence_radius,
            double_sided: if light.double_sided { 1u32 } else { 0u32 },
            shadows,

            _dummy0: [0; 4],
            _dummy1: [0; 4],
            _dummy2: [0; 4],
            _dummy3: [0; 4],
            _dummy4: [0; 12],
        }
    }

    pub fn zero() -> Self {
        Self {
            color: [0f32; 3],
            position: [0f32; 3],
            normal: [0f32; 3],
            tangent: [0f32; 3],
            size: [0f32; 2],
            influence_radius: 0f32,
            double_sided: 0,
            shadows: 0,

            _dummy0: [0; 4],
            _dummy1: [0; 4],
            _dummy2: [0; 4],
            _dummy3: [0; 4],
            _dummy4: [0; 12],
        }
    }
}

struct LightsBuffers {
    directional_light: Arc<CpuAccessibleBuffer<[DirectionalLight]>>,
    point_light: Arc<CpuAccessibleBuffer<[PointLight]>>,
    spot_light: Arc<CpuAccessibleBuffer<[SpotLight]>>,
    area_light: Arc<CpuAccessibleBuffer<[AreaLight]>>,
    reflection_probe: Arc<CpuAccessibleBuffer<[ReflectionProbe]>>,
    n_lights: Arc<CpuAccessibleBuffer<NLightsBlock>>,
}

#[derive(Clone, Copy, Debug)]
struct ShadowAtlasTile {
    start: Vector2<u16>,
    size: u32,
}

impl LightsBuffers {
    pub fn new(vulkan_common: &VulkanCommon) -> Result<Self, DeviceMemoryAllocationError> {
        unsafe {
            Ok(Self {
                directional_light: CpuAccessibleBuffer::uninitialized_array(
                    vulkan_common.get_device(),
                    100,
                    BufferUsage::all(),
                    false,
                )?,
                point_light: CpuAccessibleBuffer::uninitialized_array(
                    vulkan_common.get_device(),
                    100,
                    BufferUsage::all(),
                    false,
                )?,
                spot_light: CpuAccessibleBuffer::uninitialized_array(
                    vulkan_common.get_device(),
                    100,
                    BufferUsage::all(),
                    false,
                )?,
                area_light: CpuAccessibleBuffer::uninitialized_array(
                    vulkan_common.get_device(),
                    100,
                    BufferUsage::all(),
                    false,
                )?,
                reflection_probe: CpuAccessibleBuffer::uninitialized_array(
                    vulkan_common.get_device(),
                    100,
                    BufferUsage::all(),
                    false,
                )?,
                n_lights: CpuAccessibleBuffer::from_data(
                    vulkan_common.get_device(),
                    BufferUsage::all(),
                    false,
                    NLightsBlock {
                        n_lights: [0; 4],
                        n_directional_lights: [0; 4],
                    },
                )?,
            })
        }
    }
}

struct ModelPipelines {
    pre_pipeline: Arc<GraphicsPipeline>,
    pre_descriptor_set_layout: Option<Arc<DescriptorSetLayout>>,
    pre_second_descriptor_set_layout: Option<Arc<DescriptorSetLayout>>,
    pre_third_descriptor_set_layout: Option<Arc<DescriptorSetLayout>>,
    pre_pass_descriptor_set: Option<Arc<dyn DescriptorSet + Send + Sync>>,
    pre_pass_second_descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    third_pass_pipeline: Arc<GraphicsPipeline>,
    third_descriptor_set_layout: Option<Arc<DescriptorSetLayout>>,
    third_second_descriptor_set_layout: Option<Arc<DescriptorSetLayout>>,
    third_third_descriptor_set_layout: Option<Arc<DescriptorSetLayout>>,
    third_pass_descriptor_set: [Arc<dyn DescriptorSet + Send + Sync>; 2],
}

/*struct TilingInfo
{
    tile_size: [u32; 2],
    max_items: u32,
    max_items_tile: u32,
}*/

const ITEMS_PER_TILE: u32 = 100;
const TILE_SIZE: [u32; 2] = [16, 16];

impl TilingInfo {
    pub fn get_default(resolution: [u32; 2]) -> Self {
        Self {
            tile_size: TILE_SIZE,
            max_items: ITEMS_PER_TILE
                / (resolution[0] * resolution[1]) as u32
                / (TILE_SIZE[0] * TILE_SIZE[1]),
            max_items_tile: ITEMS_PER_TILE,
        }
    }

    pub fn get_buffer(
        &self,
        vulkan_common: VulkanCommon,
    ) -> Result<Arc<DeviceLocalBuffer<Self>>, DeviceMemoryAllocationError> {
        DeviceLocalBuffer::new(
            vulkan_common.get_device(),
            BufferUsage::all(),
            [vulkan_common.get_compute_queue().family()].iter().cloned(),
        )
    }

    pub fn n_tiles(resolution: [u32; 2]) -> [u32; 2] {
        [
            (resolution[0] as f32 / TILE_SIZE[0] as f32).ceil() as u32,
            (resolution[1] as f32 / TILE_SIZE[1] as f32).ceil() as u32,
        ]
    }

    pub fn dispatch_size(resolution: [u32; 2], n_workgroups: [u32; 2]) -> [u32; 3] {
        let n_tiles = Self::n_tiles(resolution);
        [
            (n_tiles[0] as f32 / n_workgroups[0] as f32).ceil() as u32,
            (n_tiles[1] as f32 / n_workgroups[1] as f32).ceil() as u32,
            1,
        ]
    }
}

pub trait RendererHook: Any {
    fn init<V: VulkanBasic>(
        vulkan_common: V,
        depth: Arc<AttachmentImage>,
        norm_rough: Arc<AttachmentImage>,
        velocity: Arc<AttachmentImage>,
        hdr: Arc<AttachmentImage>,
    ) -> Self
    where
        Self: Sized;
    fn frame_start(&mut self, camera: &camera::Camera) {}
    fn post_hdr_render(&mut self, _cbb: &mut UnsafeCommandBufferBuilder) {}

    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

pub struct FinalObjects {
    final_render_pass: Arc<RenderPass>,
    final_pipeline: Arc<GraphicsPipeline>,
    final_descriptor_sets: Vec<Arc<dyn DescriptorSet + Send + Sync>>, //two for each swapchain image
    final_framebuffers: Vec<Arc<Framebuffer>>,
    output_module: Box<dyn output::RenderOutput>,
    idx: usize,
}

impl FinalObjects {
    pub fn new(
        vulkan_common: impl VulkanBasic,
        output_module: Box<dyn output::RenderOutput>,
        processed_hdr: Arc<AttachmentImage>,
    ) -> Self {
        let nearest_sampler =
            Sampler::new(vulkan_common.get_device(), SamplerCreateInfo::default()).unwrap();

        let final_render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: DontCare,
                    store: Store,
                    format: output_module.format(),//swapchain.get_swapchain_images()[0].format(),
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        }
        .unwrap();

        let final_pipeline = GraphicsPipeline::start()
            .vertex_input_state(BuffersDefinition::new())
            .cull_mode_disabled()
            .viewports_dynamic_scissors_irrelevant(1)
            .viewports(iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: [
                    vulkan_common.get_config().resolution[0] as f32,
                    vulkan_common.get_config().resolution[1] as f32,
                ],
                depth_range: 0.0..1.0,
            }))
            .depth_stencil_disabled()
            .depth_write(false)
            .vertex_shader(
                final_pass_shaders::get_vertex_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .fragment_shader(
                final_pass_shaders::get_fragment_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .render_pass(Subpass::from(final_render_pass.clone(), 0).unwrap())
            .build(vulkan_common.get_device())
            .unwrap();

        let mut final_descriptor_sets: Vec<Arc<dyn DescriptorSet + Send + Sync>> = Vec::new();
        let mut final_framebuffers = vec![];
        for swapchain_image in output_module.images().iter() {
            final_descriptor_sets.push({
                PersistentDescriptorSet::new(
                    final_pipeline.clone().layout().set_layouts()[0].clone(),
                    [WriteDescriptorSet::image_view_sampler(
                        0,
                        processed_hdr.clone().to_image_view(),
                        nearest_sampler.clone(),
                    )],
                )
                .unwrap() as Arc<dyn DescriptorSet + Send + Sync>
            });

            final_framebuffers.push(
                Framebuffer::new(
                    final_render_pass.clone(),
                    FramebufferCreateInfo {
                        attachments: vec![swapchain_image.clone().to_image_view()],
                        ..Default::default()
                    },
                )
                .unwrap(),
            );
        }

        Self {
            final_render_pass,
            final_pipeline,
            final_descriptor_sets,
            final_framebuffers,
            idx: 0,
            output_module,
        }
    }

    pub fn advance_frame(&mut self, acquire_semaphore: Option<&Semaphore>) {
        self.idx = self.output_module.next_idx(acquire_semaphore);
    }

    pub fn get_next_framebuffer(&self) -> Arc<Framebuffer> {
        self.final_framebuffers[self.idx].clone()
    }

    pub fn image(&self) -> Arc<dyn ImageAccess> {
        self.output_module.images()[self.idx].clone()
    }

    pub fn descriptor_set(&self) -> Arc<dyn DescriptorSet + Send + Sync> {
        self.final_descriptor_sets[self.idx].clone()
    }

    pub fn present(&self, present_semaphore: Option<&Semaphore>, queue: &vulkano::device::Queue) {
        self.output_module
            .present(present_semaphore, queue, self.idx)
    }

    pub fn present_image_layout(&self) -> vulkano::image::ImageLayout {
        self.output_module.present_image_layout()
    }

    pub fn signals_acquire_semaphore(&self) -> bool {
        self.output_module.signals_acquire_semaphore()
    }

    pub fn take_output(self) -> Box<dyn output::RenderOutput> {
        self.output_module
    }
}

pub struct Renderer {
    vulkan_common: VulkanCommon,

    //scene data
    models: AutoIndexMap<(KeyType, model::Model, Arc<dyn DescriptorSet>)>,
    pipelines: AutoIndexMap<ModelPipelines>,
    model_descriptor_sets: AutoIndexMap<PersistentDescriptorSet<()>>,
    lights: AutoIndexMap<light::Light>,
    light_update_range: Range<u8>,
    hooks: Vec<Box<dyn RendererHook>>,

    command_pool: Arc<StandardCommandPool>,

    nearest_sampler: Arc<Sampler>,
    linear_sampler: Arc<Sampler>,

    //first pass
    pre_projection_matrix_buffer: Arc<CpuAccessibleBuffer<PreProjection>>,
    prev_mpvs_buffer: Arc<DeviceLocalBuffer<[[[f32; 4]; 4]]>>,
    prev_mpvs: Vec<[[f32; 4]; 4]>,
    pre_render_pass: Arc<RenderPass>,
    static_mesh_pre_vs: shader::RuntimeShader,
    static_mesh_pre_fs: shader::RuntimeShader,
    skeletal_mesh_pre_vs: shader::RuntimeShader,
    depth_image: Arc<AttachmentImage>,
    norm_rough_image: Arc<AttachmentImage>,
    mesh_index_image: Arc<AttachmentImage>,
    velocity_buffer_image: Arc<AttachmentImage>,
    pre_frame_buffer: Arc<Framebuffer>,
    pre_semaphore: Arc<Semaphore>,

    //depth buffer reduction
    depth_bounds_reductor: reduction::DepthBoundsReductor,

    //second pass
    tiling_info: TilingInfo,
    tiling_info_buffer: Arc<DeviceLocalBuffer<TilingInfo>>,
    lights_buffers: LightsBuffers,
    res_buffer: Arc<DeviceLocalBuffer<Res>>,

    tiles_buffer: Arc<DeviceLocalBuffer<TileData>>,
    tiling_pipeline: Arc<ComputePipeline>,
    tiling_descriptor_set: Arc<dyn DescriptorSet>,
    n_lights: NLightsBlock,

    light_pipeline: Arc<ComputePipeline>,
    light_descriptor_set: Arc<dyn DescriptorSet>,
    light_diffuse: Arc<AttachmentImage>,
    light_specular: Arc<AttachmentImage>,

    light_pass_command_buffer: Arc<LifetimedCommandBuffer>, //Arc<dyn CommandBuffer<PoolAlloc=StandardCommandPoolAlloc>>,

    pre_third_event: Event,
    //third pass
    third_render_pass: Arc<RenderPass>,
    static_mesh_third_vs: shader::RuntimeShader,
    skeletal_mesh_third_vs: shader::RuntimeShader,
    unprocessed_hdr: Arc<AttachmentImage>,
    processed_hdr: Arc<AttachmentImage>,
    third_frame_buffer: Arc<Framebuffer>,

    //shadow mapping
    shadow_atlas_image: Arc<AttachmentImage>,
    shadow_data: Arc<DeviceLocalBuffer<ShadowData>>,
    shadow_offscreen_render_pass: Arc<RenderPass>,
    shadow_offscreen_frame_buffer: Arc<Framebuffer>,
    shadow_offscreen_pipeline: Arc<GraphicsPipeline>,
    shadow_offscreen_descriptor_set_layout: Option<Arc<DescriptorSetLayout>>,
    shadow_transforms: [Matrix4<f32>; 16],
    shadow_tiles: [ShadowAtlasTile; 16],
    n_shadows: u32,

    //deferred probes gi baking
    //deferred_probes_albedo_atlas: Arc<AttachmentImage>,
    //deferred_probes_depth_atlas: Arc<AttachmentImage>,
    //deferred_probes_data<DeviceLocalBuffer<>>,

    //post processing
    half_res_specular: [Arc<AttachmentImage>; 2],
    half_res_diffuse: [Arc<AttachmentImage>; 2],

    //ssr
    ssr: ssr::Ssr,

    //compositing
    ui_image: Arc<AttachmentImage>,
    final_objects: Option<FinalObjects>,
    viewport: Viewport,

    //particle system
    particle_systems: AutoIndexMap<particle_fx::ParticleSystem>,
    quads_index_buffer: Arc<DeviceLocalBuffer<[u32]>>,

    prev_frame: Option<Arc<dyn GpuFuture>>,
    frames_count: u64,
}

impl Renderer {
    pub fn init<W: Send + Sync + 'static>(
        vulkan_common: &VulkanCommon,
        swapchain: &SwapChain<W>,
    ) -> Option<Self> {
        let vulkan_common = (*vulkan_common).clone();
        let models = AutoIndexMap::new();
        let pipelines = AutoIndexMap::new();
        let model_descriptor_sets = AutoIndexMap::new();
        let lights = AutoIndexMap::new();

        let command_pool = vulkan_common.get_graphics_command_pool();

        let nearest_sampler =
            Sampler::new(vulkan_common.get_device(), SamplerCreateInfo::default()).unwrap();

        let linear_sampler = Sampler::new(
            vulkan_common.get_device(),
            SamplerCreateInfo::simple_repeat_linear_no_mipmap(),
        )
        .unwrap();

        let pre_projection_matrix_buffer = unsafe {
            CpuAccessibleBuffer::uninitialized(
                vulkan_common.get_device(),
                BufferUsage {
                    storage_buffer: true,
                    uniform_buffer: true,
                    ..BufferUsage::none()
                },
                false,
            )
            .unwrap()
        };

        let prev_mpvs_buffer = DeviceLocalBuffer::array(
            vulkan_common.get_device(),
            8192,
            BufferUsage {
                transfer_dst: true,
                storage_buffer: true,
                uniform_buffer: true,
                ..BufferUsage::none()
            },
            [vulkan_common.get_graphics_queue_family()].iter().cloned(),
        )
        .unwrap();

        let prev_mpvs = vec![];

        let pre_render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: DontCare,
                    store: Store,
                    format: NOR_ROUGH_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::Undefined,
                    final_layout: ImageLayout::ColorAttachmentOptimal,
                },
                mesh_index: {
                    load: Clear,
                    store: Store,
                    format: MESH_INDEX_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::Undefined,
                    final_layout: ImageLayout::ColorAttachmentOptimal,
                },
                velocity: {
                    load: Clear,
                    store: Store,
                    format: VELOCITY_BUFFER_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::Undefined,
                    final_layout: ImageLayout::ColorAttachmentOptimal,
                },
                depth: {
                    load: Clear,
                    store: Store,
                    format: DEPTH_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::Undefined,
                    final_layout: ImageLayout::DepthStencilAttachmentOptimal,
                }
            },
            pass: {
                color: [color, mesh_index, velocity],
                depth_stencil: {depth}
            }
        }
        .unwrap();

        let static_mesh_pre_vs = unsafe {
            let s = pre_pass_shaders::vs::load(vulkan_common.get_device()).unwrap();
            shader::RuntimeShader::from_entry_point(s.entry_point("main").unwrap(), s.clone())
        };
        let static_mesh_pre_fs = unsafe {
            let s = pre_pass_shaders::fs::load(vulkan_common.get_device()).unwrap();
            shader::RuntimeShader::from_entry_point(s.entry_point("main").unwrap(), s.clone())
        };
        let skeletal_mesh_pre_vs = unsafe {
            let s = skeletal_pre_pass_shaders::load(vulkan_common.get_device()).unwrap();
            shader::RuntimeShader::from_entry_point(s.entry_point("main").unwrap(), s.clone())
        };

        let depth_image = AttachmentImage::sampled(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            DEPTH_FORMAT,
        )
        .unwrap();
        let norm_rough_image = AttachmentImage::sampled(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            NOR_ROUGH_FORMAT,
        )
        .unwrap();

        let mesh_index_image = AttachmentImage::sampled(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            MESH_INDEX_FORMAT,
        )
        .unwrap();

        let velocity_buffer_image = AttachmentImage::sampled(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            VELOCITY_BUFFER_FORMAT,
        )
        .unwrap();

        let pre_frame_buffer = Framebuffer::new(
            pre_render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![
                    norm_rough_image.clone().to_image_view(),
                    mesh_index_image.clone().to_image_view(),
                    velocity_buffer_image.clone().to_image_view(),
                    depth_image.clone().to_image_view(),
                ],
                ..Default::default()
            },
        )
        .unwrap();

        let pre_semaphore = Arc::new(Semaphore::from_pool(vulkan_common.get_device()).unwrap());

        let tiling_info = TilingInfo::get_default(vulkan_common.get_config().resolution);

        let depth_bounds_reductor = reduction::DepthBoundsReductor::new(
            &vulkan_common,
            tiling_info.tile_size,
            depth_image.clone(),
        );

        let lights_buffers = {
            let lights_buffers = LightsBuffers::new(&vulkan_common);
            if lights_buffers.is_err() {
                return None;
            }
            lights_buffers.unwrap()
        };

        let tiles_buffer = DeviceLocalBuffer::new(
            vulkan_common.get_device(),
            BufferUsage::all(),
            [vulkan_common.get_compute_queue().family()].iter().cloned(),
        )
        .unwrap();

        let tiling_pipeline: Arc<ComputePipeline> = ComputePipeline::new(
            vulkan_common.get_device(),
            tiling_pass_shaders::get_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let tiling_info_buffer = tiling_info.get_buffer(vulkan_common.clone()).unwrap();

        //fills the buffer
        {
            let mut builder = AutoCommandBufferBuilder::primary(
                vulkan_common.get_device(),
                vulkan_common.get_graphics_queue_family(),
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap();
            //todo remove hacky cast
            builder
                .update_buffer(
                    unsafe { &*(&tiling_info as *const _) },
                    tiling_info_buffer.clone(),
                    0,
                )
                .unwrap();

            let command_buffer = builder.build().unwrap();

            command_buffer
                .execute(vulkan_common.get_graphics_queue())
                .unwrap()
                .then_signal_fence_and_flush()
                .unwrap()
                .wait(None)
                .unwrap();
        }

        let tiling_descriptor_set = {
            PersistentDescriptorSet::new(
                tiling_pipeline.clone().layout().set_layouts()[0].clone(),
                [
                    WriteDescriptorSet::image_view_sampler(
                        0,
                        depth_bounds_reductor
                            .get_output_texture()
                            .clone()
                            .to_image_view(),
                        Sampler::new(
                            vulkan_common.get_device(),
                            SamplerCreateInfo::simple_repeat_linear(),
                        )
                        .unwrap(),
                    ),
                    WriteDescriptorSet::buffer(1, lights_buffers.point_light.clone()),
                    WriteDescriptorSet::buffer(2, lights_buffers.spot_light.clone()),
                    WriteDescriptorSet::buffer(3, lights_buffers.area_light.clone()),
                    WriteDescriptorSet::buffer(4, lights_buffers.reflection_probe.clone()),
                    WriteDescriptorSet::buffer(5, lights_buffers.n_lights.clone()),
                    WriteDescriptorSet::buffer(6, tiles_buffer.clone()),
                    WriteDescriptorSet::buffer(7, tiling_info_buffer.clone()),
                ],
            )
            .unwrap()
        };

        let n_lights = NLightsBlock {
            n_lights: [0; 4],
            n_directional_lights: [0; 4],
        };

        let light_pipeline: Arc<ComputePipeline> = ComputePipeline::new(
            vulkan_common.get_device(),
            light_pass_shaders::get_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let light_diffuse = AttachmentImage::with_usage(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            HDR_FORMAT,
            ImageUsage {
                sampled: true,
                storage: true,
                ..ImageUsage::none()
            },
        )
        .unwrap();

        let light_specular = AttachmentImage::with_usage(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            HDR_FORMAT,
            ImageUsage {
                sampled: true,
                storage: true,
                ..ImageUsage::none()
            },
        )
        .unwrap();

        let res_buffer = DeviceLocalBuffer::new(
            vulkan_common.get_device(),
            BufferUsage::all(),
            [vulkan_common.get_compute_queue().family()].iter().cloned(),
        )
        .unwrap();

        let shadow_atlas_image = AttachmentImage::sampled(
            vulkan_common.get_device(),
            [SHADOW_MAP_ATLAS_SIZE, SHADOW_MAP_ATLAS_SIZE],
            SHADOW_MAP_FORMAT,
        )
        .unwrap();

        let shadow_data = DeviceLocalBuffer::new(
            vulkan_common.get_device(),
            BufferUsage {
                storage_buffer: true,
                uniform_buffer: true,
                transfer_dst: true,
                ..BufferUsage::none()
            },
            [vulkan_common.get_graphics_queue().family()]
                .iter()
                .cloned(),
        )
        .unwrap();

        let light_descriptor_set = {
            PersistentDescriptorSet::new(
                light_pipeline.clone().layout().set_layouts()[0].clone(),
                [
                    WriteDescriptorSet::image_view_sampler(
                        0,
                        depth_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        1,
                        norm_rough_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view(2, light_diffuse.clone().to_image_view()),
                    WriteDescriptorSet::image_view(3, light_specular.clone().to_image_view()),
                    WriteDescriptorSet::buffer(4, lights_buffers.directional_light.clone()),
                    WriteDescriptorSet::buffer(5, lights_buffers.point_light.clone()),
                    WriteDescriptorSet::buffer(6, lights_buffers.spot_light.clone()),
                    WriteDescriptorSet::buffer(7, lights_buffers.area_light.clone()),
                    //WriteDescriptorSet::buffer(8, lights_buffers.reflection_probe.clone()),
                    WriteDescriptorSet::buffer(9, lights_buffers.n_lights.clone()),
                    WriteDescriptorSet::buffer(10, tiles_buffer.clone()),
                    WriteDescriptorSet::buffer(11, res_buffer.clone()),
                    WriteDescriptorSet::image_view_sampler(
                        12,
                        shadow_atlas_image.clone().to_image_view(),
                        Sampler::new(
                            vulkan_common.get_device(),
                            SamplerCreateInfo {
                                mag_filter: Filter::Linear,
                                min_filter: Filter::Linear,
                                mipmap_mode: SamplerMipmapMode::Nearest,
                                compare: Some(CompareOp::Less),
                                address_mode: [SamplerAddressMode::MirroredRepeat; 3],
                                ..Default::default()
                            },
                        )
                        .unwrap(), /*.filter(Filter::Linear)
                                   .mipmap_mode(SamplerMipmapMode::Nearest)
                                   .compare(Some(CompareOp::Less))
                                   .address_mode(SamplerAddressMode::MirroredRepeat)
                                   .build()
                                   .unwrap()*/
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        13,
                        shadow_atlas_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::buffer(14, shadow_data.clone()),
                ],
            )
            .unwrap()
        };

        /*let light_pass_command_buffer = Arc::new(AutoCommandBufferBuilder::primary(vulkan_common.get_device(), vulkan_common.get_compute_queue().family()).unwrap()
        .dispatch(
                    {let res = vulkan_common.get_config().resolution; [res[0]/tiling_info.tile_size[0] as u32, res[1]/tiling_info.tile_size[1] as u32, 1]},
                    tiling_pipeline.clone(),
                    tiling_descriptor_set.clone(),
                    ()).unwrap()
        .dispatch(
                    {let res = vulkan_common.get_config().resolution; [res[0], res[1], 1]},
                    light_pipeline.clone(),
                    light_descriptor_set.clone(),
                    ()).unwrap()
        .build().unwrap());*/
        let light_pass_command_buffer = Arc::new(unsafe {
            let mut builder = LifetimedCommandBufferBuilder::new(
                command_pool.clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::MultipleSubmit,
            )
            .unwrap();
            //generate per tile data
            builder
                .builder
                .bind_pipeline_compute(&tiling_pipeline.clone());
            builder.builder.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                tiling_pipeline.layout(),
                0,
                [tiling_descriptor_set.inner()].iter().map(|s| (*s).clone()),
                [].iter().map(|s: &u32| (*s).clone()),
            );
            //barrier to transition the depth buffer to a layout that is suitable for sampling it
            let first_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        late_fragment_tests: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        depth_stencil_attachment_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::DepthStencilAttachmentOptimal,
                    new_layout: ImageLayout::ShaderReadOnlyOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: depth_image.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(depth_image.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            builder.builder.pipeline_barrier(&first_barrier);
            builder.builder.dispatch({
                let res = vulkan_common.get_config().resolution;
                [
                    res[0] / tiling_info.tile_size[0] as u32,
                    res[1] / tiling_info.tile_size[1] as u32,
                    1,
                ]
            });
            //calculate light
            builder
                .builder
                .bind_pipeline_compute(&light_pipeline.clone());
            builder.builder.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                light_pipeline.layout(),
                0,
                [light_descriptor_set.inner()].iter().map(|s| (*s).clone()),
                [].iter().map(|s: &u32| (*s).clone()),
            );
            builder.builder.dispatch({
                let res = vulkan_common.get_config().resolution;
                [res[0], res[1], 1]
            });

            //barrier to transition the depth buffer back to a layout suitable for depth attachment
            let second_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        early_fragment_tests: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        depth_stencil_attachment_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::ShaderReadOnlyOptimal,
                    new_layout: ImageLayout::DepthStencilAttachmentOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: depth_image.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(depth_image.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            builder.builder.pipeline_barrier(&second_barrier);
            builder.build().unwrap()
        });

        let pre_third_event = Event::from_pool(vulkan_common.get_device()).unwrap();

        let third_render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: HDR_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::ColorAttachmentOptimal,
                    final_layout: ImageLayout::General,
                },
                color1: {
                    load: Clear,
                    store: Store,
                    format: HDR_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::ColorAttachmentOptimal,
                    final_layout: ImageLayout::General,
                },
                depth: {
                    load: Load,
                    store: Store,
                    format: DEPTH_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::DepthStencilAttachmentOptimal,
                    final_layout:   ImageLayout::DepthStencilAttachmentOptimal,
                }
            },
            pass: {
                color: [color, color1],
                depth_stencil: {depth}
            }
        }
        .unwrap();

        let static_mesh_third_vs = unsafe {
            let s = third_pass_shaders::vs::load(vulkan_common.get_device()).unwrap();
            shader::RuntimeShader::from_entry_point(s.entry_point("main").unwrap(), s.clone())
        };

        let skeletal_mesh_third_vs = unsafe {
            let s = skeletal_third_pass_shaders::vs::load(vulkan_common.get_device()).unwrap();
            shader::RuntimeShader::from_entry_point(s.entry_point("main").unwrap(), s.clone())
        };

        let unprocessed_hdr = AttachmentImage::sampled(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            HDR_FORMAT,
        )
        .unwrap();

        let processed_hdr = AttachmentImage::with_usage(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            HDR_FORMAT,
            ImageUsage {
                sampled: true,
                storage: true,
                ..ImageUsage::none()
            },
        )
        .unwrap();

        let third_frame_buffer = Framebuffer::new(
            third_render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![
                    unprocessed_hdr.clone().to_image_view(),
                    processed_hdr.clone().to_image_view(),
                    depth_image.clone().to_image_view(),
                ],
                ..Default::default()
            },
        )
        .unwrap();

        //shadow mapping

        let shadow_offscreen_render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                depth: {
                    load: Clear,
                    store: Store,
                    format: SHADOW_MAP_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::Undefined,
                    final_layout: ImageLayout::ShaderReadOnlyOptimal,
                }
            },
            pass: {
                color: [],
                depth_stencil: {depth}
            }
        }
        .unwrap();

        let shadow_offscreen_frame_buffer = Framebuffer::new(
            shadow_offscreen_render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![shadow_atlas_image.clone().to_image_view()],
                ..Default::default()
            },
        )
        .unwrap();

        let shadow_offscreen_vs =
            shadow_mapping_offscreen_shaders::vs::load(vulkan_common.get_device()).unwrap();
        let shadow_offscreen_fs =
            shadow_mapping_offscreen_shaders::fs::load(vulkan_common.get_device()).unwrap();

        let shadow_offscreen_pipeline = GraphicsPipeline::start()
            .vertex_input_single_buffer::<Vertex3>()
            .triangle_list()
            .viewports_dynamic_scissors_irrelevant(1)
            .depth_stencil_simple_depth()
            .depth_write(true)
            .vertex_shader(shadow_offscreen_vs.entry_point("main").unwrap(), ())
            .fragment_shader(shadow_offscreen_fs.entry_point("main").unwrap(), ())
            .render_pass(Subpass::from(shadow_offscreen_render_pass.clone(), 0).unwrap())
            .build(vulkan_common.get_device())
            .unwrap();

        let shadow_offscreen_descriptor_set_layout = shadow_offscreen_pipeline
            .layout()
            .set_layouts()
            .get(0)
            .cloned();

        //post processing
        let half_res = {
            let res = vulkan_common.get_config().resolution;
            [res[0] / 2, res[1] / 2]
        };

        let half_res_specular = [
            AttachmentImage::with_usage(
                vulkan_common.get_device(),
                half_res,
                HDR_FORMAT,
                ImageUsage {
                    sampled: true,
                    storage: true,
                    ..ImageUsage::none()
                },
            )
            .unwrap(),
            AttachmentImage::with_usage(
                vulkan_common.get_device(),
                half_res,
                HDR_FORMAT,
                ImageUsage {
                    sampled: true,
                    storage: true,
                    ..ImageUsage::none()
                },
            )
            .unwrap(),
        ];

        let half_res_diffuse = [
            AttachmentImage::with_usage(
                vulkan_common.get_device(),
                half_res,
                HDR_FORMAT,
                ImageUsage {
                    sampled: true,
                    storage: true,
                    ..ImageUsage::none()
                },
            )
            .unwrap(),
            AttachmentImage::with_usage(
                vulkan_common.get_device(),
                half_res,
                HDR_FORMAT,
                ImageUsage {
                    sampled: true,
                    storage: true,
                    ..ImageUsage::none()
                },
            )
            .unwrap(),
        ];

        //ssr
        let ssr = ssr::Ssr::new(
            vulkan_common.clone(),
            depth_image.clone(),
            norm_rough_image.clone(),
            half_res_specular.clone(),
            unprocessed_hdr.clone(),
            mesh_index_image.clone(),
            velocity_buffer_image.clone(),
            res_buffer.clone(),
        );

        let ui_image = AttachmentImage::sampled(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            UI_FORMAT,
        )
        .unwrap();

        let final_objects = Some(FinalObjects::new(
            vulkan_common.clone(),
            Box::new(output::SwapchainOutout::new(
                swapchain.clone(),
                vulkan_common.clone(),
            )) as _,
            processed_hdr.clone(),
        ));

        let viewport = Viewport {
            origin: [0.0, 0.0],
            dimensions: [
                vulkan_common.get_config().resolution[0] as f32,
                vulkan_common.get_config().resolution[1] as f32,
            ],
            depth_range: 0.0..1.0,
        };

        let particle_systems = AutoIndexMap::new();
        let quads_index_buffer = utils::get_quads_idx_buffer(
            vulkan_common.clone(),
            BufferUsage {
                index_buffer: true,
                ..BufferUsage::none()
            },
            MAX_NO_PARTICLES as usize,
            [
                vulkan_common.get_graphics_queue_family(),
                vulkan_common.get_transfer_queue_family(),
            ]
            .iter()
            .cloned(),
        );

        let prev_frame =
            Some(Arc::new(sync::now(vulkan_common.get_device())) as Arc<dyn GpuFuture>);

        let frames_count = 0;

        Some(Self {
            vulkan_common,

            models,
            pipelines,
            model_descriptor_sets,
            lights,
            light_update_range: 0..0,
            hooks: vec![],

            command_pool,

            nearest_sampler,
            linear_sampler,

            pre_projection_matrix_buffer,
            prev_mpvs_buffer,
            prev_mpvs,
            pre_render_pass,
            static_mesh_pre_vs,
            static_mesh_pre_fs,
            skeletal_mesh_pre_vs,
            depth_image,
            norm_rough_image,
            mesh_index_image,
            velocity_buffer_image,
            pre_frame_buffer,
            pre_semaphore,

            depth_bounds_reductor,

            tiling_info,
            tiling_info_buffer,
            lights_buffers,
            res_buffer,

            tiles_buffer,
            tiling_pipeline,
            tiling_descriptor_set,
            n_lights,

            light_pipeline,
            light_descriptor_set,
            light_diffuse,
            light_specular,

            light_pass_command_buffer,

            pre_third_event,
            third_render_pass,
            static_mesh_third_vs,
            skeletal_mesh_third_vs,
            unprocessed_hdr,
            processed_hdr,
            third_frame_buffer,

            shadow_atlas_image,
            shadow_offscreen_render_pass,
            shadow_offscreen_frame_buffer,
            shadow_offscreen_pipeline,
            shadow_offscreen_descriptor_set_layout,
            shadow_data,
            shadow_transforms: [Matrix4::from_scale(1.0); 16],
            shadow_tiles: [ShadowAtlasTile {
                start: Vector2::new(0u16, 0u16),
                size: 0,
            }; 16],
            n_shadows: 0,

            half_res_diffuse,
            half_res_specular,

            ssr,

            ui_image,
            final_objects,
            viewport,

            particle_systems,
            quads_index_buffer,
            prev_frame,
            frames_count,
        })
    }

    pub fn get_vulkan_common(&self) -> VulkanCommon {
        self.vulkan_common.clone()
    }

    /*fn get_pre_pass_shaders(&self, model: &model::Model) -> ShaderPair
    {
        let mut norm = false;
        let mut rough = false;
        let mut met = false;
        for texture::Texture{texture:_, texture_type} in model.material.textures
        {
            match texture_type
            {
                texture::TextureType::Normal => norm=true,
                texture::TextureType::Roughness => rough=true,
                texture::TextureType::Metalness => met=true,
            }

            if norm && rough && met{break;}
        }

        self.pre_pass_shaders.get_combo(norm, rough, met)
    }*/

    pub fn replace_output(
        &mut self,
        new_output: Box<dyn output::RenderOutput>,
    ) -> Box<dyn output::RenderOutput> {
        let final_objects = FinalObjects::new(
            self.vulkan_common.clone(),
            new_output,
            self.processed_hdr.clone(),
        );
        self.final_objects
            .replace(final_objects)
            .unwrap()
            .take_output()
    }

    pub fn set_viewport_size(&mut self, size: [u32; 2]) {
        let res = self.vulkan_common.config.resolution;

        self.viewport.dimensions = [size[0].min(res[0]) as f32, size[1].min(res[1]) as f32];
    }

    //generate pipelines for a given model
    fn generate_pipelines(&self, model: &model::Model, idx: usize) -> ModelPipelines {
        let pre_vs = unsafe {
            match *model.mesh {
                mesh::Mesh::StaticMesh(_) => self.static_mesh_pre_vs.graphics_entry_point(),
                mesh::Mesh::SkeletalMesh(_) => self.skeletal_mesh_pre_vs.graphics_entry_point(),
            }
        };

        let material = model.material.clone();

        let pre_fs = unsafe {
            material
                .pre_fragment
                .as_ref()
                .map(|f| f.graphics_entry_point())
                .unwrap_or_else(|| self.static_mesh_pre_fs.graphics_entry_point())
        };

        let third_vs = match *model.mesh {
            mesh::Mesh::StaticMesh(_) => unsafe {
                material
                    .custom_vertex_shader
                    .as_ref()
                    .unwrap()
                    .graphics_entry_point()
            },
            mesh::Mesh::SkeletalMesh(_) => unsafe {
                self.skeletal_mesh_third_vs.graphics_entry_point()
            },
        };

        let third_fs = unsafe { material.fragment_shader.graphics_entry_point() };

        /*let third_vs =
            third_pass_shaders::vs::Shader::load(self.vulkan_common.get_device()).unwrap();
        let third_fs =
            third_pass_shaders::fs::Shader::load(self.vulkan_common.get_device()).unwrap();*/

        let pre_pipeline: Arc<GraphicsPipeline> = match *model.mesh {
            mesh::Mesh::StaticMesh(ref static_mesh) => {
                GraphicsPipeline::start()
                    //.vertex_input_single_buffer::<Vertex3>()
                    //.vertex_input(TwoBuffersDefinition::<Vertex3, mesh::Normal>::new())
                    .vertex_input_state(mesh::get_static_mesh_definition())
                    .rasterization_state(
                        RasterizationState::new()
                            .cull_mode(static_mesh.cull_mode.vulkano_cull_mode()),
                    )
                    .triangle_list()
                    .viewports_dynamic_scissors_irrelevant(1)
                    /*.viewports(iter::once(Viewport {
                        origin: [0.0, 0.0],
                        dimensions: [
                            self.vulkan_common.get_config().resolution[0] as f32,
                            self.vulkan_common.get_config().resolution[1] as f32,
                        ],
                        depth_range: 0.0..1.0,
                    }))*/
                    .depth_stencil_simple_depth()
                    .depth_write(true)
                    .vertex_shader(pre_vs, ())
                    .fragment_shader(pre_fs, ())
                    .render_pass(Subpass::from(self.pre_render_pass.clone(), 0).unwrap())
                    .build(self.vulkan_common.get_device())
                    .unwrap()
            }
            mesh::Mesh::SkeletalMesh(ref skeletal_mesh) => {
                GraphicsPipeline::start()
                    //.vertex_input_single_buffer::<Vertex3>()
                    //.vertex_input(TwoBuffersDefinition::<Vertex3, mesh::Normal>::new())
                    .vertex_input_state(mesh::get_skeletal_mesh_definition())
                    .rasterization_state(
                        RasterizationState::new()
                            .cull_mode(skeletal_mesh.mesh.cull_mode.vulkano_cull_mode()),
                    )
                    .triangle_list()
                    .viewports_dynamic_scissors_irrelevant(1)
                    /*.viewports(iter::once(Viewport {
                        origin: [0.0, 0.0],
                        dimensions: [
                            self.vulkan_common.get_config().resolution[0] as f32,
                            self.vulkan_common.get_config().resolution[1] as f32,
                        ],
                        depth_range: 0.0..1.0,
                    }))*/
                    .depth_stencil_simple_depth()
                    .depth_write(true)
                    .vertex_shader(pre_vs, ())
                    .fragment_shader(pre_fs, ())
                    .render_pass(Subpass::from(self.pre_render_pass.clone(), 0).unwrap())
                    .build(self.vulkan_common.get_device())
                    .unwrap()
            }
        };

        let (
            third_pass_pipeline,
            third_descriptor_set_layout,
            third_second_descriptor_set_layout,
            third_third_descriptor_set_layout,
        ) = {
            let agp: Arc<GraphicsPipeline>;
            let descriptor_set_layout;
            let second_descriptor_set_layout;
            let third_descriptor_set_layout;
            let gp = GraphicsPipeline::start()
                .viewports_dynamic_scissors_irrelevant(1)
                /*.viewports(iter::once(Viewport {
                    origin: [0.0, 0.0],
                    dimensions: [
                        self.vulkan_common.get_config().resolution[0] as f32,
                        self.vulkan_common.get_config().resolution[1] as f32,
                    ],
                    depth_range: 0.0..1.0,
                }))*/
                .depth_stencil(depth_stencil::DepthStencilState {
                    depth: Some(DepthState {
                        enable_dynamic: false,
                        write_enable: StateMode::Fixed(false),
                        compare_op: StateMode::Fixed(CompareOp::Equal),
                    }),
                    depth_bounds: None,
                    stencil: None,
                })
                .vertex_shader(third_vs, ())
                .fragment_shader(third_fs, ())
                .render_pass(Subpass::from(self.third_render_pass.clone(), 0).unwrap());

            match *model.mesh {
                mesh::Mesh::StaticMesh(ref static_mesh) => {
                    agp =
                        /*gp.vertex_input(TwoBuffersDefinition::<Vertex3, mesh::Normal>::new())
                        .triangle_list()
                        .build(self.vulkan_common.get_device())
                        .unwrap(),*/
                        gp.vertex_input_state(mesh::get_static_mesh_definition())
                            .triangle_list()
                            .rasterization_state(RasterizationState::new().cull_mode(static_mesh.cull_mode.vulkano_cull_mode()))
                            .build(self.vulkan_common.get_device())
                            .unwrap();

                    descriptor_set_layout = agp.layout().set_layouts().get(0).cloned();
                    second_descriptor_set_layout = agp.layout().set_layouts().get(1).cloned();
                    third_descriptor_set_layout = agp.layout().set_layouts().get(2).cloned();
                }
                mesh::Mesh::SkeletalMesh(ref skeletal_mesh) => {
                    agp = gp
                        .vertex_input_state(mesh::get_skeletal_mesh_definition())
                        .triangle_list()
                        .rasterization_state(
                            RasterizationState::new()
                                .cull_mode(skeletal_mesh.mesh.cull_mode.vulkano_cull_mode()),
                        )
                        .build(self.vulkan_common.get_device())
                        .unwrap();

                    descriptor_set_layout = agp.layout().set_layouts().get(0).cloned();
                    second_descriptor_set_layout = agp.layout().set_layouts().get(1).cloned();
                    third_descriptor_set_layout = agp.layout().set_layouts().get(2).cloned();
                }
            }

            (
                agp,
                descriptor_set_layout,
                second_descriptor_set_layout,
                third_descriptor_set_layout,
            )
        };

        let pre_descriptor_set_layout = pre_pipeline.layout().set_layouts().get(0).cloned();
        let pre_second_descriptor_set_layout = pre_pipeline.layout().set_layouts().get(1).cloned();
        let pre_third_descriptor_set_layout = pre_pipeline.layout().set_layouts().get(2).cloned();

        let pre_pass_descriptor_set = if let Some(ref set_layout) = pre_descriptor_set_layout {
            Some({
                let mut descriptors = vec![WriteDescriptorSet::image_view_sampler(
                    0,
                    model
                        .material
                        .get_normal_map()
                        .unwrap()
                        .texture
                        .clone()
                        .to_image_view(),
                    model.material.get_normal_map().unwrap().sampler.clone(),
                )];
                if let Some(metalness_roughness_map) = material.get_metalness_roughness_map() {
                    descriptors.push(WriteDescriptorSet::image_view_sampler(
                        descriptors.len() as u32,
                        metalness_roughness_map.texture.clone().to_image_view(),
                        metalness_roughness_map.sampler.clone(),
                    ));
                } else {
                    descriptors.push(WriteDescriptorSet::image_view_sampler(
                        descriptors.len() as u32,
                        model
                            .material
                            .get_roughness_map()
                            .unwrap()
                            .texture
                            .clone()
                            .to_image_view(),
                        model.material.get_roughness_map().unwrap().sampler.clone(),
                    ));
                    descriptors.push(WriteDescriptorSet::image_view_sampler(
                        descriptors.len() as u32,
                        model
                            .material
                            .get_metalness_map()
                            .unwrap()
                            .texture
                            .clone()
                            .to_image_view(),
                        model.material.get_metalness_map().unwrap().sampler.clone(),
                    ));
                }

                PersistentDescriptorSet::new(set_layout.clone(), descriptors.into_iter()).unwrap()
                    as Arc<dyn DescriptorSet + Send + Sync>
            })
        } else {
            None
        };

        let pre_pass_second_descriptor_set = {
            PersistentDescriptorSet::new(
                pre_second_descriptor_set_layout.as_ref().unwrap().clone(),
                [
                    /*WriteDescriptorSet::buffer(0, Arc::new(self.pre_projection_matrix_buffer.clone())),*/
                    WriteDescriptorSet::buffer(1, self.prev_mpvs_buffer.clone())
                ],
            ).unwrap() as Arc<dyn DescriptorSet + Send + Sync>
        };

        let build_third_descriptor_set = |i: usize| {
            PersistentDescriptorSet::new(
                third_descriptor_set_layout.as_ref().unwrap().clone(),
                [
                    WriteDescriptorSet::image_view_sampler(
                        0,
                        self.light_diffuse.clone().to_image_view(),
                        self.nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        1,
                        self.light_specular.clone().to_image_view(),
                        self.nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        2,
                        self.half_res_specular[i].clone().to_image_view(),
                        self.linear_sampler.clone(),
                    ),
                ],
            )
            .unwrap() as Arc<dyn DescriptorSet + Send + Sync>
        };
        let third_pass_descriptor_set =
            [build_third_descriptor_set(0), build_third_descriptor_set(1)];

        ModelPipelines {
            pre_pipeline,
            pre_descriptor_set_layout,
            pre_second_descriptor_set_layout,
            pre_third_descriptor_set_layout,
            pre_pass_descriptor_set,
            pre_pass_second_descriptor_set,

            third_pass_pipeline,
            third_descriptor_set_layout,
            third_second_descriptor_set_layout,
            third_third_descriptor_set_layout,
            third_pass_descriptor_set,
        }
    }

    fn compare_pipelines(a: &ModelPipelines, b: &ModelPipelines) -> bool {
        &a.pre_pipeline == &b.pre_pipeline && &a.third_pass_pipeline == &b.third_pass_pipeline
    }

    pub fn add_model(&mut self, model: model::Model) -> KeyType {
        let new_pipeline = self.generate_pipelines(&model, self.models.len());

        let mut pid = None;

        if !self
            .pipelines
            .iter()
            .any(|(_, pipeline)| Self::compare_pipelines(pipeline, &new_pipeline))
        {
            pid = Some(self.pipelines.insert(new_pipeline));
            log_msg!(
                "creaed pipeline {}, n.o. pipelines {}",
                pid.unwrap(),
                self.pipelines.len()
            );
        }

        let pid = pid.unwrap();

        let pipeline = self.pipelines.get(pid).unwrap();

        let albedo_map = model
            .material
            .get_texture_by_usage(texture::TextureUsage::Color)
            .unwrap();
        let metalness_map = model.material.get_metalness_map().unwrap();
        let emissive_map = model.material.get_emissive_map().unwrap();
        let descriptor_set = PersistentDescriptorSet::new(
            pipeline
                .third_second_descriptor_set_layout
                .as_ref()
                .unwrap()
                .clone(),
            [
                WriteDescriptorSet::image_view_sampler(
                    0,
                    albedo_map.texture.clone().to_image_view(),
                    albedo_map.sampler.clone(),
                ),
                WriteDescriptorSet::image_view_sampler(
                    1,
                    metalness_map.texture.clone().to_image_view(),
                    metalness_map.sampler.clone(),
                ),
                WriteDescriptorSet::image_view_sampler(
                    2,
                    emissive_map.texture.clone().to_image_view(),
                    emissive_map.sampler.clone(),
                ),
            ],
        )
        .unwrap();

        self.models.insert((pid as u64, model, descriptor_set))
    }

    pub fn remove_model(&mut self, index: KeyType) {
        /*let i = self.models.iter().enumerate().find(|(i, (k, _))| *k == index);
        if i.is_none() {
            return;
        }
        let (i, _) = i.unwrap();
        self.prev_mpvs.remove(i);*/
        let r = self.models.remove(index);
        if r.is_none() {
            return;
        }
        let (_pid, _, _) = r.unwrap();
        //TODO keep track of unused pipelines
    }

    pub fn get_models(&self) -> impl Iterator<Item = (KeyType, &model::Model)> {
        self.models.iter().map(|(k, (_, m, _))| (k, m))
    }

    pub fn add_render_hook<H>(&mut self) -> KeyType
    where
        H: RendererHook + 'static,
    {
        let hook = H::init(
            self.vulkan_common.clone(),
            self.depth_image.clone(),
            self.norm_rough_image.clone(),
            self.velocity_buffer_image.clone(),
            self.processed_hdr.clone(),
        );
        self.hooks.push(Box::new(hook));
        self.hooks.len() as u64 - 1
    }

    pub fn get_render_hook(&self, k: KeyType) -> Option<&Box<dyn RendererHook>> {
        self.hooks.get(k as usize)
    }

    pub fn get_render_hook_mut(&mut self, k: KeyType) -> Option<&mut Box<dyn RendererHook>> {
        self.hooks.get_mut(k as usize)
    }

    pub fn add_particle_system(&mut self, particle_system: particle_fx::ParticleSystem) -> KeyType {
        self.particle_systems.insert(particle_system)
    }

    pub fn remove_particle_system(&mut self, key: KeyType) -> Option<particle_fx::ParticleSystem> {
        self.particle_systems.remove(key)
    }

    pub fn get_particle_system(&self, k: KeyType) -> Option<&particle_fx::ParticleSystem> {
        self.particle_systems.get(k)
    }

    pub fn get_particle_system_mut(
        &mut self,
        k: KeyType,
    ) -> Option<&mut particle_fx::ParticleSystem> {
        self.particle_systems.get_mut(k)
    }

    pub fn get_particle_systems(&self) -> impl Iterator<Item = (KeyType, &ParticleSystem)> {
        self.particle_systems.iter()
    }

    pub fn get_model(&self, key: KeyType) -> Option<&model::Model> {
        self.models.get(key).map(|m| &m.1)
    }

    pub fn get_model_mut(&mut self, key: KeyType) -> Option<&mut model::Model> {
        self.models.get_mut(key).map(|m| &mut m.1)
    }

    pub fn get_lights(&self) -> impl Iterator<Item = (u64, &light::Light)> {
        self.lights.iter()
    }

    pub fn remove_light(&mut self, key: KeyType) {
        self.lights.remove(key);
        self.update_light_range(self.lights.len() as u8 - 1);
    }

    fn update_light_range(&mut self, key: u8) {
        if self.light_update_range.start_bound() == self.light_update_range.end_bound() {
            self.light_update_range = (key)..(key + 1);
        } else {
            //updates the range
            self.light_update_range = (key).min({
                match self.light_update_range.start_bound() {
                    Included(i) => *i,
                    _ => panic!("TODO"),
                }
            })..(key).max({
                match self.light_update_range.end_bound() {
                    Excluded(i) => *i,
                    _ => panic!("TODO"),
                }
            });
        }
    }

    pub fn add_light(&mut self, light: light::Light) -> KeyType {
        let key = self.lights.insert(light);

        self.update_light_range(key as u8);

        key
    }

    pub fn update_light(&mut self, key: KeyType, new_light: light::Light) {
        if let Some(light) = self.lights.get_mut(key) {
            *light = new_light;

            self.update_light_range(key as u8);
        }
    }

    pub fn current_frame_idx(&self) -> usize {
        (self.frames_count % 2) as usize
    }

    fn aspect_ratio(&self) -> f32 {
        self.viewport.dimensions[0] as f32 / self.viewport.dimensions[1] as f32
    }

    fn first_pass(
        &mut self,
        camera: &camera::Camera,
        builder: &mut UnsafeCommandBufferBuilder,
    ) -> Vec<Arc<dyn DescriptorSet + Send + Sync>> {
        let aspect_ratio = self.aspect_ratio();
        let view_matrix = camera.get_view();
        let proj_matrix = camera.get_proj(aspect_ratio);

        ///////////////////////////////////////////////////////////////////////////////////////////
        let mut first_command_buffer_builder = builder;

        self.prev_mpvs
            .resize(self.models.len(), Matrix4::identity().into());

        {
            *self.pre_projection_matrix_buffer.write().unwrap() = PreProjection {
                view: view_matrix.into(),
                projection: proj_matrix.into(),
                res: self.vulkan_common.get_config().resolution,
            };
        }

        unsafe {
            first_command_buffer_builder.update_buffer(
                self.prev_mpvs.as_slice(),
                &self
                    .prev_mpvs_buffer
                    .slice(0..self.models.len() as u64)
                    .unwrap(),
                0,
            );

            //sync prev mpvs
            //sync pre_projection_matrix_buffer
            let barrier = {
                let memory_barrier = MemoryBarrier {
                    source_stages: PipelineStages {
                        transfer: true,
                        host: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        transfer_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        //vertex_shader: true,
                        all_graphics: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        memory_read: true,
                        ..AccessFlags::none()
                    },
                    ..Default::default()
                };

                DependencyInfo {
                    memory_barriers: vec![memory_barrier].into(),
                    ..Default::default()
                }
            };
            first_command_buffer_builder.pipeline_barrier(&barrier);
        }

        unsafe {
            first_command_buffer_builder.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![
                        None,
                        Some(u32::MAX.into()),
                        Some([1.0, 1.0].into()),
                        Some(1.0.into()),
                    ],
                    ..RenderPassBeginInfo::framebuffer(self.pre_frame_buffer.clone())
                },
                SubpassContents::Inline,
            );

            first_command_buffer_builder.set_viewport(0, [self.viewport.clone()].iter().cloned());
        }

        let mut descriptor_sets = vec![];

        let mut last_pipeline_id = None;
        let mut last_pipeline = None;

        for (idx, (mesh_index, (pipeline, model, _))) in self.models.iter().enumerate() {
            let model_matrix = model.transform;

            let uniform_data = PreProjectionData {
                mvp: (proj_matrix * view_matrix * model_matrix).into(),
                normal_matrix: mat3_extend(submat3(mat4_transponse(
                    &model_matrix.invert().unwrap_or(Matrix4::identity()),
                ))),
                prev_mvp_index: [idx as u32, mesh_index as u32, 0, 0],
            };

            let pipeline_bind_required;
            if last_pipeline_id.is_none() || last_pipeline_id.unwrap() != *pipeline {
                last_pipeline = Some(self.pipelines.get(*pipeline).unwrap());
                last_pipeline_id = Some(*pipeline);
                pipeline_bind_required = true;
            } else {
                pipeline_bind_required = false;
            }
            let pipeline = last_pipeline.unwrap();

            let mut first_descriptor_sets = vec![];
            if let Some(ref set) = pipeline.pre_pass_descriptor_set {
                first_descriptor_sets.push(set.clone());
            }

            let second_descriptor_set = pipeline.pre_pass_second_descriptor_set.clone();

            match model.mesh.as_ref() {
                mesh::Mesh::StaticMesh(static_mesh) => unsafe {
                    let mut pcbb = &mut first_command_buffer_builder;

                    pcbb.push_constants(
                        &pipeline.pre_pipeline.layout(),
                        ShaderStages {
                            vertex: true,
                            fragment: true,
                            ..ShaderStages::all()
                        },
                        0,
                        std::mem::size_of::<PreProjectionData>() as u32,
                        &uniform_data,
                    );
                    if pipeline_bind_required {
                        pcbb.bind_pipeline_graphics(&pipeline.pre_pipeline);
                    }
                    pcbb.bind_vertex_buffers(0, {
                        let mut builder = UnsafeCommandBufferBuilderBindVertexBuffer::new();
                        builder.add(&static_mesh.vertex_buffer);
                        builder.add(static_mesh.normal_buffer.as_ref().unwrap());
                        for uv_buffer in static_mesh.uv_buffers.iter() {
                            builder.add(&uv_buffer);
                        }
                        builder
                    });
                    pcbb.bind_index_buffer(&static_mesh.index_buffer, IndexType::U32);

                    if !first_descriptor_sets.is_empty() {
                        pcbb.bind_descriptor_sets(
                            PipelineBindPoint::Graphics,
                            &pipeline.pre_pipeline.layout(),
                            0,
                            first_descriptor_sets
                                .iter()
                                .chain([second_descriptor_set].iter())
                                .map(|d| d.inner()),
                            [].iter().map(|d| *d),
                        );
                    }
                    pcbb.draw_indexed(static_mesh.index_buffer.size() as u32 / 4, 1, 0, 0, 0);
                    //at last we update prev_mpv so that it will contan the previous mpv next frame
                },
                mesh::Mesh::SkeletalMesh(skeletal_mesh) => unsafe {
                    let mut pcbb = &mut first_command_buffer_builder;

                    pcbb.push_constants(
                        &pipeline.pre_pipeline.layout(),
                        ShaderStages {
                            vertex: true,
                            fragment: true,
                            ..ShaderStages::all()
                        },
                        0,
                        std::mem::size_of::<PreProjectionData>() as u32,
                        &uniform_data,
                    );
                    if pipeline_bind_required {
                        pcbb.bind_pipeline_graphics(&pipeline.pre_pipeline);
                    }
                    pcbb.bind_vertex_buffers(0, {
                        let mut builder = UnsafeCommandBufferBuilderBindVertexBuffer::new();
                        builder.add(&skeletal_mesh.mesh.vertex_buffer);
                        builder.add(skeletal_mesh.mesh.normal_buffer.as_ref().unwrap());
                        for uv_buffer in skeletal_mesh.mesh.uv_buffers.iter() {
                            builder.add(&uv_buffer);
                        }
                        builder.add(&skeletal_mesh.affecting_bones_buffer.clone());
                        builder.add(&skeletal_mesh.weights_buffer.clone());
                        builder
                    });
                    pcbb.bind_index_buffer(&skeletal_mesh.mesh.index_buffer, IndexType::U32);

                    if !first_descriptor_sets.is_empty() {
                        pcbb.bind_descriptor_sets(
                            PipelineBindPoint::Graphics,
                            &pipeline.pre_pipeline.layout(),
                            0,
                            first_descriptor_sets
                                .iter()
                                .chain([second_descriptor_set].iter())
                                .chain(
                                    [Self::add_and_get(&mut descriptor_sets, || {
                                        PersistentDescriptorSet::new(
                                            pipeline
                                                .pre_third_descriptor_set_layout
                                                .as_ref()
                                                .unwrap()
                                                .clone(),
                                            [WriteDescriptorSet::buffer(
                                                0,
                                                skeletal_mesh.skeleton_buffer.clone(),
                                            )],
                                        )
                                        .unwrap()
                                            as Arc<dyn DescriptorSet + Send + Sync>
                                    })]
                                    .iter(),
                                )
                                .map(|d| d.inner()),
                            [].iter().map(|d| *d),
                        );
                    }
                    pcbb.draw_indexed(
                        skeletal_mesh.mesh.index_buffer.size() as u32 / 4,
                        1,
                        0,
                        0,
                        0,
                    );
                    //at last we update prev_mpv so that it will contan the previous mpv next frame
                },
            }
            self.prev_mpvs[idx] = (proj_matrix * view_matrix * model_matrix).into();
        }

        unsafe {
            first_command_buffer_builder.end_render_pass();
            first_command_buffer_builder.set_event(
                &self.pre_third_event,
                PipelineStages {
                    color_attachment_output: true,
                    ..PipelineStages::none()
                },
            );
        }
        descriptor_sets
    }

    fn add_and_get_one<T: Clone, F: Fn() -> T>(v: &mut Vec<T>, el: F) -> T {
        if !v.is_empty() {
            return (*v.get(v.len() - 1).unwrap()).clone();
        }
        let el = el();
        v.push(el.clone());
        el
    }

    fn add_and_get<T: Clone, F: Fn() -> T>(v: &mut Vec<T>, el: F) -> T {
        let el = el();
        v.push(el.clone());
        el
    }

    fn third_pass(
        &mut self,
        camera: &camera::Camera,
    ) -> (
        LifetimedCommandBuffer,
        Vec<Arc<dyn DescriptorSet + Send + Sync>>,
    ) {
        let aspect_ratio = self.aspect_ratio();
        let view_matrix = camera.get_view();
        let proj_matrix = camera.get_proj(aspect_ratio);

        let mut third_command_buffer_builder = unsafe {
            LifetimedCommandBufferBuilder::new(
                self.command_pool.clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap()
        };
        unsafe {
            third_command_buffer_builder.builder.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![
                        Some([0.1, 0.1, 0.3].into()),
                        Some([0.1, 0.1, 0.3].into()),
                        None,
                    ],
                    ..RenderPassBeginInfo::framebuffer(self.third_frame_buffer.clone())
                },
                SubpassContents::Inline,
            );

            /*self.vulkan_common.get_device().pointers().CmdWaitEvents(
                third_command_buffer_builder.builder.internal_object(),
                1,
                &self.pre_third_event.internal_object() as *const _,
                0x00000400,
                0x00000100,
                0,
                std::ptr::null(),
                0,
                std::ptr::null(),
                0,
                std::ptr::null(),
            );*/

            third_command_buffer_builder
                .builder
                .set_viewport(0, [self.viewport.clone()].iter().cloned());

            third_command_buffer_builder.builder.reset_event(
                &self.pre_third_event,
                PipelineStages {
                    color_attachment_output: true,
                    ..PipelineStages::none()
                },
            );
        }

        let mut descriptor_sets = vec![];

        let mut last_pipeline_id = None;
        let mut last_pipeline = None;
        for (_i, (pipeline, model, model_descriptor_set)) in self.models.iter() {
            let model_matrix = model.transform;

            //if we were to calculate the matrix directly it would come out
            //slightly different due to float multiplication erros.
            //In order to avoid the same of order of the first pass is kept
            let uniform_data = ProjectionData {
                //view: view_matrix.into(),
                //proj: proj_matrix.into(),
                //model: model_matrix.into(),
                mvp: (proj_matrix * view_matrix * model_matrix).into(),
                normal_matrix: mat3_extend(submat3(mat4_transponse(
                    &model_matrix.invert().unwrap_or(Matrix4::identity()),
                ))),
            };
            //let uniform_subbufer = self.pre_uniform_buffer.next(uniform_data).unwrap();

            let pipeline_bind_required;
            if last_pipeline_id.is_none() || last_pipeline_id.unwrap() != *pipeline {
                last_pipeline = Some(self.pipelines.get(*pipeline).unwrap());
                last_pipeline_id = Some(*pipeline);
                pipeline_bind_required = true;
            } else {
                pipeline_bind_required = false;
            }
            let pipeline = last_pipeline.unwrap();

            let third_descriptor_set = pipeline.third_pass_descriptor_set.clone();
            match model.mesh.as_ref() {
                mesh::Mesh::StaticMesh(static_mesh) => unsafe {
                    let tcbb = &mut third_command_buffer_builder.builder;

                    tcbb.push_constants(
                        &pipeline.third_pass_pipeline.layout(),
                        ShaderStages {
                            vertex: true,
                            fragment: true,
                            ..ShaderStages::all()
                        },
                        0,
                        std::mem::size_of::<ProjectionData>() as u32,
                        &uniform_data,
                    );
                    if pipeline_bind_required {
                        tcbb.bind_pipeline_graphics(&pipeline.third_pass_pipeline);
                    }
                    tcbb.bind_vertex_buffers(0, {
                        let mut builder = UnsafeCommandBufferBuilderBindVertexBuffer::new();
                        builder.add(&static_mesh.vertex_buffer);
                        builder.add(static_mesh.normal_buffer.as_ref().unwrap());
                        for uv_buffer in static_mesh.uv_buffers.iter() {
                            builder.add(&uv_buffer);
                        }
                        builder
                    });
                    tcbb.bind_index_buffer(&static_mesh.index_buffer, IndexType::U32);
                    if pipeline_bind_required {
                        tcbb.bind_descriptor_sets(
                            PipelineBindPoint::Graphics,
                            &pipeline.third_pass_pipeline.layout(),
                            0,
                            [
                                third_descriptor_set[self.current_frame_idx()].inner(),
                                model_descriptor_set.inner(),
                            ]
                            .iter()
                            .map(|d| *d),
                            [].iter().map(|d| *d),
                        );
                    }
                    tcbb.draw_indexed(static_mesh.index_buffer.size() as u32 / 4, 1, 0, 0, 0);
                },

                mesh::Mesh::SkeletalMesh(skeletal_mesh) => unsafe {
                    let tcbb = &mut third_command_buffer_builder.builder;

                    tcbb.push_constants(
                        &pipeline.third_pass_pipeline.layout(),
                        ShaderStages {
                            vertex: true,
                            fragment: true,
                            ..ShaderStages::all()
                        },
                        0,
                        std::mem::size_of::<ProjectionData>() as u32,
                        &uniform_data,
                    );
                    if true || pipeline_bind_required {
                        tcbb.bind_pipeline_graphics(&pipeline.third_pass_pipeline);
                    }
                    tcbb.bind_vertex_buffers(0, {
                        let mut builder = UnsafeCommandBufferBuilderBindVertexBuffer::new();
                        builder.add(&skeletal_mesh.mesh.vertex_buffer);
                        builder.add(skeletal_mesh.mesh.normal_buffer.as_ref().unwrap());
                        for uv_buffer in skeletal_mesh.mesh.uv_buffers.iter() {
                            builder.add(&uv_buffer);
                        }
                        builder.add(&skeletal_mesh.affecting_bones_buffer.clone());
                        builder.add(&skeletal_mesh.weights_buffer.clone());
                        builder
                    });

                    tcbb.bind_index_buffer(&skeletal_mesh.mesh.index_buffer, IndexType::U32);

                    if pipeline_bind_required {
                        tcbb.bind_descriptor_sets(
                            PipelineBindPoint::Graphics,
                            &pipeline.third_pass_pipeline.layout(),
                            0,
                            [
                                third_descriptor_set[self.current_frame_idx()].clone(),
                                model_descriptor_set.clone(),
                                Self::add_and_get(&mut descriptor_sets, || {
                                    PersistentDescriptorSet::new(
                                        pipeline
                                            .third_third_descriptor_set_layout
                                            .as_ref()
                                            .unwrap()
                                            .clone(),
                                        [WriteDescriptorSet::buffer(
                                            0,
                                            skeletal_mesh.skeleton_buffer.clone(),
                                        )],
                                    )
                                    .unwrap()
                                        as Arc<dyn DescriptorSet + Send + Sync>
                                }),
                            ]
                            .iter()
                            .map(|ds| ds.inner()),
                            [].iter().map(|d| *d),
                        );
                    }

                    tcbb.draw_indexed(
                        skeletal_mesh.mesh.index_buffer.size() as u32 / 4,
                        1,
                        0,
                        0,
                        0,
                    );
                },
                //TODO: remove duplicated code(buffers)
            }
        }

        unsafe {
            let first_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        color_attachment_output: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        color_attachment_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::ColorAttachmentOptimal,
                    new_layout: ImageLayout::ShaderReadOnlyOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.unprocessed_hdr.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.unprocessed_hdr.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            //third_command_buffer_builder.pipeline_barrier(&first_barrier);
            third_command_buffer_builder.builder.end_render_pass();
        }

        (
            third_command_buffer_builder.build().unwrap(),
            descriptor_sets,
        )
    }

    fn directional_shadow_transform(
        light: light::DirectionalLight,
        camera: &camera::Camera,
        scale: f32,
    ) -> Matrix4<f32> {
        let eye = Vector3::new(camera.eye.x, camera.eye.y, camera.eye.z); //+ Vector3::new(light.direction.x, light.direction.y, light.direction.z).normalize() * -50.0 * scale;
        cgmath::ortho(
            -25f32 * scale,
            25f32 * scale,
            -25f32 * scale,
            25f32 * scale,
            -1500f32 * scale,
            500f32 * scale,
        ) * Matrix4::look_at_dir(
            cgmath::Point3::new(eye.x, eye.y, eye.z),
            light.direction,
            Vector3::new(1.0, 0.0, 0.0),
        )
    }

    fn spot_shadow_transform(light: light::SpotLight) -> Matrix4<f32> {
        let fov_rad: cgmath::Rad<f32> = cgmath::Rad::turn_div_2() * light.cone_angle / 180.0;
        cgmath::perspective(fov_rad, 1.0, light.cone_length / 100.0, light.cone_length)
            * Matrix4::look_at_dir(
                cgmath::Point3::new(light.position.x, light.position.y, light.position.z),
                light.direction,
                Vector3::new(1.0, 0.0, 0.0),
            )
    }

    fn area_shadow_transform(light: light::AreaLight) -> Matrix4<f32> {
        let fov_rad: cgmath::Rad<f32> = cgmath::Rad::turn_div_4();
        cgmath::perspective(
            fov_rad,
            1.0,
            light.influence_radius / 100.0,
            light.influence_radius,
        ) * Matrix4::look_at_dir(
            cgmath::Point3::new(light.position.x, light.position.y, light.position.z),
            light.normal,
            Vector3::new(1.0, 0.0, 0.0),
        )
    }

    fn update_lights_data(
        &mut self,
        command_buffer: &mut UnsafeCommandBufferBuilder,
        camera: &camera::Camera,
    ) {
        if self.light_update_range.start_bound() == self.light_update_range.end_bound() {
            return;
        } else {
            let mut directional_lights = vec![];
            let mut point_lights = vec![];
            let mut spot_lights = vec![];
            let mut area_lights = vec![];

            let mut shadow_transforms = vec![];
            let mut shadow_sizes = vec![];

            for (_, light) in self.lights.iter() {
                let shadow_index = if light.shadow_map_size.is_some() {
                    (shadow_transforms.len()) as u32
                } else {
                    0xffffffff
                };
                match light.light {
                    light::LightType::Directional(dlight) => {
                        directional_lights.push(DirectionalLight::new(&dlight, shadow_index));
                        if shadow_index != 0xffffffff {
                            for i in 0..dlight.n_cascades {
                                shadow_transforms.push(Self::directional_shadow_transform(
                                    dlight,
                                    camera,
                                    dlight.cascade_scale.powi(i as i32),
                                ));
                                shadow_sizes.push(light.shadow_map_size.unwrap());
                            }
                        }
                    }
                    light::LightType::Point(plight) => {
                        point_lights.push(PointLight::new(&plight, shadow_index));
                    }
                    light::LightType::Spot(slight) => {
                        spot_lights.push(SpotLight::new(&slight, shadow_index));
                        if shadow_index != 0xffffffff {
                            shadow_transforms.push(Self::spot_shadow_transform(slight));
                            shadow_sizes.push(light.shadow_map_size.unwrap());
                        }
                    }
                    light::LightType::Area(alight) => {
                        area_lights.push(AreaLight::new(&alight, shadow_index));
                        if shadow_index != 0xffffffff {
                            shadow_transforms.push(Self::area_shadow_transform(alight));
                            shadow_sizes.push(light.shadow_map_size.unwrap());
                        }
                    }
                    _ => (),
                }
            }

            if directional_lights.len() > 0 {
                unsafe {
                    command_buffer.update_buffer(
                        directional_lights
                            .iter()
                            .take(100)
                            .map(|e| *e)
                            /*.chain(
                                (0..(10 - directional_lights.len()))
                                    .map(|_| DirectionalLight::zero()),
                            )*/
                            .collect::<Vec<DirectionalLight>>()
                            .as_slice(), //.try_into()
                        &self
                            .lights_buffers
                            .directional_light
                            .slice(0..directional_lights.len().min(100) as u64)
                            .unwrap(),
                        0,
                    );
                }
            }

            if point_lights.len() > 0 {
                unsafe {
                    command_buffer.update_buffer(
                        point_lights
                            .iter()
                            .take(100)
                            .map(|e| *e)
                            //.chain((0..(10 - point_lights.len())).map(|_| PointLight::zero()))
                            .collect::<Vec<PointLight>>()
                            .as_slice(),
                        &self
                            .lights_buffers
                            .point_light
                            .slice(0..point_lights.len().min(100) as u64)
                            .unwrap(),
                        0,
                    );
                }
            }

            if spot_lights.len() > 0 {
                unsafe {
                    command_buffer.update_buffer(
                        spot_lights
                            .iter()
                            .take(100)
                            .map(|e| *e)
                            //.chain((0..(10 - spot_lights.len())).map(|_| SpotLight::zero()))
                            .collect::<Vec<SpotLight>>()
                            .as_slice(),
                        &self
                            .lights_buffers
                            .spot_light
                            .slice(0..spot_lights.len().min(100) as u64)
                            .unwrap(),
                        0,
                    );
                }
            }

            if area_lights.len() > 0 {
                unsafe {
                    command_buffer.update_buffer(
                        area_lights
                            .iter()
                            .take(100)
                            .map(|e| *e)
                            //.chain((0..(10 - area_lights.len())).map(|_| AreaLight::zero()))
                            .collect::<Vec<AreaLight>>()
                            .as_slice(),
                        &self
                            .lights_buffers
                            .area_light
                            .slice(0..area_lights.len().min(100) as u64)
                            .unwrap(),
                        0,
                    );
                }
            }

            self.n_lights = NLightsBlock {
                n_lights: [
                    point_lights.len() as i32,
                    spot_lights.len() as i32,
                    area_lights.len() as i32,
                    0,
                ],
                n_directional_lights: [directional_lights.len() as i32, 0, 0, 0],
            };

            unsafe {
                command_buffer.update_buffer(&self.n_lights, &self.lights_buffers.n_lights, 0);
            }

            //shadow data update
            let n_shadowed_lights = shadow_transforms.len();
            self.n_shadows = n_shadowed_lights as u32;
            let shadow_transforms_m: [Matrix4<f32>; 16] = shadow_transforms
                .into_iter()
                .chain((0..(16 - n_shadowed_lights)).map(|_| Matrix4::from_scale(1.0)))
                .take(16)
                .collect::<Vec<_>>()
                .as_slice()
                .try_into()
                .unwrap();

            let shadow_transforms: [[[f32; 4]; 4]; 16] = shadow_transforms_m
                .iter()
                .map(|m| {
                    let n: [[f32; 4]; 4] = (*m).into();
                    n
                })
                .collect::<Vec<_>>()
                .as_slice()
                .try_into()
                .unwrap();

            self.shadow_transforms = shadow_transforms_m;

            let shadow_sizes: Vec<_> = shadow_sizes
                .into_iter()
                .map(|s| s as u32)
                .enumerate()
                .collect();

            let shadow_tiles = Self::partition_shadow_atlas(shadow_sizes.clone());
            let shadow_tiles: [[f32; 4]; 16] = shadow_sizes
                .iter()
                .enumerate()
                .map(|(tile_id, _)| {
                    let tile = &shadow_tiles[&tile_id];
                    [
                        tile.start.x as f32 / SHADOW_MAP_ATLAS_SIZE as f32,
                        tile.start.y as f32 / SHADOW_MAP_ATLAS_SIZE as f32,
                        tile.size as f32 / SHADOW_MAP_ATLAS_SIZE as f32,
                        0.0002,
                    ] //TODO remove hardcoded bias
                })
                .chain((0..(16 - n_shadowed_lights)).map(|_| [0.0; 4]))
                .collect::<Vec<_>>()
                .as_slice()
                .try_into()
                .unwrap();

            self.shadow_tiles = shadow_tiles
                .iter()
                .map(|t| ShadowAtlasTile {
                    start: Vector2::new(
                        (t[0] * SHADOW_MAP_ATLAS_SIZE as f32) as u16,
                        (t[1] * SHADOW_MAP_ATLAS_SIZE as f32) as u16,
                    ),
                    size: (t[2] * SHADOW_MAP_ATLAS_SIZE as f32) as u32,
                })
                .collect::<Vec<_>>()
                .as_slice()
                .try_into()
                .unwrap();

            unsafe {
                command_buffer.update_buffer(
                    &ShadowData {
                        shadow_transforms,
                        shadow_maps: shadow_tiles,
                    },
                    &self.shadow_data,
                    0,
                );
            }
        }
    }

    pub fn get_2d_dispatch_size(size: [u32; 2], local_size: [u32; 2]) -> [u32; 3] {
        [
            (size[0] as f32 / local_size[0] as f32).ceil() as u32,
            (size[1] as f32 / local_size[1] as f32).ceil() as u32,
            1,
        ]
    }

    fn screen_dispatch_size(&self, local_size: [u32; 2]) -> [u32; 3] {
        let res = self.vulkan_common.get_config().resolution;
        [
            (res[0] as f32 / local_size[0] as f32).ceil() as u32,
            (res[1] as f32 / local_size[1] as f32).ceil() as u32,
            1,
        ]
    }

    fn second_pass(&mut self, camera: &camera::Camera, builder: &mut UnsafeCommandBufferBuilder) {
        let aspect_ratio = self.aspect_ratio();
        let model_view_matrix = camera.get_view() * camera.get_proj(aspect_ratio);

        unsafe {
            self.update_lights_data(builder, camera);

            self.render_shadow_maps(builder);

            //updates resolution data
            builder.update_buffer(
                &Res {
                    res: [
                        self.viewport.dimensions[0] as u32,
                        self.viewport.dimensions[1] as u32,
                    ],
                },
                &self.res_buffer,
                0,
            );

            //generate per tile data
            builder.bind_pipeline_compute(&self.tiling_pipeline.clone());
            builder.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                self.tiling_pipeline.layout(),
                0,
                [self.tiling_descriptor_set.inner()]
                    .iter()
                    .map(|s| (*s).clone()),
                [].iter().map(|s: &u32| (*s).clone()),
            );
            //barrier to transition the depth buffer to a layout that is suitable for sampling it
            let first_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        late_fragment_tests: true,
                        transfer: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        depth_stencil_attachment_write: true,
                        transfer_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::DepthStencilAttachmentOptimal,
                    new_layout: ImageLayout::ShaderReadOnlyOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.depth_image.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.depth_image.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            builder.pipeline_barrier(&first_barrier);
            builder.push_constants(
                self.tiling_pipeline.layout(),
                ShaderStages {
                    compute: true,
                    ..ShaderStages::all()
                },
                0,
                std::mem::size_of::<TilingProjectionData>() as u32,
                &TilingProjectionData {
                    mv: camera.get_view().into(),
                    p: {
                        let mut p = camera.get_proj(aspect_ratio);
                        let v = camera.get_eye().extend(0.0);
                        p[0][3] = v.x;
                        p[1][3] = v.y;
                        p[2][3] = v.z;
                        p.into()
                    },
                    //view_pos: camera.get_position().into(),
                },
            );
            builder.dispatch({
                let res = self.vulkan_common.get_config().resolution;
                TilingInfo::dispatch_size(res, [8, 8])
            });

            let second_barrier = {
                let memory_barrier = MemoryBarrier {
                    source_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        shader_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        compute_shader: true,
                        bottom_of_pipe: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    ..Default::default()
                };

                DependencyInfo {
                    memory_barriers: vec![memory_barrier].into(),
                    ..Default::default()
                }
            };
            builder.pipeline_barrier(&second_barrier);

            //TODO: move this barrier to an appropriate location
            /*let fourth_barrier = {
                let mut barrier_builder = UnsafeCommandBufferBuilderPipelineBarrier::new();
                barrier_builder.add_image_memory_barrier(
                    &self.depth_image,
                    0..1,
                    0..1,
                    PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    AccessFlags {
                        shader_write: true,
                        ..AccessFlags::none()
                    },
                    PipelineStages {
                        fragment_shader: true,
                        ..PipelineStages::none()
                    },
                    AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    false,
                    None,
                    ImageLayout::ColorAttachmentOptimal,
                    ImageLayout::ShaderReadOnlyOptimal,
                );
                barrier_builder
            };
            builder.pipeline_barrier(&fourth_barrier);*/

            //calculate light
            builder.bind_pipeline_compute(&self.light_pipeline.clone());
            builder.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                self.light_pipeline.layout(),
                0,
                [self.light_descriptor_set.inner()]
                    .iter()
                    .map(|s| (*s).clone()),
                [].iter().map(|s: &u32| (*s).clone()),
            );
            builder.dispatch(self.screen_dispatch_size([16, 16]));

            //barrier to transition the depth buffer back to a layout suitable for depth attachment
            let third_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        early_fragment_tests: true,
                        bottom_of_pipe: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        depth_stencil_attachment_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::ShaderReadOnlyOptimal,
                    new_layout: ImageLayout::General,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.depth_image.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.depth_image.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            builder.pipeline_barrier(&third_barrier);
        }
    }

    fn render_particles(
        &mut self,
        cbb: &mut UnsafeCommandBufferBuilder,
        delta_time: f32,
        camera: &camera::Camera,
    ) {
        for (_, particle_system) in self.particle_systems.iter_mut() {
            unsafe {
                particle_system.update_non_colliding(cbb, delta_time);
            }
        }

        let aspect_ratio = self.aspect_ratio();

        for (_, particle_system) in self.particle_systems.iter_mut() {
            unsafe {
                particle_system.render_particles(
                    cbb,
                    camera.get_view(),
                    camera.get_proj(aspect_ratio),
                    self.viewport.clone(),
                    self.quads_index_buffer.clone(),
                    delta_time,
                );
            }
        }
    }

    fn final_future<U, W>(
        &mut self,
        delta_time: f32,
        camera: &camera::Camera,
        ui: Option<&mut U>,
        swapchain: &SwapChain<W>,
    ) where
        U: ui_module::UiModule,
        W: Send + Sync + 'static,
    {
        /*let (swapchain_acquire_future, idx) =
        self.vulkan_common.get_next_swapchain_image_index()?;*/

        /*let future = Box::new(
            sync::now(self.vulkan_common.get_device())
                .join(swapchain_acquire_future)
                /*.then_execute(
                    self.vulkan_common.get_graphics_queue(),
                    self.final_command_buffers[idx].clone(),
                )
                .unwrap()*/,
        ) as Box<dyn GpuFuture>;*/

        //future.then_signal_fence_and_flush().unwrap().wait(None).unwrap();

        use vulkano::swapchain::{acquire_next_image_raw, AcquiredImage};

        let acquire_semaphore = Semaphore::from_pool(self.vulkan_common.get_device()).unwrap();

        /*let AcquiredImage {
            id: idx,
            suboptimal: _,
        } = unsafe {
            acquire_next_image_raw(
                &swapchain.get_swapchain(),
                None,
                Some(&acquire_semaphore),
                None,
            )
            .unwrap()
        };*/
        self.final_objects
            .as_mut()
            .unwrap()
            .advance_frame(Some(&acquire_semaphore));

        /*if let Some(ui) = ui {
            let mut cbb = AutoCommandBufferBuilder::primary(
                self.vulkan_common.get_device(),
                self.vulkan_common.get_graphics_queue_family(),
            )
            .unwrap();

            ui.render(
                &mut cbb,
                self.vulkan_common.get_graphics_queue(),
                self.ui_image.clone(),
            );

            let cb = cbb.build().unwrap();
            let ui_future = sync::now(self.vulkan_common.get_device());
            ui_future
                .then_execute(self.vulkan_common.get_graphics_queue(), cb)
                .unwrap()
                .then_signal_fence_and_flush()
                .unwrap()
                .wait(None)
                .unwrap();
        }*/

        let pcb = unsafe {
            let mut pcbb = LifetimedCommandBufferBuilder::new(
                self.command_pool.clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap();

            let present_layout = self.final_objects.as_ref().unwrap().present_image_layout();
            if (present_layout != ImageLayout::ColorAttachmentOptimal) {
                let barrier = {
                    let im = self.final_objects.as_ref().unwrap().image();
                    let image_barrier = ImageMemoryBarrier {
                        source_stages: PipelineStages {
                            compute_shader: true,
                            ..PipelineStages::none()
                        },
                        source_access: AccessFlags {
                            shader_read: true,
                            ..AccessFlags::none()
                        },
                        destination_stages: PipelineStages {
                            early_fragment_tests: true,
                            bottom_of_pipe: true,
                            ..PipelineStages::none()
                        },
                        destination_access: AccessFlags {
                            depth_stencil_attachment_read: true,
                            ..AccessFlags::none()
                        },
                        old_layout: present_layout,
                        new_layout: ImageLayout::ColorAttachmentOptimal,

                        subresource_range: ImageSubresourceRange {
                            aspects: im.format().aspects(),
                            mip_levels: 0..1,
                            array_layers: 0..1,
                        },
                        ..ImageMemoryBarrier::image(im.inner().image.clone())
                    };

                    DependencyInfo {
                        image_memory_barriers: vec![image_barrier].into(),
                        ..Default::default()
                    }
                };
                pcbb.builder.pipeline_barrier(&barrier);
            }

            self.render_particles(&mut pcbb.builder, delta_time, camera);

            for hook in self.hooks.iter_mut() {
                hook.post_hdr_render(&mut pcbb.builder);
            }

            pcbb.builder.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![None],
                    ..RenderPassBeginInfo::framebuffer(
                        self.final_objects.as_ref().unwrap().get_next_framebuffer(),
                    )
                },
                SubpassContents::Inline,
            );

            pcbb.builder
                .bind_pipeline_graphics(&self.final_objects.as_ref().unwrap().final_pipeline);
            pcbb.builder.bind_descriptor_sets(
                PipelineBindPoint::Graphics,
                &self.final_objects.as_ref().unwrap().final_pipeline.layout(),
                0,
                [self
                    .final_objects
                    .as_ref()
                    .unwrap()
                    .descriptor_set()
                    .inner()]
                .iter()
                .cloned(),
                [].iter().cloned(),
            );
            pcbb.builder.draw(3, 1, 0, 0);
            pcbb.builder.end_render_pass();

            let barrier = {
                let im = self.final_objects.as_ref().unwrap().image();
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        color_attachment_output: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        color_attachment_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        color_attachment_output: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        color_attachment_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::ColorAttachmentOptimal,
                    new_layout: ImageLayout::ColorAttachmentOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: im.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(im.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            pcbb.builder.pipeline_barrier(&barrier);

            if let Some(ui) = ui {
                unsafe {
                    ui.render(
                        &mut pcbb.builder,
                        self.vulkan_common.get_graphics_queue(),
                        self.final_objects.as_ref().unwrap().image().to_image_view(),
                    );
                }
            }

            if (present_layout != ImageLayout::ColorAttachmentOptimal) {
                let barrier = {
                    let im = self.final_objects.as_ref().unwrap().image();
                    let image_barrier = ImageMemoryBarrier {
                        source_stages: PipelineStages {
                            fragment_shader: true,
                            color_attachment_output: true,
                            ..PipelineStages::none()
                        },
                        source_access: AccessFlags {
                            color_attachment_write: true,
                            ..AccessFlags::none()
                        },
                        destination_stages: PipelineStages {
                            bottom_of_pipe: true,
                            color_attachment_output: true,
                            ..PipelineStages::none()
                        },
                        destination_access: AccessFlags {
                            color_attachment_read: true,
                            color_attachment_write: true,
                            ..AccessFlags::none()
                        },
                        old_layout: ImageLayout::ColorAttachmentOptimal,
                        new_layout: present_layout,

                        subresource_range: ImageSubresourceRange {
                            aspects: im.format().aspects(),
                            mip_levels: 0..1,
                            array_layers: 0..1,
                        },
                        ..ImageMemoryBarrier::image(im.inner().image.clone())
                    };

                    DependencyInfo {
                        image_memory_barriers: vec![image_barrier].into(),
                        ..Default::default()
                    }
                };
                pcbb.builder.pipeline_barrier(&barrier);
            }

            pcbb.build().unwrap()
        };

        let fence = Fence::from_pool(self.vulkan_common.get_device()).unwrap();
        let mut submission_builder = SubmitCommandBufferBuilder::new();
        let present_semaphore = Semaphore::from_pool(self.vulkan_common.get_device()).unwrap();
        unsafe {
            if self
                .final_objects
                .as_ref()
                .unwrap()
                .signals_acquire_semaphore()
            {
                submission_builder.add_wait_semaphore(
                    &acquire_semaphore,
                    PipelineStages {
                        all_commands: true,
                        ..PipelineStages::none()
                    },
                );
            }
            submission_builder.add_command_buffer(&pcb.cb);
            submission_builder.set_fence_signal(&fence);
            submission_builder.add_signal_semaphore(&present_semaphore);
        }

        submission_builder
            .submit(&self.vulkan_common.get_graphics_queue())
            .unwrap();
        fence.wait(None).unwrap();

        self.final_objects.as_ref().unwrap().present(
            Some(&present_semaphore),
            &self.vulkan_common.get_graphics_queue(),
        );

        /*let mut future = Box::new(
            sync::now(self.vulkan_common.get_device())
        ) as Box<dyn GpuFuture>;

        if let Some(ui) = ui {
            let mut cbb = AutoCommandBufferBuilder::primary(
                self.vulkan_common.get_device(),
                self.vulkan_common.get_graphics_queue_family(),
            )
            .unwrap();

            ui.render(
                &mut cbb,
                self.vulkan_common.get_graphics_queue(),
                self.vulkan_common.get_swapchain_images()[idx].clone(),
            );

            let cb = cbb.build().unwrap();

            future = Box::new(
                future
                    .then_execute(self.vulkan_common.get_graphics_queue(), cb)
                    .unwrap(),
            );
        }*/
    }

    fn partition_shadow_atlas(mut sizes: Vec<(usize, u32)>) -> HashMap<usize, ShadowAtlasTile> {
        let mut current_pos = Vector2::new(0u16, 0u16);
        sizes.sort_unstable_by(|a, b| b.1.cmp(&a.1));
        let mut res = HashMap::new();

        let mut sizes_it = sizes.iter();
        let mut n = sizes_it.next();
        loop {
            if n.is_none() || current_pos.y > SHADOW_MAP_ATLAS_SIZE as u16 {
                break;
            }

            let current_height = n.unwrap().1;
            loop {
                if n.is_some() && current_pos.x as u32 + n.unwrap().1 <= SHADOW_MAP_ATLAS_SIZE {
                    let (index, shadow_map) = n.unwrap();
                    res.insert(
                        *index,
                        ShadowAtlasTile {
                            start: current_pos,
                            size: *shadow_map,
                        },
                    );
                    current_pos.x += *shadow_map as u16;
                    n = sizes_it.next();
                } else {
                    break;
                }
            }
            current_pos.x = 0;
            current_pos.y += current_height as u16;
        }

        res
    }

    //renders single shadow map in atlas
    fn render_shadow_tile(
        &self,
        builder: &mut UnsafeCommandBufferBuilder,
        matrix: Matrix4<f32>,
        tile: ShadowAtlasTile,
    ) {
        let mut command_buffer_builder = builder;
        /*for tile in &[tile]*/
        {
            let viewport = Viewport {
                origin: [tile.start.x as f32, tile.start.y as f32],
                dimensions: [tile.size as f32, tile.size as f32],
                depth_range: 0f32..1f32,
            };
            unsafe { command_buffer_builder.set_viewport(0, [viewport].iter().cloned()) }

            for (i, (pipeline, model, _)) in self.models.iter() {
                let model_matrix = model.transform;

                let projection_data = ShadowProjectionData {
                    //view: view_matrix.into(),
                    //proj: proj_matrix.into(),
                    //model: model_matrix.into(),
                    mvp: (matrix * model.transform).into(),
                };

                if let mesh::Mesh::StaticMesh(static_mesh) = &*model.mesh {
                    unsafe {
                        let mut pcbb = &mut command_buffer_builder;

                        pcbb.push_constants(
                            &self.shadow_offscreen_pipeline.layout(),
                            ShaderStages {
                                vertex: true,
                                fragment: true,
                                ..ShaderStages::all()
                            },
                            0,
                            std::mem::size_of::<ShadowProjectionData>() as u32,
                            &projection_data,
                        );

                        pcbb.bind_vertex_buffers(0, {
                            let mut builder = UnsafeCommandBufferBuilderBindVertexBuffer::new();
                            builder.add(&static_mesh.vertex_buffer);
                            builder.add(static_mesh.normal_buffer.as_ref().unwrap());
                            builder
                        });
                        pcbb.bind_index_buffer(&static_mesh.index_buffer, IndexType::U32);
                        pcbb.draw_indexed(static_mesh.index_buffer.size() as u32 / 4, 1, 0, 0, 0);
                    }
                }
            }
        }
    }

    fn render_shadow_maps(&self, builder: &mut UnsafeCommandBufferBuilder) {
        let mut transform_it = self.shadow_transforms.iter();
        let mut tile_it = self.shadow_tiles.iter();

        unsafe {
            builder.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![Some(1.0.into())],
                    ..RenderPassBeginInfo::framebuffer(self.shadow_offscreen_frame_buffer.clone())
                },
                SubpassContents::Inline,
            );

            builder.bind_pipeline_graphics(&self.shadow_offscreen_pipeline);
        }

        for _ in 0..self.n_shadows {
            self.render_shadow_tile(
                builder,
                *transform_it.next().unwrap(),
                *tile_it.next().unwrap(),
            );
        }

        unsafe {
            //TODO transition
            let barrier;
            barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        late_fragment_tests: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        depth_stencil_attachment_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::DepthStencilAttachmentOptimal,
                    new_layout: ImageLayout::ShaderReadOnlyOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.shadow_atlas_image.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.shadow_atlas_image.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            builder.pipeline_barrier(&barrier);
            builder.end_render_pass();
        }
    }

    /*fn start_ssr(&self, semaphore_to_wait: Arc<Semaphore>, camera: &camera::Camera) {
        let half_res = {
            let res = self.vulkan_common.get_config().resolution;
            [res[0] / 2, res[1] / 2]
        };

        let ssr_command_buffer = Arc::new(self.ssr_cbb(
            &self.vulkan_common,
            self.ssr_pipeline.clone(),
            self.ssr_descriptor_sets[self.current_frame_idx()].clone(),
            half_res,
            &self.half_res_specular[self.current_frame_idx()],
            camera,
        ));

        let mut submission_builder = SubmitCommandBufferBuilder::new();
        unsafe {
            submission_builder.add_wait_semaphore(
                &semaphore_to_wait,
                PipelineStages {
                    compute_shader: true,
                    ..PipelineStages::none()
                },
            );
            submission_builder.add_signal_semaphore(&semaphore_to_wait);
            submission_builder.add_command_buffer(&ssr_command_buffer.cb);
            submission_builder.add_signal_semaphore(&self.ssr_semaphore);
        }

        submission_builder
            .submit(Arc::as_ref(&self.vulkan_common.get_graphics_queue()))
            .unwrap();
    }*/

    pub fn render_frame<U, W>(
        &mut self,
        delta_time: f32,
        camera: &camera::Camera,
        ui: Option<&mut U>,
        swapchain: &SwapChain<W>,
    ) where
        U: ui_module::UiModule,
        W: Send + Sync + 'static,
    {
        for hook in self.hooks.iter_mut() {
            hook.frame_start(camera);
        }

        let mut pre_cb_builder = unsafe {
            LifetimedCommandBufferBuilder::new(
                self.command_pool.clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap()
        };

        let mut fds = self.first_pass(camera, &mut pre_cb_builder.builder);

        //depth write barrier
        unsafe {
            let barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        late_fragment_tests: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        depth_stencil_attachment_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::DepthStencilAttachmentOptimal,
                    new_layout: ImageLayout::ShaderReadOnlyOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.depth_image.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.depth_image.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            pre_cb_builder.builder.pipeline_barrier(&barrier);
        }

        let pre_cb = pre_cb_builder.build().unwrap();
        let mut pre_submission_builder = SubmitCommandBufferBuilder::new();
        unsafe {
            pre_submission_builder.add_command_buffer(&pre_cb.cb);
            //pre_submission_builder.add_signal_semaphore(&self.pre_semaphore);
        }
        pre_submission_builder
            .submit(Arc::as_ref(&self.vulkan_common.get_graphics_queue()))
            .unwrap();

        //self.start_ssr(self.pre_semaphore.clone(), camera);
        let half_res = {
            let res = self.vulkan_common.get_config().resolution;
            [res[0] / 2, res[1] / 2]
        };

        /*let ssr_command_buffer = Arc::new(self.ssr_cbb(
            &self.vulkan_common,
            self.ssr_pipeline.clone(),
            self.ssr_descriptor_sets[self.current_frame_idx()].clone(),
            half_res,
            &self.half_res_specular[self.current_frame_idx()],
            camera,
        ));*/
        let ssr_command_buffer = self.ssr.ssr_cbb(
            self.vulkan_common.clone(),
            half_res,
            &camera,
            self.frames_count,
            self.current_frame_idx(),
        );

        let mut builder = unsafe {
            LifetimedCommandBufferBuilder::new(
                self.command_pool.clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap()
        };

        self.depth_bounds_reductor
            .reduce(self.depth_image.clone(), &mut builder.builder);

        self.second_pass(&camera, &mut builder.builder); //.unwrap().then_signal_fence_and_flush().unwrap().wait(None).unwrap();

        let (command_buffer, mut ds) = self.third_pass(camera);

        let command_buffer1 = builder.build().unwrap();

        let mut submission_builder = SubmitCommandBufferBuilder::new();

        unsafe {
            /*submission_builder.add_wait_semaphore(
                &self.pre_semaphore,
                PipelineStages{
                    top_of_pipe: true,
                    .. PipelineStages::none()
                },
            );*/
            submission_builder.add_command_buffer(&ssr_command_buffer.cb);
            submission_builder.add_command_buffer(&command_buffer1.cb);
        }

        let fence = Fence::from_pool(self.vulkan_common.get_device()).unwrap();

        unsafe {
            /*submission_builder.add_wait_semaphore(
                &self.ssr_semaphore,
                PipelineStages{
                    top_of_pipe: true,
                    .. PipelineStages::none()
                },
            );*/
            submission_builder.add_command_buffer(&command_buffer.cb);
            submission_builder.set_fence_signal(&fence);
        }
        submission_builder
            .submit(Arc::as_ref(&self.vulkan_common.get_graphics_queue()))
            .unwrap();
        fence.wait(None).unwrap();

        self.final_future(delta_time, camera, ui, swapchain);

        ds.clear();
        fds.clear();

        self.frames_count += 1;
    }
}

impl crate::game_kernel::MainRenderer {
    pub fn build_particle_system(
        &mut self,
        builder: &dyn particle_fx::ParticleSystemBuilder,
    ) -> KeyType {
        let particle_system = builder.build(
            self.vulkan_common.clone(),
            self.processed_hdr.clone(),
            self.depth_image.clone(),
        );
        self.particle_systems.insert(particle_system)
    }
}

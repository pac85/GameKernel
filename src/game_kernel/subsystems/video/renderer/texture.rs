use super::super::common::vulkan::*;
use super::vulkano;
use ktx::{header::KtxInfo, Decoder};
use vulkano::buffer::cpu_access::CpuAccessibleBuffer;
use vulkano::buffer::BufferUsage;
use vulkano::command_buffer::submit::SubmitCommandBufferBuilder;
use vulkano::command_buffer::{
    sys::UnsafeCommandBufferBuilder, AutoCommandBufferBuilder, CommandBufferLevel,
    CommandBufferUsage, PrimaryCommandBuffer,
};
use vulkano::command_buffer::{BufferImageCopy, CopyBufferToImageInfo};
use vulkano::device::physical::QueueFamily;
use vulkano::format::Format;
use vulkano::image::ImageLayout;
use vulkano::image::ImageUsage;
use vulkano::image::{
    ImageAccess, ImageCreateFlags, ImageSubresourceLayers, ImageSubresourceRange,
};
use vulkano::image::{ImageAspect, ImageDimensions, ImmutableImage};
use vulkano::sampler::{Sampler, SamplerCreateInfo};
use vulkano::sync::{
    AccessFlags, DependencyInfo, Fence, GpuFuture, ImageMemoryBarrier, PipelineStages,
};

use cgmath::Vector3;

use std::convert::TryInto;
use std::io::Read;
use std::sync::Arc;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum TextureUsage {
    Color,
    Normal,
    Bump,
    Metalness,
    Roughness,
    MetalnessRoughness,
    Emission,
}

#[derive(Clone)]
pub struct Texture {
    pub texture: Arc<ImmutableImage>,
    pub texture_usage: TextureUsage,
    pub sampler: Arc<Sampler>,
}

pub const RGB8LINEAR: Format = Format::R8G8B8A8_UNORM;
pub const SRGB: Format = Format::R8G8B8A8_SRGB;

impl Texture {
    pub fn from_texture_data<V: VulkanBasic>(
        data: &TextureData,
        texture_usage: TextureUsage,
        vulkan_common: V,
        format: Option<Format>,
    ) -> Result<Self, LoadError> {
        let dimensions = ImageDimensions::Dim2d {
            width: data.size[0],
            height: data.size[1],
            array_layers: 1,
        };

        let (buffer, init) = ImmutableImage::uninitialized(
            vulkan_common.get_device(),
            dimensions,
            format.unwrap_or(
                to_vulkan_format(data.gl_internal_format).ok_or(LoadError::UnsupportedFormat)?,
            ),
            data.mip_levels,
            ImageUsage {
                transfer_dst: true,
                sampled: true,
                ..ImageUsage::none()
            },
            ImageCreateFlags::none(),
            ImageLayout::Undefined,
            [
                vulkan_common.get_graphics_queue_family(),
                /*vulkan_common.get_compute_queue_family(),
                vulkan_common.get_transfer_queue_family(),*/
            ]
            .iter()
            .map(|q| (*q).clone()),
        )
        .unwrap(); //.map_err(|_| LoadError::UploadError)?;
                   /*let buffer = ImmutableImage::with_mipmaps(
                       vulkan_common.get_device(),
                       dimensions,
                       to_vulkan_format(data.gl_internal_format).ok_or(LoadError::UnsupportedFormat)?,
                       data.mip_levels,
                       [
                           vulkan_common.get_graphics_queue_family(),
                           vulkan_common.get_compute_queue_family(),
                           vulkan_common.get_transfer_queue_family(),
                       ]
                       .iter()
                       .cloned(),
                   )
                       .unwrap();*/

        let mut concatenated_mipmaps = vec![];

        let has_alpha = data.gl_internal_format == 35907;

        for level in data.layers_data.iter() {
            concatenated_mipmaps = [
                concatenated_mipmaps,
                level
                    .chunks_exact(if has_alpha { 4 } else { 3 })
                    .map(|pixel| {
                        let p: [u8; 4] = [
                            pixel[0],
                            pixel[1],
                            pixel[2],
                            if has_alpha { pixel[3] } else { 0 },
                        ];
                        p
                    })
                    .collect(),
            ]
            .concat();
        }
        let staging = CpuAccessibleBuffer::from_iter(
            vulkan_common.get_device(),
            BufferUsage {
                transfer_src: true,
                transfer_dst: true,
                ..BufferUsage::none()
            },
            false,
            concatenated_mipmaps.into_iter(),
        )
        .unwrap(); //.map_err(|_| LoadError::UploadError)?;

        unsafe {
            let mut copy_command_buffer_builder = LifetimedCommandBufferBuilder::new(
                vulkan_common.get_transfer_command_pool().clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .map_err(|_| LoadError::UploadError)?;

            let mut current_offset = 0;
            let transition_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        top_of_pipe: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags::none(),
                    destination_stages: PipelineStages {
                        transfer: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        transfer_write: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::Undefined,
                    new_layout: ImageLayout::TransferDstOptimal,
                    subresource_range: ImageSubresourceRange {
                        aspects: buffer.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(buffer.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };

            copy_command_buffer_builder
                .builder
                .pipeline_barrier(&transition_barrier);

            for mip_level in 0..data.mip_levels {
                let current_mip_dimenensions = dimensions
                    .mip_level_dimensions(mip_level)
                    .unwrap()
                    .width_height_depth();
                let region = BufferImageCopy {
                    buffer_offset: current_offset,
                    buffer_row_length: 0,
                    buffer_image_height: 0,
                    image_subresource: ImageSubresourceLayers {
                        aspects: buffer.format().aspects(),
                        mip_level,
                        array_layers: 0..1,
                    },
                    /*image_aspect: ImageAspect::Color,
                    image_mip_level: mip_level,
                    image_base_array_layer: 0,
                    image_layer_count: 1,*/
                    image_offset: [0, 0, 0],
                    image_extent: current_mip_dimenensions,
                    ..Default::default()
                };

                let copy_buffer_to_image_info = CopyBufferToImageInfo {
                    regions: vec![region].into(),
                    ..CopyBufferToImageInfo::buffer_image(staging.clone(), buffer.clone())
                };

                current_offset += (current_mip_dimenensions[0]
                    * (current_mip_dimenensions[1])
                    * (current_mip_dimenensions[2])) as u64
                    * 4;

                /*copy_command_buffer_builder
                .copy_buffer_to_image(
                    staging.clone(),
                    buffer.clone(),
                    ImageLayout::ShaderReadOnlyOptimal,
                    [0, 0, 0],
                    dimensions.to_image_dimensions().mipmap_dimensions(mip_level).unwrap().width_height_depth(),
                    0,
                    1,
                    mip_level,
                )
                .unwrap();*/
                copy_command_buffer_builder
                    .builder
                    .copy_buffer_to_image(&copy_buffer_to_image_info);
            }

            let transition_barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        transfer: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        transfer_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        bottom_of_pipe: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags::none(),
                    old_layout: ImageLayout::TransferDstOptimal,
                    new_layout: ImageLayout::Undefined,
                    subresource_range: ImageSubresourceRange {
                        aspects: buffer.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(buffer.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            copy_command_buffer_builder
                .builder
                .pipeline_barrier(&transition_barrier);

            let copy_command_buffer = copy_command_buffer_builder.build().unwrap();
            let mut submission_builder = SubmitCommandBufferBuilder::new();
            submission_builder.add_command_buffer(&copy_command_buffer.cb);
            let fence = Fence::from_pool(vulkan_common.get_device()).unwrap();
            submission_builder.set_fence_signal(&fence);
            submission_builder
                .submit(&vulkan_common.get_transfers_queue())
                .unwrap();
            fence.wait(None).unwrap();

            use vulkano::sampler::{Filter, SamplerAddressMode, SamplerMipmapMode};
            let sampler = Sampler::new(
                vulkan_common.get_device(),
                SamplerCreateInfo {
                    mag_filter: Filter::Linear,
                    min_filter: Filter::Linear,
                    mipmap_mode: SamplerMipmapMode::Linear,
                    address_mode: [SamplerAddressMode::Repeat; 3],
                    ..Default::default()
                },
            )
            .unwrap();

            Ok(Self {
                texture: buffer,
                texture_usage,
                sampler,
            })
        }
    }
}

fn to_vulkan_format(gl_format: u32) -> Option<Format> {
    match gl_format {
        35905 => Some(Format::R8G8B8A8_UNORM),
        35907 => Some(Format::R8G8B8A8_UNORM),
        a => {
            println!("unsupported format {}", a);
            None
        }
    }
}

pub struct TextureData {
    pub gl_internal_format: u32,
    pub size: [u32; 2], //width and height
    pub is_cubemap: bool,
    pub mip_levels: u32,
    pub layers_data: Vec<Vec<u8>>,
}

#[derive(Debug)]
pub enum LoadError {
    ReadError,
    UnsupportedFormat,
    UploadError,
}

impl TextureData {
    pub fn load_from_kxt<R: Read>(reader: R) -> Result<Self, LoadError> {
        let ktx_decoder = Decoder::new(reader).map_err(|_| LoadError::ReadError)?;
        let header = ktx_decoder.header();
        let layers_data = ktx_decoder.read_textures().collect();

        Ok(Self {
            gl_internal_format: header.gl_internal_format(),
            size: [header.pixel_width(), header.pixel_height()],
            is_cubemap: header.faces() == 6,
            mip_levels: header.mipmap_levels(),

            layers_data,
        })
    }

    pub fn from_uniform_color(color: Vector3<f32>) -> Self {
        let (r, g, b) = (
            (color.x * 255.0) as u8,
            (color.y * 255.0) as u8,
            (color.z * 255.0) as u8,
        );

        let layers_data = vec![vec![r, g, b, 255u8]];

        Self {
            gl_internal_format: 35905, //TODO: fix hack
            size: [1, 1],
            is_cubemap: false,
            mip_levels: 1,
            layers_data,
        }
    }
}

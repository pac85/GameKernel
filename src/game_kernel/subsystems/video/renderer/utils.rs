use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer, TypedBufferAccess};
use vulkano::command_buffer::*;
use vulkano::device::physical::QueueFamily;
use vulkano::device::Device;

use super::super::common::vulkan::VulkanBasic;

use std::sync::Arc;

pub fn get_quads_idx_buffer<'a, I, V>(
    common: V,
    usage: BufferUsage,
    n: usize,
    queue_families: I,
) -> Arc<DeviceLocalBuffer<[u32]>>
where
    I: IntoIterator<Item = QueueFamily<'a>>,
    V: VulkanBasic,
{
    let viter = (0..(n * 6)).map(|i| {
        let cv = i % 6;
        ((4 * (i / 6)) + [0, 1, 2, 2, 3, 0][cv]) as u32
    });
    let staging_buf = CpuAccessibleBuffer::from_iter(
        common.get_device(),
        BufferUsage {
            transfer_src: true,
            ..BufferUsage::none()
        },
        false,
        viter,
    )
    .unwrap();

    let mut queue_families: Vec<_> = queue_families.into_iter().collect();
    queue_families.sort_by_key(QueueFamily::id);
    queue_families.dedup_by_key(|qf| qf.id());

    let buf = DeviceLocalBuffer::array(
        common.get_device(),
        staging_buf.len(),
        BufferUsage {
            transfer_dst: true,
            ..usage
        },
        queue_families, //TODO: make sure transfer queue is there
    )
    .unwrap();

    let mut cbb = AutoCommandBufferBuilder::primary(
        common.get_device(),
        common.get_transfer_queue_family(),
        CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap();
    let copy_buffer_info = CopyBufferInfo::buffers(staging_buf.clone(), buf.clone());
    cbb.copy_buffer(copy_buffer_info).unwrap();
    let cb = cbb.build().unwrap();

    cb.execute(common.get_transfers_queue()).unwrap();

    buf
}

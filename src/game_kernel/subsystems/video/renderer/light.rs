use cgmath::*;

#[derive(Debug, Copy, Clone)]
pub struct DirectionalLight {
    pub color: Vector3<f32>,
    pub direction: Vector3<f32>,
    pub angle: f32,
    pub n_cascades: u32,
    pub cascade_scale: f32,
}

#[derive(Debug, Copy, Clone)]
pub struct PointLight {
    pub color: Vector3<f32>,
    pub position: Vector3<f32>,

    pub size: f32,
    pub influence_radius: f32,
}

#[derive(Debug, Copy, Clone)]
pub struct SpotLight {
    pub color: Vector3<f32>,
    pub position: Vector3<f32>,
    pub direction: Vector3<f32>,

    pub size: f32,
    pub cone_length: f32,
    pub cone_angle: f32,
}

#[derive(Debug, Copy, Clone)]
pub struct AreaLight {
    pub color: Vector3<f32>,
    pub position: Vector3<f32>,

    pub normal: Vector3<f32>,
    pub tangent: Vector3<f32>,

    pub size: Vector2<f32>,
    pub influence_radius: f32,

    pub double_sided: bool,
}

#[derive(Debug, Copy, Clone)]
pub enum LightType {
    Directional(DirectionalLight),
    Point(PointLight),
    Spot(SpotLight),
    Area(AreaLight),
}

#[derive(Debug, Copy, Clone)]
pub struct Light {
    pub light: LightType,
    pub shadow_map_size: Option<u16>,
}

impl Light {
    pub fn get_color(&self) -> Vector3<f32> {
        match self.light {
            LightType::Directional(ref light) => light.color,
            LightType::Point(ref light) => light.color,
            LightType::Spot(ref light) => light.color,
            LightType::Area(ref light) => light.color,
        }
    }

    pub fn set_color(&mut self, color: Vector3<f32>) {
        match self.light {
            LightType::Directional(ref mut light) => light.color = color,
            LightType::Point(ref mut light) => light.color = color,
            LightType::Spot(ref mut light) => light.color = color,
            LightType::Area(ref mut light) => light.color = color,
        }
    }

    ///sets position if it makes sense to
    pub fn maybe_set_position(&mut self, position: cgmath::Vector3<f32>) -> &mut Self {
        match self.light {
            LightType::Point(ref mut light) => light.position = position,
            LightType::Spot(ref mut light) => light.position = position,
            LightType::Area(ref mut light) => light.position = position,
            _ => (),
        }

        self
    }

    pub fn maybe_set_rotation(&mut self, rotation: cgmath::Quaternion<f32>) -> &mut Self {
        let dir = rotation.rotate_vector(cgmath::Vector3::new(0.0, -1.0, 0.0));

        match self.light {
            LightType::Spot(ref mut light) => light.direction = dir,
            LightType::Directional(ref mut light) => light.direction = dir,
            _ => (),
        }

        self
    }
}

use vulkano::command_buffer::submit::SubmitPresentBuilder;
use vulkano::command_buffer::sys::UnsafeCommandBufferBuilder;
use vulkano::descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::Queue;
use vulkano::format::Format;
use vulkano::image::view::ImageViewAbstract;
use vulkano::image::{AttachmentImage, ImageAccess, ImageUsage, SwapchainImage};

use vulkano::pipeline::graphics::vertex_input::BuffersDefinition;
use vulkano::pipeline::graphics::viewport::Viewport;
use vulkano::pipeline::{GraphicsPipeline, Pipeline};
use vulkano::render_pass::{Framebuffer, FramebufferCreateInfo, RenderPass, Subpass};
use vulkano::sampler::{Sampler, SamplerCreateInfo};
use vulkano::single_pass_renderpass;
use vulkano::sync::Semaphore;
use winit::window::Window;

use std::convert::TryFrom;
use std::sync::Arc;

use crate::vulkan::{SwapChain, ToImageView, VulkanBasic, VulkanCommon};

use super::final_pass_shaders;

pub trait RenderOutput {
    /*fn output(
        &mut self,
        command_buffer_builder: &mut UnsafeCommandBufferBuilder,
        queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + Send + Sync + 'static>,
    );*/
    fn format(&self) -> Format;
    fn images(&self) -> Vec<Arc<dyn ImageAccess>>;
    fn next_idx(&mut self, acquire_semaphore: Option<&Semaphore>) -> usize;
    fn present(&self, present_semaphore: Option<&Semaphore>, queue: &Queue, idx: usize);
    fn present_image_layout(&self) -> vulkano::image::ImageLayout;
    fn signals_acquire_semaphore(&self) -> bool;
}

pub struct SwapchainOutout<W> {
    pub swapchain: SwapChain<W>,
    vulkan_common: VulkanCommon,
}

impl<W> Clone for SwapchainOutout<W> {
    fn clone(&self) -> Self {
        Self {
            swapchain: self.swapchain.clone(),
            vulkan_common: self.vulkan_common.clone(),
        }
    }
}

impl<W> SwapchainOutout<W> {
    pub fn new(swapchain: SwapChain<W>, vulkan_common: VulkanCommon) -> Self {
        Self {
            swapchain,
            vulkan_common,
        }
    }
}

impl<W: 'static> RenderOutput for SwapchainOutout<W>
where
    SwapchainImage<W>: ImageAccess,
{
    fn format(&self) -> Format {
        self.swapchain.get_swapchain_images()[0].format()
    }

    fn images(&self) -> Vec<Arc<dyn ImageAccess>> {
        self.swapchain
            .get_swapchain_images()
            .iter()
            .map(|image| image.clone() as _)
            .collect()
    }

    fn next_idx(&mut self, acquire_semaphore: Option<&Semaphore>) -> usize {
        use vulkano::swapchain::{acquire_next_image_raw, AcquiredImage};

        let AcquiredImage {
            id: idx,
            suboptimal: _,
        } = unsafe {
            acquire_next_image_raw(
                &self.swapchain.get_swapchain(),
                None,
                acquire_semaphore,
                None,
            )
            .unwrap()
        };

        idx
    }

    fn present(&self, present_semaphore: Option<&Semaphore>, queue: &Queue, idx: usize) {
        let mut pb = SubmitPresentBuilder::new();
        let swapchain = self.swapchain.get_swapchain();
        unsafe {
            if let Some(present_semaphore) = present_semaphore {
                pb.add_wait_semaphore(present_semaphore);
            }
            pb.add_swapchain(&swapchain, idx as u32, None);
        }
        pb.submit(queue).unwrap();
    }

    fn present_image_layout(&self) -> vulkano::image::ImageLayout {
        vulkano::image::ImageLayout::PresentSrc
    }

    fn signals_acquire_semaphore(&self) -> bool {
        true
    }
}

#[derive(Clone)]
pub struct OffscreenOutput {
    pub image: Arc<AttachmentImage>,
    vulkan_common: VulkanCommon,
}

impl OffscreenOutput {
    pub fn new(format: Format, vulkan_common: VulkanCommon) -> Self {
        Self::new_with_image(
            AttachmentImage::with_usage(
                vulkan_common.get_device(),
                vulkan_common.get_config().resolution,
                format,
                ImageUsage {
                    sampled: true,
                    ..ImageUsage::none()
                },
            )
            .unwrap(),
            vulkan_common,
        )
    }

    pub fn new_with_image(image: Arc<AttachmentImage>, vulkan_common: VulkanCommon) -> Self {
        Self {
            image,
            vulkan_common,
        }
    }
}

impl RenderOutput for OffscreenOutput {
    fn format(&self) -> Format {
        self.image.format()
    }

    fn images(&self) -> Vec<Arc<dyn ImageAccess>> {
        vec![self.image.clone()]
    }

    fn next_idx(&mut self, acquire_semaphore: Option<&Semaphore>) -> usize {
        0
    }

    fn present(&self, present_semaphore: Option<&Semaphore>, queue: &Queue, idx: usize) {}

    fn present_image_layout(&self) -> vulkano::image::ImageLayout {
        vulkano::image::ImageLayout::ColorAttachmentOptimal
    }

    fn signals_acquire_semaphore(&self) -> bool {
        false
    }
}

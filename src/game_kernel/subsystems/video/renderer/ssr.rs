use super::camera;
use crate::vulkan::*;
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::{CommandBufferLevel, CommandBufferUsage};
use vulkano::descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::Device;
use vulkano::image::{AttachmentImage, ImageLayout};
use vulkano::pipeline::{ComputePipeline, Pipeline, PipelineBindPoint};
use vulkano::sampler::{Filter, Sampler, SamplerAddressMode, SamplerCreateInfo, SamplerMipmapMode};
use vulkano::shader::{ShaderModule, ShaderStages};
use vulkano::sync::{AccessFlags, DependencyInfo, MemoryBarrier, PipelineStages, Semaphore};

use std::sync::Arc;

pub mod ssr_shader {
    vulkano_shaders::shader! {
        ty: "compute",
        path: "data/shaders/ssr/compute.glsl",
        types_meta: {
            use bytemuck::{Pod, Zeroable};

            #[derive(Clone, Copy, Zeroable, Pod)]
        },
        include: ["data/shaders/include"]
    }

    use super::*;
    pub fn get_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        load(device.clone()).unwrap()
    }
}

use self::ssr_shader::ty::ProjectionData as SsrProjectionData;
use self::ssr_shader::ty::SsrFrameInfo;

pub struct Ssr {
    half_res_specular: [Arc<AttachmentImage>; 2],
    //  screen space reflections
    ssr_pipeline: Arc<ComputePipeline>,
    ssr_descriptor_sets: [Arc<dyn DescriptorSet>; 2],
    ssr_semaphore: Arc<Semaphore>,
    ssr_frame_info_buffer: Arc<CpuAccessibleBuffer<SsrFrameInfo>>,
    ssr_frame_info_descriptor_set: Arc<dyn DescriptorSet>,
}

impl Ssr {
    pub fn new(
        vulkan_common: impl VulkanBasic,
        depth_image: Arc<AttachmentImage>,
        norm_rough_image: Arc<AttachmentImage>,
        half_res_specular: [Arc<AttachmentImage>; 2],
        unprocessed_hdr: Arc<AttachmentImage>,
        mesh_index_image: Arc<AttachmentImage>,
        velocity_buffer_image: Arc<AttachmentImage>,
        res_buffer: Arc<DeviceLocalBuffer<super::Res>>,
    ) -> Self {
        let nearest_sampler =
            Sampler::new(vulkan_common.get_device(), SamplerCreateInfo::default()).unwrap();

        let linear_sampler = Sampler::new(
            vulkan_common.get_device(),
            SamplerCreateInfo::simple_repeat_linear_no_mipmap(),
        )
        .unwrap();

        //  screen space reflections
        let ssr_pipeline: Arc<ComputePipeline> = ComputePipeline::new(
            vulkan_common.get_device(),
            ssr_shader::get_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let build_ssr_descriptor_set = |is_first| {
            PersistentDescriptorSet::new(
                ssr_pipeline.clone().layout().set_layouts()[0].clone(),
                [
                    WriteDescriptorSet::image_view_sampler(
                        0,
                        norm_rough_image.clone().to_image_view(),
                        linear_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        1,
                        depth_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        2,
                        unprocessed_hdr.clone().to_image_view(),
                        linear_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        3,
                        mesh_index_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        4,
                        velocity_buffer_image.clone().to_image_view(),
                        nearest_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view_sampler(
                        5,
                        half_res_specular[if is_first { 1 } else { 0 }]
                            .clone()
                            .to_image_view(),
                        linear_sampler.clone(),
                    ),
                    WriteDescriptorSet::image_view(
                        6,
                        half_res_specular[if is_first { 0 } else { 1 }]
                            .clone()
                            .to_image_view(),
                    ),
                    WriteDescriptorSet::buffer(7, res_buffer.clone()),
                ],
            )
            .unwrap() as Arc<dyn DescriptorSet>
        };

        let ssr_descriptor_sets = [
            build_ssr_descriptor_set(true),
            build_ssr_descriptor_set(false),
        ];

        let ssr_semaphore = Arc::new(Semaphore::from_pool(vulkan_common.get_device()).unwrap());

        let ssr_frame_info_buffer = CpuAccessibleBuffer::from_data(
            vulkan_common.get_device(),
            BufferUsage {
                uniform_buffer: true,
                storage_buffer: true,
                ..BufferUsage::none()
            },
            false,
            SsrFrameInfo { frame_hash: 0f32 },
        )
        .unwrap();

        let ssr_frame_info_descriptor_set = {
            PersistentDescriptorSet::new(
                ssr_pipeline.clone().layout().set_layouts()[1].clone(),
                [WriteDescriptorSet::buffer(0, ssr_frame_info_buffer.clone())],
            )
            .unwrap()
        };

        Self {
            half_res_specular,
            //  screen space reflections
            ssr_pipeline,
            ssr_descriptor_sets,
            ssr_semaphore,
            ssr_frame_info_buffer,
            ssr_frame_info_descriptor_set,
        }
    }

    pub fn ssr_cbb(
        &self,
        vulkan_common: impl VulkanBasic,
        half_res: [u32; 2],
        camera: &camera::Camera,
        frames_count: u64,
        current_frame_idx: usize,
    ) -> LifetimedCommandBuffer {
        //updates frame seed
        *self.ssr_frame_info_buffer.write().unwrap() = SsrFrameInfo {
            frame_hash: (frames_count % 10000) as f32 / 10000.0,
        };
        unsafe {
            let mut lbuilder = LifetimedCommandBufferBuilder::new(
                vulkan_common.get_graphics_command_pool().clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap();

            let builder = &mut lbuilder.builder;

            /*let barrier = {
                let mut barrier_builder = UnsafeCommandBufferBuilderPipelineBarrier::new();
                barrier_builder.add_image_memory_barrier(
                    &target,
                    0..1,
                    0..1,
                    PipelineStages {
                        top_of_pipe: true,
                        ..PipelineStages::none()
                    },
                    AccessFlags::none(),
                    PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    false,
                    None,
                    ImageLayout::PresentSrc,
                    ImageLayout::ShaderReadOnlyOptimal,
                );
                barrier_builder
            };
            builder.pipeline_barrier(&barrier);*/

            builder.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                self.ssr_pipeline.layout(),
                0,
                [
                    self.ssr_descriptor_sets[current_frame_idx].inner(),
                    self.ssr_frame_info_descriptor_set.inner(),
                ]
                .iter()
                .cloned(),
                [].iter().cloned(),
            );
            builder.bind_pipeline_compute(&self.ssr_pipeline.clone());

            let aspect_ratio = vulkan_common.get_config().resolution[0] as f32
                / vulkan_common.get_config().resolution[1] as f32;
            builder.push_constants(
                self.ssr_pipeline.layout(),
                ShaderStages {
                    compute: true,
                    ..ShaderStages::none()
                },
                0,
                std::mem::size_of::<SsrProjectionData>() as u32,
                &SsrProjectionData {
                    mv: camera.get_view().into(),
                    p: {
                        let mut p = camera.get_proj(aspect_ratio);
                        let v = camera.get_eye().extend(0.0);
                        p[0][3] = v.x;
                        p[1][3] = v.y;
                        p[2][3] = v.z;
                        p.into()
                    },
                    //view_pos: camera.get_position().into(),
                },
            );
            builder.dispatch({
                let local_size = [16, 16];
                [
                    (half_res[0] as f32 / local_size[0] as f32).ceil() as u32,
                    (half_res[1] as f32 / local_size[1] as f32).ceil() as u32,
                    1,
                ]
            });

            let barrier = {
                let memory_barrier = MemoryBarrier {
                    source_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        shader_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        bottom_of_pipe: true,
                        fragment_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    ..Default::default()
                };

                DependencyInfo {
                    memory_barriers: vec![memory_barrier].into(),
                    ..Default::default()
                }
            };
            builder.pipeline_barrier(&barrier);

            lbuilder.build().unwrap()
        }
    }
}

use std::collections::BinaryHeap;
use std::convert::TryInto;

use cgmath::*;

pub fn v3_min(a: Vector3<f32>, b: Vector3<f32>) -> Vector3<f32> {
    Vector3::new(a.x.min(b.x), a.y.min(b.y), a.z.min(b.z))
}

pub fn v3_max(a: Vector3<f32>, b: Vector3<f32>) -> Vector3<f32> {
    Vector3::new(a.x.max(b.x), a.y.max(b.y), a.z.max(b.z))
}

#[derive(PartialEq, Clone, Copy)]
pub struct AABB {
    pub lower_bound: Vector3<f32>,
    pub upper_bound: Vector3<f32>,
}

impl AABB {
    pub fn new(lower_bound: Vector3<f32>, upper_bound: Vector3<f32>) -> Self {
        Self {
            lower_bound,
            upper_bound,
        }
    }

    pub fn union(&self, rhs: &Self) -> Self {
        Self::new(
            v3_min(self.lower_bound, rhs.lower_bound),
            v3_max(self.upper_bound, rhs.upper_bound),
        )
    }

    pub fn area(&self) -> f32 {
        let d = self.upper_bound - self.lower_bound;
        2.0 * (d.x * d.y + d.y * d.z + d.z * d.x)
    }
}

pub enum NodeLink {
    Leaf([u32; 3]),
    Internal([u32; 2]),
}

impl NodeLink {
    pub fn get_internal(&self) -> Option<&[u32; 2]> {
        if let Self::Internal(c) = self {
            Some(c)
        } else {
            None
        }
    }

    pub fn get_internal_mut(&mut self) -> Option<&mut [u32; 2]> {
        if let Self::Internal(c) = self {
            Some(c)
        } else {
            None
        }
    }

    pub fn is_leaf(&self) -> bool {
        if let Self::Leaf(_) = self {
            true
        } else {
            false
        }
    }
}

pub struct BVHNode {
    pub bbox: AABB,
    pub parent_index: u32,
    pub link: NodeLink,
}

impl BVHNode {
    pub fn new(bbox: AABB, parent_index: u32, link: NodeLink) -> Self {
        Self {
            bbox,
            parent_index,
            link,
        }
    }
}

pub const NULL_INDEX: u32 = 0xffffffff;

#[derive(PartialEq, PartialOrd, Clone, Copy)]
struct OrdFloat(f32);
impl std::ops::Deref for OrdFloat {
    type Target = f32;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::cmp::Eq for OrdFloat {}

impl std::cmp::Ord for OrdFloat {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        //swapped
        if self < other {
            std::cmp::Ordering::Less
        } else if self > other {
            std::cmp::Ordering::Greater
        } else {
            std::cmp::Ordering::Equal
        }
    }
}

pub struct BVHTree {
    pub nodes: Vec<BVHNode>,
    pub root: u32,
}

impl BVHTree {
    pub fn new() -> Self {
        Self {
            nodes: vec![],
            root: NULL_INDEX,
        }
    }

    fn allocate_leaf(&mut self, vertices_indices: [u32; 3], bbox: AABB) -> u32 {
        let index = self.nodes.len() as u32;
        self.nodes.push(BVHNode::new(
            bbox,
            NULL_INDEX,
            NodeLink::Leaf(vertices_indices),
        ));
        index
    }

    fn allocate_internal(&mut self, bbox: AABB, parent: u32) -> u32 {
        let index = self.nodes.len() as u32;
        self.nodes.push(BVHNode::new(
            bbox,
            parent,
            NodeLink::Internal([NULL_INDEX; 2]),
        ));
        index
    }

    fn insert_cost(&self, index: u32, bbox: &AABB) -> f32 {
        let mut index = index;
        let mut cost = bbox.area();
        //walk up the tree and adjust boxes
        while index != NULL_INDEX {
            let cbbox = self.nodes[index as usize].bbox;
            cost += cbbox.union(&bbox).area() - cbbox.area();

            index = self.nodes[index as usize].parent_index;
        }

        cost
    }

    pub fn pick_best_sibiling(&self, root: u32, bbox: &AABB) -> u32 {
        let mut best_node = 0; //root;
        let mut best_cost = f32::MAX; //self.insert_cost(best_node, bbox);

        for node in (0..self.nodes.len() - 1) {
            let cost = self.insert_cost(node as u32, bbox);
            if cost < best_cost {
                best_node = node as u32;
                best_cost = cost;
            }
        }
        return best_node;

        use std::cmp::Reverse;
        let mut queue = BinaryHeap::new();
        queue.push((Reverse(OrdFloat(best_cost)), best_node));

        while let Some((node_cost, node)) = queue.pop() {
            if *node_cost.0 < best_cost {
                best_cost = *node_cost.0;
                best_node = node;
            }

            //if it can be then explore children
            //if internal node
            if let Some([a, b]) = self.nodes[node as usize].link.get_internal() {
                let lower_bound = self.insert_cost(*a, bbox);
                if lower_bound < best_cost {
                    let a_cost = self.insert_cost(*a, bbox);
                    queue.push((Reverse(OrdFloat(a_cost)), *a));

                    let b_cost = self.insert_cost(*b, bbox);
                    queue.push((Reverse(OrdFloat(b_cost)), *b));
                }
            }
        }

        best_node
    }

    fn rotate_nodes(&mut self, a: u32, b: u32) {
        let a_parent = self.nodes[a as usize].parent_index;
        let b_parent = self.nodes[b as usize].parent_index;

        if let NodeLink::Internal([l, r]) = &mut self.nodes[a_parent as usize].link {
            if a == *l {
                *l = b;
            } else {
                *r = b;
            }
        } else {
            panic!("broken bvh");
        }

        if let NodeLink::Internal([l, r]) = &mut self.nodes[b_parent as usize].link {
            if b == *l {
                *l = a;
            } else {
                *r = a;
            }
        } else {
            panic!("broken bvh");
        }

        self.nodes[a as usize].parent_index = b_parent;
        self.nodes[b as usize].parent_index = a_parent;
    }

    fn rotate(&mut self, index: u32) {
        if let NodeLink::Internal([b, c]) = self.nodes[index as usize].link {
            if let NodeLink::Internal([d, e]) = self.nodes[b as usize].link {
                if self.nodes[c as usize]
                    .bbox
                    .union(&self.nodes[e as usize].bbox)
                    .area()
                    < self.nodes[b as usize].bbox.area()
                {
                    self.nodes[b as usize].bbox = self.nodes[c as usize]
                        .bbox
                        .union(&self.nodes[e as usize].bbox);
                    self.rotate_nodes(c, d);
                } else if self.nodes[c as usize]
                    .bbox
                    .union(&self.nodes[d as usize].bbox)
                    .area()
                    < self.nodes[b as usize].bbox.area()
                {
                    self.nodes[b as usize].bbox = self.nodes[c as usize]
                        .bbox
                        .union(&self.nodes[d as usize].bbox);
                    self.rotate_nodes(c, e);
                }
            }
            if let NodeLink::Internal([f, g]) = self.nodes[c as usize].link {
                if self.nodes[b as usize]
                    .bbox
                    .union(&self.nodes[g as usize].bbox)
                    .area()
                    < self.nodes[c as usize].bbox.area()
                {
                    self.nodes[c as usize].bbox = self.nodes[b as usize]
                        .bbox
                        .union(&self.nodes[g as usize].bbox);
                    self.rotate_nodes(b, f);
                } else if self.nodes[b as usize]
                    .bbox
                    .union(&self.nodes[f as usize].bbox)
                    .area()
                    < self.nodes[c as usize].bbox.area()
                {
                    self.nodes[c as usize].bbox = self.nodes[b as usize]
                        .bbox
                        .union(&self.nodes[f as usize].bbox);
                    self.rotate_nodes(b, g);
                }
            }
        }
    }

    pub fn insert_leaf(&mut self, vertices_indices: [u32; 3], bbox: AABB) {
        let leaf_index = self.allocate_leaf(vertices_indices, bbox);
        if leaf_index == 0 {
            self.root = leaf_index;
            return;
        }

        //1: find best sibiling
        let best_sibiling_index = self.pick_best_sibiling(self.root, &bbox);
        /*for i in 0..self.nodes.len() {
            self.pick_best_sibiling(best_sibiling_index, i as u32);
        }*/

        //2: create parent
        let old_parent = self.nodes[best_sibiling_index as usize].parent_index;
        let new_parent = self.allocate_internal(
            bbox.union(&self.nodes[best_sibiling_index as usize].bbox),
            old_parent,
        );

        //if the sibiling's parent wasn't the root
        if old_parent != NULL_INDEX {
            if self.nodes[old_parent as usize].link.get_internal().unwrap()[0]
                == best_sibiling_index
            {
                self.nodes[old_parent as usize]
                    .link
                    .get_internal_mut()
                    .unwrap()[0] = new_parent;
            } else {
                self.nodes[old_parent as usize]
                    .link
                    .get_internal_mut()
                    .unwrap()[1] = new_parent;
            }
        } else {
            self.root = new_parent;
        }

        self.nodes[new_parent as usize]
            .link
            .get_internal_mut()
            .unwrap()[0] = best_sibiling_index;
        self.nodes[new_parent as usize]
            .link
            .get_internal_mut()
            .unwrap()[1] = leaf_index;
        self.nodes[best_sibiling_index as usize].parent_index = new_parent;
        self.nodes[leaf_index as usize].parent_index = new_parent;

        //3: refit
        {
            let mut index = self.nodes[leaf_index as usize].parent_index;
            //walf up the tree and adjust boxes
            while index != NULL_INDEX {
                let [a, b] = self.nodes[index as usize].link.get_internal().unwrap();

                self.nodes[index as usize].bbox = self.nodes[*a as usize]
                    .bbox
                    .union(&self.nodes[*b as usize].bbox);

                self.rotate(index);
                index = self.nodes[index as usize].parent_index;
            }
        }
    }

    fn check_rec(&self, node: u32) -> Option<AABB> {
        match self.nodes[node as usize].link {
            NodeLink::Leaf(_) => Some(self.nodes[node as usize].bbox),
            NodeLink::Internal([l, r]) => {
                let lb = self.check_rec(l)?;
                let rb = self.check_rec(r)?;

                let ub = lb.union(&rb);
                if ub == self.nodes[node as usize].bbox {
                    Some(ub)
                } else {
                    None
                }
            }
        }
    }

    pub fn check(&self) -> bool {
        self.check_rec(self.root).is_some()
    }

    pub fn bfprint(&self) {
        use std::collections::VecDeque;

        let mut queue = VecDeque::new();
        queue.push_back((0, self.root));

        let mut dm = std::collections::HashMap::new();
        while let Some((depth, index)) = queue.pop_front() {
            if let Some(md) = dm.get_mut(&depth) {
                *md += 1;
            } else {
                dm.insert(depth, 0);
            }

            if let NodeLink::Internal([l, r]) = self.nodes[index as usize].link {
                queue.push_back((depth + 1, l));
                queue.push_back((depth + 1, r));
            }
        }

        let mut v = dm.iter().collect::<Vec<_>>();
        v.sort();
        println!("{:?}", v);
    }

    pub fn insert_mesh(&mut self, mesh_data: &super::super::mesh::MeshData<u32>) {
        let n = mesh_data.index_buffer.len() / 3;
        let mut processed = 0;
        for [a, b, c] in mesh_data.triangle_indices().map(|a| {
            let a: [u32; 3] = a.try_into().unwrap();
            a
        }) {
            let v1 = mesh_data.vertex_buffer[a as usize];
            let v2 = mesh_data.vertex_buffer[b as usize];
            let v3 = mesh_data.vertex_buffer[c as usize];

            let min = v3_min(v1, v3_min(v2, v3));
            let max = v3_max(v1, v3_max(v2, v3));

            let bbox = AABB::new(min, max);

            self.insert_leaf([a, b, c], bbox);

            processed += 1;
            print!(
                "\r BVH Build {}%  {}/{}   ",
                100.0 * processed as f32 / n as f32,
                processed,
                n
            );
        }

        if !self.check() {
            panic!("bvh build failed");
        }
        self.bfprint();
    }
}

#[test]
fn tt() {
    let mut t = BVHTree::new();

    for i in 0..10 {
        t.insert_leaf(
            [0; 3],
            AABB::new(
                Vector3::new(-10.0, -10.0, -10.0),
                Vector3::new(-9.0, -9.0, -9.0),
            ),
        );
        t.insert_leaf(
            [0; 3],
            AABB::new(Vector3::new(9.0, 9.0, 9.0), Vector3::new(10.0, 10.0, 10.0)),
        );
    }
    /*for i in 0..10 {
        t.insert_leaf([0; 3], AABB::new(Vector3::new(9.0, 9.0, 9.0), Vector3::new(10.0, 10.0, 10.0)));
    }*/

    t.bfprint();
    panic!("");
}

use std::any::type_name;
use std::any::Any;
use std::convert::TryFrom;
use std::iter;
use std::sync::Arc;

use cgmath::*;
use game_kernel_utils::factory::*;
use game_kernel_utils::get_dynamic_arg;
use lazy_static::lazy_static;
use rand::prelude::*;
use vulkano::buffer::BufferUsage;
use vulkano::buffer::{BufferAccess, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::RenderPassBeginInfo;
use vulkano::command_buffer::{sys::UnsafeCommandBufferBuilder, SubpassContents};
use vulkano::descriptor_set::*;
use vulkano::device::Device;
use vulkano::format::ClearValue;
use vulkano::image::view::ImageView;
use vulkano::image::AttachmentImage;
use vulkano::image::ImageDimensions;
use vulkano::image::ImageSubresourceRange;
use vulkano::image::{ImageAccess, ImageLayout, ImageUsage};
use vulkano::pipeline::graphics::color_blend::{AttachmentBlend, BlendFactor, BlendOp};
use vulkano::pipeline::graphics::input_assembly::IndexType;
use vulkano::pipeline::graphics::vertex_input::BuffersDefinition;
use vulkano::pipeline::graphics::viewport::Viewport;
use vulkano::pipeline::{ComputePipeline, GraphicsPipeline, Pipeline, PipelineBindPoint};
use vulkano::render_pass::FramebufferCreateInfo;
use vulkano::render_pass::{Framebuffer, RenderPass, Subpass};
use vulkano::sampler::Sampler;
use vulkano::sampler::SamplerCreateInfo;
use vulkano::shader::{EntryPoint, ShaderModule, ShaderStages};
use vulkano::single_pass_renderpass;
use vulkano::sync::DependencyInfo;
use vulkano::sync::ImageMemoryBarrier;
use vulkano::sync::{AccessFlags, PipelineStages};

use crate::vulkan::*;

pub mod bvh;

mod shaders {
    pub mod cs {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/raytracing/compute.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/raytracing/", "data/shaders/include/"]
        }
    }

    pub mod vs {
        vulkano_shaders::shader! {
            ty:  "vertex",
            path: "data/shaders/raytracing/vertex.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/raytracing/"]
        }
    }

    pub mod fs {
        vulkano_shaders::shader! {
            ty:  "fragment",
            path: "data/shaders/raytracing/test.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
            include: ["data/shaders/raytracing/", "data/shaders/include/"]
        }
    }

    pub use fs::ty::ProjectionData as GpuProjectionData;

    pub use cs::ty::BVHMeta as GpuBVHMeta;
    pub use cs::ty::BVHNode as GpuBVHNode;
    pub use cs::ty::BVHTree as GpuBVHTree;

    impl GpuBVHNode {
        pub fn from_node(node: &bvh::BVHNode) -> Self {
            let bmin = node.bbox.lower_bound.into();
            let bmax = node.bbox.upper_bound.into();
            let parent = node.parent_index;
            let (lchild, rchild, indices) = match node.link {
                bvh::NodeLink::Internal([l, r]) => (l, r, [0; 3]),
                bvh::NodeLink::Leaf(indices) => (bvh::NULL_INDEX, bvh::NULL_INDEX, indices),
            };

            Self {
                bmin,
                lchild,
                bmax,
                rchild,
                object: indices,
                parent,
            }
        }
    }

    use super::*;

    pub fn get_compute_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        cs::load(device.clone()).unwrap()
    }

    pub fn get_vertex_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        vs::load(device.clone()).unwrap()
    }

    pub fn get_fragment_shader(device: Arc<Device>) -> Arc<ShaderModule> {
        fs::load(device.clone()).unwrap()
    }
}

pub struct RayTracer {
    rt_out: Arc<AttachmentImage>,
    compute_pipeline: Arc<ComputePipeline>,
    compute_descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,

    bvh_meta_buffer: Arc<DeviceLocalBuffer<shaders::GpuBVHMeta>>,
    bvh_nodes_buffer: Arc<DeviceLocalBuffer<[shaders::GpuBVHNode]>>,
    bvh_descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    test_descriptor_set: Option<Arc<dyn DescriptorSet + Send + Sync>>,
    should_update_buffers: bool,

    render_pass: Arc<RenderPass>,
    pipeline: Arc<GraphicsPipeline>,
    framebuffer: Arc<Framebuffer>,
    descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    output_image: Arc<AttachmentImage>,
    camera: Option<super::camera::Camera>,

    test_bvh: bvh::BVHTree,
    update_data: (
        Vec<shaders::GpuBVHNode>,
        shaders::GpuBVHMeta,
        Option<shaders::GpuProjectionData>,
    ),
}

impl RayTracer {
    pub fn new<V: VulkanBasic>(
        vulkan_common: V,
        depth: Arc<AttachmentImage>,
        norm_rough: Arc<AttachmentImage>,
        velocity: Arc<AttachmentImage>,
        output_image: Arc<AttachmentImage>,
    ) -> Self {
        let rt_out = AttachmentImage::with_usage(
            vulkan_common.get_device(),
            vulkan_common.get_config().resolution,
            super::HDR_FORMAT,
            ImageUsage {
                sampled: true,
                storage: true,
                ..ImageUsage::none()
            },
        )
        .unwrap();

        let compute_pipeline = ComputePipeline::new(
            vulkan_common.get_device(),
            shaders::get_compute_shader(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &(),
            None,
            |_| {},
        )
        .unwrap();

        let linear_sampler = Sampler::new(
            vulkan_common.get_device(),
            SamplerCreateInfo::simple_repeat_linear_no_mipmap(),
        )
        .unwrap();

        let compute_descriptor_set = PersistentDescriptorSet::new(
            compute_pipeline.layout().set_layouts()[0].clone(),
            [
                WriteDescriptorSet::image_view_sampler(
                    0,
                    norm_rough.clone().to_image_view(),
                    linear_sampler.clone(),
                ),
                WriteDescriptorSet::image_view_sampler(
                    1,
                    depth.clone().to_image_view(),
                    linear_sampler.clone(),
                ),
                /*WriteDescriptorSet::image_view_sampler(
                    2,
                    velocity.clone().to_image_view(),
                    linear_sampler.clone(),
                ),*/
                WriteDescriptorSet::image_view(3, rt_out.clone().to_image_view()),
            ],
        )
        .unwrap();

        let bvh_meta_buffer = DeviceLocalBuffer::new(
            vulkan_common.get_device(),
            BufferUsage::all(),
            [vulkan_common.get_graphics_queue().family()]
                .iter()
                .cloned(),
        )
        .unwrap();

        let bvh_nodes_buffer = DeviceLocalBuffer::array(
            vulkan_common.get_device(),
            1000000 as u64,
            BufferUsage::all(),
            [vulkan_common.get_graphics_queue().family()]
                .iter()
                .cloned(),
        )
        .unwrap();

        let bvh_descriptor_set = PersistentDescriptorSet::new(
            compute_pipeline.layout().set_layouts()[1].clone(),
            [
                WriteDescriptorSet::buffer(0, bvh_meta_buffer.clone()),
                WriteDescriptorSet::buffer(1, bvh_nodes_buffer.clone()),
            ],
        )
        .unwrap();

        let render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: Load,
                    store: Store,
                    format: super::HDR_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::ColorAttachmentOptimal,
                    final_layout: ImageLayout::General,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        }
        .unwrap();

        let pipeline = GraphicsPipeline::start()
            .vertex_input_state(BuffersDefinition::new())
            .cull_mode_disabled()
            .viewports_dynamic_scissors_irrelevant(1)
            .viewports(iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: {
                    let dimensions = output_image.dimensions();
                    [dimensions.width() as f32, dimensions.height() as f32]
                },
                depth_range: 0.0..1.0,
            }))
            .vertex_shader(
                shaders::get_vertex_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .fragment_shader(
                shaders::get_fragment_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .blend_collective(AttachmentBlend {
                color_op: BlendOp::Add,
                color_source: BlendFactor::One,
                color_destination: BlendFactor::One,
                alpha_op: BlendOp::Add,
                alpha_source: BlendFactor::Zero,
                alpha_destination: BlendFactor::One,
            })
            .build(vulkan_common.get_device())
            .unwrap();

        let framebuffer = Framebuffer::new(
            render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![output_image.clone().to_image_view()],
                ..Default::default()
            },
        )
        .unwrap();

        let descriptor_set = PersistentDescriptorSet::new(
            pipeline.layout().set_layouts()[0].clone(),
            [
                /*WriteDescriptorSet::image_view_sampler(
                    0,
                    norm_rough.clone().to_image_view(),
                    linear_sampler.clone(),
                ),
                WriteDescriptorSet::image_view_sampler(
                    1,
                    depth.clone().to_image_view(),
                    linear_sampler.clone(),
                ),
                WriteDescriptorSet::image_view_sampler(
                    2,
                    velocity.clone().to_image_view(),
                    linear_sampler.clone(),
                ),
                WriteDescriptorSet::image_view(
                    3,
                    rt_out.clone().to_image_view()
                ),*/
            ],
        )
        .unwrap();

        Self {
            rt_out,
            compute_pipeline,
            compute_descriptor_set,

            bvh_meta_buffer,
            bvh_nodes_buffer,
            bvh_descriptor_set,
            test_descriptor_set: None,
            should_update_buffers: false,

            render_pass,
            pipeline,
            framebuffer,
            descriptor_set,
            output_image: output_image.clone(),
            camera: None,
            test_bvh: bvh::BVHTree::new(),
            update_data: (vec![], shaders::GpuBVHMeta { root: 0 }, None),
        }
    }

    pub fn render(&mut self, cbb: &mut UnsafeCommandBufferBuilder) {
        let dims = self.output_image.dimensions();
        let aspect_ratio = dims.width() as f32 / dims.height() as f32;
        let camera = self.camera.as_ref().unwrap();
        self.update_data.2 = Some(shaders::GpuProjectionData {
            mv: camera.get_view().into(),
            p: {
                let mut p = camera.get_proj(aspect_ratio);
                let v = camera.get_eye().extend(0.0);
                p[0][3] = v.x;
                p[1][3] = v.y;
                p[2][3] = v.z;
                p.into()
            },
        });
        unsafe {
            if self.should_update_buffers {
                self.should_update_buffers = false;
                self.update_data.1 = shaders::GpuBVHMeta {
                    root: self.test_bvh.root,
                };
                cbb.update_buffer(&self.update_data.1, &self.bvh_meta_buffer, 0);

                self.update_data.0 = self
                    .test_bvh
                    .nodes
                    .iter()
                    .map(|node| shaders::GpuBVHNode::from_node(node))
                    .collect::<Vec<_>>();
                println!(
                    "ksnjkdakjsbndkjsadkdk size {} root {}",
                    self.update_data.0.len(),
                    self.test_bvh.root
                );

                cbb.update_buffer(
                    self.update_data.0.as_slice(),
                    &self
                        .bvh_nodes_buffer
                        .slice(0..self.update_data.0.len() as u64)
                        .unwrap(),
                    0,
                );
            }

            let barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        top_of_pipe: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        //shader_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_write: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::Undefined,
                    new_layout: ImageLayout::General,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.rt_out.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.rt_out.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            cbb.pipeline_barrier(&barrier);

            cbb.bind_pipeline_compute(&self.compute_pipeline);
            cbb.push_constants(
                &self.compute_pipeline.layout(),
                ShaderStages {
                    compute: true,
                    ..ShaderStages::none()
                },
                0,
                std::mem::size_of::<shaders::GpuProjectionData>() as u32,
                self.update_data.2.as_ref().unwrap(),
            );

            cbb.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                self.compute_pipeline.layout(),
                0,
                [
                    self.compute_descriptor_set.inner(),
                    self.bvh_descriptor_set.inner(),
                    self.test_descriptor_set.as_ref().unwrap().inner(),
                ]
                .iter()
                .cloned(),
                [].iter().cloned(),
            );

            cbb.dispatch([
                (dims.width() as f32 / 8.0).ceil() as u32,
                (dims.height() as f32 / 8.0).ceil() as u32,
                1,
            ]);

            let barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        compute_shader: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        shader_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        fragment_shader: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        shader_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::General,
                    new_layout: ImageLayout::General,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.rt_out.format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.rt_out.inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            cbb.pipeline_barrier(&barrier);

            //blend with output

            cbb.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![None],
                    ..RenderPassBeginInfo::framebuffer(self.framebuffer.clone())
                },
                SubpassContents::Inline,
            );

            cbb.bind_pipeline_graphics(&self.pipeline);

            cbb.push_constants(
                &self.pipeline.layout(),
                ShaderStages {
                    fragment: true,
                    ..ShaderStages::none()
                },
                0,
                std::mem::size_of::<shaders::GpuProjectionData>() as u32,
                self.update_data.2.as_ref().unwrap(),
            );

            cbb.bind_descriptor_sets(
                PipelineBindPoint::Graphics,
                self.pipeline.layout(),
                0,
                [self.descriptor_set.inner()].iter().cloned(),
                [].iter().cloned(),
            );

            cbb.draw(3, 1, 0, 0);

            cbb.end_render_pass();
        }
    }

    pub fn insert_mesh(
        &mut self,
        mesh_data: &super::mesh::MeshData<u32>,
        mesh: &super::mesh::StaticMesh,
    ) {
        self.test_bvh.insert_mesh(mesh_data);
        self.test_descriptor_set = Some(
            PersistentDescriptorSet::new(
                self.compute_pipeline.layout().set_layouts()[2].clone(),
                [
                    WriteDescriptorSet::buffer(0, mesh.vertex_buffer.clone()),
                    //WriteDescriptorSet::buffer(1, mesh.index_buffer.clone()),
                ],
            )
            .unwrap(),
        );
        self.should_update_buffers = true;
    }
}

impl super::RendererHook for RayTracer {
    fn init<V: VulkanBasic>(
        vulkan_common: V,
        depth: Arc<AttachmentImage>,
        norm_rough: Arc<AttachmentImage>,
        velocity: Arc<AttachmentImage>,
        hdr: Arc<AttachmentImage>,
    ) -> Self {
        Self::new(vulkan_common, depth, norm_rough, velocity, hdr)
    }

    fn frame_start(&mut self, camera: &super::camera::Camera) {
        self.camera = Some(camera.clone());
    }

    fn post_hdr_render(&mut self, cbb: &mut UnsafeCommandBufferBuilder) {
        self.render(cbb);
    }

    fn as_any(&self) -> &dyn std::any::Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn std::any::Any {
        self
    }
}

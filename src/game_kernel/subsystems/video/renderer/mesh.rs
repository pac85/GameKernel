use bytemuck::{Pod, Zeroable};
use byteorder;
use cgmath::{self, InnerSpace};
use gltf;
use obj;
use vulkano::impl_vertex;
use vulkano::shader::spirv::QuantizationModes;

use crate::{log_err, log_warn};

use super::super::super::resources;
use super::vulkano;
use std::convert::TryFrom;
use std::sync::Arc;
use vulkano::buffer::{
    BufferAccess, BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer, ImmutableBuffer,
};
use vulkano::command_buffer::{
    AutoCommandBufferBuilder, CommandBufferUsage, CopyBufferInfo, PrimaryAutoCommandBuffer,
    PrimaryCommandBuffer,
};
use vulkano::device::Queue;
use vulkano::pipeline::graphics::rasterization::CullMode as VulkanoCullMode;
use vulkano::pipeline::graphics::vertex_input::{
    BuffersDefinition, IncompatibleVertexDefinitionError, Vertex, VertexDefinition,
};
use vulkano::shader::ShaderInterface;

use self::byteorder::{ByteOrder, LittleEndian};

#[derive(Clone)]
pub enum FaceDirection {
    ClockWise,
    CounterClockwise,
}

#[derive(Clone)]
pub enum CullMode {
    Disabled,
    Back,
    Front,
}

impl CullMode {
    pub fn vulkano_cull_mode(&self) -> VulkanoCullMode {
        match self {
            Disabled => VulkanoCullMode::None,
            Back => VulkanoCullMode::Back,
            Front => VulkanoCullMode::Front,
        }
    }
}

pub struct MeshData<I> {
    pub vertex_buffer: Box<[cgmath::Vector3<f32>]>,
    pub normal_buffer: Option<Box<[cgmath::Vector3<f32>]>>,
    pub uv_buffers: Vec<Box<[cgmath::Vector2<f32>]>>,
    pub index_buffer: Box<[I]>,

    pub face_direction: FaceDirection,
    pub cull_mode: CullMode,
}

impl<I> MeshData<I> {
    pub fn triangle_indices<'a>(&'a self) -> std::slice::ChunksExact<'a, I> {
        self.index_buffer.chunks_exact(3)
    }
}

impl<'a> TryFrom<(gltf::Primitive<'a>, &gltf::buffer::Data)> for MeshData<u32> {
    type Error = &'static str;

    fn try_from(
        (primitive, data): (gltf::Primitive<'a>, &gltf::buffer::Data),
    ) -> Result<Self, Self::Error> {
        let mut vertex_buffer = None;
        let mut normal_buffer = None;
        let mut uv_buffers = vec![];
        let mut index_buffer = None;

        for (semantic, accessor) in primitive.attributes() {
            match (semantic) {
                gltf::mesh::Semantic::Positions => {
                    if accessor.data_type() != gltf::accessor::DataType::F32 {
                        return Err("wrong format");
                    }
                    let offset = accessor.view().unwrap().offset();
                    let stride = accessor.view().unwrap().stride().unwrap_or(12);
                    let length = accessor.view().unwrap().length();

                    vertex_buffer = Some(
                        /*data.0[offset..offset + length]
                        .chunks_exact(stride)
                        .map(|el| {
                            cgmath::Vector3::new(
                                LittleEndian::read_f32(el),
                                LittleEndian::read_f32(&el[4..]),
                                LittleEndian::read_f32(&el[8..]),
                            )
                        })
                        .collect(),*/
                        unsafe {
                            //std::mem::transmute::<&[u8], &[cgmath::Vector3<f32>]>(&data.0[offset..offset + length]).into()
                            let bytes_slice = &data.0[offset..offset + length];
                            std::slice::from_raw_parts(
                                bytes_slice.as_ptr() as *const cgmath::Vector3<f32>,
                                bytes_slice.len() / std::mem::size_of::<cgmath::Vector3<f32>>(),
                            )
                            .into()
                        },
                    );
                }

                gltf::mesh::Semantic::Normals => {
                    if accessor.data_type() != gltf::accessor::DataType::F32 {
                        return Err("wrong format");
                    }
                    let offset = accessor.view().unwrap().offset();
                    let stride = accessor.view().unwrap().stride().unwrap_or(12);
                    let length = accessor.view().unwrap().length();

                    normal_buffer = Some(
                        /*data.0[offset..offset + length]
                        .chunks_exact(stride)
                        .map(|el| {
                            cgmath::Vector3::new(
                                LittleEndian::read_f32(el),
                                LittleEndian::read_f32(&el[4..]),
                                LittleEndian::read_f32(&el[8..]),
                            )
                        })
                        .collect(),*/
                        unsafe {
                            //std::mem::transmute::<&[u8], &[cgmath::Vector3<f32>]>(&data.0[offset..offset + length]).into()
                            let bytes_slice = &data.0[offset..offset + length];
                            std::slice::from_raw_parts(
                                bytes_slice.as_ptr() as *const cgmath::Vector3<f32>,
                                bytes_slice.len() / std::mem::size_of::<cgmath::Vector3<f32>>(),
                            )
                            .into()
                        },
                    );
                }

                gltf::mesh::Semantic::TexCoords(_) => {
                    if accessor.data_type() != gltf::accessor::DataType::F32 {
                        return Err("wrong format");
                    }
                    let offset = accessor.view().unwrap().offset();
                    let stride = accessor.view().unwrap().stride().unwrap_or(8);
                    let length = accessor.view().unwrap().length();

                    uv_buffers.push(
                        /*data.0[offset..offset + length]
                        .chunks_exact(stride)
                        .map(|el| {
                            cgmath::Vector2::new(
                                LittleEndian::read_f32(el),
                                LittleEndian::read_f32(&el[4..]),
                            )
                        })
                        .collect(),*/
                        unsafe {
                            //std::mem::transmute::<&[u8], &[cgmath::Vector3<f32>]>(&data.0[offset..offset + length]).into()
                            let bytes_slice = &data.0[offset..offset + length];
                            std::slice::from_raw_parts(
                                bytes_slice.as_ptr() as *const cgmath::Vector2<f32>,
                                bytes_slice.len() / std::mem::size_of::<cgmath::Vector2<f32>>(),
                            )
                            .into()
                        },
                    );
                }

                _ => {}
            }
        }

        if vertex_buffer.is_none() || normal_buffer.is_none() {
            return Err("error reading buffer, {:?}");
        }

        let indices_accessor = primitive.indices();
        if indices_accessor.is_none() {
            return Err("no indices in mesh");
        }
        let indices_accessor = indices_accessor.unwrap();
        index_buffer = Some({
            if indices_accessor.data_type() == gltf::accessor::DataType::F32 {
                return Err("wrong mesh indices format");
            }

            let offset = indices_accessor.view().unwrap().offset();
            let stride = indices_accessor.view().unwrap().stride().unwrap_or(
                if indices_accessor.data_type() == gltf::accessor::DataType::U32 {
                    4
                } else if indices_accessor.data_type() == gltf::accessor::DataType::U16 {
                    2
                } else {
                    return Err("wrong mesh indices format");
                },
            );

            let length = indices_accessor.view().unwrap().length();

            if stride == 4 {
                /*data.0[offset..offset + length]
                .chunks_exact(stride)
                .map(|el| LittleEndian::read_u32(el) as u32)
                .collect()*/
                unsafe {
                    //std::mem::transmute::<&[u8], &[cgmath::Vector3<f32>]>(&data.0[offset..offset + length]).into()
                    let bytes_slice = &data.0[offset..offset + length];
                    std::slice::from_raw_parts(
                        bytes_slice.as_ptr() as *const u32,
                        bytes_slice.len() / std::mem::size_of::<u32>(),
                    )
                    .into()
                }
            } else {
                data.0[offset..offset + length]
                    .chunks_exact(stride)
                    .map(|el| LittleEndian::read_u16(el) as u32)
                    .collect()
            }
        });

        Ok(Self {
            vertex_buffer: vertex_buffer.unwrap(),
            normal_buffer: Some(normal_buffer.unwrap()),
            uv_buffers,
            index_buffer: index_buffer.unwrap(),

            face_direction: FaceDirection::ClockWise,
            cull_mode: CullMode::Front,
        })
    }
}

impl<'a> TryFrom<obj::Obj<obj::TexturedVertex>> for MeshData<u32> {
    type Error = &'static str;

    fn try_from(obj: obj::Obj<obj::TexturedVertex>) -> Result<Self, Self::Error> {
        Ok(MeshData {
            vertex_buffer: obj
                .vertices
                .iter()
                .map(|v| cgmath::Vector3::new(v.position[0], v.position[1], v.position[2]))
                .collect(),
            normal_buffer: Some(
                obj.vertices
                    .iter()
                    .map(|v| cgmath::Vector3::new(v.normal[0], v.normal[1], v.normal[2]))
                    .collect(),
            ),
            uv_buffers: vec![obj
                .vertices
                .iter()
                .map(|v| cgmath::Vector2::new(v.texture[0], 1.0 - v.texture[1]))
                .collect()],
            index_buffer: obj.indices.iter().map(|i| *i as u32).collect(),

            face_direction: FaceDirection::ClockWise,
            cull_mode: CullMode::Front,
        })
    }
}

#[repr(transparent)]
#[derive(Debug, Clone, Copy, Default, Pod, Zeroable)]
pub struct Vertex2 {
    pub data: [f32; 2],
}
#[repr(transparent)]
#[derive(Debug, Clone, Copy, Default, Pod, Zeroable)]
pub struct Vertex3 {
    pub data: [f32; 3],
}
#[repr(transparent)]
#[derive(Debug, Clone, Copy, Default, Pod, Zeroable)]
pub struct Vertex4 {
    pub data: [f32; 4],
}
#[repr(transparent)]
#[derive(Debug, Clone, Copy, Default, Pod, Zeroable)]
pub struct UVertex4 {
    pub data: [u32; 4],
}

impl_vertex!(Vertex2, data);
impl_vertex!(Vertex3, data);
impl_vertex!(Vertex4, data);
impl_vertex!(UVertex4, data);

fn vtx2(v: cgmath::Vector2<f32>) -> Vertex2 {
    Vertex2 { data: v.into() }
}

fn vtx3(v: cgmath::Vector3<f32>) -> Vertex3 {
    Vertex3 { data: v.into() }
}

fn vtx4(v: cgmath::Vector4<f32>) -> Vertex4 {
    Vertex4 { data: v.into() }
}

fn uvtx4(v: cgmath::Vector4<u32>) -> UVertex4 {
    UVertex4 { data: v.into() }
}

#[derive(Clone)]
pub struct StaticMesh {
    pub vertex_buffer: Arc<ImmutableBuffer<[Vertex3]>>,
    pub normal_buffer: Option<Arc<ImmutableBuffer<[Vertex3]>>>,
    pub uv_buffers: Vec<Arc<ImmutableBuffer<[Vertex2]>>>,
    pub index_buffer: Arc<ImmutableBuffer<[u32]>>,

    pub face_direction: FaceDirection,
    pub cull_mode: CullMode,
}

impl StaticMesh {
    pub fn from_mesh_data(queue: Arc<Queue>, mesh_data: &MeshData<u32>) -> Self {
        let (vertex_buffer, vertex_buffer_future) = ImmutableBuffer::from_iter(
            mesh_data.vertex_buffer.iter().map(|v| vtx3(*v)),
            BufferUsage {
                storage_buffer: true,
                ..BufferUsage::vertex_buffer()
            },
            queue.clone(),
        )
        .unwrap();

        let (normal_buffer, normal_buffer_future) = match (mesh_data.normal_buffer) {
            Some(ref buffer) => Some(
                ImmutableBuffer::from_iter(
                    buffer.iter().map(|v| vtx3(*v)),
                    BufferUsage {
                        storage_buffer: true,
                        ..BufferUsage::vertex_buffer()
                    },
                    queue.clone(),
                )
                .unwrap(),
            ),
            None => None,
        }
        .map_or((None, None), |opt_tup| (Some(opt_tup.0), Some(opt_tup.1)));

        let uv_buffers: Vec<_> = mesh_data
            .uv_buffers
            .iter()
            .map(|data| {
                ImmutableBuffer::from_iter(
                    data.iter().map(|v| vtx2(*v)),
                    BufferUsage {
                        storage_buffer: true,
                        ..BufferUsage::vertex_buffer()
                    },
                    queue.clone(),
                )
                .unwrap()
            })
            .collect();

        let (index_buffer, index_buffer_future) = ImmutableBuffer::from_iter(
            mesh_data.index_buffer.iter().map(|v| (*v).into()),
            BufferUsage {
                storage_buffer: true,
                ..BufferUsage::index_buffer()
            },
            queue.clone(),
        )
        .unwrap();

        //TODO: wait for the futures or chamge the function to return them

        Self {
            vertex_buffer,
            normal_buffer,
            uv_buffers: uv_buffers
                .iter()
                .map(|(buffer, _)| buffer)
                .cloned()
                .collect(),
            index_buffer,

            face_direction: mesh_data.face_direction.clone(),
            cull_mode: mesh_data.cull_mode.clone(),
        }
    }
}

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Position {
    position: [f32; 3],
}
vulkano::impl_vertex!(Position, position);

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Normal {
    normal: [f32; 3],
}
vulkano::impl_vertex!(Normal, normal);

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct UvCoord {
    uv: [f32; 2],
}
vulkano::impl_vertex!(UvCoord, uv);

pub fn get_static_mesh_definition() -> BuffersDefinition {
    BuffersDefinition::new()
        .vertex::<Position>()
        .vertex::<Normal>()
        .vertex::<UvCoord>()
}

/*pub struct StaticMeshVertexDefinition {}

unsafe impl VertexSource<Vec<Arc<dyn BufferAccess + Send + Sync>>> for StaticMeshVertexDefinition {
    fn decode(
        &self,
        source: Vec<Arc<dyn BufferAccess + Send + Sync>>,
    ) -> (Vec<Box<dyn BufferAccess + Send + Sync>>, usize, usize) {
        let vertices = source
            .iter()
            .enumerate()
            .map(|(i, s)| {
                s.size()
                    / match i {
                        0 => std::mem::size_of::<Vertex3>(),
                        1 => std::mem::size_of::<Normal>(),
                        2 => std::mem::size_of::<UvCoord>(),
                        _ => panic!("wtf"),
                    }
            })
            .min()
            .unwrap();
        (
            /*source
            .iter()
            .map(|v| Box::new((v).clone()))
            .collect(),*/
            vec![
                Box::new(source[0].clone()),
                Box::new(source[1].clone()),
                Box::new(source[2].clone()),
            ],
            vertices,
            1,
        )
    }
}

unsafe impl VertexDefinition for StaticMeshVertexDefinition
{
    type BuffersIter = std::vec::IntoIter<(u32, usize, InputRate)>;
    type AttribsIter = std::vec::IntoIter<(u32, u32, AttributeInfo)>;

    fn definition(
        &self,
        interface: &ShaderInterface,
    ) -> Result<(Self::BuffersIter, Self::AttribsIter), IncompatibleVertexDefinitionError> {
        let attrib = {
            let mut attribs = Vec::with_capacity(interface.elements().len());
            for e in interface.elements() {
                let name = e.name.as_ref().unwrap();

                let (infos, buf_offset) = if let Some(infos) = <Vertex3 as Vertex>::member(name) {
                    (infos, 0)
                } else if let Some(infos) = <Normal as Vertex>::member(name) {
                    (infos, 1)
                } else if let Some(infos) = <UvCoord as Vertex>::member(name) {
                    (infos, 2)
                } else {
                    return Err(IncompatibleVertexDefinitionError::MissingAttribute {
                        attribute: name.clone().into_owned(),
                    });
                };

                if !infos.ty.matches(
                    infos.array_size,
                    e.format,
                    e.location.end - e.location.start,
                ) {
                    return Err(IncompatibleVertexDefinitionError::FormatMismatch {
                        attribute: name.clone().into_owned(),
                        shader: (e.format, (e.location.end - e.location.start) as usize),
                        definition: (infos.ty, infos.array_size),
                    });
                }

                let mut offset = infos.offset;
                for loc in e.location.clone() {
                    attribs.push((
                        loc,
                        buf_offset,
                        AttributeInfo {
                            offset: offset,
                            format: e.format,
                        },
                    ));
                    offset += e.format.size().unwrap();
                }
            }
            attribs
        }
        .into_iter();

        let buffers = vec![
            (0, std::mem::size_of::<Vertex3>(), InputRate::Vertex),
            (1, std::mem::size_of::<Normal>(), InputRate::Vertex),
            (2, std::mem::size_of::<UvCoord>(), InputRate::Vertex),
        ]
        .into_iter();

        Ok((buffers, attrib))
    }
}*/

use std::collections::{BTreeMap, HashMap, VecDeque};
use std::ops::Mul;

use cgmath::Rotation;
use cgmath::SquareMatrix;
use cgmath::VectorSpace;
use cgmath::{Quaternion, Vector3, Vector4};

#[derive(Clone, Debug)]
pub struct BoneTransform {
    pub scale: cgmath::Vector4<f32>,
    pub rotation: cgmath::Quaternion<f32>,
    pub offset: cgmath::Vector4<f32>,
}

fn non_unifrom_scale(a: Vector4<f32>, b: Vector4<f32>) -> Vector4<f32> {
    Vector4::new(a.x * b.x, a.y * b.y, a.z * b.z, 0.0)
}

impl Mul for BoneTransform {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        Self {
            scale: non_unifrom_scale(rhs.scale, self.scale),
            rotation: self.rotation * rhs.rotation,
            offset: self.offset
                + self
                    .rotation
                    .rotate_vector(non_unifrom_scale(self.scale, rhs.offset).truncate())
                    .extend(0.0),
            //+ rotate_by_quat(self.offset.truncate(), self.rotation).extend(0.0),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Bone {
    transformation: BoneTransform,
    children: Vec<usize>,
    inverse_bind_matrix: cgmath::Matrix4<f32>,
}

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct GpuBone {
    pub scale: [f32; 4],
    pub rotation: [f32; 4],
    pub offset: [f32; 4],
    pub inverse_bind_matrix: [[f32; 4]; 4],
}

impl From<&Bone> for GpuBone {
    fn from(bone: &Bone) -> Self {
        Self {
            scale: bone.transformation.scale.into(),
            rotation: {
                let [w, i, j, k]: [f32; 4] = bone.transformation.rotation.into();
                [i, j, k, w]
            },
            offset: bone.transformation.offset.into(),
            inverse_bind_matrix: bone.inverse_bind_matrix.into(),
        }
    }
}

fn s_slerp(a: Quaternion<f32>, b: Quaternion<f32>, t: f32) -> Quaternion<f32> {
    if a.v.dot(b.v) < 1e-16 {
        a
    } else {
        a.slerp(b, t)
    }
}

impl Bone {
    pub fn interpolate(&self, other: &Self, factor: f32) -> Self {
        Self {
            transformation: BoneTransform {
                scale: self
                    .transformation
                    .scale
                    .lerp(other.transformation.scale, factor),
                rotation: s_slerp(
                    self.transformation.rotation,
                    other.transformation.rotation,
                    factor,
                ),
                offset: self
                    .transformation
                    .offset
                    .lerp(other.transformation.offset, factor),
            },
            children: self.children.clone(),
            inverse_bind_matrix: self.inverse_bind_matrix,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Skeleton {
    pub bones: Box<[Bone]>,
    pub roots: Box<[usize]>,
    indices_map: Option<HashMap<usize, usize>>, //TODO: generalize
}

impl Skeleton {
    pub fn new(bones: Box<[Bone]>, roots: Box<[usize]>) -> Self {
        Self {
            bones,
            roots,
            indices_map: None,
        }
    }

    pub fn interpolate(&self, other: &Skeleton, factor: f32) -> Self {
        Self {
            bones: self
                .bones
                .iter()
                .zip(other.bones.iter())
                .map(|(a, b)| a.interpolate(b, factor))
                .collect(),
            ..self.clone() //TODO: copies all bones?
        }
    }

    pub fn skeleton_frame(&self) -> Self {
        let mut res = self.clone();
        //explores the skeleton graph breadth first calculating the transormations
        let mut queue = VecDeque::new();

        for root in self.roots.iter().map(|idx| self.bones[*idx].clone()) {
            queue.push_back(root);
            while let Some(bone) = queue.pop_front() {
                for children in bone.children.iter() {
                    res.bones[*children].transformation =
                        bone.transformation.clone() * res.bones[*children].transformation.clone();
                }
                queue.extend(bone.children.iter().map(|idx| res.bones[*idx].clone()));
            }
        }
        res
    }

    pub fn get_gpu_data(&self) -> Vec<GpuBone> {
        self.bones.iter().map(|bone| bone.into()).collect()
    }
}

impl<'a>
    TryFrom<(
        gltf::skin::Skin<'a>,
        gltf::scene::Node<'a>,
        &gltf::buffer::Data,
    )> for Skeleton
{
    type Error = &'static str;

    fn try_from(
        (skin, armature, data): (
            gltf::skin::Skin<'a>,
            gltf::scene::Node<'a>,
            &gltf::buffer::Data,
        ),
    ) -> Result<Self, Self::Error> {
        let joints = skin.joints().collect::<Box<[_]>>();
        let inverse_bind_matrices = skin
            .inverse_bind_matrices()
            .map(|accessor| {
                /*if accessor.data_type() != gltf::accessor::DataType::F32{
                    return Err("wrong format");
                }*/

                let offset = accessor.view().unwrap().offset();
                let stride = accessor.view().unwrap().stride().unwrap_or(64);
                let length = accessor.view().unwrap().length();

                let inverse_bind_matrices: Vec<_> = data.0[offset..offset + length]
                    .chunks_exact(stride)
                    .map(|el| {
                        cgmath::Matrix4::new(
                            LittleEndian::read_f32(el),
                            LittleEndian::read_f32(&el[4..]),
                            LittleEndian::read_f32(&el[8..]),
                            LittleEndian::read_f32(&el[12..]),
                            LittleEndian::read_f32(&el[16..]),
                            LittleEndian::read_f32(&el[20..]),
                            LittleEndian::read_f32(&el[24..]),
                            LittleEndian::read_f32(&el[28..]),
                            LittleEndian::read_f32(&el[32..]),
                            LittleEndian::read_f32(&el[36..]),
                            LittleEndian::read_f32(&el[40..]),
                            LittleEndian::read_f32(&el[44..]),
                            LittleEndian::read_f32(&el[48..]),
                            LittleEndian::read_f32(&el[52..]),
                            LittleEndian::read_f32(&el[56..]),
                            LittleEndian::read_f32(&el[60..]),
                        )
                    })
                    .collect();

                inverse_bind_matrices
            })
            .unwrap_or_else(|| {
                std::iter::repeat(cgmath::Matrix4::identity())
                    .take(joints.len())
                    .collect()
            });

        let indices_map: HashMap<_, _> = joints
            .iter()
            .map(|node| node.index())
            .enumerate()
            .map(|(a, b)| (b, a))
            .collect();

        Ok(Self {
            bones: joints
                .iter()
                .zip(inverse_bind_matrices.iter().cloned())
                .map(|(node, inverse_bind_matrix)| Bone {
                    transformation: {
                        if let gltf::scene::Transform::Decomposed {
                            translation,
                            rotation,
                            scale,
                        } = node.transform()
                        {
                            BoneTransform {
                                scale: cgmath::Vector4::new(scale[0], scale[1], scale[2], 0.0),
                                offset: cgmath::Vector4::new(
                                    translation[0],
                                    translation[1],
                                    translation[2],
                                    0.0,
                                ),
                                rotation: cgmath::Quaternion::new(
                                    rotation[3],
                                    rotation[0],
                                    rotation[1],
                                    rotation[2],
                                ),
                            }
                        } else {
                            panic!("");
                        }
                    },
                    children: node
                        .children()
                        .map(|children| indices_map.get(&children.index()).unwrap())
                        .cloned()
                        .collect(),
                    inverse_bind_matrix,
                })
                .collect(),
            roots: armature
                .children()
                .filter(|child| indices_map.contains_key(&child.index()))
                .map(|children| indices_map.get(&children.index()).unwrap())
                .cloned()
                .collect(),
            indices_map: Some(indices_map),
        })
    }
}

pub struct SkeletalMeshData<I> {
    pub mesh_data: MeshData<I>,
    pub affecting_bones: Box<[cgmath::Vector4<u32>]>,
    pub weights: Box<[cgmath::Vector4<f32>]>,

    pub skeleton: Skeleton,
}

impl<'a>
    TryFrom<(
        gltf::scene::Node<'a>,
        gltf::scene::Node<'a>,
        &gltf::buffer::Data,
    )> for SkeletalMeshData<u32>
{
    type Error = &'static str;

    fn try_from(
        (node, armature, data): (gltf::scene::Node, gltf::scene::Node, &gltf::buffer::Data),
    ) -> Result<Self, Self::Error> {
        let mut affecting_bones = None;
        let mut weights = None;

        let primitive = &node
            .mesh()
            .ok_or("the gltf node doesn't contain a mesh")?
            .primitives()
            .next()
            .ok_or("the gltf mesh has no primitives")?;

        let mesh_data = MeshData::try_from((primitive.clone(), data))?;

        for (semantic, accessor) in primitive.attributes() {
            match (semantic) {
                gltf::mesh::Semantic::Joints(0) => {
                    if accessor.data_type() != gltf::accessor::DataType::U8
                        && accessor.data_type() != gltf::accessor::DataType::U16
                    {
                        log_err!("wrong format {:?}", accessor.data_type());
                        return Err("wrong format");
                    }

                    let is_16bit = accessor.data_type() == gltf::accessor::DataType::U16;

                    let offset = accessor.view().unwrap().offset();
                    let stride =
                        accessor
                            .view()
                            .unwrap()
                            .stride()
                            .unwrap_or(if is_16bit { 8 } else { 4 });
                    let length = accessor.view().unwrap().length();

                    affecting_bones = Some(
                        data.0[offset..offset + length]
                            .chunks_exact(stride)
                            .map(|el| {
                                if is_16bit {
                                    cgmath::Vector4::new(
                                        LittleEndian::read_u16(el) as u32,
                                        LittleEndian::read_u16(&el[1..]) as u32,
                                        LittleEndian::read_u16(&el[2..]) as u32,
                                        LittleEndian::read_u16(&el[3..]) as u32,
                                    )
                                } else {
                                    cgmath::Vector4::new(
                                        el[0] as u32,
                                        el[1] as u32,
                                        el[2] as u32,
                                        el[3] as u32,
                                    )
                                }
                            })
                            .collect(),
                    );
                }

                gltf::mesh::Semantic::Weights(0) => {
                    if accessor.data_type() != gltf::accessor::DataType::F32 {
                        return Err("wrong format");
                    }
                    let offset = accessor.view().unwrap().offset();
                    let stride = accessor.view().unwrap().stride().unwrap_or(16);
                    let length = accessor.view().unwrap().length();

                    weights = Some(
                        data.0[offset..offset + length]
                            .chunks_exact(stride)
                            .map(|el| {
                                cgmath::Vector4::new(
                                    LittleEndian::read_f32(el),
                                    LittleEndian::read_f32(&el[4..]),
                                    LittleEndian::read_f32(&el[8..]),
                                    LittleEndian::read_f32(&el[12..]),
                                )
                            })
                            .collect(),
                    );
                }

                _ => {}
            }
        }

        if affecting_bones.is_none() || weights.is_none() {
            return Err("error reading buffer, {:?}");
        }

        let affecting_bones = affecting_bones.unwrap();
        let weights = weights.unwrap();

        let skeleton =
            Skeleton::try_from((node.skin().ok_or("no skin for node")?, armature, data))?;

        Ok(Self {
            mesh_data,
            affecting_bones,
            weights,

            skeleton,
        })
    }
}

#[derive(Clone)]
pub struct SkeletalMesh {
    pub mesh: StaticMesh,
    pub affecting_bones_buffer: Arc<ImmutableBuffer<[UVertex4]>>,
    pub weights_buffer: Arc<ImmutableBuffer<[Vertex4]>>,
    pub skeleton_buffer: Arc<DeviceLocalBuffer<[GpuBone]>>,
}

impl SkeletalMesh {
    pub fn from_skeleton_data(
        queue: Arc<Queue>,
        skeletal_mesh_data: &SkeletalMeshData<u32>,
    ) -> Result<Self, &'static str> {
        let mesh = StaticMesh::from_mesh_data(queue.clone(), &skeletal_mesh_data.mesh_data);

        let (affecting_bones_buffer, _) = ImmutableBuffer::from_iter(
            skeletal_mesh_data.affecting_bones.iter().map(|v| uvtx4(*v)),
            BufferUsage::vertex_buffer(),
            queue.clone(),
        )
        .map_err(|_| "failed uploading affecting bones")?;

        let (weights_buffer, _) = ImmutableBuffer::from_iter(
            skeletal_mesh_data.weights.iter().map(|v| vtx4(*v)),
            BufferUsage::vertex_buffer(),
            queue.clone(),
        )
        .map_err(|_| "failed uploading wights")?;

        let skeleton_buffer: Arc<DeviceLocalBuffer<[GpuBone]>> = DeviceLocalBuffer::array(
            queue.device().clone(),
            skeletal_mesh_data.skeleton.bones.len() as u64,
            BufferUsage {
                transfer_dst: true,
                storage_buffer: true,
                uniform_buffer: true,
                ..BufferUsage::none()
            },
            [queue.family()].iter().cloned(),
        )
        .map_err(|_| "failed uploading affecting bones")?;

        let skeleton_staging_buffer: Arc<CpuAccessibleBuffer<[GpuBone]>> =
            CpuAccessibleBuffer::from_iter(
                queue.device().clone(),
                BufferUsage::transfer_src(),
                false,
                skeletal_mesh_data
                    .skeleton
                    .bones
                    .iter()
                    .map(|bone| bone.into()),
            )
            .map_err(|_| "failed uploading skeleton")?;

        let mut cbb = AutoCommandBufferBuilder::primary(
            queue.device().clone(),
            queue.family(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        cbb.copy_buffer(CopyBufferInfo::buffers(
            skeleton_staging_buffer.clone(),
            skeleton_buffer.clone(),
        ))
        .unwrap();
        let cb = cbb.build().unwrap();

        cb.execute(queue).unwrap(); //future is executed when it goes out of scope

        Ok(Self {
            mesh,
            affecting_bones_buffer,
            weights_buffer,
            skeleton_buffer,
        })
    }

    //TODO: find safe abstraction
    pub unsafe fn update_skeleton(
        &mut self,
        skeleton: &Skeleton,
        queue: Arc<Queue>,
    ) -> Result<(), &'static str> {
        let skeleton_staging_buffer: Arc<CpuAccessibleBuffer<[GpuBone]>> =
            CpuAccessibleBuffer::from_iter(
                queue.device().clone(),
                BufferUsage::transfer_src(),
                false,
                skeleton.bones.iter().map(|bone| bone.into()),
            )
            .map_err(|_| "failed uploading skeleton")?;

        let mut cbb = AutoCommandBufferBuilder::primary(
            queue.device().clone(),
            queue.family(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();
        cbb.copy_buffer(CopyBufferInfo::buffers(
            skeleton_staging_buffer.clone(),
            self.skeleton_buffer.clone(),
        ))
        .unwrap();
        let cb = cbb.build().unwrap();

        cb.execute(queue).unwrap(); //future is executed when it goes out of scope

        Ok(())
    }
}

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct VertexAffectingBones {
    affecting_bones_indices: [u32; 4],
}
vulkano::impl_vertex!(VertexAffectingBones, affecting_bones_indices);

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct VertexWeights {
    weights: [f32; 4],
}
vulkano::impl_vertex!(VertexWeights, weights);

pub fn get_skeletal_mesh_definition() -> BuffersDefinition {
    BuffersDefinition::new()
        .vertex::<Position>()
        .vertex::<Normal>()
        .vertex::<UvCoord>()
        .vertex::<VertexAffectingBones>()
        .vertex::<VertexWeights>()
}

/*pub struct SkeletalMeshVertexDefinition {}

unsafe impl VertexSource<Vec<Arc<dyn BufferAccess + Send + Sync>>>
    for SkeletalMeshVertexDefinition
{
    fn decode(
        &self,
        source: Vec<Arc<dyn BufferAccess + Send + Sync>>,
    ) -> (Vec<Box<dyn BufferAccess + Send + Sync>>, usize, usize) {
        let vertices = source
            .iter()
            .enumerate()
            .map(|(i, s)| {
                s.size()
                    / match i {
                        0 => std::mem::size_of::<Vertex3>(),
                        1 => std::mem::size_of::<Normal>(),
                        2 => std::mem::size_of::<UvCoord>(),
                        3 => std::mem::size_of::<VertexAffectingBones>(),
                        4 => std::mem::size_of::<VertexWeights>(),
                        _ => panic!("wtf"),
                    }
            })
            .min()
            .unwrap();
        (
            /*source
            .iter()
            .map(|v| Box::new((v).clone()))
            .collect(),*/
            vec![
                Box::new(source[0].clone()),
                Box::new(source[1].clone()),
                Box::new(source[2].clone()),
                Box::new(source[3].clone()),
                Box::new(source[4].clone()),
            ],
            vertices,
            1,
        )
    }
}

unsafe impl VertexDefinition for SkeletalMeshVertexDefinition
{
    type BuffersIter = std::vec::IntoIter<(u32, usize, InputRate)>;
    type AttribsIter = std::vec::IntoIter<(u32, u32, AttributeInfo)>;

    fn definition(
        &self,
        interface: &ShaderInterface,
    ) -> Result<(Self::BuffersIter, Self::AttribsIter), IncompatibleVertexDefinitionError> {
        let attrib = {
            let mut attribs = Vec::with_capacity(interface.elements().len());
            for e in interface.elements() {
                let name = e.name.as_ref().unwrap();

                let (infos, buf_offset) = if let Some(infos) = <Vertex3 as Vertex>::member(name) {
                    (infos, 0)
                } else if let Some(infos) = <Normal as Vertex>::member(name) {
                    (infos, 1)
                } else if let Some(infos) = <UvCoord as Vertex>::member(name) {
                    (infos, 2)
                } else if let Some(infos) = <VertexAffectingBones as Vertex>::member(name) {
                    (infos, 3)
                } else if let Some(infos) = <VertexWeights as Vertex>::member(name) {
                    (infos, 4)
                } else {
                    return Err(IncompatibleVertexDefinitionError::MissingAttribute {
                        attribute: name.clone().into_owned(),
                    });
                };

                if !infos.ty.matches(
                    infos.array_size,
                    e.format,
                    e.location.end - e.location.start,
                ) {
                    return Err(IncompatibleVertexDefinitionError::FormatMismatch {
                        attribute: name.clone().into_owned(),
                        shader: (e.format, (e.location.end - e.location.start) as usize),
                        definition: (infos.ty, infos.array_size),
                    });
                }

                let mut offset = infos.offset;
                for loc in e.location.clone() {
                    attribs.push((
                        loc,
                        buf_offset,
                        AttributeInfo {
                            offset: offset,
                            format: e.format,
                        },
                    ));
                    offset += e.format.size().unwrap();
                }
            }
            attribs
        }
        .into_iter();

        let buffers = vec![
            (0, std::mem::size_of::<Vertex3>(), InputRate::Vertex),
            (1, std::mem::size_of::<Normal>(), InputRate::Vertex),
            (2, std::mem::size_of::<UvCoord>(), InputRate::Vertex),
            (
                3,
                std::mem::size_of::<VertexAffectingBones>(),
                InputRate::Vertex,
            ),
            (4, std::mem::size_of::<VertexWeights>(), InputRate::Vertex),
        ]
        .into_iter();

        Ok((buffers, attrib))
    }
}*/

#[derive(Clone, Debug)]
pub enum KeyVal {
    Translation(cgmath::Vector3<f32>),
    Rotation(cgmath::Quaternion<f32>),
    Scale(cgmath::Vector4<f32>),
}

#[derive(Clone, Debug)]
pub struct KeyFrame {
    time: f32,
    key_vals: Vec<(KeyVal, usize)>,
}

impl KeyFrame {
    pub fn apply(&self, prev: &Skeleton) -> Skeleton {
        let mut res = prev.clone();
        for (ref val, idx) in self.key_vals.iter() {
            match val {
                KeyVal::Translation(t) => {
                    res.bones[*idx].transformation.offset = cgmath::Vector4::new(t.x, t.y, t.z, 0.0)
                }
                KeyVal::Rotation(r) => res.bones[*idx].transformation.rotation = r.clone(),
                KeyVal::Scale(s) => res.bones[*idx].transformation.scale = s.clone(),
            }
        }
        res
    }
}

pub struct CachedKeyframe {
    time: f32,
    skeleton: Skeleton,
}

impl CachedKeyframe {
    pub fn new(time: f32, skeleton: Skeleton) -> Self {
        Self { time, skeleton }
    }
}

pub struct SkeletalAnimation {
    pub key_frames: Vec<KeyFrame>,
    cached_skeletons: Vec<CachedKeyframe>,
}

impl SkeletalAnimation {
    pub fn new(skeleton: &Skeleton, key_frames: Vec<KeyFrame>) -> Self {
        let first_time = key_frames.first().unwrap().time;
        //first skeleton is bind pose
        let mut cached_skeletons = vec![CachedKeyframe::new(
            first_time,
            key_frames.first().unwrap().apply(&skeleton),
        )];
        for key_frame in key_frames[1..].iter() {
            let last_skeleton = &cached_skeletons.last().unwrap();
            let new_skeleton = key_frame.apply(&last_skeleton.skeleton);
            cached_skeletons.push(CachedKeyframe::new(key_frame.time, new_skeleton));
        }
        Self {
            key_frames,
            cached_skeletons,
        }
    }

    pub fn get_key_frame(&mut self, time: f32) -> Skeleton {
        std::iter::once(&self.cached_skeletons[0])
            .chain(self.cached_skeletons.iter())
            .zip(self.cached_skeletons.iter())
            .find(|(_, key_frame)| key_frame.time >= time)
            .map(|(c1, c2)| {
                let delta = c2.time - c1.time;
                let factor = if delta != 0.0 {
                    (time - c1.time) / delta
                } else {
                    //shouldn't this be 0?
                    c1.time
                };

                c1.skeleton.interpolate(&c2.skeleton, factor)
            })
            .unwrap_or_else(|| self.cached_skeletons.last().unwrap().skeleton.clone())
    }
}

use std::cmp::{PartialEq, PartialOrd};

#[derive(PartialOrd, Debug)]
struct OrdF32 {
    pub v: f32,
}

impl PartialEq for OrdF32 {
    fn eq(&self, other: &Self) -> bool {
        self.v == other.v
    }
}

impl Eq for OrdF32 {}

fn ord_f32(v: f32) -> OrdF32 {
    OrdF32 { v }
}

impl core::cmp::Ord for OrdF32 {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        if self.v < other.v {
            core::cmp::Ordering::Less
        } else if self.v > other.v {
            core::cmp::Ordering::Greater
        } else {
            core::cmp::Ordering::Equal
        }
    }

    fn max(self, other: Self) -> Self {
        ord_f32(self.v.max(other.v))
    }

    fn min(self, other: Self) -> Self {
        ord_f32(self.v.min(other.v))
    }

    /*fn clamp(self, min: Self, max: Self) -> Self {
        self.v.clamp(min, max))
    }*/
}

fn property_stride(property: gltf::animation::Property) -> usize {
    match property {
        gltf::animation::Property::Translation => 12,
        gltf::animation::Property::Rotation => 16,
        gltf::animation::Property::Scale => 12,
        gltf::animation::Property::MorphTargetWeights => 4,
    }
}

impl<'a>
    TryFrom<(
        gltf::animation::Animation<'a>,
        &Skeleton,
        &gltf::buffer::Data,
    )> for SkeletalAnimation
{
    type Error = &'static str;

    fn try_from(
        (gltf_anim, skeleton, data): (
            gltf::animation::Animation<'a>,
            &Skeleton,
            &gltf::buffer::Data,
        ),
    ) -> Result<Self, Self::Error> {
        let mut keyframes_map: BTreeMap<_, Vec<_>> = BTreeMap::new();
        let node_indices_map = skeleton.indices_map.as_ref().unwrap();
        for (target, sampler) in gltf_anim
            .channels()
            .map(|channel| (channel.target(), channel.sampler()))
        {
            //times
            let times_offset = sampler.input().view().unwrap().offset();
            let times_stride = sampler.input().view().unwrap().stride().unwrap_or(4);
            let times_length = sampler.input().view().unwrap().length();

            let times_iterator = data[times_offset..times_offset + times_length]
                .chunks_exact(times_stride)
                .map(|time| LittleEndian::read_f32(time));

            let values_offset = sampler.output().view().unwrap().offset();
            let values_stride = sampler
                .output()
                .view()
                .unwrap()
                .stride()
                .unwrap_or(property_stride(target.property()));
            let values_length = sampler.output().view().unwrap().length();

            let values_iterator =
                data[values_offset..values_offset + values_length].chunks_exact(values_stride);

            match target.property() {
                gltf::animation::Property::Translation => {
                    let translations_iterator = values_iterator.map(|value| {
                        Vector3::new(
                            LittleEndian::read_f32(value),
                            LittleEndian::read_f32(&value[4..]),
                            LittleEndian::read_f32(&value[8..]),
                        )
                    });

                    for (time, translation) in times_iterator.zip(translations_iterator) {
                        if !node_indices_map.contains_key(&target.node().index()) {
                            log_warn!(
                                "target \"{}\" ( index {}) not foundi in skeleton",
                                &target.node().name().unwrap_or(""),
                                target.node().index()
                            );
                            continue;
                        }

                        if let Some(kf) = keyframes_map.get_mut(&ord_f32(time)) {
                            kf.push((
                                KeyVal::Translation(translation),
                                node_indices_map[&target.node().index()],
                            ));
                        } else {
                            keyframes_map.insert(
                                ord_f32(time),
                                vec![(
                                    KeyVal::Translation(translation),
                                    node_indices_map[&target.node().index()],
                                )],
                            );
                        }
                    }
                }

                gltf::animation::Property::Rotation => {
                    let rotations_iterator = values_iterator.map(|value| {
                        Quaternion::new(
                            LittleEndian::read_f32(&value[12..]),
                            LittleEndian::read_f32(value),
                            LittleEndian::read_f32(&value[4..]),
                            LittleEndian::read_f32(&value[8..]),
                        )
                    });

                    for (time, rotation) in times_iterator.zip(rotations_iterator) {
                        if !node_indices_map.contains_key(&target.node().index()) {
                            log_warn!(
                                "target \"{}\" ( index {}) not foundi in skeleton",
                                &target.node().name().unwrap_or(""),
                                target.node().index()
                            );
                            continue;
                        }

                        if let Some(kf) = keyframes_map.get_mut(&ord_f32(time)) {
                            kf.push((
                                KeyVal::Rotation(rotation),
                                node_indices_map[&target.node().index()],
                            ));
                        } else {
                            keyframes_map.insert(
                                ord_f32(time),
                                vec![(
                                    KeyVal::Rotation(rotation),
                                    node_indices_map[&target.node().index()],
                                )],
                            );
                        }
                    }
                }

                gltf::animation::Property::Scale => {
                    let scales_iterator = values_iterator.map(|value| {
                        Vector3::new(
                            LittleEndian::read_f32(value),
                            LittleEndian::read_f32(&value[4..]),
                            LittleEndian::read_f32(&value[8..]),
                        )
                    });

                    for (time, scale) in times_iterator.zip(scales_iterator) {
                        if !node_indices_map.contains_key(&target.node().index()) {
                            log_warn!(
                                "target \"{}\" ( index {}) not foundi in skeleton",
                                &target.node().name().unwrap_or(""),
                                target.node().index()
                            );
                            continue;
                        }

                        if let Some(kf) = keyframes_map.get_mut(&ord_f32(time)) {
                            kf.push((
                                KeyVal::Scale(scale.extend(1f32)),
                                node_indices_map[&target.node().index()],
                            ));
                        } else {
                            keyframes_map.insert(
                                ord_f32(time),
                                vec![(
                                    KeyVal::Scale(scale.extend(1f32)),
                                    node_indices_map[&target.node().index()],
                                )],
                            );
                        }
                    }
                }

                _ => {}
            }

            //for keyframe in
        }

        let key_frames: Vec<KeyFrame> = keyframes_map
            .iter()
            .map(|(time, key_vals)| KeyFrame {
                time: time.v,
                key_vals: key_vals.clone(),
            })
            .collect();

        if key_frames.len() == 0 {
            return Err("No frames where extracted");
        }

        Ok(Self::new(skeleton, key_frames))
    }
}

#[derive(Clone)]
pub enum Mesh {
    StaticMesh(StaticMesh),
    SkeletalMesh(SkeletalMesh),
}

impl Mesh {
    pub fn is_static_mesh(&self) -> bool {
        match self {
            Self::StaticMesh(_) => true,
            _ => false,
        }
    }

    pub fn is_skeletal_mesh(&self) -> bool {
        match self {
            Self::SkeletalMesh(_) => true,
            _ => false,
        }
    }
}

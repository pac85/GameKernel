use super::vulkano::shader::{EntryPoint, ShaderInterface, ShaderModule};
use serde::Deserialize;

use vulkano::device::Device;
use vulkano::pipeline::layout::PipelineLayout;

use std::sync::Arc;

#[derive(Deserialize)]
pub struct DeserializableShaderInterfaceDefEnrty {
    format: u32,
    name: String,
}

#[derive(Deserialize)]
pub struct ShaderInfo {
    input_description: Vec<DeserializableShaderInterfaceDefEnrty>,
    output_description: Vec<DeserializableShaderInterfaceDefEnrty>,
}

pub trait AbstractShader<'a> {
    unsafe fn graphics_entry_point(&'a self) -> EntryPoint<'a>;
}

type PipelineLayoutTraitObject = Arc<PipelineLayout>;

pub struct RuntimeShader {
    pub shader_module: Arc<ShaderModule>,
    pub entry_name: std::ffi::CString,
}

impl RuntimeShader {
    pub fn from_entry_point<'a>(
        entry_point: EntryPoint<'a>,
        shader_module: Arc<ShaderModule>,
    ) -> Self {
        Self {
            shader_module: shader_module.clone(),
            entry_name: std::ffi::CString::new(entry_point.name().to_bytes()).unwrap(),
        }
    }
    /*pub unsafe fn from_entry_point<I, O, L>(entry_point: &'a GraphicsEntryPoint<'a, (), I, O, L>) -> Self
        where I: 'a+ShaderInterfaceDef,
              O: 'a+ShaderInterfaceDef,
              L: PipelineLayoutDesc+Send+Sync+Clone+'static
    {
        let entry_point = entry_point.module().graphics_entry_point(entry_point.name(),
                                                                    RuntimeShaderInterface::from_interface(entry_point.input()),
                                                                    RuntimeShaderInterface::from_interface(entry_point.output()),
                                                                    Arc::new((*entry_point.layout()).clone()) as Arc<dyn PipelineLayoutDesc+Send+Sync>,
                                                                    entry_point.ty());
        Self{
            entry_point: entry_point.clone()
        }
    }*/
}

use vulkano::shader::SpecializationConstants;

impl<'a> AbstractShader<'a> for RuntimeShader {
    unsafe fn graphics_entry_point(&'a self) -> EntryPoint<'a> {
        self.shader_module
            .entry_point(self.entry_name.to_str().unwrap())
            .unwrap() //self.entry_point.clone()
    }
}

use lazy_static::lazy_static;
use std::collections::HashMap;
use std::sync::Mutex;

lazy_static! {
    static ref SHADER_CACHE: Mutex<HashMap<(Arc<Device>, u64), Arc<RuntimeShader>>> =
        Mutex::new(HashMap::new());
}

//TODO manually passing an id sucks
pub fn get_cached_or_build_shader<F>(
    device: Arc<Device>,
    id: u64,
    make_shader: F,
) -> Arc<RuntimeShader>
where
    F: Fn() -> Arc<RuntimeShader>,
{
    let mut locked_cache = SHADER_CACHE.lock().unwrap();
    if let Some(shader_module) = locked_cache.get(&(device.clone(), id)) {
        shader_module.clone()
    } else {
        let shader_module = make_shader();
        locked_cache.insert((device.clone(), id), shader_module.clone());
        shader_module
    }
}

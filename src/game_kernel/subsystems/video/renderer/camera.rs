use cgmath;

#[derive(Clone, Copy)]
pub struct Camera {
    pub eye: cgmath::Point3<f32>,
    pub dir: cgmath::Vector3<f32>,
    pub up: cgmath::Vector3<f32>,

    pub fov: cgmath::Rad<f32>,

    clipping: (f32, f32),
}

impl Camera {
    pub fn new(
        eye: cgmath::Point3<f32>,
        dir: cgmath::Vector3<f32>,
        up: cgmath::Vector3<f32>,
        fov: cgmath::Rad<f32>,
        clipping: (f32, f32),
    ) -> Self {
        Self {
            eye,
            dir,
            up,
            fov,
            clipping,
        }
    }

    pub fn get_view(&self) -> cgmath::Matrix4<f32> {
        cgmath::Matrix4::<f32>::look_at_dir(self.eye, self.dir, self.up)
    }

    pub fn get_proj(&self, aspect_ratio: f32) -> cgmath::Matrix4<f32> {
        cgmath::perspective(self.fov, aspect_ratio, self.clipping.0, self.clipping.1)
    }

    pub fn get_eye(&self) -> cgmath::Vector3<f32> {
        cgmath::Vector3::new(self.eye.x, self.eye.y, self.eye.z)
    }
}

use std::any::type_name;
use std::any::Any;
use std::convert::TryFrom;
use std::iter;
use std::sync::Arc;

use cgmath::*;
use game_kernel_utils::factory::*;
use game_kernel_utils::get_dynamic_arg;
use lazy_static::lazy_static;
use rand::prelude::*;
use vulkano::buffer::BufferUsage;
use vulkano::buffer::{BufferAccess, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::DrawIndexedIndirectCommand;
use vulkano::command_buffer::RenderPassBeginInfo;
use vulkano::command_buffer::{sys::UnsafeCommandBufferBuilder, SubpassContents};
use vulkano::descriptor_set::*;
use vulkano::device::Device;
use vulkano::format::ClearValue;
use vulkano::image::view::ImageView;
use vulkano::image::ImageLayout;
use vulkano::image::{AttachmentImage, ImageAccess};
use vulkano::pipeline::graphics::color_blend::{AttachmentBlend, BlendFactor, BlendOp};
use vulkano::pipeline::graphics::input_assembly::IndexType;
use vulkano::pipeline::graphics::vertex_input::BuffersDefinition;
use vulkano::pipeline::graphics::viewport::Viewport;
use vulkano::pipeline::Pipeline;
use vulkano::pipeline::{ComputePipeline, GraphicsPipeline, PipelineBindPoint};
use vulkano::render_pass::FramebufferCreateInfo;
use vulkano::render_pass::{Framebuffer, RenderPass, Subpass};
use vulkano::shader::EntryPoint;
use vulkano::shader::ShaderStages;
use vulkano::single_pass_renderpass;

use crate::game_kernel;
use crate::vulkan::*;

const MAX_NEW_PARTICLES: u32 = 1024;

mod shaders {
    pub mod simple_physics {
        vulkano_shaders::shader! {
            ty: "compute",
            path: "data/shaders/particle_fx/simple_physics.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }

    pub use simple_physics::ty::FrameData as GpuFrameData;
    pub use simple_physics::ty::ParticleState as GpuParticleState;
    pub use simple_physics::ty::ParticleSystemState as GpuParticleSystemState;
    pub use simple_physics::ty::SimplePhysicsProperties as GpuSimplePhysicsProperties;
    pub use simple_physics::SpecializationConstants as SimplePhysicsSpecs;
    use vulkano::shader::ShaderModule;

    use super::*;
    pub fn get_simple_physics(device: Arc<Device>) -> Arc<ShaderModule> {
        simple_physics::load(device.clone()).unwrap()
    }

    pub mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "data/shaders/particle_fx/vertex.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }

    pub use vs::ty::ProjectionData as GpuProjectionData;

    impl GpuProjectionData {
        pub fn from_projection_data(pd: ProjectionData, time: f32, delta: f32) -> Self {
            Self {
                mvp: (pd.projection * pd.view * pd.model).into(),
                camera_up_time: [pd.view.x.y, pd.view.y.y, pd.view.z.y, time],
                camera_right_delta: [pd.view.x.x, pd.view.y.x, pd.view.z.x, delta],
            }
        }
    }

    pub fn get_vs(device: Arc<Device>) -> Arc<ShaderModule> {
        vs::load(device.clone()).unwrap()
    }

    pub mod alpha_renderer {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "data/shaders/particle_fx/alpha.glsl",
            types_meta: {
                use bytemuck::{Pod, Zeroable};

                #[derive(Clone, Copy, Zeroable, Pod)]
            },
        }
    }

    pub fn get_alpha_renderer(device: Arc<Device>) -> Arc<ShaderModule> {
        alpha_renderer::load(device.clone()).unwrap()
    }

    pub use alpha_renderer::ty::Params as GpuParams;
    pub use alpha_renderer::ty::ParticleTexture as GpuParticleTexture;

    impl GpuParticleTexture {
        pub fn from_particle_texture(pt: &ParticleTexture) -> Self {
            Self {
                uv_offset: pt.offset.into(),
                size: pt.size.into(),
                fps: pt.fps.into(),
                frames: pt.frames.into(),
            }
        }
    }

    impl GpuParams {
        pub fn from_render_params(params: &AlphaParticleRendererParams) -> Self {
            Self {
                life: params.life,
                _dummy0: [0; 4],
                size: params.size.into(),
                txt: GpuParticleTexture::from_particle_texture(&params.texture),
            }
        }
    }
}

impl shaders::GpuParticleState {
    pub fn from_particle_state(p: ParticleState) -> Self {
        Self {
            position_spawn_timestamp: p.position.extend(p.spawn_timestamp).into(),
            velocity: p.velocity.extend(0.0).into(),
        }
    }
}

struct NewParticlesBuffers {
    pub new_particles: Arc<CpuAccessibleBuffer<[shaders::GpuParticleState]>>,
}

impl NewParticlesBuffers {
    pub fn new<V: VulkanBasic>(vulkan_common: V) -> Self {
        Self {
            new_particles: unsafe {
                CpuAccessibleBuffer::uninitialized_array(
                    vulkan_common.get_device(),
                    MAX_NEW_PARTICLES as u64,
                    BufferUsage {
                        storage_buffer: true,
                        uniform_buffer: true,
                        ..BufferUsage::none()
                    },
                    false,
                )
                .unwrap()
            },
        }
    }

    pub fn spawn(&mut self, time: f32, delta_time: f32, emitter: &mut dyn Emitter) -> u32 {
        let new_particles: Vec<_> = emitter
            .spawn(delta_time)
            .take(MAX_NEW_PARTICLES as usize)
            .map(|emitted_particle| emitted_particle.to_particle_state(time))
            .map(shaders::GpuParticleState::from_particle_state)
            .collect();

        let mut w = self.new_particles.write().unwrap();

        w[0..new_particles.len()].clone_from_slice(new_particles.as_slice());

        new_particles.len() as u32
    }
}

impl shaders::GpuParticleSystemState {
    fn new() -> Self {
        Self { start_end: [0, 0] }
    }
}

#[derive(Clone)]
pub struct ParticlesBuffers {
    pub particles_state_buffer: Arc<DeviceLocalBuffer<[shaders::GpuParticleState]>>,
    pub particle_system_state_buffer: Arc<DeviceLocalBuffer<shaders::GpuParticleSystemState>>,
    pub indirect_buffer: Arc<DeviceLocalBuffer<[DrawIndexedIndirectCommand]>>,
}

pub struct ParticleState {
    position: Vector3<f32>,
    velocity: Vector3<f32>,
    spawn_timestamp: f32,
}

pub trait ParticlePhysics: Any {
    fn update_particles_cb(
        &mut self,
        cb: &mut UnsafeCommandBufferBuilder,
        time: f32,
        delta_time: f32,
        no_paricles: u32,
        emitter: &mut dyn Emitter,
    );

    fn get_buffers(&self) -> ParticlesBuffers;
    fn realloc_buffers(
        &mut self,
        vulkan_common: crate::game_kernel::MainVulkanCommon,
        max_no_particles: u32,
    );

    fn gk_as_any(&self) -> &dyn Any;
    fn gk_as_any_mut(&mut self) -> &mut dyn Any;
}

pub struct SimpleParticlePhysics {
    pipeline: Arc<ComputePipeline>,
    particles_buffers: ParticlesBuffers,
    simple_physics_properties_buffer: Arc<DeviceLocalBuffer<shaders::GpuSimplePhysicsProperties>>,
    params_update: Option<shaders::GpuSimplePhysicsProperties>,
    physics_descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    new_particles_buffers: NewParticlesBuffers,
    frame_data: Option<shaders::GpuFrameData>,
    max_no_particles: u32,

    accelleration: Vector3<f32>,
}

impl SimpleParticlePhysics {
    pub fn new<V: VulkanBasic>(
        vulkan_common: V,
        max_no_particles: u32,
        accelleration: Vector3<f32>,
    ) -> Self {
        let specialization_costants = shaders::SimplePhysicsSpecs {
            MAX_PARTICLES: max_no_particles,
        };
        let pipeline = ComputePipeline::new(
            vulkan_common.get_device(),
            shaders::get_simple_physics(vulkan_common.get_device())
                .entry_point("main")
                .unwrap(),
            &specialization_costants,
            None,
            |_| {},
        )
        .unwrap();

        let particles_state_buffer = unsafe {
            DeviceLocalBuffer::array(
                vulkan_common.get_device(),
                max_no_particles as u64,
                BufferUsage {
                    storage_buffer: true,
                    uniform_buffer: true,
                    ..BufferUsage::none()
                },
                std::iter::once(vulkan_common.get_graphics_queue_family()),
            )
            .unwrap()
        };

        let particle_system_state_buffer = create_filled_device_buffer(
            vulkan_common.clone(),
            BufferUsage {
                storage_buffer: true,
                uniform_buffer: true,
                ..BufferUsage::none()
            },
            std::iter::once(vulkan_common.get_graphics_queue_family()),
            shaders::GpuParticleSystemState::new(),
        )
        .unwrap();

        let simple_physics_properties_buffer = create_filled_device_buffer(
            vulkan_common.clone(),
            BufferUsage {
                storage_buffer: true,
                uniform_buffer: true,
                ..BufferUsage::none()
            },
            std::iter::once(vulkan_common.get_graphics_queue_family()),
            shaders::GpuSimplePhysicsProperties {
                accelleration: accelleration.extend(0.0).into(),
            },
        )
        .unwrap();

        let dsl = pipeline.layout().set_layouts()[0].clone();

        let new_particles_buffers = NewParticlesBuffers::new(vulkan_common.clone());

        let indirect_buffer = create_filled_device_array(
            vulkan_common.clone(),
            BufferUsage {
                indirect_buffer: true,
                storage_buffer: true,
                ..BufferUsage::none()
            },
            std::iter::once(vulkan_common.get_graphics_queue_family()),
            [DrawIndexedIndirectCommand {
                index_count: 6 * max_no_particles,
                instance_count: 1,
                first_index: 0,
                vertex_offset: 0,
                first_instance: 0,
            }],
        )
        .unwrap();

        let physics_descriptor_set = PersistentDescriptorSet::new(
            dsl,
            [
                WriteDescriptorSet::buffer(0, particles_state_buffer.clone()),
                WriteDescriptorSet::buffer(1, particle_system_state_buffer.clone()),
                WriteDescriptorSet::buffer(2, indirect_buffer.clone()),
                WriteDescriptorSet::buffer(3, new_particles_buffers.new_particles.clone()),
                WriteDescriptorSet::buffer(4, simple_physics_properties_buffer.clone()),
            ],
        )
        .unwrap();

        let particles_buffers = ParticlesBuffers {
            particle_system_state_buffer,
            particles_state_buffer,
            indirect_buffer,
        };

        Self {
            pipeline,
            particles_buffers,
            simple_physics_properties_buffer,
            params_update: None,
            physics_descriptor_set,
            new_particles_buffers,
            frame_data: None,
            max_no_particles,

            accelleration,
        }
    }

    pub fn get_accelleration(&self) -> Vector3<f32> {
        self.accelleration
    }

    pub fn update_accelleration(&mut self, accelleration: Vector3<f32>) {
        self.accelleration = accelleration;
        self.params_update = Some(shaders::GpuSimplePhysicsProperties {
            accelleration: accelleration.extend(0.0).into(),
        });
    }
}

impl ParticlePhysics for SimpleParticlePhysics {
    fn update_particles_cb(
        &mut self,
        cb: &mut UnsafeCommandBufferBuilder,
        time: f32,
        delta_time: f32,
        no_paricles: u32,
        emitter: &mut dyn Emitter,
    ) {
        let n_new_particles = self.new_particles_buffers.spawn(time, delta_time, emitter);
        self.frame_data = Some(shaders::GpuFrameData {
            delta_time,
            time,
            n_new_particles,
        });
        unsafe {
            if let Some(ref update) = self.params_update {
                cb.update_buffer(update, &self.simple_physics_properties_buffer, 0);
            }

            cb.bind_descriptor_sets(
                PipelineBindPoint::Compute,
                self.pipeline.layout(),
                0,
                [self.physics_descriptor_set.inner()].iter().cloned(),
                [].iter().cloned(),
            );

            cb.bind_pipeline_compute(&self.pipeline);
            cb.push_constants(
                &self.pipeline.layout(),
                ShaderStages {
                    compute: true,
                    ..ShaderStages::none()
                },
                0,
                std::mem::size_of::<shaders::GpuFrameData>() as u32,
                &self.frame_data.unwrap(),
            );

            cb.dispatch([no_paricles, 1, 1]);
        }
    }

    fn get_buffers(&self) -> ParticlesBuffers {
        self.particles_buffers.clone()
    }

    fn realloc_buffers(
        &mut self,
        vulkan_common: crate::game_kernel::MainVulkanCommon,
        max_no_particles: u32,
    ) {
        self.particles_buffers.particles_state_buffer = unsafe {
            DeviceLocalBuffer::array(
                vulkan_common.get_device(),
                max_no_particles as u64,
                BufferUsage {
                    storage_buffer: true,
                    uniform_buffer: true,
                    ..BufferUsage::none()
                },
                std::iter::once(vulkan_common.get_graphics_queue_family()),
            )
            .unwrap()
        };

        let dsl = self.pipeline.layout().set_layouts()[0].clone();
        self.physics_descriptor_set = PersistentDescriptorSet::new(
            dsl,
            [
                WriteDescriptorSet::buffer(
                    0,
                    self.particles_buffers.particles_state_buffer.clone(),
                ),
                WriteDescriptorSet::buffer(
                    1,
                    self.particles_buffers.particle_system_state_buffer.clone(),
                ),
                WriteDescriptorSet::buffer(2, self.particles_buffers.indirect_buffer.clone()),
                WriteDescriptorSet::buffer(3, self.new_particles_buffers.new_particles.clone()),
                WriteDescriptorSet::buffer(4, self.simple_physics_properties_buffer.clone()),
            ],
        )
        .unwrap();
    }

    fn gk_as_any(&self) -> &dyn Any {
        self
    }

    fn gk_as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

pub struct ProjectionData {
    pub model: Matrix4<f32>,
    pub view: Matrix4<f32>,
    pub projection: Matrix4<f32>,
}

pub trait ParticlesRendererCreation<P> {
    fn new<V>(
        vulkan_common: V,
        output_image: Arc<AttachmentImage>,
        depth_image: Arc<AttachmentImage>,
        particles_buffers: ParticlesBuffers,
        params: P,
    ) -> Self
    where
        Self: Sized,
        V: VulkanBasic;
}

pub trait ParticlesRenderer {
    unsafe fn render_particles(
        &mut self,
        cbb: &mut UnsafeCommandBufferBuilder,
        projection_data: ProjectionData,
        viewport: Viewport,
        max_no_particles: u32,
        quads_index_buffer: Arc<dyn BufferAccess + Send + Sync + 'static>,
        time: f32,
        delta: f32,
    );

    fn update_buffers(&mut self, buffers: ParticlesBuffers);

    fn gk_as_any(&self) -> &dyn Any;
    fn gk_as_any_mut(&mut self) -> &mut dyn Any;
}

pub trait CreatableParticleRenderer<P>: ParticlesRendererCreation<P> + ParticlesRenderer {}
impl<P, T> CreatableParticleRenderer<P> for T where
    T: ParticlesRendererCreation<P> + ParticlesRenderer
{
}

pub struct PropertyCurveNode<T> {
    time: f32,
    value: T,
}

#[derive(Clone)]
pub struct ParticleTexture {
    pub atlas: super::texture::Texture,
    pub offset: Vector2<f32>,
    pub size: Vector2<f32>,
    pub fps: Vector2<f32>,
    pub frames: Vector2<u32>,
}

#[derive(Clone, Copy)]
pub enum ParticleBlending {
    Alpha,
    Additive,
    AdditiveAlpha,
}

#[derive(Clone)]
pub struct AlphaParticleRendererParams {
    pub life: f32,
    pub size: Vector2<f32>,
    pub texture: ParticleTexture,
    pub blending: ParticleBlending,
}

impl AlphaParticleRendererParams {
    pub fn new(
        life: f32,
        size: Vector2<f32>,
        texture: ParticleTexture,
        blending: ParticleBlending,
    ) -> Self {
        Self {
            life,
            size,
            texture,
            blending,
        }
    }
}

pub struct AlphaParticleRenderer {
    render_pass: Arc<RenderPass>,
    pipeline: Arc<GraphicsPipeline>,
    framebuffer: Arc<Framebuffer>,
    particles_buffers: ParticlesBuffers,
    params_buffer: Arc<DeviceLocalBuffer<shaders::GpuParams>>,
    descriptor_set: Arc<dyn DescriptorSet + Send + Sync>,
    projection_data: Option<shaders::GpuProjectionData>,
    params: AlphaParticleRendererParams,
}

impl AlphaParticleRenderer {
    pub fn new<V: VulkanBasic>(
        vulkan_common: V,
        output_image: Arc<AttachmentImage>,
        depth_image: Arc<AttachmentImage>,
        particles_buffers: ParticlesBuffers,
        params: AlphaParticleRendererParams,
    ) -> Self {
        let render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: Load,
                    store: Store,
                    format: super::HDR_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::ColorAttachmentOptimal,
                    final_layout: ImageLayout::General,
                },
                depth: {
                    load: Load,
                    store: Store,
                    format: super::DEPTH_FORMAT,
                    samples: 1,
                    initial_layout: ImageLayout::DepthStencilAttachmentOptimal,
                    final_layout:   ImageLayout::DepthStencilAttachmentOptimal,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {depth}
            }
        }
        .unwrap();

        let pipeline = GraphicsPipeline::start()
            .vertex_input_state(BuffersDefinition::new())
            .cull_mode_back()
            .triangle_list()
            .viewports_dynamic_scissors_irrelevant(1)
            /*.viewports(iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: {
                    let dimensions = output_image.dimensions();
                    [dimensions.width() as f32, dimensions.height() as f32]
                },
                depth_range: 0.0..1.0,
            }))*/
            .depth_stencil_simple_depth()
            .depth_write(false)
            .vertex_shader(
                shaders::get_vs(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .fragment_shader(
                shaders::get_alpha_renderer(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .blend_collective(match params.blending {
                ParticleBlending::Alpha => AttachmentBlend::alpha(),
                ParticleBlending::Additive => AttachmentBlend {
                    color_op: BlendOp::Add,
                    color_source: BlendFactor::One,
                    color_destination: BlendFactor::One,
                    alpha_op: BlendOp::Add,
                    alpha_source: BlendFactor::Zero,
                    alpha_destination: BlendFactor::One,
                },
                ParticleBlending::AdditiveAlpha => AttachmentBlend {
                    color_op: BlendOp::Add,
                    color_source: BlendFactor::SrcAlpha,
                    color_destination: BlendFactor::One,
                    alpha_op: BlendOp::Add,
                    alpha_source: BlendFactor::Zero,
                    alpha_destination: BlendFactor::One,
                },
            })
            .build(vulkan_common.get_device())
            .unwrap();

        let framebuffer = Framebuffer::new(
            render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![output_image.to_image_view(), depth_image.to_image_view()],
                ..Default::default()
            },
        )
        .unwrap();

        let params_buffer = create_filled_device_buffer(
            vulkan_common.clone(),
            BufferUsage {
                storage_buffer: true,
                uniform_buffer: true,
                ..BufferUsage::none()
            },
            std::iter::once(vulkan_common.get_graphics_queue_family()),
            shaders::GpuParams::from_render_params(&params),
        )
        .unwrap();

        let descriptor_set = PersistentDescriptorSet::new(
            pipeline.layout().set_layouts()[0].clone(),
            [
                WriteDescriptorSet::buffer(0, particles_buffers.particles_state_buffer.clone()),
                WriteDescriptorSet::buffer(
                    1,
                    particles_buffers.particle_system_state_buffer.clone(),
                ),
                WriteDescriptorSet::buffer(2, params_buffer.clone()),
                WriteDescriptorSet::image_view_sampler(
                    3,
                    params.texture.atlas.texture.clone().to_image_view(),
                    params.texture.atlas.sampler.clone(),
                ),
            ],
        )
        .unwrap();

        Self {
            render_pass,
            pipeline,
            framebuffer,
            particles_buffers,
            params_buffer,
            descriptor_set,
            projection_data: None,
            params,
        }
    }

    pub fn mutate_params(
        &mut self,
        f: impl FnOnce(&mut AlphaParticleRendererParams),
        vulkan_common: impl VulkanBasic,
    ) {
        f(&mut self.params);
        self.update_params_buffer(vulkan_common);
    }

    fn update_params_buffer(&mut self, vulkan_common: impl VulkanBasic) {
        self.params_buffer = create_filled_device_buffer(
            vulkan_common.clone(),
            BufferUsage {
                storage_buffer: true,
                uniform_buffer: true,
                ..BufferUsage::none()
            },
            std::iter::once(vulkan_common.get_graphics_queue_family()),
            shaders::GpuParams::from_render_params(&self.params),
        )
        .unwrap();

        self.descriptor_set = {
            PersistentDescriptorSet::new(
                self.pipeline.layout().set_layouts()[0].clone(),
                [
                    WriteDescriptorSet::buffer(
                        0,
                        self.particles_buffers.particles_state_buffer.clone(),
                    ),
                    WriteDescriptorSet::buffer(
                        1,
                        self.particles_buffers.particle_system_state_buffer.clone(),
                    ),
                    WriteDescriptorSet::buffer(2, self.params_buffer.clone()),
                    WriteDescriptorSet::image_view_sampler(
                        3,
                        self.params.texture.atlas.texture.clone().to_image_view(),
                        self.params.texture.atlas.sampler.clone(),
                    ),
                ],
            )
            .unwrap()
        };
    }

    pub fn set_texture(&mut self, texture: ParticleTexture, vulkan_common: impl VulkanBasic) {
        self.params.texture = texture;
        self.update_params_buffer(vulkan_common);
    }
}

impl ParticlesRenderer for AlphaParticleRenderer {
    unsafe fn render_particles(
        &mut self,
        cbb: &mut UnsafeCommandBufferBuilder,
        projection_data: ProjectionData,
        viewport: Viewport,
        max_no_particles: u32,
        quads_index_buffer: Arc<dyn BufferAccess + Send + Sync + 'static>,
        time: f32,
        delta: f32,
    ) {
        cbb.begin_render_pass(
            &RenderPassBeginInfo {
                clear_values: vec![None],
                ..RenderPassBeginInfo::framebuffer(self.framebuffer.clone())
            },
            SubpassContents::Inline,
        );
        cbb.set_viewport(0, [viewport].iter().cloned());
        cbb.bind_pipeline_graphics(&self.pipeline.clone());
        cbb.bind_index_buffer(&quads_index_buffer, IndexType::U32);
        cbb.bind_descriptor_sets(
            PipelineBindPoint::Graphics,
            &self.pipeline.layout(),
            0,
            iter::once(self.descriptor_set.inner()),
            [].iter().cloned(),
        );

        self.projection_data = Some(shaders::GpuProjectionData::from_projection_data(
            projection_data,
            time,
            delta,
        ));

        cbb.push_constants(
            &self.pipeline.layout(),
            ShaderStages {
                vertex: true,
                ..ShaderStages::none()
            },
            0,
            std::mem::size_of::<shaders::GpuProjectionData>() as u32,
            &self.projection_data.unwrap(),
        );

        //cbb.draw_indexed(6 * max_no_particles, 1, 0, 0, 0);
        cbb.draw_indexed_indirect(
            &self.particles_buffers.indirect_buffer,
            1,
            std::mem::size_of::<DrawIndexedIndirectCommand>() as u32,
        );
        cbb.end_render_pass();
    }

    fn update_buffers(&mut self, buffers: ParticlesBuffers) {
        self.particles_buffers = buffers;

        self.descriptor_set = PersistentDescriptorSet::new(
            self.pipeline.layout().set_layouts()[0].clone(),
            [
                WriteDescriptorSet::buffer(
                    0,
                    self.particles_buffers.particles_state_buffer.clone(),
                ),
                WriteDescriptorSet::buffer(
                    1,
                    self.particles_buffers.particle_system_state_buffer.clone(),
                ),
                WriteDescriptorSet::buffer(2, self.params_buffer.clone()),
                WriteDescriptorSet::image_view_sampler(
                    3,
                    self.params.texture.atlas.texture.clone().to_image_view(),
                    self.params.texture.atlas.sampler.clone(),
                ),
            ],
        )
        .unwrap();
    }

    fn gk_as_any(&self) -> &dyn Any {
        self
    }

    fn gk_as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl ParticlesRendererCreation<AlphaParticleRendererParams> for AlphaParticleRenderer {
    fn new<V>(
        vulkan_common: V,
        output_image: Arc<AttachmentImage>,
        depth_image: Arc<AttachmentImage>,
        particles_buffers: ParticlesBuffers,
        params: AlphaParticleRendererParams,
    ) -> Self
    where
        Self: Sized,
        V: VulkanBasic,
    {
        Self::new(
            vulkan_common,
            output_image,
            depth_image,
            particles_buffers,
            params,
        )
    }
}

pub struct EmittedParticleState {
    position: Vector3<f32>,
    velocity: Vector3<f32>,
}

impl EmittedParticleState {
    pub fn to_particle_state(self, spawn_timestamp: f32) -> ParticleState {
        ParticleState {
            position: self.position,
            velocity: self.velocity,
            spawn_timestamp,
        }
    }
}

pub trait Emitter: Any {
    fn spawn(&mut self, delta_time: f32) -> Box<dyn Iterator<Item = EmittedParticleState>>;
    fn maybe_set_position(&mut self, position: Vector3<f32>) {}
    //TODO move to another trait
    fn gk_as_any(&self) -> &dyn Any;
    fn gk_as_any_mut(&mut self) -> &mut dyn Any;
}

pub trait DynCloneEmitter: Emitter {
    fn dyn_clone(&self) -> Box<dyn DynCloneEmitter + Send + Sync>;
    fn as_emitter(&self) -> Box<dyn Emitter + Send + Sync>;
    fn wrapped(&self) -> DynCloneEmitterWrapper;
}

pub struct DynCloneEmitterWrapper {
    pub inner: Box<dyn DynCloneEmitter + Send + Sync>,
}

impl DynCloneEmitterWrapper {
    pub fn new(inner: Box<dyn DynCloneEmitter + Send + Sync>) -> Self {
        Self { inner }
    }
}

impl Clone for DynCloneEmitterWrapper {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.dyn_clone(),
        }
    }
}

impl<E> DynCloneEmitter for E
where
    E: Emitter + Send + Sync + Clone,
{
    fn dyn_clone(&self) -> Box<dyn DynCloneEmitter + Send + Sync> {
        Box::new(self.clone())
    }

    fn as_emitter(&self) -> Box<dyn Emitter + Send + Sync> {
        Box::new(self.clone())
    }

    fn wrapped(&self) -> DynCloneEmitterWrapper {
        DynCloneEmitterWrapper::new(self.dyn_clone())
    }
}

pub struct EmitterLimiter<E: Emitter> {
    frames: Option<u32>,
    particles_per_frame: Option<u32>,

    inner: E,
}

impl<E: Emitter> EmitterLimiter<E> {
    pub fn new(frames: Option<u32>, particles_per_frame: Option<u32>, inner: E) -> Self {
        Self {
            frames: frames.map(|f| f + 1),
            particles_per_frame,

            inner,
        }
    }
}

impl<E: Emitter + Clone> Clone for EmitterLimiter<E> {
    fn clone(&self) -> Self {
        Self {
            frames: self.frames.clone(),
            particles_per_frame: self.particles_per_frame.clone(),
            inner: self.inner.clone(),
        }
    }
}

pub trait IntoLimitedEmitter {
    fn limited(self, frames: Option<u32>, particles_per_frame: Option<u32>) -> EmitterLimiter<Self>
    where
        Self: Sized + Emitter,
    {
        EmitterLimiter::new(frames, particles_per_frame, self)
    }
}

impl<E: Emitter> IntoLimitedEmitter for E {}

impl<E: Emitter> Emitter for EmitterLimiter<E> {
    fn spawn(&mut self, delta_time: f32) -> Box<dyn Iterator<Item = EmittedParticleState>> {
        if self.frames.is_some() && self.frames.unwrap() > 0 {
            *self.frames.as_mut().unwrap() -= 1;
        }
        match (self.frames, self.particles_per_frame) {
            (Some(0), _) => Box::new(vec![].into_iter()),
            (_, Some(particles_per_frame)) => Box::new(
                self.inner
                    .spawn(delta_time)
                    .take(particles_per_frame as usize),
            ),
            (_, None) => self.inner.spawn(delta_time),
        }
    }

    fn gk_as_any(&self) -> &dyn Any {
        self
    }

    fn gk_as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

#[derive(Clone)]
pub struct SphereEmitter {
    pub center: Vector3<f32>,
    pub random_vel: Vector3<f32>,
    pub radius: f32,
    once: bool,
    pub particles_second: f32,
}

impl SphereEmitter {
    pub fn new(
        center: Vector3<f32>,
        random_vel: Vector3<f32>,
        radius: f32,
        particles_second: f32,
    ) -> Self {
        Self {
            center,
            random_vel,
            radius,
            once: false,
            particles_second,
        }
    }
}

impl Emitter for SphereEmitter {
    fn spawn(&mut self, delta_time: f32) -> Box<dyn Iterator<Item = EmittedParticleState>> {
        let mut rng = rand::thread_rng();
        let mut rng1 = rand::thread_rng();
        let radius = self.radius;
        let center = self.center;
        let random_vel = self.random_vel;
        let iter = std::iter::from_fn(move || {
            Some((
                radius * (2.0 * rng.gen::<f32>() - 1.0),
                radius * (2.0 * rng.gen::<f32>() - 1.0),
                radius * (2.0 * rng.gen::<f32>() - 1.0),
                rng.gen::<f32>(),
            ))
        })
        .filter(move |(x, y, z, _)| x * x + y * y + z * z < radius)
        .map(move |(x, y, z, w)| EmittedParticleState {
            position: Vector3::new(x, y, z) + center,
            velocity: random_vel.mul_element_wise(Vector3::new(x, y, z).normalize()) * w,
        })
        .take(if self.once {
            0
        } else {
            (rng1.gen::<f32>() + self.particles_second * delta_time / 1000.0) as _
        });
        self.once = false;
        Box::new(iter)
    }

    fn gk_as_any(&self) -> &dyn Any {
        self
    }

    fn gk_as_any_mut(&mut self) -> &mut dyn Any {
        self
    }

    fn maybe_set_position(&mut self, position: Vector3<f32>) {
        self.center = position;
    }
}

impl Buildable<Box<dyn DynCloneEmitter + Send + Sync>> for SphereEmitter {
    fn get_builder() -> FactoryConstructor<Box<dyn DynCloneEmitter + Send + Sync>> {
        |args| {
            Ok(Box::new(Self {
                center: get_dynamic_arg!(args, "center", Vector3<f32>)
                    .as_ref()
                    .clone(),
                random_vel: get_dynamic_arg!(args, "random_vel", Vector3<f32>)
                    .as_ref()
                    .clone(),
                radius: get_dynamic_arg!(args, "radius", f32).as_ref().clone(),
                once: false,
                particles_second: get_dynamic_arg!(args, "emission_rate", f32)
                    .as_ref()
                    .clone(),
            }))
        }
    }

    fn get_name() -> String {
        "SphereEmitter".to_owned()
    }
}

#[derive(Clone)]
pub struct CylinderEmitter {
    pub center: Vector3<f32>,
    pub radius: f32,
    pub height: f32,
    pub initial_vel: Vector3<f32>,
    pub random_vel: Vector3<f32>,
    pub emission_rate: f32,
    pub rotation: Matrix3<f32>,
}

impl CylinderEmitter {
    pub fn new(
        center: Vector3<f32>,
        radius: f32,
        height: f32,
        initial_vel: Vector3<f32>,
        random_vel: Vector3<f32>,
        emission_rate: f32,
        rotation: Matrix3<f32>,
    ) -> Self {
        Self {
            center,
            radius,
            height,
            initial_vel,
            random_vel,
            emission_rate,
            rotation,
        }
    }
}

impl Emitter for CylinderEmitter {
    fn spawn(&mut self, delta_time: f32) -> Box<dyn Iterator<Item = EmittedParticleState>> {
        let mut rng = rand::thread_rng();
        let mut rng1 = rand::thread_rng();
        let radius = self.radius;
        let height = self.height;
        let center = self.center;
        let random_vel = self.random_vel;
        let initial_vel = self.initial_vel;
        let iter = std::iter::from_fn(move || {
            let rad = radius * rng.gen::<f32>().sqrt();
            let alpha = rng.gen::<f32>() * 2.0 * std::f32::consts::PI;
            Some((
                alpha.cos() * rad,
                height * rng.gen::<f32>(),
                alpha.sin() * rad,
                (2.0 * rng.gen::<f32>() - 1.0),
                (2.0 * rng.gen::<f32>() - 1.0),
                (2.0 * rng.gen::<f32>() - 1.0),
            ))
        })
        .map(move |(x, y, z, vx, vy, vz)| EmittedParticleState {
            position: Vector3::new(x, y, z) + center,
            velocity: random_vel.mul_element_wise(Vector3::new(vx, vy, vz)) + initial_vel,
        })
        .take((rng1.gen::<f32>() + self.emission_rate * delta_time / 1000.0) as _);
        Box::new(iter)
    }

    fn gk_as_any(&self) -> &dyn Any {
        self
    }

    fn gk_as_any_mut(&mut self) -> &mut dyn Any {
        self
    }

    fn maybe_set_position(&mut self, position: Vector3<f32>) {
        self.center = position;
    }
}

impl Buildable<Box<dyn DynCloneEmitter + Send + Sync>> for CylinderEmitter {
    fn get_builder() -> FactoryConstructor<Box<dyn DynCloneEmitter + Send + Sync>> {
        |args| {
            Ok(Box::new(Self {
                center: get_dynamic_arg!(args, "center", Vector3<f32>)
                    .as_ref()
                    .clone(),
                radius: get_dynamic_arg!(args, "radius", f32).as_ref().clone(),
                height: get_dynamic_arg!(args, "height", f32).as_ref().clone(),
                initial_vel: get_dynamic_arg!(args, "initial_vel", Vector3<f32>)
                    .as_ref()
                    .clone(),
                random_vel: get_dynamic_arg!(args, "random_vel", Vector3<f32>)
                    .as_ref()
                    .clone(),
                emission_rate: get_dynamic_arg!(args, "emission_rate", f32)
                    .as_ref()
                    .clone(),
                rotation: get_dynamic_arg!(args, "rotation", Matrix3<f32>)
                    .as_ref()
                    .clone(),
            }))
        }
    }

    fn get_name() -> String {
        "CylinderEmitter".to_owned()
    }
}

pub struct ParticleSystem {
    emitter: Box<dyn Emitter + Send + Sync>,
    particle_physics: Box<dyn ParticlePhysics + Send + Sync>,
    renderer: Box<dyn ParticlesRenderer + Send + Sync>,
    max_no_particles: u32,
    time: f32,
    transform: Matrix4<f32>,
}

impl ParticleSystem {
    pub fn new<V>(
        vulkan_common: V,
        emitter: Box<dyn Emitter + Send + Sync>,
        output_image: Arc<AttachmentImage>,
        depth_image: Arc<AttachmentImage>,
        particle_physics: Box<dyn ParticlePhysics + Send + Sync>,
        renderer: Box<dyn ParticlesRenderer + Send + Sync>,
        max_no_particles: u32,
    ) -> Self
    where
        V: VulkanBasic,
    {
        /*
                 *
        impl SimpleParticlePhysics {
            pub fn new<V: VulkanBasic>(vulkan_common: Arc<V>, max_no_particles: u32) -> Self {

        impl AlphaParticleRenderer {
            pub fn new<V: VulkanBasic, B, C>(
                vulkan_common: Arc<V>,
                output_image: Arc<AttachmentImage>,
                particles_buffer: Arc<B>,
                particle_system_buffer: Arc<C>,
            ) -> Self
                 */
        //TODO: proper particles_physics and renderer, buffers

        Self {
            emitter,
            particle_physics,
            renderer,
            max_no_particles,
            time: 0.0,
            transform: Matrix4::identity(),
        }
    }

    pub fn set_transform(&mut self, transform: Matrix4<f32>) {
        self.transform = transform;
    }

    pub fn get_max_no_particles(&self) -> u32 {
        self.max_no_particles
    }

    pub fn resize_buffers(
        &mut self,
        new_size: u32,
        vulkan_common: crate::game_kernel::MainVulkanCommon,
    ) {
        self.max_no_particles = new_size;
        self.particle_physics
            .realloc_buffers(vulkan_common.clone(), self.max_no_particles);
        self.renderer
            .update_buffers(self.particle_physics.get_buffers());
    }

    //TODO:
    //1 Two update methods:
    //1.1 For particles that do not need the depth buffer
    //1.2 For particles  that do
    //2 Rendering method

    pub unsafe fn update_non_colliding(
        &mut self,
        cb: &mut UnsafeCommandBufferBuilder,
        delta_time: f32,
    ) {
        self.particle_physics.update_particles_cb(
            cb,
            self.time,
            delta_time,
            self.max_no_particles,
            self.emitter.as_mut(),
        );

        self.time += delta_time;
    }

    pub unsafe fn render_particles(
        &mut self,
        cbb: &mut UnsafeCommandBufferBuilder,
        view: Matrix4<f32>,
        projection: Matrix4<f32>,
        viewport: Viewport,
        quads_index_buffer: Arc<dyn BufferAccess + Send + Sync + 'static>,
        delta_time: f32,
    ) {
        let projection_data = ProjectionData {
            model: self.transform,
            view,
            projection,
        };

        self.renderer.render_particles(
            cbb,
            projection_data,
            viewport,
            self.max_no_particles,
            quads_index_buffer,
            self.time,
            delta_time,
        );
    }

    pub fn get_particle_physics(&self) -> &Box<dyn ParticlePhysics + Send + Sync> {
        &self.particle_physics
    }

    pub fn get_particle_physics_mut(&mut self) -> &mut Box<dyn ParticlePhysics + Send + Sync> {
        &mut self.particle_physics
    }

    pub fn get_renderer(&self) -> &Box<dyn ParticlesRenderer + Send + Sync> {
        &self.renderer
    }

    pub fn get_renderer_mut(&mut self) -> &mut Box<dyn ParticlesRenderer + Send + Sync> {
        &mut self.renderer
    }

    pub fn get_emitter(&self) -> &Box<dyn Emitter + Send + Sync> {
        &self.emitter
    }

    pub fn get_emitter_mut(&mut self) -> &mut Box<dyn Emitter + Send + Sync> {
        &mut self.emitter
    }
}

use std::any::TypeId;
use std::collections::HashMap;
use std::sync::Mutex;
lazy_static! {
    pub static ref EMITTER_FACTORY: Mutex<Factory<Box<dyn DynCloneEmitter + Send + Sync>>> = {
        let mut factory = Factory::new();

        //Register basic emitters
        factory.register::<SphereEmitter>();
        factory.register::<CylinderEmitter>();

        Mutex::new(factory)
    };
}

pub struct ParticleSystemData {}

pub trait ParticleSystemBuilder {
    fn build(
        &self,
        vulkan_common: crate::game_kernel::MainVulkanCommon,
        output_image: Arc<AttachmentImage>,
        depth_image: Arc<AttachmentImage>,
    ) -> ParticleSystem;

    fn wrapped(self) -> ParticleSystemBuilderWrapper
    where
        Self: Sized + 'static + Send + Sync,
    {
        ParticleSystemBuilderWrapper::new(Box::new(self))
    }
}

//this is needed beacuse the builder has to be downcasted from Any
pub struct ParticleSystemBuilderWrapper {
    pub builder: Box<dyn ParticleSystemBuilder + Send + Sync>,
}

impl ParticleSystemBuilderWrapper {
    pub fn new(builder: Box<dyn ParticleSystemBuilder + Send + Sync>) -> Self {
        Self { builder }
    }
}

use std::marker::PhantomData;

#[derive(Clone)]
pub struct GenericParticleSystemBuilder<P, F, R>
where
    F: FnOnce(crate::game_kernel::MainVulkanCommon, u32) -> Box<dyn ParticlePhysics + Send + Sync>,
    R: CreatableParticleRenderer<P> + 'static + Send + Sync,
{
    emitter: DynCloneEmitterWrapper,
    renderer_params: P,
    particle_physics_constructor: F,
    max_no_particles: u32,
    _ph: PhantomData<R>,
}

impl<P, F, R> GenericParticleSystemBuilder<P, F, R>
where
    F: FnOnce(crate::game_kernel::MainVulkanCommon, u32) -> Box<dyn ParticlePhysics + Send + Sync>,
    R: CreatableParticleRenderer<P> + 'static + Send + Sync,
{
    pub fn new(
        emitter: Box<dyn DynCloneEmitter + Send + Sync>,
        renderer_params: P,
        particle_physics_constructor: F,
        max_no_particles: u32,
    ) -> Self {
        Self {
            emitter: DynCloneEmitterWrapper::new(emitter),
            renderer_params,
            particle_physics_constructor,
            max_no_particles,
            _ph: PhantomData,
        }
    }
}

impl<P, F, R> ParticleSystemBuilder for GenericParticleSystemBuilder<P, F, R>
where
    F: FnOnce(crate::game_kernel::MainVulkanCommon, u32) -> Box<dyn ParticlePhysics + Send + Sync>
        + 'static
        + Copy
        + Send
        + Sync,
    R: CreatableParticleRenderer<P> + 'static + Send + Sync,
    P: 'static + Clone + Send + Sync,
{
    fn build(
        &self,
        vulkan_common: crate::game_kernel::MainVulkanCommon,
        output_image: Arc<AttachmentImage>,
        depth_image: Arc<AttachmentImage>,
    ) -> ParticleSystem {
        let particle_physics =
            (self.particle_physics_constructor)(vulkan_common.clone(), self.max_no_particles);
        let renderer = Box::new(R::new(
            vulkan_common.clone(),
            output_image.clone(),
            depth_image.clone(),
            particle_physics.get_buffers(),
            self.renderer_params.clone(),
        ));

        ParticleSystem::new(
            vulkan_common.clone(),
            self.emitter.inner.as_emitter(),
            output_image,
            depth_image,
            particle_physics,
            renderer,
            self.max_no_particles,
        )
    }
}

pub type RendererConstructor = dyn Fn(
        crate::game_kernel::MainVulkanCommon,
        Arc<AttachmentImage>,
        Arc<AttachmentImage>,
        ParticlesBuffers,
    ) -> Box<dyn ParticlesRenderer + Send + Sync>
    + 'static
    + Send
    + Sync;

pub type PhysicsConstructor = dyn Fn(crate::game_kernel::MainVulkanCommon, u32) -> Box<dyn ParticlePhysics + Send + Sync>
    + 'static
    + Send
    + Sync;

#[derive(Clone)]
pub struct DynamicParticleSystemBuilder {
    emitter: DynCloneEmitterWrapper,
    renderer_constructor: Arc<Box<RendererConstructor>>,
    particle_physics_constructor: Arc<Box<PhysicsConstructor>>,
    max_no_particles: u32,
}

impl DynamicParticleSystemBuilder {
    pub fn new(
        emitter: DynCloneEmitterWrapper,
        renderer_constructor: Arc<Box<RendererConstructor>>,
        particle_physics_constructor: Arc<Box<PhysicsConstructor>>,
        max_no_particles: u32,
    ) -> Self {
        Self {
            emitter,
            renderer_constructor,
            particle_physics_constructor,
            max_no_particles,
        }
    }
}

impl ParticleSystemBuilder for DynamicParticleSystemBuilder {
    fn build(
        &self,
        vulkan_common: crate::game_kernel::MainVulkanCommon,
        output_image: Arc<AttachmentImage>,
        depth_image: Arc<AttachmentImage>,
    ) -> ParticleSystem {
        let particle_physics =
            (self.particle_physics_constructor)(vulkan_common.clone(), self.max_no_particles);
        let renderer = (self.renderer_constructor)(
            vulkan_common.clone(),
            output_image.clone(),
            depth_image.clone(),
            particle_physics.get_buffers(),
        );

        ParticleSystem::new(
            vulkan_common.clone(),
            self.emitter.inner.as_emitter(),
            output_image,
            depth_image,
            particle_physics,
            renderer,
            self.max_no_particles,
        )
    }
}

use vulkano::command_buffer::pool::StandardCommandPool;
use vulkano::command_buffer::submit::SubmitCommandBufferBuilder;
use vulkano::command_buffer::{sys::UnsafeCommandBufferBuilder, CommandBufferLevel};
use vulkano::command_buffer::{CommandBufferUsage, RenderPassBeginInfo, SubpassContents};
use vulkano::device::Queue;
use vulkano::format::ClearValue;
use vulkano::image::view::ImageViewAbstract;
use vulkano::image::{ImageAccess, ImageLayout, ImageSubresourceRange};
use vulkano::pipeline::graphics::vertex_input::BuffersDefinition;
use vulkano::pipeline::graphics::viewport::Viewport;
use vulkano::render_pass::{Framebuffer, FramebufferCreateInfo, RenderPass, Subpass};
use vulkano::sync::{
    AccessFlags, DependencyInfo, Fence, ImageMemoryBarrier, PipelineStages, Semaphore,
};
use vulkano::{pipeline::*, single_pass_renderpass};

use std::convert::TryFrom;
use std::sync::Arc;

use crate::vulkan::{LifetimedCommandBufferBuilder, ToImageView, VulkanBasic, VulkanCommon};

pub trait UiModule {
    fn render(
        &mut self,
        command_buffer_builder: &mut UnsafeCommandBufferBuilder,
        queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + Send + Sync + 'static>,
    );
}

pub struct DummyUiModule {}

impl UiModule for DummyUiModule {
    fn render(
        &mut self,
        command_buffer_builder: &mut UnsafeCommandBufferBuilder,
        queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + Send + Sync + 'static>,
    ) {
    }
}

pub struct UiRenderer {
    final_render_pass: Arc<RenderPass>,
    final_pipeline: Arc<GraphicsPipeline>,
    final_framebuffers: Vec<Arc<Framebuffer>>,
    output_module: Box<dyn super::output::RenderOutput>,
    command_pool: Arc<StandardCommandPool>,
    vulkan_common: VulkanCommon,
    next_idx: usize,
}

impl UiRenderer {
    pub fn new(
        output_module: Box<dyn super::output::RenderOutput>,
        vulkan_common: VulkanCommon,
    ) -> Self {
        let final_render_pass = single_pass_renderpass! {
            vulkan_common.get_device(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: output_module.format(),
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        }
        .unwrap();

        let final_pipeline = GraphicsPipeline::start()
            .vertex_input_state(BuffersDefinition::new())
            .cull_mode_disabled()
            .viewports_dynamic_scissors_irrelevant(1)
            .viewports(std::iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: [
                    vulkan_common.get_config().resolution[0] as f32,
                    vulkan_common.get_config().resolution[1] as f32,
                ],
                depth_range: 0.0..1.0,
            }))
            .depth_stencil_disabled()
            .depth_write(false)
            .vertex_shader(
                super::final_pass_shaders::get_vertex_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .fragment_shader(
                super::final_pass_shaders::get_fragment_shader(vulkan_common.get_device())
                    .entry_point("main")
                    .unwrap(),
                (),
            )
            .render_pass(Subpass::from(final_render_pass.clone(), 0).unwrap())
            .build(vulkan_common.get_device())
            .unwrap();

        let mut final_framebuffers = vec![];
        for swapchain_image in output_module.images().iter() {
            final_framebuffers.push(
                Framebuffer::new(
                    final_render_pass.clone(),
                    FramebufferCreateInfo {
                        attachments: vec![swapchain_image.clone().to_image_view()],
                        ..Default::default()
                    },
                )
                .unwrap(),
            );
        }

        Self {
            final_render_pass,
            final_pipeline,
            final_framebuffers,
            output_module,
            command_pool: Arc::new(StandardCommandPool::new(
                vulkan_common.get_device(),
                vulkan_common.get_graphics_queue_family(),
            )),
            vulkan_common,
            next_idx: 0,
        }
    }

    fn image(&self) -> Arc<dyn ImageAccess> {
        self.output_module.images()[self.next_idx].clone()
    }

    pub fn render_frame<U>(&mut self, ui: Option<&mut U>)
    where
        U: UiModule,
    {
        use vulkano::swapchain::{acquire_next_image_raw, AcquiredImage};

        let acquire_semaphore = Semaphore::from_pool(self.vulkan_common.get_device()).unwrap();

        self.next_idx = self.output_module.next_idx(Some(&acquire_semaphore));

        let pcb = unsafe {
            let mut pcbb = LifetimedCommandBufferBuilder::new(
                self.command_pool.clone(),
                CommandBufferLevel::Primary,
                CommandBufferUsage::OneTimeSubmit,
            )
            .unwrap();

            let present_layout = self.output_module.present_image_layout();
            if (present_layout != ImageLayout::ColorAttachmentOptimal) {
                let barrier = {
                    let image_barrier = ImageMemoryBarrier {
                        source_stages: PipelineStages {
                            top_of_pipe: true,
                            ..PipelineStages::none()
                        },
                        source_access: AccessFlags {
                            ..AccessFlags::none()
                        },
                        destination_stages: PipelineStages {
                            compute_shader: true,
                            ..PipelineStages::none()
                        },
                        destination_access: AccessFlags {
                            shader_write: true,
                            ..AccessFlags::none()
                        },
                        old_layout: present_layout,
                        new_layout: ImageLayout::ColorAttachmentOptimal,

                        subresource_range: ImageSubresourceRange {
                            aspects: self.image().format().aspects(),
                            mip_levels: 0..1,
                            array_layers: 0..1,
                        },
                        ..ImageMemoryBarrier::image(self.image().inner().image.clone())
                    };

                    DependencyInfo {
                        image_memory_barriers: vec![image_barrier].into(),
                        ..Default::default()
                    }
                };
                pcbb.builder.pipeline_barrier(&barrier);
            }

            pcbb.builder.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![Some([0.0, 0.3, 0.1, 1.0].into())],
                    ..RenderPassBeginInfo::framebuffer(
                        self.final_framebuffers[self.next_idx].clone(),
                    )
                },
                SubpassContents::Inline,
            );

            pcbb.builder.bind_pipeline_graphics(&self.final_pipeline);
            pcbb.builder.end_render_pass();

            let barrier = {
                let image_barrier = ImageMemoryBarrier {
                    source_stages: PipelineStages {
                        color_attachment_output: true,
                        ..PipelineStages::none()
                    },
                    source_access: AccessFlags {
                        color_attachment_write: true,
                        ..AccessFlags::none()
                    },
                    destination_stages: PipelineStages {
                        color_attachment_output: true,
                        ..PipelineStages::none()
                    },
                    destination_access: AccessFlags {
                        color_attachment_read: true,
                        ..AccessFlags::none()
                    },
                    old_layout: ImageLayout::ColorAttachmentOptimal,
                    new_layout: ImageLayout::ColorAttachmentOptimal,

                    subresource_range: ImageSubresourceRange {
                        aspects: self.image().format().aspects(),
                        mip_levels: 0..1,
                        array_layers: 0..1,
                    },
                    ..ImageMemoryBarrier::image(self.image().inner().image.clone())
                };

                DependencyInfo {
                    image_memory_barriers: vec![image_barrier].into(),
                    ..Default::default()
                }
            };
            pcbb.builder.pipeline_barrier(&barrier);

            if let Some(ui) = ui {
                unsafe {
                    ui.render(
                        &mut pcbb.builder,
                        self.vulkan_common.get_graphics_queue(),
                        self.image().to_image_view(),
                    );
                }
            }

            if (present_layout != ImageLayout::ColorAttachmentOptimal) {
                let barrier = {
                    let image_barrier = ImageMemoryBarrier {
                        source_stages: PipelineStages {
                            fragment_shader: true,
                            color_attachment_output: true,
                            ..PipelineStages::none()
                        },
                        source_access: AccessFlags {
                            color_attachment_write: true,
                            ..AccessFlags::none()
                        },
                        destination_stages: PipelineStages {
                            bottom_of_pipe: true,
                            color_attachment_output: true,
                            ..PipelineStages::none()
                        },
                        destination_access: AccessFlags {
                            color_attachment_read: true,
                            color_attachment_write: true,
                            ..AccessFlags::none()
                        },
                        old_layout: ImageLayout::ColorAttachmentOptimal,
                        new_layout: present_layout,

                        subresource_range: ImageSubresourceRange {
                            aspects: self.image().format().aspects(),
                            mip_levels: 0..1,
                            array_layers: 0..1,
                        },
                        ..ImageMemoryBarrier::image(self.image().inner().image.clone())
                    };

                    DependencyInfo {
                        image_memory_barriers: vec![image_barrier].into(),
                        ..Default::default()
                    }
                };
                pcbb.builder.pipeline_barrier(&barrier);
            }

            pcbb.build().unwrap()
        };

        let fence = Fence::from_pool(self.vulkan_common.get_device()).unwrap();
        let mut submission_builder = SubmitCommandBufferBuilder::new();
        let present_semaphore = Semaphore::from_pool(self.vulkan_common.get_device()).unwrap();
        unsafe {
            submission_builder.add_wait_semaphore(
                &acquire_semaphore,
                PipelineStages {
                    all_commands: true,
                    ..PipelineStages::none()
                },
            );
            submission_builder.add_command_buffer(&pcb.cb);
            submission_builder.set_fence_signal(&fence);
            submission_builder.add_signal_semaphore(&present_semaphore);
        }

        submission_builder
            .submit(&self.vulkan_common.get_graphics_queue())
            .unwrap();
        fence.wait(None).unwrap();

        self.output_module.present(
            Some(&present_semaphore),
            &self.vulkan_common.get_graphics_queue(),
            self.next_idx,
        );
    }
}

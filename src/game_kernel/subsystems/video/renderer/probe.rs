use cgmath::Vector3;

struct DeferredProbe {
    position: Vector3<f32>,
    influence_radius: f32,
}

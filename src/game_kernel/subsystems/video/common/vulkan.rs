use gltf::buffer::Target;
use vulkano;
use vulkano::command_buffer::sys::CommandBufferBeginInfo;
use vulkano::command_buffer::CopyBufferInfo;
use vulkano::device::DeviceCreateInfo;
use vulkano::device::QueueCreateInfo;
use vulkano::instance::InstanceCreateInfo;
use vulkano::swapchain::SwapchainCreateInfo;
use winit;

use crate::log_msg;
use crate::log_warn;

use self::winit::window::Window;

use self::vulkano::device::{
    physical::{PhysicalDevice, QueueFamily},
    Device, DeviceExtensions, Queue,
};
use self::vulkano::image::{ImageUsage, SwapchainImage};
use self::vulkano::instance::{Instance, InstanceExtensions, Version};
use self::vulkano::swapchain;
use self::vulkano::swapchain::{
    AcquireError, PresentMode, Surface, Swapchain, SwapchainAcquireFuture,
};
use vulkano::command_buffer::pool::{
    standard::{StandardCommandPool, StandardCommandPoolAlloc, StandardCommandPoolBuilder},
    UnsafeCommandPool,
};
use vulkano::format::Format;
use vulkano::impl_vertex;

use bytemuck::{Pod, Zeroable};

use serde::Deserialize;

use std::collections::HashMap;
use std::sync::Arc;

const DESIDERED_FORMAT: Format = Format::B8G8R8A8_SRGB;

//hack, UnsafeCommandPoolCreateInfo is erroneusly not exposed
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, Hash)]
pub struct NonExhaustive(());
#[derive(Clone, Debug)]
pub struct UnsafeCommandPoolCreateInfo {
    pub queue_family_index: u32,
    pub transient: bool,
    pub reset_command_buffer: bool,
    pub _ne: NonExhaustive,
}

impl Default for UnsafeCommandPoolCreateInfo {
    #[inline]
    fn default() -> Self {
        Self {
            queue_family_index: u32::MAX,
            transient: false,
            reset_command_buffer: false,
            _ne: NonExhaustive(()),
        }
    }
}

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Vertex2 {
    position: [f32; 2],
}

impl_vertex!(Vertex2, position);

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Vertex3 {
    position: [f32; 3],
}

impl_vertex!(Vertex3, position);

#[repr(C)]
#[derive(Default, Clone, Copy, Pod, Zeroable)]
pub struct Vertex4 {
    position: [f32; 4],
}

impl_vertex!(Vertex4, position);

#[derive(Clone, Deserialize)]
pub struct VulkanConfig {
    device_index: usize,
    device_name: String,

    pub resolution: [u32; 2],
}

pub trait VulkanBasic: Clone {
    fn get_config(&self) -> &VulkanConfig;
    fn get_instance(&self) -> Arc<Instance>;
    fn get_device(&self) -> Arc<Device>;
    fn get_graphics_queue(&self) -> Arc<Queue>;
    fn get_transfers_queue(&self) -> Arc<Queue>;
    fn get_compute_queue(&self) -> Arc<Queue>;
    fn get_graphics_queue_family(&self) -> QueueFamily;
    fn get_compute_queue_family(&self) -> QueueFamily;
    fn get_transfer_queue_family(&self) -> QueueFamily;
    fn get_graphics_command_pool(&self) -> Arc<StandardCommandPool>;
    fn get_graphics_unsafe_command_pool(&self) -> Arc<UnsafeCommandPool>;
    fn get_transfer_command_pool(&self) -> Arc<StandardCommandPool>;
    fn get_transfer_unsafe_command_pool(&self) -> Arc<UnsafeCommandPool>;
    fn get_compute_command_pool(&self) -> Arc<StandardCommandPool>;
}

/*pub trait AbstractVulkanCommon: VulkanBasic {
    fn get_swapchain(&self) -> Arc<Swapchain<W>>;
    fn get_swapchain_images(&self) -> Vec<Arc<SwapchainImage<W>>>;
    fn update_resolution_from_window(&mut self, window: &Window) -> Option<&VulkanConfig>;
    fn update_resolution(&mut self, res: (u32, u32)) -> Option<&VulkanConfig>;
    fn get_next_swapchain_image_index(&self) -> Option<(SwapchainAcquireFuture<W>, usize)>;
    fn get_next_swapchain_image(
        &self,
    ) -> Option<(SwapchainAcquireFuture<W>, Arc<SwapchainImage<W>>)>;
}*/

pub struct SwapChain<W> {
    pub swapchain: Arc<Swapchain<W>>,
    pub swapchain_images: Vec<Arc<SwapchainImage<W>>>,
    pub surface: Arc<Surface<W>>,
    pub swapchain_format: Format,
}

impl<W> Clone for SwapChain<W> {
    fn clone(&self) -> Self {
        Self {
            swapchain: self.swapchain.clone(),
            swapchain_images: self.swapchain_images.clone(),
            surface: self.surface.clone(),
            swapchain_format: self.swapchain_format,
        }
    }
}

impl<W> SwapChain<W> {
    pub fn new(vk: VulkanCommon, surface: Arc<Surface<W>>) -> Self {
        let format;
        let (swapchain, images) = {
            let caps = vk
                .device
                .physical_device()
                .surface_capabilities(&surface, Default::default())
                .unwrap();

            let usage = ImageUsage {
                color_attachment: true,
                ..ImageUsage::none()
            };

            let alpha = caps.supported_composite_alpha.iter().next().unwrap();

            let supported_formats = vk
                .device
                .physical_device()
                .surface_formats(&surface, Default::default())
                .unwrap();

            format = supported_formats
                .iter()
                .find(|f| f.0 == DESIDERED_FORMAT)
                .unwrap_or_else(|| {
                    log_warn!(
                        "format {:?} not available, output may look wrong",
                        DESIDERED_FORMAT
                    );
                    &supported_formats[0]
                })
                .0;
            log_msg!("choosing format {:?}", format);

            Swapchain::new(
                vk.device.clone(),
                surface.clone(),
                SwapchainCreateInfo {
                    min_image_count: caps.min_image_count,
                    image_format: Some(format),
                    image_extent: vk.config.resolution,
                    image_usage: usage,
                    composite_alpha: alpha,
                    present_mode: PresentMode::Immediate,
                    ..Default::default()
                },
            )
            /*.num_images(caps.min_image_count)
            .format(format)
            .dimensions(vk.config.resolution)
            .usage(usage)
            .sharing_mode(&vk.graphics_queue)
            .present_mode(PresentMode::Immediate)
            .build()*/
            .unwrap()
        };

        Self {
            swapchain,
            swapchain_images: images,
            surface,
            swapchain_format: format,
        }
    }

    pub fn get_swapchain(&self) -> Arc<Swapchain<W>> {
        self.swapchain.clone()
    }

    pub fn get_swapchain_images(&self) -> Vec<Arc<SwapchainImage<W>>> {
        self.swapchain_images.clone()
    }
}

//#[derive(Clone)]
pub struct VulkanCommon {
    pub instance: Arc<Instance>,
    pub device: Arc<Device>,
    pub physical_device_index: usize,
    pub graphics_queue: Arc<Queue>,
    pub transfer_queue: Arc<Queue>,
    pub compute_queue: Arc<Queue>,
    pub graphics_queue_family_id: u32,
    pub compute_queue_family_id: u32,
    pub transfer_queue_family_id: u32,
    pub graphics_command_pool: Arc<StandardCommandPool>,
    pub graphics_unsafe_command_pool: Arc<UnsafeCommandPool>,
    pub transfer_command_pool: Arc<StandardCommandPool>,
    pub transfer_unsafe_command_pool: Arc<UnsafeCommandPool>,
    pub compute_command_pool: Arc<StandardCommandPool>,

    pub config: VulkanConfig,
}

impl Clone for VulkanCommon {
    fn clone(&self) -> Self {
        Self {
            instance: self.instance.clone(),
            device: self.device.clone(),
            physical_device_index: self.physical_device_index,
            graphics_queue: self.graphics_queue.clone(),
            transfer_queue: self.transfer_queue.clone(),
            compute_queue: self.compute_queue.clone(),
            graphics_queue_family_id: self.graphics_queue_family_id,
            compute_queue_family_id: self.compute_queue_family_id,
            transfer_queue_family_id: self.transfer_queue_family_id,
            graphics_command_pool: self.graphics_command_pool.clone(),
            graphics_unsafe_command_pool: self.graphics_unsafe_command_pool.clone(),
            transfer_command_pool: self.transfer_command_pool.clone(),
            transfer_unsafe_command_pool: self.transfer_unsafe_command_pool.clone(),
            compute_command_pool: self.compute_command_pool.clone(),
            config: self.config.clone(),
        }
    }
}

impl VulkanBasic for VulkanCommon {
    fn get_config(&self) -> &VulkanConfig {
        &self.config
    }

    fn get_instance(&self) -> Arc<Instance> {
        self.instance.clone()
    }

    fn get_device(&self) -> Arc<Device> {
        self.device.clone()
    }

    fn get_graphics_queue(&self) -> Arc<Queue> {
        self.graphics_queue.clone()
    }

    fn get_transfers_queue(&self) -> Arc<Queue> {
        self.transfer_queue.clone()
    }

    fn get_compute_queue(&self) -> Arc<Queue> {
        self.compute_queue.clone()
    }

    fn get_graphics_queue_family(&self) -> QueueFamily {
        let physical_device =
            PhysicalDevice::from_index(&self.instance, self.physical_device_index).unwrap();
        physical_device
            .queue_family_by_id(self.get_graphics_queue().family().id())
            .unwrap()
    }

    fn get_transfer_queue_family(&self) -> QueueFamily {
        let physical_device =
            PhysicalDevice::from_index(&self.instance, self.physical_device_index).unwrap();
        physical_device
            .queue_family_by_id(self.get_transfers_queue().family().id())
            .unwrap()
    }

    fn get_compute_queue_family(&self) -> QueueFamily {
        let physical_device =
            PhysicalDevice::from_index(&self.instance, self.physical_device_index).unwrap();
        physical_device
            .queue_family_by_id(self.get_compute_queue().family().id())
            .unwrap()
    }

    fn get_graphics_command_pool(&self) -> Arc<StandardCommandPool> {
        self.graphics_command_pool.clone()
    }

    fn get_graphics_unsafe_command_pool(&self) -> Arc<UnsafeCommandPool> {
        self.graphics_unsafe_command_pool.clone()
    }

    fn get_transfer_command_pool(&self) -> Arc<StandardCommandPool> {
        self.transfer_command_pool.clone()
    }

    fn get_transfer_unsafe_command_pool(&self) -> Arc<UnsafeCommandPool> {
        self.transfer_unsafe_command_pool.clone()
    }

    fn get_compute_command_pool(&self) -> Arc<StandardCommandPool> {
        self.compute_command_pool.clone()
    }
}

/*impl AbstractVulkanCommon for VulkanCommon {
    fn get_swapchain(&self) -> Arc<Swapchain<W>> {
        self.swapchain.clone()
    }

    fn get_swapchain_images(&self) -> Vec<Arc<SwapchainImage<W>>> {
        self.swapchain_images.clone()
    }

    fn update_resolution_from_window(&mut self, window: &Window) -> Option<&VulkanConfig> {
        self.config.resolution = {
            let resolution: (u32, u32) = window.inner_size().into();
            [resolution.0, resolution.1]
        };
        Some(self.get_config())
    }

    fn update_resolution(&mut self, res: (u32, u32)) -> Option<&VulkanConfig> {
        self.config.resolution = {
            let resolution: (u32, u32) = res;
            [resolution.0, resolution.1]
        };
        Some(self.get_config())
    }

    fn get_next_swapchain_image_index(&self) -> Option<(SwapchainAcquireFuture<W>, usize)> {
        match swapchain::acquire_next_image(self.get_swapchain(), None) {
            Ok((image_index, _, acquire_future)) => Some((acquire_future, image_index)),
            Err(AcquireError::OutOfDate) => None,
            Err(err) =>
            /*TODO proper handling*/
            {
                panic!("Error while acquiring swapchain image: {:?}", err)
            }
        }
    }

    fn get_next_swapchain_image(
        &self,
    ) -> Option<(SwapchainAcquireFuture<W>, Arc<SwapchainImage<W>>)> {
        match swapchain::acquire_next_image(self.get_swapchain(), None) {
            Ok((image_index, _, acquire_future)) => {
                Some((acquire_future, self.swapchain_images[image_index].clone()))
            }
            Err(AcquireError::OutOfDate) => None,
            Err(err) =>
            /*TODO proper handling*/
            {
                panic!("Error while acquiring swapchain image: {:?}", err)
            }
        }
    }
}*/

trait DeviceName {
    fn name(&self) -> String;
}

impl<'a> DeviceName for PhysicalDevice<'a> {
    fn name(&self) -> String {
        self.properties().device_name.clone()
    }
}

impl VulkanCommon {
    pub fn new<Ext, W>(
        instance: Arc<Instance>,
        config: &mut VulkanConfig,
        extensions: Ext,
        surface: Arc<Surface<W>>,
    ) -> Self
//where Ext: Into<RawInstanceExtensions>,
    {
        let mut physical_device =
            PhysicalDevice::from_index(&instance, config.device_index).unwrap();
        if physical_device.name() != config.device_name {
            //if an hw change is
            physical_device = PhysicalDevice::enumerate(&instance).next().unwrap();
            config.device_index = physical_device.index();
            config.device_name = physical_device.name();
        }
        log_msg!("using physical device {}", physical_device.name());

        for family in physical_device.queue_families() {
            log_msg!(
                "family {} supports graphics:{} transfers:{} compute:{} and max {}",
                family.id(),
                family.supports_graphics(),
                family.explicitly_supports_transfers(),
                family.supports_compute(),
                family.queues_count()
            );
        }

        //contains remaining queues for used queue family
        let mut remaining_queues = HashMap::new();

        //finds queue families
        let graphics_queue_family = physical_device
            .queue_families()
            .find(|&queue| {
                queue.supports_graphics() && queue.supports_surface(&surface).unwrap_or(false)
            })
            .unwrap();

        remaining_queues.insert(
            graphics_queue_family.id(),
            graphics_queue_family.queues_count() - 1,
        );

        let transfer_queue_family = physical_device
            .queue_families()
            .find(|&queue| {
                queue.explicitly_supports_transfers()
                    && !queue.supports_graphics()
                    && remaining_queues.get(&queue.id()).cloned().unwrap_or(1) > 0
            })
            .map(|x| Some(x))
            .unwrap_or(physical_device.queue_families().find(|&queue| {
                queue.explicitly_supports_transfers()
                    && remaining_queues.get(&queue.id()).cloned().unwrap_or(1) > 0
            }));

        if let Some(transfer_queue_family) = transfer_queue_family {
            remaining_queues.insert(
                transfer_queue_family.id(),
                transfer_queue_family.queues_count() - 1,
            );
        }

        let compute_queue_family = physical_device
            .queue_families()
            .find(|&queue| {
                queue.supports_compute()
                    && !queue.supports_graphics()
                    && remaining_queues.get(&queue.id()).cloned().unwrap_or(1) > 0
            })
            .map(|x| Some(x))
            .unwrap_or(physical_device.queue_families().find(|&queue| {
                queue.supports_compute()
                    && remaining_queues.get(&queue.id()).cloned().unwrap_or(1) > 0
            }));

        if let Some(compute_queue_family) = compute_queue_family {
            remaining_queues.insert(
                compute_queue_family.id(),
                compute_queue_family.queues_count() - 1,
            );
        }

        let device_ext = DeviceExtensions {
            khr_swapchain: true,
            khr_storage_buffer_storage_class: true,
            ..DeviceExtensions::none()
        };
        let (device, mut queues) =
            if let (Some(transfer_queue_family), Some(compute_queue_family)) =
                (transfer_queue_family, compute_queue_family)
            {
                log_msg!(
                "graphics_queue_family: {}, compute_queue_family: {}, transfer_queue_family: {}",
                graphics_queue_family.id(),
                compute_queue_family.id(),
                transfer_queue_family.id()
            );

                let mut queues = vec![
                    graphics_queue_family,
                    transfer_queue_family,
                    compute_queue_family,
                ];
                queues.sort_by_key(QueueFamily::id);
                queues.dedup_by_key(|qf| qf.id());

                Device::new(
                    physical_device,
                    DeviceCreateInfo {
                        enabled_extensions: device_ext,
                        enabled_features: physical_device.supported_features().clone(),
                        queue_create_infos: queues
                            .iter()
                            .map(|qf| QueueCreateInfo::family(*qf))
                            .collect(),

                        ..Default::default()
                    },
                )
                .unwrap()
            } else {
                log_msg!("using single family: {}", graphics_queue_family.id(),);

                Device::new(
                    physical_device,
                    DeviceCreateInfo {
                        enabled_extensions: device_ext,
                        enabled_features: physical_device.supported_features().clone(),
                        queue_create_infos: vec![QueueCreateInfo::family(graphics_queue_family)],

                        ..Default::default()
                    },
                )
                .unwrap()
            };

        //TODO: proper handling
        let graphics_queue = queues
            .find(|q| q.family() == graphics_queue_family)
            .unwrap();
        let transfer_queue = if let Some(transfer_queue_family) = transfer_queue_family {
            queues
                .find(|q| q.family() == transfer_queue_family)
                .unwrap_or(graphics_queue.clone())
        } else {
            graphics_queue.clone()
        };
        let transfer_queue_family = transfer_queue_family.unwrap_or(graphics_queue_family.clone());
        let compute_queue = if let Some(compute_queue_family) = compute_queue_family {
            queues
                .find(|q| q.family() == compute_queue_family)
                .unwrap_or(graphics_queue.clone())
        } else {
            graphics_queue.clone()
        };
        let compute_queue_family = compute_queue_family.unwrap_or(graphics_queue_family.clone());

        let graphics_command_pool = Arc::new(StandardCommandPool::new(
            device.clone(),
            graphics_queue.family(),
        ));

        let graphics_unsafe_command_pool = Arc::new(
            UnsafeCommandPool::new(device.clone(), unsafe {
                std::mem::transmute(UnsafeCommandPoolCreateInfo {
                    queue_family_index: graphics_queue_family.id(),
                    transient: false,
                    reset_command_buffer: true, //TODO verify that this is correct
                    ..Default::default()
                })
            })
            .unwrap(),
        );

        let transfer_command_pool = Arc::new(StandardCommandPool::new(
            device.clone(),
            transfer_queue.family(),
        ));

        let transfer_unsafe_command_pool = Arc::new(
            UnsafeCommandPool::new(device.clone(), unsafe {
                std::mem::transmute(UnsafeCommandPoolCreateInfo {
                    queue_family_index: transfer_queue_family.id(),
                    transient: false,
                    reset_command_buffer: true, //TODO verify that this is correct
                    ..Default::default()
                })
            })
            .unwrap(),
        );

        let compute_command_pool = Arc::new(StandardCommandPool::new(
            device.clone(),
            compute_queue.family(),
        ));

        VulkanCommon {
            instance: instance.clone(),
            device,
            physical_device_index: physical_device.index(),
            graphics_queue,
            transfer_queue,
            compute_queue,
            graphics_queue_family_id: graphics_queue_family.id(),
            compute_queue_family_id: graphics_queue_family.id(), //compute_queue_family.id(),
            transfer_queue_family_id: graphics_queue_family.id(), //compute_queue_family.id(),
            graphics_command_pool,
            graphics_unsafe_command_pool,
            transfer_command_pool,
            transfer_unsafe_command_pool,
            compute_command_pool,
            config: config.clone(),
        }
    }

    pub fn get_instance<'b, L>(extensions: &InstanceExtensions, layers: L) -> Arc<Instance>
    where
        L: IntoIterator<Item = &'b str>,
    {
        Instance::new(InstanceCreateInfo {
            max_api_version: Some(Version::V1_3),
            enabled_extensions: *extensions,
            enabled_layers: layers.into_iter().map(|s| s.to_owned()).collect(),
            ..Default::default()
        })
        .unwrap()
    }
}

use vulkano::image::view::ImageView;
use vulkano::image::ImageAccess;

pub trait ToImageView: ImageAccess + Sized {
    fn to_image_view(self) -> Arc<ImageView<Self>> {
        ImageView::new_default(Arc::new(self)).unwrap()
    }
}

impl<T> ToImageView for T where T: ImageAccess + Sized {}

use vulkano::command_buffer::pool::CommandPool;
use vulkano::command_buffer::pool::CommandPoolBuilderAlloc;
use vulkano::command_buffer::sys::{UnsafeCommandBuffer, UnsafeCommandBufferBuilder};
use vulkano::command_buffer::{CommandBufferLevel, CommandBufferUsage};
use vulkano::render_pass::Framebuffer;
use vulkano::OomError;

pub struct LifetimedCommandBufferBuilder {
    pub builder: UnsafeCommandBufferBuilder,
    alloc: StandardCommandPoolBuilder,
    pool: Arc<StandardCommandPool>,
}

impl LifetimedCommandBufferBuilder {
    pub unsafe fn new(
        pool: Arc<StandardCommandPool>,
        level: CommandBufferLevel,
        usage: CommandBufferUsage,
    ) -> Result<Self, OomError> {
        let alloc = pool.allocate(level, 1).unwrap().next().unwrap();

        let builder = UnsafeCommandBufferBuilder::new(
            alloc.inner(),
            CommandBufferBeginInfo {
                usage,
                ..Default::default()
            },
        )?;

        Ok(Self {
            builder,
            alloc,
            pool,
        })
    }

    pub fn build(self) -> Result<LifetimedCommandBuffer, OomError> {
        Ok(LifetimedCommandBuffer {
            cb: self.builder.build()?,
            alloc: Some(self.alloc.into_alloc()),
            pool: self.pool,
        })
    }
}

pub struct LifetimedCommandBuffer {
    pub cb: UnsafeCommandBuffer,
    alloc: Option<StandardCommandPoolAlloc>,
    pool: Arc<StandardCommandPool>,
}

use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, DeviceLocalBuffer};
use vulkano::command_buffer::{AutoCommandBufferBuilder, PrimaryCommandBuffer};
use vulkano::memory::DeviceMemoryAllocationError;
use vulkano::sync::GpuFuture;

pub fn write_device_buffer<T, V: VulkanBasic>(
    vk: V,
    data: T,
    device_buffer: Arc<DeviceLocalBuffer<T>>,
) -> Result<(), DeviceMemoryAllocationError>
where
    T: Send + Sync + Copy + Pod + 'static,
{
    let staging = CpuAccessibleBuffer::from_data(
        vk.get_device(),
        BufferUsage {
            transfer_src: true,
            ..BufferUsage::none()
        },
        false,
        data,
    )?;

    let mut copy_cbb = AutoCommandBufferBuilder::primary(
        vk.get_device(),
        vk.get_transfer_queue_family(),
        CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap();

    copy_cbb
        .copy_buffer(CopyBufferInfo::buffers(staging, device_buffer.clone()))
        .unwrap();

    let copy_cb = copy_cbb.build().unwrap();

    copy_cb
        .execute(vk.get_transfers_queue())
        .unwrap()
        .flush()
        .unwrap();

    Ok(())
}

pub fn create_filled_device_buffer<'a, T, V: VulkanBasic, I>(
    vk: V,
    usage: BufferUsage,
    families: I,
    data: T,
) -> Result<Arc<DeviceLocalBuffer<T>>, DeviceMemoryAllocationError>
where
    I: IntoIterator<Item = QueueFamily<'a>>,
    T: Send + Sync + Copy + Pod + 'static,
{
    //make sure transfers queue is always present
    let families = {
        let mut families: Vec<_> = families.into_iter().collect();
        let already_has_transfer = families
            .iter()
            .any(|q| *q == vk.get_transfer_queue_family());

        if !already_has_transfer {
            families.push(vk.get_transfer_queue_family());
        }

        families.into_iter()
    };

    let device_buffer = DeviceLocalBuffer::new(
        vk.get_device(),
        BufferUsage {
            transfer_dst: true,
            ..usage
        },
        families,
    )?;

    write_device_buffer(vk, data, device_buffer.clone());

    Ok(device_buffer)
}

pub fn write_device_array<T, I, V: VulkanBasic>(
    vk: V,
    data: I,
    device_buffer: Arc<DeviceLocalBuffer<[T]>>,
) -> Result<(), DeviceMemoryAllocationError>
where
    T: Send + Sync + Copy + Pod + 'static,
    I: IntoIterator<Item = T>,
    I::IntoIter: ExactSizeIterator,
{
    let staging = CpuAccessibleBuffer::from_iter(
        vk.get_device(),
        BufferUsage {
            transfer_src: true,
            ..BufferUsage::none()
        },
        false,
        data,
    )?;

    let mut copy_cbb = AutoCommandBufferBuilder::primary(
        vk.get_device(),
        vk.get_transfer_queue_family(),
        CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap();

    copy_cbb
        .copy_buffer(CopyBufferInfo::buffers(staging, device_buffer.clone()))
        .unwrap();

    let copy_cb = copy_cbb.build().unwrap();

    copy_cb
        .execute(vk.get_transfers_queue())
        .unwrap()
        .flush()
        .unwrap();

    Ok(())
}

pub fn create_filled_device_array<'a, T, I, V: VulkanBasic, F>(
    vk: V,
    usage: BufferUsage,
    families: F,
    data: I,
) -> Result<Arc<DeviceLocalBuffer<[T]>>, DeviceMemoryAllocationError>
where
    F: IntoIterator<Item = QueueFamily<'a>>,
    T: Send + Sync + Copy + Pod + 'static,
    I: IntoIterator<Item = T>,
    I::IntoIter: ExactSizeIterator,
{
    //make sure transfers queue is always present
    let families = {
        let mut families: Vec<_> = families.into_iter().collect();
        let already_has_transfer = families
            .iter()
            .any(|q| *q == vk.get_transfer_queue_family());

        if !already_has_transfer {
            families.push(vk.get_transfer_queue_family());
        }

        families.into_iter()
    };

    //TODO: avoid copying
    let data: Vec<_> = data.into_iter().collect();
    let device_buffer = DeviceLocalBuffer::array(
        vk.get_device(),
        data.len() as u64,
        BufferUsage {
            transfer_dst: true,
            ..usage
        },
        families,
    )?;

    write_device_array(vk, data, device_buffer.clone());

    Ok(device_buffer)
}

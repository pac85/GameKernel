use std::sync::Arc;

use imgui::{FontConfig, FontGlyphRanges, FontSource, Ui};
use imgui_winit_support::{HiDpiMode, WinitPlatform};

use crate::vulkan::VulkanBasic;
use crate::vulkan::{self, SwapChain};
use crate::winit::window::Window;

pub struct GkImgui {
    pub context: imgui::Context,
    pub platform: WinitPlatform,
    pub renderer: imgui_vulkano_renderer_unsafe::Renderer,
}

impl GkImgui {
    pub fn init(
        window: &Window,
        vulkan_common: vulkan::VulkanCommon,
        swapchain: SwapChain<Window>,
    ) -> Result<Self, ()> {
        let mut context = imgui::Context::create();
        //no docking for now :(
        //context.io_mut().config_flags |= imgui::ConfigFlags::DOCKING_ENABLE;
        let mut platform = WinitPlatform::init(&mut context);
        platform.attach_window(context.io_mut(), window, HiDpiMode::Rounded);
        context.fonts().add_font(&[
            FontSource::DefaultFontData {
                config: Some(FontConfig::default()),
            },
            FontSource::TtfData {
                data: include_bytes!("../../../../../data/fonts/NotoSans-Regular.ttf"),
                size_pixels: 10.0,
                config: Some(FontConfig {
                    rasterizer_multiply: 1.75,
                    glyph_ranges: FontGlyphRanges::japanese(),
                    ..FontConfig::default()
                }),
            },
        ]);
        let renderer = imgui_vulkano_renderer_unsafe::Renderer::init(
            &mut context,
            vulkan_common.get_device(),
            vulkan_common.get_graphics_queue(),
            swapchain.swapchain_format,
        )
        .unwrap();

        Ok(Self {
            context,
            platform,
            renderer,
        })
    }

    pub fn prepare_frame(&mut self, window: &Window) {
        self.platform.prepare_frame(self.context.io_mut(), window);
    }

    pub fn get_mod<'a>(
        renderer: &'a mut imgui_vulkano_renderer_unsafe::Renderer,
        ui: &'a imgui::DrawData,
    ) -> ImguiModule<'a> {
        ImguiModule { renderer, ui }
    }
}

use vulkano::command_buffer::sys::UnsafeCommandBufferBuilder;
use vulkano::device::Queue;
use vulkano::image::ImageViewAbstract;

pub struct ImguiModule<'a> {
    renderer: &'a mut imgui_vulkano_renderer_unsafe::Renderer,
    ui: &'a imgui::DrawData,
}

impl<'a> crate::UiModule for ImguiModule<'a> {
    fn render(
        &mut self,
        command_buffer_builder: &mut UnsafeCommandBufferBuilder,
        queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + Send + Sync + 'static>,
    ) {
        unsafe {
            self.renderer
                .unsafe_draw_commands(command_buffer_builder, queue, target, self.ui);
        }
    }
}

use cgmath::Vector2;
use game_kernel_utils::{AutoIndexMap, KeyType as MapKeyType};

use std::collections::{HashMap, HashSet};

pub type ScanCode = u32;

#[derive(PartialEq, Eq, Hash)]
pub enum MouseKeys {
    Left,
    Middle,
    Right,
}

impl MouseKeys {
    pub fn from_winit(button: &MouseButton) -> Option<Self> {
        match button {
            MouseButton::Left => Some(Self::Left),
            MouseButton::Right => Some(Self::Middle),
            MouseButton::Middle => Some(Self::Right),
            _ => None,
        }
    }
}

#[derive(PartialEq, Eq, Hash)]
pub enum KeyTypes {
    Mouse(MouseKeys),
    KeyBoard(ScanCode),
}

#[derive(PartialEq, Eq, Hash)]
pub enum EventTypes {
    KeyDown(KeyTypes),
    KeyUp(KeyTypes),
    MouseUpdate(Vector2<u16>),
    Empty,
}

use winit::event::{ElementState::Pressed, Event, MouseButton, WindowEvent};

impl<'a, T: 'static> From<&Event<'a, T>> for EventTypes {
    fn from(e: &Event<'a, T>) -> Self {
        match e {
            Event::WindowEvent {
                event: WindowEvent::KeyboardInput { input, .. },
                ..
            } => {
                let key = KeyTypes::KeyBoard(input.scancode);
                if input.state == Pressed {
                    EventTypes::KeyDown(key)
                } else {
                    EventTypes::KeyUp(key)
                }
            }
            Event::WindowEvent {
                event: WindowEvent::MouseInput { state, button, .. },
                ..
            } => {
                let key = MouseKeys::from_winit(button);
                if key.is_none() {
                    return EventTypes::Empty;
                }
                let key = key.unwrap();

                if *state == Pressed {
                    EventTypes::KeyDown(KeyTypes::Mouse(key))
                } else {
                    EventTypes::KeyUp(KeyTypes::Mouse(key))
                }
            }
            _ => EventTypes::Empty, //TODO: handle mouse
        }
    }
}

/// this struct manages input.
/// Events are first abstracted into actions. An action occurs whenever
/// a KeyDown event happens with the corrisponding scan code.
/// KeyUp events are also tracked.
/// The state of an action can be queried with ```is_action_triggered``` or
/// with ```is_action_down```.
/// Controls are built on top of actions and they are mainly used for
/// things like player movement. Each control has a name and a value is
/// bound to an action. If multiple controls have the same name the
/// different values will be added whenever the event occurs.
/// Their value can be queried with ```get_control_value```
/// the following is a minimal example:
/// ```
/// use game_kernel::input::KeyTypes;
/// // ...
/// //assuming input is an instance of the struct
///
/// //first, during setup, action and controls are bound
/// input.bind_action(KeyTypes::KeyBoard(17), "forward");
/// input.bind_action(KeyTypes::KeyBoard(31), "backward");
/// input.bind_control("forward", "forward", 1.0);
/// //notice that we use the same control but with a different action and
/// //negative value, this allows to go in the opposite direction
/// input.bind_control("backward", "forward", -1.0);
///
/// input.bind_action(KeyTypes::KeyBoard(32), "left");
/// input.bind_action(KeyTypes::KeyBoard(30), "right");
/// input.bind_control("left", "side", 1.0);
/// input.bind_control("right", "side", -1.0);
///
/// //as well as being the answer to the ultimate question, 42 also
/// //happens to be the scancode for shift
/// input.bind_action(KeyTypes::KeyBoard(42), "sprint");
///
/// //...
/// //every frame or whenever the input needs to be queried
/// let velocity = if input.is_action_down("sprint") {
///     0.04
/// }
/// else {
///     0.02
/// };
///
/// //whenever needed control values can be queried like this:
/// player_position.x +=
///     input.get_control_value("forward").unwrap() * velocity;
/// player_position.y +=
///     input.get_control_value("side").unwrap() * velocity;
///
/// ```
///
pub struct Input {
    mappings: HashMap<KeyTypes, String>,

    frame_actions: HashMap<String, bool>,
    released_actions: HashSet<String>,
    controls: HashMap<String, (String, f32)>,
    control_values: HashMap<String, f32>,
}

impl Input {
    pub fn new() -> Self {
        Self {
            mappings: HashMap::new(),
            frame_actions: HashMap::new(),
            released_actions: HashSet::new(),
            controls: HashMap::new(),
            control_values: HashMap::new(),
        }
    }

    /// must be called on any new event
    pub fn register_event(&mut self, event: EventTypes) {
        match event {
            EventTypes::KeyDown(k) => {
                if let Some(action) = self.mappings.get(&k) {
                    if let None = self.frame_actions.get(action) {
                        self.frame_actions.insert(action.clone(), false);
                    }
                }
            }
            EventTypes::KeyUp(k) => {
                if let Some(action) = self.mappings.get(&k) {
                    if let Some(handled) = self.frame_actions.get(action) {
                        if *handled {
                            self.frame_actions.remove(action);
                            self.released_actions.insert(action.clone());
                        }
                    }
                }
            }
            _ => {} //TODO: manage mouse events
        }
    }

    pub fn on_frame_start(&mut self) {
        for (action, sign) in self
            .frame_actions
            .iter()
            .filter(|(_, handled)| !**handled)
            .map(|(action, _)| (action, 1.0))
            .chain(self.released_actions.iter().map(|action| (action, -1.0)))
        {
            if let Some(c) = self.controls.get(action) {
                if let Some(cval) = self.control_values.get_mut(&c.0) {
                    *cval += c.1 * sign;
                } else {
                    self.control_values.insert(c.0.clone(), c.1 * sign);
                }
            }
        }
    }

    pub fn on_frame_end(&mut self) {
        for (_, handled) in self.frame_actions.iter_mut() {
            *handled = true;
        }
        self.released_actions.clear();
    }

    pub fn bind_action<A: AsRef<str>>(&mut self, key: KeyTypes, action_name: A) {
        self.mappings.insert(key, action_name.as_ref().to_owned());
    }

    pub fn bind_control<A: AsRef<str>, N: AsRef<str>>(
        &mut self,
        action_name: A,
        control_name: N,
        value: f32,
    ) {
        //check that the action actually exists
        if self
            .mappings
            .values()
            .position(|a| a == action_name.as_ref())
            .is_some()
        {
            self.controls.insert(
                action_name.as_ref().to_owned(),
                (control_name.as_ref().to_owned(), value),
            );
        }
    }

    pub fn is_action_triggered<N: AsRef<str>>(&self, action_name: N) -> bool {
        self.frame_actions
            .get(action_name.as_ref())
            .map(|v| !v)
            .unwrap_or(false)
    }

    pub fn is_action_down<N: AsRef<str>>(&self, action_name: N) -> bool {
        self.frame_actions.get(action_name.as_ref()).is_some()
    }

    pub fn get_control_value<N: AsRef<str>>(&self, control_name: N) -> Option<f32> {
        if let Some(value) = self.control_values.get(control_name.as_ref()) {
            Some(*value)
        } else {
            if self
                .controls
                .iter()
                .find(|(_, (cname, _))| cname.as_str() == control_name.as_ref())
                .is_some()
            {
                Some(0.0)
            } else {
                None
            }
        }
    }
}

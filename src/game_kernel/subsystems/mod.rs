pub mod audio;
pub mod input;
pub mod resources;
pub mod video;
pub mod window;

use vulkano;
use vulkano_win;

use self::input::EventTypes;
use self::video::common::vulkan;
use self::vulkano::swapchain::Surface;
use self::vulkano_win::VkSurfaceBuild;
use crate::game_kernel::ecs::{SystemManager, World};
use crate::renderer::output::{self, OffscreenOutput, SwapchainOutout};
use crate::renderer::ui_module::UiRenderer;
use crate::CameraComponent;
use physfs_rs;
use video::ui::{egui::*, imgui::*};
use winit::dpi::LogicalSize;
use winit::{
    event::DeviceEvent, event::Event, event::WindowEvent, event_loop::ControlFlow,
    event_loop::EventLoop, window::Window, window::WindowBuilder,
};

use std::cell::{RefCell, RefMut};
use std::collections::VecDeque;
use std::fs;
use std::ops::Deref;
use std::rc::Rc;
use std::sync::Arc;
use std::time::{Duration, Instant};

pub struct Subsystems {
    //window: Option<window::Window>,
    //window: &'static winit::Window,
    pub surface: Arc<Surface<Window>>,
    pub events_loop: Option<EventLoop<()>>,
    vulkan_common: Option<vulkan::VulkanCommon>,
    pub renderer: video::Video,
    pub ui_renderer: UiRenderer,
    pub swapchain_output: SwapchainOutout<Window>,
    pub offscreen_output: OffscreenOutput,
    pub resource_manager: resources::ResourceManager,
    pub world: Rc<RefCell<World>>,
    pub systems_manager: SystemManager,
    pub input: input::Input,
    pub gk_imgui: GkImgui,
    pub gk_egui: GkEgui,
    pub wren_scripting_engine: Rc<RefCell<super::wren_scripting::WrenScriptingEngine>>,
    pub physfs: physfs_rs::PhysFs,
    pub audio: Arc<RefCell<audio::AudioContext>>,
    pub user_events: VecDeque<EventTypes>,
}

impl Subsystems {
    fn update_cursor(&self, captured: bool) {
        self.surface.window().set_cursor_visible(!captured);
        if let Err(e) = self.surface.window().set_cursor_grab(captured) {
            crate::log_warn!("Unable to grab cursor {:?}", e);
        }
    }

    pub fn main_loop<F>(mut self, mut update: F)
    where
        F: FnMut(
                f32,
                cgmath::Vector2<f32>,
                &[bool],
                Rc<RefCell<World>>,
                &imgui::Ui,
                &input::Input,
                &mut VecDeque<EventTypes>,
                &SystemManager,
                &mut GkEgui,
                &mut bool,
            ) -> bool
            + 'static,
    {
        let mut prev_time = Instant::now();
        let start_instant = Instant::now();
        let el = self.events_loop.unwrap();
        self.events_loop = None;
        let dimensions: (u32, u32) = self.surface.window().inner_size().into();
        let half_res = cgmath::Vector2::new((dimensions.0 / 2) as f32, (dimensions.1 / 2) as f32);
        let mut mouse_pos = half_res;
        let mut mouse_delta = (0.0, 0.0);
        let mut buttons = [false; 4];
        let mut captured = true;
        let mut was_captured = true;
        self.update_cursor(captured);
        el.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Poll;

            //TODO proper logic
            if !captured || true {
                self.gk_imgui.platform.handle_event(
                    self.gk_imgui.context.io_mut(),
                    self.surface.window(),
                    &event,
                );
                self.gk_egui.on_event(&event);
            }
            self.input.register_event((&event).into());
            match event {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => {
                    crate::log_msg!("exiting");
                    *control_flow = ControlFlow::Exit;
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => {
                    match input.scancode {
                        17 => buttons[0] = input.state == winit::event::ElementState::Pressed,
                        31 => buttons[1] = input.state == winit::event::ElementState::Pressed,
                        30 => buttons[2] = input.state == winit::event::ElementState::Pressed,
                        32 => buttons[3] = input.state == winit::event::ElementState::Pressed,

                        1 => {
                            if input.state == winit::event::ElementState::Pressed {
                                captured = !captured;
                                self.update_cursor(captured);
                            }
                        }
                        //scancode => log_msg!("\n{}", scancode)
                        _ => (),
                    }
                }
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    mouse_pos = cgmath::Vector2::new(position.x as f32, position.y as f32);
                    return;
                }
                Event::MainEventsCleared => {
                    while let Some(user_event) = self.user_events.pop_front() {
                        self.input.register_event(user_event);
                    }
                    self.input.on_frame_start();

                    self.gk_imgui.prepare_frame(self.surface.window());

                    let delta = prev_time.elapsed().as_micros() as f32 / 1000 as f32;

                    prev_time = Instant::now();
                    print!(
                        "\r frametime: {} framerate {}",
                        delta,
                        if delta != 0f32 { 1000f32 / delta } else { 0f32 }
                    );

                    let ui = self.gk_imgui.context.frame();
                    self.gk_egui
                        .context
                        .begin_frame(self.gk_egui.platform.take_egui_input(self.surface.window()));
                    /*self.gk_egui
                    .platform
                    .update_time(start_instant.elapsed().as_secs_f64());*///TODO needed?

                    if !captured {
                        mouse_delta = (0.0, 0.0);
                    }
                    if !update(
                        delta,
                        cgmath::Vector2::new(mouse_delta.0 as f32, mouse_delta.1 as f32),
                        &buttons,
                        self.world.clone(),
                        &ui,
                        &self.input,
                        &mut self.user_events,
                        &self.systems_manager,
                        &mut self.gk_egui,
                        &mut captured,
                    ) {
                        *control_flow = ControlFlow::Exit;
                    }
                    mouse_delta = (0.0, 0.0);
                    self.systems_manager.update_systems(
                        self.world.borrow_mut(),
                        Duration::from_micros(1000 * delta as u64),
                    );
                    let render = &mut self.renderer.renderer.as_mut().unwrap();

                    let active_camera_entity = self
                        .systems_manager
                        .get_system::<crate::CameraSystem>()
                        .unwrap()
                        .get_active_camera(self.world.borrow().deref())
                        .unwrap();

                    let w = self.world.borrow();
                    let camera = w
                        .get_entity_components::<&CameraComponent>(active_camera_entity)
                        .unwrap();

                    self.gk_imgui
                        .platform
                        .prepare_render(&ui, self.surface.window());

                    let draw_data = ui.render();
                    //call render before
                    if captured {
                        /*render.borrow_mut().render_frame::<crate::DummyUiModule, _>(
                            delta,
                            &camera.camera,
                            None,
                            self.renderer.swapchain.as_ref().unwrap(),
                        );*/
                        if captured != was_captured {
                            render
                                .borrow_mut()
                                .replace_output(Box::new(self.swapchain_output.clone()));
                        }
                        render.borrow_mut().render_frame(
                            delta,
                            &camera.camera,
                            /*Some(&mut GkImgui::get_mod(
                                &mut self.gk_imgui.renderer,
                                draw_data,
                            ))*/
                            Some(&mut (
                                GkImgui::get_mod(&mut self.gk_imgui.renderer, draw_data),
                                self.gk_egui.get_mod(),
                            )),
                            self.renderer.swapchain.as_ref().unwrap(),
                        );
                    } else {
                        if captured != was_captured {
                            render
                                .borrow_mut()
                                .replace_output(Box::new(self.offscreen_output.clone()));
                        }
                        render.borrow_mut().render_frame::<crate::DummyUiModule, _>(
                            delta,
                            &camera.camera,
                            None,
                            self.renderer.swapchain.as_ref().unwrap(),
                        );
                        self.ui_renderer.render_frame(Some(&mut (
                            GkImgui::get_mod(&mut self.gk_imgui.renderer, draw_data),
                            self.gk_egui.get_mod(),
                        )));
                    }
                    self.input.on_frame_end();

                    was_captured = captured;
                }

                Event::DeviceEvent { event, .. } => match event {
                    DeviceEvent::MouseMotion { delta } => {
                        mouse_delta.0 += delta.0;
                        mouse_delta.1 += delta.1;
                    }

                    _ => {}
                },

                Event::WindowEvent {
                    event: WindowEvent::Focused(is_focused),
                    ..
                } => {
                    captured = is_focused;
                    self.update_cursor(captured);
                }
                _ => (), //e => log_msg!("{:?}", e),
            }
            ()
        });
    }

    pub fn load(&mut self, filename: &str) {
        let reader = self.physfs.open_read(filename).unwrap();
        self.resource_manager
            .load_world(self.world.borrow_mut(), reader, &self.physfs)
            .unwrap();
    }

    pub fn get_vulkan_common(&self) -> Option<vulkan::VulkanCommon> {
        Some(self.vulkan_common.as_ref()?.clone())
    }
}

pub fn init() -> Result<Subsystems, String> {
    crate::log_msg!("------------------starting-------------------");
    let events_loop = EventLoop::new();
    crate::log_msg!("initializing window");
    let extensions = vulkano_win::required_extensions();

    vulkano::instance::layers_list()
        .unwrap()
        .for_each(|l| crate::log_msg!("available layer {}", l.name()));
    let layers: Vec<_> = vulkano::instance::layers_list()
        .unwrap()
        .filter(|l| l.name().contains("vvalidation"))
        .collect();
    layers
        .iter()
        .for_each(|l| crate::log_msg!("using layer {}", l.name()));
    let instance = vulkan::VulkanCommon::get_instance(&extensions, layers.iter().map(|l| l.name()));

    crate::log_msg!("loading config");
    let mut vk_config: vulkan::VulkanConfig =
        ::toml::from_str(&fs::read_to_string("vulkan.toml").expect("config file not found"))
            .unwrap();

    let surface = WindowBuilder::new()
        .with_inner_size(LogicalSize::new(
            vk_config.resolution[0],
            vk_config.resolution[1],
        ))
        .with_resizable(false)
        .build_vk_surface(&events_loop, instance.clone())
        .unwrap();
    /*if let Some(monitor) = surface.window().primary_monitor()
    {
        if let Some(s) = monitor.video_modes().next() {
            surface.window().set_fullscreen(Some(Fullscreen::Exclusive(s)));
        }
        else {
            crate::log_err!("failed to find mode");
        }
    }
    else {
        crate::log_err!("failed to set fullscreen");
    }*/
    surface.window().set_inner_size(LogicalSize::new(
        vk_config.resolution[0],
        vk_config.resolution[1],
    ));
    //wait for the window to resize
    std::thread::sleep(std::time::Duration::from_millis(2000));

    /*vk_config.resolution = {
        let dimensions: (u32, u32) = surface.window().inner_size().into();
        [dimensions.0, dimensions.1]
    };*/
    vk_config.resolution = surface.window().inner_size().into();
    crate::log_msg!("opened window dimentions are {:?}", vk_config.resolution);
    crate::log_msg!("initializing vulkan");
    let vulkan_common =
        vulkan::VulkanCommon::new(instance, &mut vk_config, &extensions, surface.clone());
    crate::log_msg!("initializing renderer");
    let mut renderer = video::Video::new();
    renderer
        .init_renderer(&vulkan_common.clone(), surface.clone())
        .unwrap();

    let ui_renderer = UiRenderer::new(
        Box::new(output::SwapchainOutout::new(
            renderer.get_swapchain(),
            vulkan_common.clone(),
        )),
        vulkan_common.clone(),
    );

    let swapchain_output =
        output::SwapchainOutout::new(renderer.get_swapchain(), vulkan_common.clone());
    let offscreen_output = output::OffscreenOutput::new(
        renderer.get_swapchain().swapchain_format,
        vulkan_common.clone(),
    );

    crate::game_kernel::engine_components::register_components();

    let mut resource_manager = resources::ResourceManager::new();
    resources::loaders::register_loaders(&mut resource_manager, vulkan_common.clone());

    let world = Rc::new(RefCell::new(World::new()));
    let systems_manager = SystemManager::new(crate::Config::new(""));

    let input = input::Input::new();

    let wren_scripting_engine = Rc::new(RefCell::new(
        super::wren_scripting::WrenScriptingEngine::new(),
    ));

    let mut physfs =
        physfs_rs::PhysFs::get().expect("PhysFs::get must not be called outside game_kernel");

    let audio = Arc::new(RefCell::new(audio::AudioContext::new()));

    let gk_imgui = GkImgui::init(
        surface.window(),
        vulkan_common.clone(),
        renderer.get_swapchain(),
    )
    .unwrap();
    let gk_egui = GkEgui::init(
        surface.window(),
        vulkan_common.clone(),
        renderer.get_swapchain(),
        None,
    )
    .unwrap();

    Ok(Subsystems {
        surface: surface.clone(),
        events_loop: Some(events_loop),
        vulkan_common: Some(vulkan_common.clone()),
        renderer,
        ui_renderer,
        swapchain_output,
        offscreen_output,
        resource_manager,
        world,
        systems_manager,
        input,
        gk_imgui,
        gk_egui,
        wren_scripting_engine,
        physfs,
        audio,
        user_events: VecDeque::new(),
    })
}

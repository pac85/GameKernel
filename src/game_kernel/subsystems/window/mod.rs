use winit;

use self::winit::{event::Event, event::WindowEvent, event_loop::EventLoop, window::WindowBuilder};
use game_kernel_ecs::ecs::system::*;

pub struct Window {
    events_loop: EventLoop<()>,
}

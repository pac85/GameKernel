pub mod hrtf;

use rodio::Source;
use std::collections::VecDeque;

use std::cell::RefCell;
use std::iter::Iterator;
use std::sync::{Arc, Mutex};

pub struct Comb<S: Iterator<Item = f32> + Source> {
    source: S,
    delay_queue: VecDeque<f32>,
    gain: f32,
}

impl<S: Iterator<Item = f32> + Source> Comb<S> {
    pub fn new(source: S, delay_samples: u32, gain: f32) -> Self {
        let mut delay_queue = VecDeque::with_capacity(delay_samples as usize);
        for _ in 0..delay_samples {
            delay_queue.push_back(0.0);
        }

        Self {
            source,
            delay_queue,
            gain,
        }
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for Comb<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let out = self.delay_queue.pop_front()?;
        self.delay_queue
            .push_back(self.source.next()? + self.gain * out);
        Some(out)
    }
}

impl<S: Iterator<Item = f32> + Source> Source for Comb<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.source.channels()
    }

    fn sample_rate(&self) -> u32 {
        self.source.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

struct SplitInner<S: Iterator<Item = f32> + Source> {
    source: S,
    splits: u32,
    processed: u32,
    current_sample: Option<f32>,
}

pub struct Split<S: Iterator<Item = f32> + Source> {
    inner: Arc<Mutex<SplitInner<S>>>,
}

impl<S: Iterator<Item = f32> + Source> Split<S> {
    pub fn new(source: S) -> Self {
        let inner = SplitInner {
            source,
            splits: 0,
            processed: 0,
            current_sample: None,
        };

        Self {
            inner: Arc::new(Mutex::new(inner)),
        }
    }

    pub fn split(&mut self) -> Splitted<S> {
        self.inner.lock().unwrap().splits += 1;
        Splitted {
            source: self.inner.clone(),
        }
    }
}

pub struct Splitted<S: Iterator<Item = f32> + Source> {
    source: Arc<Mutex<SplitInner<S>>>,
}

impl<S: Iterator<Item = f32> + Source> Iterator for Splitted<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let mut cell = self.source.lock().unwrap();
        cell.processed += 1;
        cell.processed %= cell.splits;
        if cell.processed == 0 {
            cell.current_sample = cell.source.next();
        }

        cell.current_sample
    }
}

impl<S: Iterator<Item = f32> + Source> Source for Splitted<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        let cell = self.source.lock().unwrap();
        cell.source.channels()
    }

    fn sample_rate(&self) -> u32 {
        let cell = self.source.lock().unwrap();
        cell.source.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

pub struct Allpass<S: Iterator<Item = f32> + Source> {
    comb: Comb<Splitted<S>>,
    original: Splitted<S>,
    gain: f32,
    mix_gain: f32,
}

impl<S: Iterator<Item = f32> + Source> Allpass<S> {
    pub fn new(source: S, delay_samples: u32, gain: f32) -> Self {
        let mut split = Split::new(source);
        let comb = Comb::new(split.split(), delay_samples, gain);
        let original = split.split();
        let mix_gain = 1.0 - gain * gain;

        Self {
            comb,
            original,
            gain,
            mix_gain,
        }
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for Allpass<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let comb = self.comb.next()?;
        let original = self.original.next().unwrap_or(0.0);

        Some(-original * self.gain + self.mix_gain * comb)
    }
}

impl<S: Iterator<Item = f32> + Source> Source for Allpass<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.original.channels()
    }

    fn sample_rate(&self) -> u32 {
        self.original.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

use rodio::source::Mix;

macro_rules! impl_inner_source {
    ($t:tt) => {
        impl<S: Iterator<Item = f32> + Source> Iterator for $t<S> {
            type Item = f32;

            fn next(&mut self) -> Option<Self::Item> {
                self.inner.next()
            }
        }

        impl<S: Iterator<Item = f32> + Source> Source for $t<S> {
            fn current_frame_len(&self) -> Option<usize> {
                None
            }

            fn channels(&self) -> u16 {
                self.inner.channels()
            }

            fn sample_rate(&self) -> u32 {
                self.inner.sample_rate()
            }

            fn total_duration(&self) -> Option<core::time::Duration> {
                None
            }
        }
    };
}

pub struct Mix4<S>
where
    S: Iterator<Item = f32> + Source,
{
    inner: Mix<Mix<S, S>, Mix<S, S>>,
}

impl<S: Iterator<Item = f32> + Source> Mix4<S> {
    pub fn new(s1: S, s2: S, s3: S, s4: S) -> Self {
        Self {
            inner: (s1.mix(s2)).mix(s3.mix(s4)),
        }
    }
}

impl_inner_source!(Mix4);

pub struct Schroeder<S: Iterator<Item = f32> + Source> {
    inner: Allpass<Allpass<Mix4<Comb<Splitted<S>>>>>,
}

impl<S: Iterator<Item = f32> + Source> Schroeder<S> {
    pub fn new(source: S) -> Self {
        let mut split = Split::new(source);

        let a = split.split();
        let b = split.split();
        let c = split.split();
        let d = split.split();

        let mixed = Mix4::new(
            Comb::new(a, 4799, 0.742),
            Comb::new(b, 4999, 0.733),
            Comb::new(c, 5399, 0.715),
            Comb::new(d, 5399, 0.697),
        );

        Self {
            inner: Allpass::new(Allpass::new(mixed, 1051, 0.7), 337, 0.7),
        }
    }
}

impl_inner_source!(Schroeder);

pub struct Downmix<S: Iterator<Item = f32> + Source> {
    source: S,
    volumes: [f32; super::MAX_CHANNELS],
}

impl<S: Iterator<Item = f32> + Source> Downmix<S> {
    pub fn new(source: S, volumes: &[f32]) -> Self {
        assert!(source.channels() < super::MAX_CHANNELS as u16);
        let mut vs = [0.0; super::MAX_CHANNELS];
        for (i, v) in volumes.iter().enumerate() {
            if i >= super::MAX_CHANNELS {
                break;
            }
            vs[i] = *v;
        }
        Self {
            source,
            volumes: vs,
        }
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for Downmix<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let mut a = 0.0;
        let mut div = 0.0;
        for i in 0..(self.source.channels() as usize) {
            let s = self.source.next()? * self.volumes[i];
            a += s;
            div += self.volumes[i];
        }

        Some(a / div)
    }
}

impl<S: Iterator<Item = f32> + Source> Source for Downmix<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        self.source.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

pub struct Upmix<S: Iterator<Item = f32> + Source> {
    source: S,
    channels: u16,
    repeated: u16,
    sample: Option<f32>,
}

impl<S: Iterator<Item = f32> + Source> Upmix<S> {
    pub fn new(source: S, channels: u16) -> Self {
        Self {
            source,
            channels,
            repeated: 0,
            sample: None,
        }
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for Upmix<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.repeated == 0 {
            self.sample = self.source.next();
        }
        self.repeated += 1;
        self.repeated %= self.channels;

        self.sample
    }
}

impl<S: Iterator<Item = f32> + Source> Source for Upmix<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn sample_rate(&self) -> u32 {
        self.source.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

pub struct Resample<S: Iterator<Item = f32> + Source> {
    source: S,
    factor: u32,
    sample: Option<f32>,
    csample: u32,
}

impl<S: Iterator<Item = f32> + Source> Resample<S> {
    pub fn new(source: S, factor: u32) -> Self {
        Self {
            source,
            factor,
            sample: None,
            csample: 0,
        }
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for Resample<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.csample == 0 {
            self.sample = self.source.next();
        }

        self.csample += 1; //self.source.sample_rate();
        self.csample %= self.factor;

        self.sample
    }
}

impl<S: Iterator<Item = f32> + Source> Source for Resample<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.source.channels()
    }

    fn sample_rate(&self) -> u32 {
        self.source.sample_rate() * self.factor
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

use rustfft::num_complex::Complex;
use rustfft::Fft;

//https://en.wikipedia.org/wiki/Overlap%E2%80%93save_method
//https://docs.rs/rustfft/6.0.1/rustfft/
pub struct OverlapSave<S: Iterator<Item = f32> + Source> {
    m: usize,
    source: S,
    irtf: Vec<Complex<f32>>,

    processing: Vec<Complex<f32>>,
    output: Vec<Complex<f32>>,
    signal_buffer: Vec<f32>,
    current_buffer: bool,
    idx: usize,

    forward: Arc<dyn Fft<f32>>,
    inverse: Arc<dyn Fft<f32>>,
}

impl<S: Iterator<Item = f32> + Source> OverlapSave<S> {
    pub fn new(source: S, fir: &[f32]) -> Self {
        let m = fir.len();
        let overlap = fir.len() - 1;
        let buf_len = 8 * overlap;

        let mut planner = rustfft::FftPlanner::new();
        let mut forward = planner.plan_fft_forward(buf_len);
        let mut irtf = fir
            .iter()
            .cloned()
            .chain(std::iter::repeat(0.0).take(buf_len - m))
            .map(|v| Complex::new(v / fir.len() as f32, 0.0))
            .collect::<Vec<_>>();
        forward.process(&mut irtf);
        let mut processing = Vec::with_capacity(buf_len);
        for i in 0..buf_len {
            processing.push(Complex::new(0.0, 0.0));
        }
        let mut output = Vec::with_capacity(buf_len);
        for i in 0..buf_len {
            output.push(Complex::new(0.0, 0.0));
        }
        let mut signal_buffer = Vec::with_capacity(buf_len - overlap);
        for i in 0..(buf_len - overlap) {
            signal_buffer.push(0.0);
        }
        Self {
            m,
            source,
            irtf,

            processing,
            output,
            signal_buffer,
            current_buffer: false,
            idx: m,

            forward,
            inverse: planner.plan_fft_inverse(buf_len),
        }
    }

    pub fn update_fir(&mut self, fir: &[f32]) {
        let mut planner = rustfft::FftPlanner::new();
        let mut forward = planner.plan_fft_forward(self.buf_len());
        let mut irtf = fir
            .iter()
            .cloned()
            .chain(std::iter::repeat(0.0).take(self.buf_len() - self.m))
            .map(|v| Complex::new(v / fir.len() as f32, 0.0))
            .collect::<Vec<_>>();
        forward.process(&mut irtf);

        self.irtf = irtf;
    }

    fn buf_len(&self) -> usize {
        self.processing.len()
    }

    fn current_a(&self) -> usize {
        if self.current_buffer {
            0
        } else {
            1
        }
    }

    fn current_b(&self) -> usize {
        if self.current_buffer {
            1
        } else {
            0
        }
    }

    fn swap_buffers(&mut self) {
        self.idx = self.m;
        self.current_buffer = !self.current_buffer;
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for OverlapSave<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        self.signal_buffer[self.idx - self.m] = self.source.next()?;
        self.idx += 1;
        if (self.idx - self.m) >= self.signal_buffer.len() {
            //shift the processing buffer
            let m = self.signal_buffer.len();
            for i in 0..self.processing.len() - m {
                self.processing[i] = self.processing[i + m];
            }
            let n = self.processing.len();
            for i in 0..m {
                self.processing[n - m + i] = Complex::new(self.signal_buffer[i], 0.0);
            }
            self.output = self.processing.clone();
            //do the actual processing in the new buffer
            let a = self.output[self.m].re;
            self.forward.process(&mut self.output);
            for (i, s) in self.output.iter_mut().enumerate() {
                *s *= self.irtf[i];
            }
            self.inverse.process(&mut self.output);
            //println!("{} {}", self.output[self.m].re / a.max(0.0001), a);

            self.swap_buffers();
        }
        Some(self.output[self.idx - 1].re / self.output.len() as f32)
    }
}

impl<S: Iterator<Item = f32> + Source> Source for OverlapSave<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        self.source.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

pub struct DelayLine<S: Iterator<Item = f32> + Source> {
    source: S,
    delay_queue: VecDeque<f32>,
}

impl<S: Iterator<Item = f32> + Source> DelayLine<S> {
    pub fn new(source: S, delay_samples: u32) -> Self {
        let mut delay_queue = VecDeque::with_capacity(delay_samples as usize);
        for _ in 0..delay_samples {
            delay_queue.push_back(0.0);
        }

        Self {
            source,
            delay_queue,
        }
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for DelayLine<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let out = self.delay_queue.pop_front()?;
        self.delay_queue
            .push_back(self.source.next().unwrap_or(0.0));
        Some(out)
    }
}

impl<S: Iterator<Item = f32> + Source> Source for DelayLine<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        self.source.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

pub struct CrossFade<S: Iterator<Item = f32> + Source> {
    a: S,
    b: S,
    fade: f32,
    vf: f32,
}

impl<S: Iterator<Item = f32> + Source> CrossFade<S> {
    pub fn new(a: S, b: S, speed: f32) -> Self {
        Self {
            a,
            b,
            fade: 0.0,
            vf: speed,
        }
    }

    pub fn get_next_mut(&mut self) -> &mut S {
        if self.fade == 0.0 {
            &mut self.b
        } else {
            &mut self.a
        }
    }

    pub fn switch_dir(&mut self) {
        self.vf = -self.vf;
    }

    pub fn mutate_and_switch(&mut self, f: impl Fn(&mut S)) {
        f(self.get_next_mut());
        self.switch_dir();
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for CrossFade<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        self.fade += self.vf;
        self.fade = self.fade.max(0.0).min(1.0);

        let a = self.a.next()?;
        let b = self.b.next()?;

        Some(a * self.fade + b * (1.0 - self.fade))
    }
}

impl<S: Iterator<Item = f32> + Source> Source for CrossFade<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        self.a.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

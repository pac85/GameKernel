use byteorder::{ByteOrder, LittleEndian};
use cgmath::*;
use std::convert::TryInto;

pub fn default_hrir_data() -> [&'static [u8]; 2] {
    [
        include_bytes!("../../../../../data/default_hrir/l.bin"),
        include_bytes!("../../../../../data/default_hrir/r.bin"),
    ]
}

fn parse_data(data: &'static [u8]) -> Box<[[[f32; 200]; 50]; 25]> {
    let samples: [f32; 250000] = data
        .chunks_exact(4)
        .map(|bytes| LittleEndian::read_f32(bytes) * 200.0)
        .collect::<Vec<_>>()
        .as_slice()
        .try_into()
        .unwrap();

    unsafe { Box::new(std::mem::transmute(samples)) }
}

pub struct Hrir {
    l: Box<[[[f32; 200]; 50]; 25]>,
    r: Box<[[[f32; 200]; 50]; 25]>,
}

impl Hrir {
    pub fn from_data(data: [&'static [u8]; 2]) -> Self {
        let [ld, rd] = data;
        Self {
            l: parse_data(ld),
            r: parse_data(rd),
        }
    }

    pub fn from_default() -> Self {
        Self::from_data(default_hrir_data())
    }

    //converts angle into azimuth index
    fn to_naz(angle: i32) -> usize {
        let angle = (angle / 5) * 5;
        if angle < -65 {
            0
        } else if angle < -55 {
            1
        } else if angle >= -55 && angle < 55 {
            (12 + (angle / 5)) as usize
        } else if angle > 55 {
            23
        } else
        /*if angle >= 65*/
        {
            24
        }
    }

    //converts angle into elevation index
    fn to_nel(angle: i32) -> usize {
        (((angle + 45) as f32 / 5.625).floor() as usize)
            .max(1)
            .min(50)
            - 1
    }

    fn map_angle(angle: f32) -> f32 {
        let DEG_TO_RAD = std::f32::consts::PI / 180.0;
        let angle = (angle * DEG_TO_RAD).sin().atan2((angle * DEG_TO_RAD).cos()) / DEG_TO_RAD;
        if angle < -90.0 {
            angle + 360.0
        } else {
            angle
        }
    }

    pub fn get_hrir(&self, azimuth: i32, elevation: i32) -> [&[f32; 200]; 2] {
        let (naz, nel) = (Self::to_naz(azimuth), Self::to_nel(elevation));
        [&self.l[naz][nel], &self.r[naz][nel]]
    }

    pub fn get_hrir_cartesian(&self, dir: Vector3<f32>) -> [&[f32; 200]; 2] {
        let dir = dir.normalize();
        //let dir = Vector3::new(dir.y, dir.z, dir.x);

        /*let el = (dir.z / (dir.y * dir.y + dir.z * dir.z).sqrt()).acos() * dir.z.signum();
        let az = (dir.x / dir.z).atan();*/
        let az = -dir.x.asin();
        let el = dir.y.atan2(dir.z)
            + if dir.y < 0.0 && dir.z < 0.0 {
                std::f32::consts::PI * 2.0
            } else {
                0.0
            };

        let RAD_TO_DEG = 180.0 * std::f32::consts::FRAC_1_PI;
        self.get_hrir(
            Self::map_angle((az * RAD_TO_DEG)) as i32,
            Self::map_angle((el * RAD_TO_DEG)) as i32,
        )
    }
}

use super::{OverlapSave, Splitted};
use rodio::Source;
use std::iter::Iterator;

pub struct HrtfFilter<S: Iterator<Item = f32> + Source> {
    hrir: Hrir,

    l: OverlapSave<Splitted<S>>,
    r: OverlapSave<Splitted<S>>,
    current: bool,
}

impl<S: Iterator<Item = f32> + Source> HrtfFilter<S> {
    fn make_filters(
        s1: Splitted<S>,
        s2: Splitted<S>,
        hrir: &Hrir,
        dir: Vector3<f32>,
    ) -> [OverlapSave<Splitted<S>>; 2] {
        let [ls, rs] = hrir.get_hrir_cartesian(dir);
        [OverlapSave::new(s1, ls), OverlapSave::new(s2, rs)]
    }

    pub fn new(source: S, hrir: Hrir) -> Self {
        let mut s = super::Split::new(source);
        let a = s.split();
        let b = s.split();

        let [l, r] = Self::make_filters(a, b, &hrir, Vector3::new(0.0, -1.0, 0.0));

        Self {
            hrir,
            l,
            r,
            current: false,
        }
    }

    pub fn from_split(s: &mut super::Split<S>, hrir: Hrir) -> Self {
        let a = s.split();
        let b = s.split();

        let [l, r] = Self::make_filters(a, b, &hrir, Vector3::new(0.0, -1.0, 0.0));

        Self {
            hrir,
            l,
            r,
            current: false,
        }
    }

    pub fn set_dir(&mut self, dir: Vector3<f32>) {
        let [ls, rs] = self.hrir.get_hrir_cartesian(dir);

        self.l.update_fir(ls);
        self.r.update_fir(rs);
    }
}

impl<S: Iterator<Item = f32> + Source> Iterator for HrtfFilter<S> {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        self.current = !self.current;

        if self.current {
            self.l.next()
        } else {
            self.r.next()
        }
    }
}

impl<S: Iterator<Item = f32> + Source> Source for HrtfFilter<S> {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        2
    }

    fn sample_rate(&self) -> u32 {
        self.l.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

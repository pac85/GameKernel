pub mod filters;

use std::sync::Arc;

use rodio;
use rodio::Source;

use cgmath::*;

use game_kernel_utils::{AutoIndexMap, KeyType};

const MAX_CHANNELS: usize = 16;

#[derive(Clone, Copy)]
pub struct PointSource {
    position: Vector3<f32>,
}

#[derive(Clone, Copy)]
pub struct SpotSource {
    position: Vector3<f32>,
    direction: Vector3<f32>,
    angle: f32,
}

#[derive(Clone, Copy)]
pub enum SourceProperties {
    Point(PointSource),
    Spot(SpotSource),
}

impl SourceProperties {
    pub fn get_position(&self) -> Vector3<f32> {
        match self {
            Self::Point(p) => p.position,
            Self::Spot(s) => s.position,
        }
    }

    pub fn set_position(&mut self, pos: Vector3<f32>) {
        match self {
            Self::Point(p) => p.position = pos,
            Self::Spot(s) => s.position = pos,
        }
    }
}

use std::any::Any;

pub trait AnySource: rodio::Source<Item = f32> + Send + Sync + std::any::Any {
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

impl<T> AnySource for T
where
    T: rodio::Source<Item = f32> + Send + Sync + std::any::Any,
{
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }
}

pub struct SourceWrapper {
    pub inner: Box<dyn AnySource>,
}

use std::io::{Read, Seek};

impl SourceWrapper {
    pub fn new(
        source: impl rodio::Source<Item = f32> + std::any::Any + Send + Sync + 'static,
    ) -> Self {
        Self {
            inner: Box::new(source),
        }
    }

    pub fn new_sine(f: u32) -> Self {
        Self::new(rodio::source::SineWave::new(f))
    }

    pub fn new_read(r: impl Read + Seek + Send + Sync + 'static) -> Option<Self> {
        Some(Self::new(rodio::Decoder::new(r).ok()?.convert_samples()))
    }
}

impl std::iter::Iterator for SourceWrapper {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl Source for SourceWrapper {
    fn current_frame_len(&self) -> Option<usize> {
        self.inner.current_frame_len()
    }

    fn channels(&self) -> u16 {
        self.inner.channels()
    }

    fn sample_rate(&self) -> u32 {
        self.inner.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        self.inner.total_duration()
    }
}

pub struct SpatialSource {
    pub properties: SourceProperties,
    pub reverb: SourceWrapper,
    pub original: filters::CrossFade<filters::hrtf::HrtfFilter<SourceWrapper>>,
}

impl SpatialSource {
    pub fn new_point(position: Vector3<f32>, stream: SourceWrapper) -> Self {
        let mut split = filters::Split::new(SourceWrapper::new(filters::Downmix::new(
            stream,
            &[1.0, 0.0],
        )));
        let a = split.split();
        let mut fir = [0.0; 128];
        fir[16] = 128.0;
        Self {
            properties: SourceProperties::Point(PointSource { position }),
            reverb: SourceWrapper::new(filters::Upmix::new(
                filters::DelayLine::new(filters::Schroeder::new(a), 200),
                2,
            )),
            original: filters::CrossFade::new(
                filters::hrtf::HrtfFilter::from_split(
                    &mut split,
                    filters::hrtf::Hrir::from_default(),
                ),
                filters::hrtf::HrtfFilter::from_split(
                    &mut split,
                    filters::hrtf::Hrir::from_default(),
                ),
                0.0045,
            ),
        }
    }
}

pub trait SpatialSourceBuilder {
    fn build(&self) -> SpatialSource;
}

pub struct StoredSamplesBuilder {
    samples: Arc<Vec<f32>>,
    channels: u16,
    sample_rate: u32,
    should_loop: bool,
}

impl StoredSamplesBuilder {
    pub fn from_source<S: Iterator<Item = f32> + Source>(s: S, should_loop: bool) -> Self {
        let channels = s.channels();
        let sample_rate = s.sample_rate();
        Self {
            samples: Arc::new(s.collect()),
            channels,
            sample_rate,
            should_loop,
        }
    }

    pub fn from_read(r: impl Read + Seek + Send, should_loop: bool) -> Option<Self> {
        Some(Self::from_source(
            rodio::Decoder::new(r).ok()?.convert_samples(),
            should_loop,
        ))
    }

    pub fn build_source(&self) -> StoredSource {
        StoredSource {
            samples: self.samples.clone(),
            current_sample: 0,
            channels: self.channels,
            sample_rate: self.sample_rate,
            should_loop: self.should_loop,
        }
    }
}

pub struct StoredSource {
    samples: Arc<Vec<f32>>,
    current_sample: usize,
    channels: u16,
    sample_rate: u32,
    should_loop: bool,
}

impl StoredSource {
    pub fn from_source<S: Iterator<Item = f32> + Source>(s: S, should_loop: bool) -> Self {
        StoredSamplesBuilder::from_source(s, should_loop).build_source()
    }

    pub fn set_current_sample(&mut self, c: usize) {
        self.current_sample = c;
    }
}

impl Iterator for StoredSource {
    type Item = f32;
    fn next(&mut self) -> Option<Self::Item> {
        self.current_sample += 1;
        if self.should_loop && self.current_sample == self.samples.len() {
            self.current_sample = 0;
        }
        self.samples.get(self.current_sample).cloned()
    }
}

impl Source for StoredSource {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn sample_rate(&self) -> u32 {
        self.sample_rate
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

impl SpatialSourceBuilder for StoredSamplesBuilder {
    fn build(&self) -> SpatialSource {
        SpatialSource::new_point(Vector3::zero(), SourceWrapper::new(self.build_source()))
    }
}

pub struct SpatialSourceBuilderWrapper {
    pub builder: Box<dyn SpatialSourceBuilder + Send + Sync>,
}

impl SpatialSourceBuilderWrapper {
    pub fn new(builder: impl SpatialSourceBuilder + Send + Sync + 'static) -> Self {
        Self {
            builder: Box::new(builder),
        }
    }
}

use crate::renderer::camera::Camera;

#[derive(Clone, Copy)]
pub struct Listener {
    positions: [Vector3<f32>; MAX_CHANNELS],
    view_mat: Matrix4<f32>,
}

impl Listener {
    pub fn new(mut ps: impl Iterator<Item = Vector3<f32>>, view_mat: Matrix4<f32>) -> Self {
        let mut positions = [Vector3::zero(); MAX_CHANNELS];
        for np in positions.iter_mut() {
            if let Some(p) = ps.next() {
                *np = p;
            } else {
                break;
            }
        }
        Self {
            positions,
            view_mat,
        }
    }

    pub fn set_positions(&mut self, mut ps: impl Iterator<Item = Vector3<f32>>) {
        self.positions = [Vector3::zero(); MAX_CHANNELS];
        for np in self.positions.iter_mut() {
            if let Some(p) = ps.next() {
                *np = p;
            } else {
                break;
            }
        }
    }

    pub fn from_camera(camera: &Camera) -> Self {
        Self::new(
            [camera.eye.to_vec(), camera.eye.to_vec()].iter().cloned(),
            camera.get_view(),
        )
    }
}

#[derive(Debug)]
pub struct InvalidSourceIndex(KeyType);

pub struct Renderer {
    sample_rate: u32,
    channels: u16,
    sources: AutoIndexMap<SpatialSource>,
    non_spacial: AutoIndexMap<SourceWrapper>,
    listener: Option<Listener>,
    current_channel: u16,
}

impl Renderer {
    pub fn new(sample_rate: u32, channels: u16) -> Self {
        Self {
            sample_rate,
            channels,
            sources: AutoIndexMap::new(),
            non_spacial: AutoIndexMap::new(),
            listener: None,
            current_channel: 0,
        }
    }

    pub fn add_source(&mut self, source: SpatialSource) -> KeyType {
        self.sources.insert(source)
    }

    pub fn add_non_spacial(&mut self, source: SourceWrapper) -> KeyType {
        self.non_spacial.insert(source)
    }

    pub fn mutate_source(
        &mut self,
        source_index: KeyType,
        f: impl Fn(&mut SpatialSource),
    ) -> Result<(), InvalidSourceIndex> {
        if let Some(s) = self.sources.get_mut(source_index) {
            f(s);
            Ok(())
        } else {
            Err(InvalidSourceIndex(source_index))
        }
    }

    pub fn mutate_non_spacial(
        &mut self,
        source_index: KeyType,
        f: impl Fn(&mut SourceWrapper),
    ) -> Result<(), InvalidSourceIndex> {
        if let Some(s) = self.non_spacial.get_mut(source_index) {
            f(s);
            Ok(())
        } else {
            Err(InvalidSourceIndex(source_index))
        }
    }

    pub fn set_listener(&mut self, listener: Listener) {
        self.listener = Some(listener);

        for (_, source) in self.sources.iter_mut() {
            let dir = (self.listener.as_ref().unwrap().view_mat
                * source.properties.get_position().extend(1.0))
            .truncate()
            .normalize()
                * -1.0;
            source.original.mutate_and_switch(|s| s.set_dir(dir));
        }
    }

    pub fn render(&self, listener: Listener) -> Box<dyn rodio::Source<Item = f32>> {
        let mut mixed =
            Box::new(rodio::source::Zero::new(2, self.sample_rate)) as Box<dyn Source<Item = f32>>;

        for (i, source) in self.sources.iter() {
            //mixed = Box::new(mixed.mix(source.stream));
        }

        mixed
    }
}

impl std::iter::Iterator for Renderer {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(listener) = self.listener.as_ref() {
            let mut accum = 0.0;

            let channel_pos = listener.positions[self.current_channel as usize];

            for (_, source) in self.sources.iter_mut() {
                match &source.properties {
                    SourceProperties::Point(props) => {
                        let distance_squared = props.position.distance2(channel_pos).max(1.0);
                        accum += (source.original.next().unwrap_or(0.0) / distance_squared
                            + 0.03 * source.reverb.next().unwrap_or(0.0));
                    }
                    _ => (),
                }
            }

            self.current_channel += 1;
            self.current_channel %= self.channels;

            for (_, non_spacial) in self.non_spacial.iter_mut() {
                accum += non_spacial.next().unwrap_or(0.0);
            }

            Some(accum)
        } else {
            None
        }
    }
}

impl Source for Renderer {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn sample_rate(&self) -> u32 {
        self.sample_rate
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        None
    }
}

use std::sync::Mutex;

#[derive(Clone)]
pub struct MutexedRenderer(pub Arc<Mutex<Renderer>>);

impl Iterator for MutexedRenderer {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let mut guard = self.0.lock().unwrap();
        guard.next()
    }
}

impl Source for MutexedRenderer {
    fn current_frame_len(&self) -> Option<usize> {
        let guard = self.0.lock().unwrap();
        guard.current_frame_len()
    }

    fn channels(&self) -> u16 {
        let guard = self.0.lock().unwrap();
        guard.channels()
    }

    fn sample_rate(&self) -> u32 {
        let guard = self.0.lock().unwrap();
        guard.sample_rate()
    }

    fn total_duration(&self) -> Option<core::time::Duration> {
        let guard = self.0.lock().unwrap();
        guard.total_duration()
    }
}

pub struct AudioContext {
    output_stream: rodio::OutputStreamHandle,
    stream: rodio::OutputStream,
    pub renderer: MutexedRenderer,
}

impl AudioContext {
    pub fn new() -> Self {
        let (stream, output_stream) = rodio::OutputStream::try_default().unwrap();
        let mut renderer = Renderer::new(44100, 2);
        renderer.set_listener(Listener {
            positions: [Vector3::zero(); MAX_CHANNELS],
            view_mat: Matrix4::identity(),
        });
        let renderer = MutexedRenderer(Arc::new(Mutex::new(renderer)));
        output_stream.play_raw(renderer.clone()).unwrap();
        Self {
            output_stream,
            stream,
            renderer,
        }
    }
}

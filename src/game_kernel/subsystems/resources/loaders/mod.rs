pub mod particle_systems_loader;

use gltf;
use obj;
use physfs_rs;

use crate::log_err;

use super::{CreationArg, LoadError, ResourceLoadData, ResourceManager};

use super::super::video::renderer::{
    light::Light, light::*, material::Material, mesh::*, model::Model,
    particle_fx::ParticleSystemBuilder, texture::*,
};

use super::super::video::common::vulkan::VulkanBasic;

use std::any::Any;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::sync::Arc;

use cgmath::Vector3;

use self::gltf::Document;

fn mesh_loader<V: VulkanBasic, R: Read>(
    res_type: &str,
    reader: BufReader<R>,
    vulkan_common: V,
) -> Result<Mesh, LoadError> {
    //let reader = BufReader::new(File::open(res_data.filename).map_err(|_| LoadError::LoaderError)?);
    match res_type {
        "obj" => {
            let obj: obj::Obj<obj::TexturedVertex> = obj::load_obj(reader).map_err(|e| {
                println!("obj error {:?}", e);
                LoadError::LoaderError("Obj loader error")
            })?;
            let mesh_data: MeshData<u32> =
                MeshData::try_from(obj).map_err(|_| LoadError::LoaderError("Invalid obj data"))?;
            Ok(Mesh::StaticMesh(StaticMesh::from_mesh_data(
                vulkan_common.get_graphics_queue(),
                &mesh_data,
            )))
        }

        _ => Err(LoadError::LoaderError("Unsupported")),
    }
}

fn texture_loader<R: Read, V: VulkanBasic>(
    res_type: &str,
    reader: R,
    vulkan_common: V,
    usage: TextureUsage,
) -> Result<Texture, LoadError> {
    match res_type {
        "ktx" => Texture::from_texture_data(
            &TextureData::load_from_kxt(reader)
                .map_err(|_| LoadError::LoaderError("Unable to load ktx"))?,
            usage,
            vulkan_common,
            Some(if usage == TextureUsage::Color {
                SRGB
            } else {
                RGB8LINEAR
            }),
        )
        .map_err(|_| LoadError::LoaderError("Unable to upload texture data")),

        _ => Err(LoadError::LoaderError("Unsupported")),
    }
}

fn material_loader<V: VulkanBasic>(
    vulkan_common: V,
    texture: Arc<Texture>,
    normal_map: Arc<Texture>,
    roughness_map: Arc<Texture>,
    metalness_map: Arc<Texture>,
    emissive_map: Arc<Texture>,
) -> Material {
    unsafe {
        Material::test_new(
            vulkan_common.get_device(),
            texture,
            normal_map,
            roughness_map,
            metalness_map,
            emissive_map,
        )
    }
}

fn get_texture<V: 'static + VulkanBasic>(
    arg: &CreationArg,
    manager: &mut ResourceManager,
    vk: V,
    fs: &physfs_rs::PhysFs,
) -> Result<Arc<Texture>, LoadError> {
    Ok(match arg {
        CreationArg::Float3(value) => Arc::new(
            Texture::from_texture_data(
                &TextureData::from_uniform_color(Vector3::new(value[0], value[1], value[2])),
                TextureUsage::Color,
                vk.clone(),
                None,
            )
            .unwrap(),
        ),
        CreationArg::ResourceById(res_id) => {
            let roughness_map = manager.get_loaded_resource(*res_id, fs).map_err(|_| {
                println!("MaterialError");
                LoadError::LoaderError("Error while looking for roughness map")
            })?;
            roughness_map
                .clone()
                .downcast()
                .map_err(|_| LoadError::LoaderError("Not a texture"))?
        }
        _ => panic!(""),
    })
}

fn texture_from_args<V: VulkanBasic>(
    vk: V,
    creation_arg: Option<&super::CreationArg>,
    manager: &mut ResourceManager,
    usage: TextureUsage,
    fs: &physfs_rs::PhysFs,
) -> Result<Arc<Texture>, super::LoadError> {
    match creation_arg {
        Some(CreationArg::Float3(roughness_value)) => Ok(Arc::new(
            Texture::from_texture_data(
                &TextureData::from_uniform_color(Vector3::new(
                    roughness_value[0],
                    roughness_value[1],
                    roughness_value[2],
                )),
                usage,
                vk,
                None,
            )
            .unwrap(),
        )),
        Some(CreationArg::ResourceById(roughness_res_id)) => {
            let roughness_map =
                manager
                    .get_loaded_resource(*roughness_res_id, fs)
                    .map_err(|_| {
                        println!("MaterialError");
                        LoadError::LoaderError("Error while looking for roughness map")
                    })?;
            let t: Arc<Texture> = roughness_map
                .clone()
                .downcast()
                .map_err(|_| LoadError::LoaderError("Not a texture"))?;
            if t.texture_usage != usage {
                log_err!("Expected {:?}, found {:?}", usage, t.texture_usage);
                Err(LoadError::LoaderError("Wrong usage"))
            } else {
                Ok(t)
            }
        }
        _ => Err(super::LoadError::LoaderError("missing texture arg")),
    }
}

struct GltfData {
    document: Document,
    buffers: Vec<gltf::buffer::Data>,
    images: Vec<gltf::image::Data>,
}

pub fn register_loaders<V: 'static + VulkanBasic>(manager: &mut ResourceManager, vulkan_common: V) {
    let vk = vulkan_common.clone();
    manager.add_loader(
        "obj".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap();
            let reader = BufReader::new(
                File::open(load_data.filename.clone()) //TODO use vfs
                    .map_err(|_| {
                        log_err!("unable to open {}", load_data.filename.clone());
                        LoadError::LoaderError("Unable to open file")
                    })?,
            );

            mesh_loader(&load_data.resource_type, reader, vk.clone())
                .map(|m| Arc::new(m) as Arc<dyn Any + Send + Sync>)
        }),
    );

    manager.add_loader(
        "gltf".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap();
            let (document, buffers, images) = {
                let mut reader = fs.open_read(load_data.filename.clone()).map_err(|_| {
                    println!("unable to open {}", load_data.filename);
                    LoadError::LoaderError("Unable to open file")
                })?;
                let bytes = reader.read_to_vec().map_err(|_| {
                    println!("unable to open {}", load_data.filename);
                    LoadError::LoaderError("Unable to open file")
                })?;
                gltf::import_slice(bytes).map_err(|_| {
                    println!("Invalid glb file {}", load_data.filename);
                    LoadError::LoaderError("Unable to parse glb")
                })?
            };

            Ok(Arc::new(GltfData {
                document,
                buffers,
                images,
            }) as Arc<dyn Any + Send + Sync>)
        }),
    );

    let vk = vulkan_common.clone();
    manager.add_loader(
        "gltf_mesh".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();
            if let Some(CreationArg::ResourceById(gltf_id)) = load_data.creation_args.get("gltf") {
                let gltf_id = *gltf_id;
                let gltf_data = manager
                    .get_loaded_resource(gltf_id, fs)
                    .map_err(|_| LoadError::ResourceIndexOutOfRange(gltf_id))?
                    .downcast::<GltfData>()
                    .map_err(|_| LoadError::WrongResourceType)?;

                let mut nodes = gltf_data
                    .document
                    .default_scene()
                    .ok_or_else(|| {
                        log_err!("No default scene");
                        LoadError::LoaderError("Malformed gltf")
                    })?
                    .nodes();

                let node = if let Some(CreationArg::Int(node_idx)) =
                    load_data.creation_args.get("node_idx")
                {
                    nodes.nth(*node_idx as usize).ok_or_else(|| {
                        log_err!("Invalid node");
                        LoadError::LoaderError("Invalid node idx")
                    })?
                } else if let Some(CreationArg::Str(node_name)) =
                    load_data.creation_args.get("node_name")
                {
                    let mut res = None;
                    let mut nq = std::collections::VecDeque::new();
                    nq.append(&mut nodes.collect());
                    while let Some(node) = nq.pop_front() {
                        if node.name().is_some() && node.name().unwrap() == node_name {
                            res = Some(node);
                            break;
                        }
                        nq.extend(node.children());
                    }
                    res.ok_or_else(|| {
                        log_err!("Node {} not found", node_name);
                        LoadError::LoaderError("Gltf node not found")
                    })?
                } else {
                    return Err(LoadError::NoSuchKey("node_idx".to_owned()));
                };

                let mesh_data: MeshData<u32> = MeshData::try_from((
                    node.mesh()
                        .ok_or_else(|| {
                            log_err!("Node is not a mesh");
                            LoadError::LoaderError("Node not a mesh")
                        })?
                        .primitives()
                        .next()
                        .ok_or_else(|| {
                            log_err!("Node is not a mesh");
                            LoadError::LoaderError("Node not a mesh")
                        })?,
                    &gltf_data.buffers[0],
                ))
                .map_err(|_| LoadError::LoaderError(""))?;

                Ok(Arc::new(Mesh::StaticMesh(StaticMesh::from_mesh_data(
                    vk.get_graphics_queue(),
                    &mesh_data,
                ))) as Arc<dyn Any + Send + Sync>)
            } else {
                Err(LoadError::NoSuchKey("gltf".to_owned()))
            }
        }),
    );

    let vk = vulkan_common.clone();
    manager.add_loader(
        "ktx".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap();
            let reader = fs.open_read(load_data.filename.clone()).map_err(|_| {
                log_err!("unable to open {}", load_data.filename);
                LoadError::LoaderError("Ktx file not found")
            })?;

            let usage = match load_data.creation_args.get("usage") {
                Some(CreationArg::Str(name)) if name == "normal" => TextureUsage::Normal,
                Some(CreationArg::Str(name)) if name == "roughness" => TextureUsage::Roughness,
                Some(CreationArg::Str(name)) if name == "metalness" => TextureUsage::Metalness,
                Some(CreationArg::Str(name)) if name == "metalness_roughness" => {
                    TextureUsage::MetalnessRoughness
                }
                _ => TextureUsage::Color,
            };

            texture_loader(&load_data.resource_type, reader, vk.clone(), usage)
                .map(|m| Arc::new(m) as Arc<dyn Any + Send + Sync>)
        }),
    );

    let vk = vulkan_common.clone();
    manager.add_loader(
        "material".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();
            if let (Some(texture_res_id), normal_res_id) = (
                load_data.creation_args.get("albedo_texture"),
                load_data.creation_args.get("normal_map"),
            ) {
                let texture = get_texture(texture_res_id, manager, vk.clone(), fs)?;
                let normal_map =
                    if let Some(CreationArg::ResourceById(normal_res_id)) = normal_res_id {
                        let normal_map =
                            manager
                                .get_loaded_resource(*normal_res_id, fs)
                                .map_err(|_| {
                                    println!("MaterialError");
                                    LoadError::LoaderError("Error while looking for normal map")
                                })?;
                        normal_map
                            .clone()
                            .downcast()
                            .map_err(|_| LoadError::LoaderError("Not a texture"))?
                    } else {
                        Arc::new(
                            Texture::from_texture_data(
                                &TextureData::from_uniform_color(Vector3::new(0.5, 0.5, 1.0)),
                                TextureUsage::Normal,
                                vk.clone(),
                                None,
                            )
                            .unwrap(),
                        )
                    };

                let metalness_roughness = texture_from_args(
                    vk.clone(),
                    load_data.creation_args.get("metalness_roughness_map"),
                    manager,
                    TextureUsage::MetalnessRoughness,
                    fs,
                );
                if let Ok(metalness_roughness) = metalness_roughness {
                    Ok(Arc::new(unsafe {
                        Material::test_new_combined(
                            vk.get_device(),
                            texture,
                            normal_map,
                            metalness_roughness,
                            Arc::new(
                                Texture::from_texture_data(
                                    &TextureData::from_uniform_color(Vector3::new(0.0, 0.0, 0.0)),
                                    TextureUsage::Emission,
                                    vk.clone(),
                                    None,
                                )
                                .unwrap(),
                            ),
                        )
                    }) as Arc<dyn Any + Send + Sync>)
                } else {
                    Ok(Arc::new(material_loader(
                        vk.clone(),
                        texture,
                        normal_map,
                        texture_from_args(
                            vk.clone(),
                            load_data.creation_args.get("roughness_map"),
                            manager,
                            TextureUsage::Roughness,
                            fs,
                        )?,
                        texture_from_args(
                            vk.clone(),
                            load_data.creation_args.get("metalness_map"),
                            manager,
                            TextureUsage::Metalness,
                            fs,
                        )?,
                        Arc::new(
                            Texture::from_texture_data(
                                &TextureData::from_uniform_color(Vector3::new(0.0, 0.0, 0.0)),
                                TextureUsage::Emission,
                                vk.clone(),
                                None,
                            )
                            .unwrap(),
                        ),
                    )) as Arc<dyn Any + Send + Sync>)
                }
            } else {
                Err(LoadError::LoaderError("Missing creation arg for material"))
            }
        }),
    );

    manager.add_loader(
        "point_light".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();
            if let (
                Some(CreationArg::Float3(color_res)),
                Some(CreationArg::Float3(position_res)),
                Some(CreationArg::Float(size)),
                Some(CreationArg::Float(influence_radius)),
            ) = (
                load_data.creation_args.get("color"),
                load_data.creation_args.get("position"),
                load_data.creation_args.get("size"),
                load_data.creation_args.get("influence_radius"),
            ) {
                let color = Vector3::from(*color_res);
                let position = Vector3::from(*position_res);
                let size = *size;
                let influence_radius = *influence_radius;

                Ok(Arc::new(Light {
                    light: LightType::Point(PointLight {
                        color,
                        position,
                        size,
                        influence_radius,
                    }),
                    shadow_map_size: None,
                }) as Arc<dyn Any + Send + Sync>)
            } else {
                Err(LoadError::LoaderError("Missing creation arg for light"))
            }
        }),
    );

    manager.add_loader(
        "spot_light".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();
            if let (
                Some(CreationArg::Float3(color_res)),
                Some(CreationArg::Float(size_res)),
                Some(CreationArg::Float(cone_length_res)),
                Some(CreationArg::Float(cone_angle_res)),
                shadow_map_size,
            ) = (
                load_data.creation_args.get("color"),
                load_data.creation_args.get("size"),
                load_data.creation_args.get("cone_length"),
                load_data.creation_args.get("cone_angle"),
                load_data.creation_args.get("shadow_map_size"),
            ) {
                let color = Vector3::from(*color_res);
                let position = Vector3::zero();
                let direction = Vector3::zero();

                let size = *size_res;
                let cone_length = *cone_length_res;
                let cone_angle = *cone_angle_res;

                let shadow_map_size = if let Some(s) = shadow_map_size {
                    if let CreationArg::Uint(s) = s {
                        Some(*s as u16)
                    } else {
                        return Err(LoadError::LoaderError("wrong type for shadow_map_size"));
                    }
                } else {
                    None
                };

                Ok(Arc::new(Light {
                    light: LightType::Spot(SpotLight {
                        color,
                        position,
                        direction,
                        size,
                        cone_length,
                        cone_angle,
                    }),
                    shadow_map_size,
                }) as Arc<dyn Any + Send + Sync>)
            } else {
                Err(LoadError::LoaderError(
                    "Missing creation arg for spot light",
                ))
            }
        }),
    );

    use crate::renderer::particle_fx;
    use cgmath::*;
    manager.add_loader(
        "emitter".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();
            let emitter =
                particle_systems_loader::load_emitter(manager, &load_data.creation_args, fs)?;

            Ok(Arc::new(emitter) as Arc<dyn Any + Send + Sync>)
        }),
    );

    manager.add_loader(
        "simple_particle_physics".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();

            if let (Some(CreationArg::Float3(acceleration)),) =
                (load_data.creation_args.get("acceleration"),)
            {
                let accel = cgmath::Vector3::from(*acceleration);
                let builder: Box<particle_fx::PhysicsConstructor> = Box::new(move |vk, n| {
                    Box::new(particle_fx::SimpleParticlePhysics::new(vk, n, accel))
                });

                Ok(Arc::new(builder) as Arc<dyn Any + Send + Sync>)
            } else {
                Err(LoadError::LoaderError("missing particle physics argument"))
            }
        }),
    );

    manager.add_loader(
        "alpha_particle_renderer".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();

            if let (Some(CreationArg::ResourceById(atlas_id)),) =
                (load_data.creation_args.get("atlas"),)
            {
                let atlas = manager.get_loaded_resource(*atlas_id, fs).unwrap();
                let atlas: Arc<Texture> = atlas.clone().downcast().unwrap();
                let atlas: Texture = atlas.as_ref().clone();

                let builder: Arc<Box<particle_fx::RendererConstructor>> =
                    Arc::new(Box::new(move |vk, output, depth, bufs| {
                        let psr: Box<particle_fx::AlphaParticleRenderer> =
                            Box::new(particle_fx::ParticlesRendererCreation::new(
                                vk,
                                output,
                                depth,
                                bufs,
                                particle_fx::AlphaParticleRendererParams {
                                    life: 1.1,
                                    size: Vector2::new(0.5, 0.5),
                                    texture: particle_fx::ParticleTexture {
                                        atlas: atlas.clone(),
                                        offset: Vector2::new(0.0, 0.0),
                                        size: Vector2::new(1.0 / 16.0, 1.0 / 5.0),
                                        fps: Vector2::new(30.0, 30.0 / 16.0),
                                        frames: Vector2::new(16, 5),
                                    },
                                    blending: particle_fx::ParticleBlending::Additive,
                                },
                            ));
                        psr
                    }));

                Ok(builder as Arc<dyn Any + Send + Sync>)
            } else {
                Err(LoadError::LoaderError("missing particle renderer argument"))
            }
        }),
    );

    manager.add_loader(
        "particle_fx".to_string(),
        Arc::new(move |manager, index, fs| {
            let load_data = manager.get_load_data(index).unwrap().clone();

            if let (
                Some(CreationArg::ResourceById(emitter_id)),
                Some(CreationArg::ResourceById(renderer_id)),
                Some(CreationArg::ResourceById(particle_physics_id)),
            ) = (
                load_data.creation_args.get("emitter"),
                load_data.creation_args.get("renderer"),
                load_data.creation_args.get("physics"),
            ) {
                let emitter = manager.get_loaded_resource(*emitter_id, fs).map_err(|_| {
                    println!("MaterialError");
                    LoadError::LoaderError("Error while looking for normal map")
                })?;
                let emitter: Arc<Box<dyn particle_fx::DynCloneEmitter + Send + Sync>> = emitter
                    .clone()
                    .downcast()
                    .map_err(|_| LoadError::LoaderError("Not an emitter"))?;

                let renderer = manager.get_loaded_resource(*renderer_id, fs).unwrap();
                let renderer: Arc<Box<particle_fx::RendererConstructor>> = renderer
                    .clone()
                    .downcast()
                    .map_err(|_| LoadError::LoaderError("Not a particle renderer builder"))?;

                let particle_physics = manager
                    .get_loaded_resource(*particle_physics_id, fs)
                    .unwrap();
                let particle_physics: Arc<Box<particle_fx::PhysicsConstructor>> = particle_physics
                    .clone()
                    .downcast()
                    .map_err(|_| LoadError::LoaderError("Not a particle physics builder"))?;

                /*Ok(Arc::new(particle_fx::GenericParticleSystemBuilder::<_, _, particle_fx::AlphaParticleRenderer>::new(
                    emitter.dyn_clone(),
                    particle_fx::AlphaParticleRendererParams {
                        life: 1.1,
                        size: Vector2::new(0.5, 0.5),
                        texture: particle_fx::ParticleTexture {
                            atlas,
                            offset: Vector2::new(0.0, 0.0),
                            size: Vector2::new(1.0/16.0, 1.0/5.0),
                            fps: Vector2::new(30.0, 30.0/16.0),
                            frames: Vector2::new(16, 5),
                        },
                        blending: particle_fx::ParticleBlending::Additive,
                    },
                    |vk, max| {
                        Box::new(particle_fx::SimpleParticlePhysics::new(
                            vk,
                            max,
                            Vector3::new(0.0, 1.0, 0.0),
                        ))
                    },
                    50,
                )
                    .wrapped()))*/
                Ok(Arc::new(
                    particle_fx::DynamicParticleSystemBuilder::new(
                        emitter.wrapped(),
                        renderer,
                        particle_physics,
                        50,
                    )
                    .wrapped(),
                ))
            } else {
                Err(LoadError::LoaderError("missing emitter argument"))
            }
        }),
    );

    manager.add_loader(
        "audio".to_string(),
        Arc::new(move |manager, index, fs| {
            use crate::game_kernel::subsystems::audio::*;
            let load_data = manager.get_load_data(index).unwrap().clone();

            let reader = fs.open_read(load_data.filename.clone()).map_err(|_| {
                log_err!("unable to open {}", load_data.filename);
                LoadError::LoaderError("Audio file not found")
            })?;

            let should_loop = if let CreationArg::Bool(l) = load_data
                .creation_args
                .get("loop")
                .unwrap_or(&CreationArg::Bool(false))
            {
                l
            } else {
                return Err(LoadError::LoaderError("loop must be a boolean"));
            };

            if let Some(builder) = StoredSamplesBuilder::from_read(reader, *should_loop) {
                Ok(Arc::new(SpatialSourceBuilderWrapper::new(builder)))
            } else {
                Err(LoadError::LoaderError("Failed loading audio file"))
            }
        }),
    );
}

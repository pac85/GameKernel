pub mod loaders;

use crate::game_kernel::ecs::World;
use crate::log_err;
use physfs_rs;
use serde::Deserialize;

use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::io::Read;
use std::ops::DerefMut;
use std::sync::{Arc, Mutex};

#[derive(Deserialize, Clone)]
pub struct ResourceLoadData {
    pub filename: String,
    pub resource_type: String,
    pub creation_args: HashMap<String, CreationArg>,
}

pub struct ResourceManager {
    resources: Vec<ResourceLoadData>,
    loaders: HashMap<
        String,
        Arc<
            dyn Fn(
                &mut Self,
                usize,
                &physfs_rs::PhysFs,
            ) -> Result<Arc<dyn Any + 'static + Send + Sync>, LoadError>,
        >,
    >,
    loaded_resources: Vec<Option<Arc<dyn Any + 'static + Send + Sync>>>,
}

#[derive(Debug)]
pub enum LoadError {
    NoSuchFileOrDirectory,
    NoLoaderForFormat(String),
    WrongResourceType,
    ResourceIndexOutOfRange(usize),
    DeserializationError(::serde_json::Error),
    NoSuchKey(String),
    LoaderError(&'static str),
}

impl ResourceManager {
    pub fn new() -> Self {
        Self {
            resources: Vec::new(),
            loaders: HashMap::new(),
            loaded_resources: Vec::new(),
        }
    }

    fn get_load_data(&self, index: usize) -> Result<&ResourceLoadData, LoadError> {
        self.resources
            .get(index)
            .ok_or(LoadError::ResourceIndexOutOfRange(index))
    }

    fn try_load(&mut self, index: usize, fs: &physfs_rs::PhysFs) -> Result<(), LoadError> {
        let load_data = &self
            .resources
            .get(index)
            .ok_or(LoadError::ResourceIndexOutOfRange(index))?;

        let loader = self
            .loaders
            .get(&load_data.resource_type)
            .ok_or(LoadError::NoLoaderForFormat(
                load_data.resource_type.clone(),
            ))?
            .clone();

        let loaded = loader(self, index, fs)?;

        if let Some(s) = self.loaded_resources.get_mut(index) {
            *s = Some(loaded);
        } else {
            while self.loaded_resources.len() < index {
                self.loaded_resources.push(None);
            }
            self.loaded_resources.push(Some(loaded));
        }

        Ok(())
    }

    pub fn get_loaded_resource(
        &mut self,
        index: usize,
        fs: &physfs_rs::PhysFs,
    ) -> Result<Arc<dyn Any + 'static + Send + Sync>, LoadError> {
        Ok({
            if self.loaded_resources.get(index).is_none()
                || self.loaded_resources.get(index).unwrap().is_none()
            {
                self.try_load(index, fs)?;
            }

            self.loaded_resources[index].clone().unwrap()
        })
    }

    pub fn add_loader(
        &mut self,
        resource_type: String,
        loader: Arc<
            dyn Fn(
                &mut Self,
                usize,
                &physfs_rs::PhysFs,
            ) -> Result<Arc<dyn Any + 'static + Send + Sync>, LoadError>,
        >,
    ) {
        self.loaders.insert(resource_type, loader);
    }

    fn make_args_map(
        &mut self,
        args_data: &HashMap<String, CreationArg>,
        fs: &physfs_rs::PhysFs,
    ) -> Result<HashMap<String, Arc<dyn Any + 'static + Send + Sync>>, LoadError> {
        let mut loaded_args = HashMap::new();
        for (key, cca) in args_data {
            let arg = if let Some(imm) = cca.immediate_as_any() {
                imm
            } else {
                self.get_loaded_resource(cca.unwrap_index(), fs)?
            };

            loaded_args.insert(key.to_owned(), arg);
        }
        Ok(loaded_args)
    }

    pub fn load_world<T: Read, W: DerefMut<Target = World>>(
        &mut self,
        mut world: W,
        wfile: T,
        fs: &physfs_rs::PhysFs,
    ) -> Result<(), LoadError> {
        let map_load_data: MapLoadData =
            ::serde_json::from_reader(wfile).map_err(|e| LoadError::DeserializationError(e))?;

        self.resources = map_load_data.resources;
        self.loaded_resources.resize(0, None);
        world.reset();

        for entity_load_data in map_load_data.entities.iter() {
            let entity_id = world.add_entity(World::get_root()).unwrap();
            for component_data in entity_load_data.components.iter() {
                let component = crate::game_kernel::ecs::COMPONENT_FACTORY
                    .lock()
                    .unwrap()
                    .instantiate(
                        component_data.name.as_ref().unwrap(),
                        self.make_args_map(&component_data.creation_args, fs)
                            .unwrap_or_else(|e| {
                                log_err!(
                                    "Unable to create component {} because of {:?}",
                                    component_data.name.as_ref().unwrap(),
                                    e
                                );
                                panic!("");
                            }),
                    );
                world.add_component(entity_id, component.unwrap());
            }
        }

        Ok(())
    }
}

use cgmath::{Vector2, Vector3, Vector4};

#[derive(Deserialize, Clone, Debug)]
pub enum CreationArg {
    Float(f32),
    Float2([f32; 2]),
    Float3([f32; 3]),
    Float4([f32; 4]),
    Int(i32),
    Uint(u32),
    Str(String),
    Bool(bool),
    ResourceById(usize),
}

impl CreationArg {
    pub fn immediate_as_any(&self) -> Option<Arc<dyn Any + 'static + Send + Sync>> {
        match (*self).clone() {
            CreationArg::Float(f) => Some(Arc::new(f) as Arc<dyn Any + 'static + Send + Sync>),
            CreationArg::Float2(f) => {
                Some(Arc::new(Vector2::from(f)) as Arc<dyn Any + 'static + Send + Sync>)
            }
            CreationArg::Float3(f) => {
                Some(Arc::new(Vector3::from(f)) as Arc<dyn Any + 'static + Send + Sync>)
            }
            CreationArg::Float4(f) => {
                Some(Arc::new(Vector4::from(f)) as Arc<dyn Any + 'static + Send + Sync>)
            }
            CreationArg::Int(f) => Some(Arc::new(f) as Arc<dyn Any + 'static + Send + Sync>),
            CreationArg::Uint(f) => Some(Arc::new(f) as Arc<dyn Any + 'static + Send + Sync>),
            CreationArg::Str(f) => {
                Some(Arc::new(f.to_string()) as Arc<dyn Any + 'static + Send + Sync>)
            }
            CreationArg::Bool(f) => Some(Arc::new(f) as Arc<dyn Any + 'static + Send + Sync>),
            CreationArg::ResourceById(_) => None,
        }
    }

    pub fn unwrap_index(&self) -> usize {
        if let CreationArg::ResourceById(index) = *self {
            index
        } else {
            panic!("unwrapped immediate arg")
        }
    }
}

#[derive(Deserialize, Debug)]
struct ComponentLoadData {
    name: Option<String>,
    creation_args: HashMap<String, CreationArg>,
}

#[derive(Deserialize, Debug)]
struct EntityLoadData {
    pub components: Vec<ComponentLoadData>,
}

#[derive(Deserialize)]
struct MapLoadData {
    resources: Vec<ResourceLoadData>,
    entities: Vec<EntityLoadData>,
}

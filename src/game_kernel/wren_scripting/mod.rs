use ruwren;
use ruwren::*;

pub mod component;
pub mod wren_math;

use std::ffi;
use std::os::raw;
use wren_sys::{WrenForeignClassMethods, WrenVM};

pub extern "C" fn wren_bind_foreign_method(
    vm: *mut WrenVM,
    mdl: *const raw::c_char,
    clss: *const raw::c_char,
    is_static: bool,
    sgn: *const raw::c_char,
) -> Option<unsafe extern "C" fn(*mut WrenVM)> {
    let module = unsafe { ffi::CStr::from_ptr(mdl) };
    let class = unsafe { ffi::CStr::from_ptr(clss) };
    let signature = unsafe { ffi::CStr::from_ptr(sgn) };

    //println!("wren_bind_foreign_method {module:?} {class:?} {signature:?}");
    None
}

pub extern "C" fn wren_bind_foreign_class(
    vm: *mut WrenVM,
    mdl: *const raw::c_char,
    clss: *const raw::c_char,
) -> WrenForeignClassMethods {
    //println!("wren_bind_foreign_class");
    WrenForeignClassMethods {
        allocate: None,
        finalize: None,
    }
}

pub trait ScriptingEngine {}

pub struct WrenScriptingEngine {
    pub vm: VMWrapper,
}

impl ScriptingEngine for WrenScriptingEngine {}

impl WrenScriptingEngine {
    pub fn new() -> Self {
        let mut lib = ModuleLibrary::new();
        ecs::publish_module(&mut lib);

        wren_math::publish(&mut lib);

        let custom_bindings = CustomBindingFns {
            bind_foreign_method_fn: Some(wren_bind_foreign_method),
            bind_foreign_class_fn: Some(wren_bind_foreign_class),
        };
        let vm = VMConfig::new()
            .library(&lib)
            .custom_bindings(custom_bindings)
            .build();

        vm.interpret(
            "ecs",
            r##"
            foreign class World {
                construct new() {}
                foreign getEntities()
                foreign getEntityComponents(entity)
            }

            foreign class Component {
                construct new() {}
                foreign typeName()
                foreign getField(name)
                foreign setField(name, value)
                foreign toString()
            }
            "##,
        )
        .expect("failed to register wren class");

        vm.interpret("math", wren_math::MATH_WREN_SRC)
            .expect("failed to register wren class");

        Self { vm }
    }
}

impl Default for WrenScriptingEngine {
    fn default() -> Self {
        Self::new()
    }
}

//bindings

use std::cell::RefCell;
use std::collections::HashSet;
use std::rc::Rc;
use std::sync::Arc;

use crate::game_kernel::ecs::{Component, ComponentId, World};

#[derive(Hash)]
struct WrenComponentId {
    id: ComponentId,
}

impl WrenComponentId {
    pub fn get_id(&self) -> ComponentId {
        self.id
    }
}

impl Class for WrenComponentId {
    fn initialize(_vm: &VM) -> Self {
        panic!("can't be instantiate");
    }
}

struct WrenGenericComponent {
    component: Arc<RefCell<dyn Component>>,
}

impl WrenGenericComponent {
    pub fn new(component: Arc<RefCell<dyn Component>>) -> Self {
        Self { component }
    }
}

impl Class for WrenGenericComponent {
    fn initialize(vm: &VM) -> Self {
        panic!("no");
    }
}

pub struct WrenWorld {
    world: Rc<RefCell<World>>,
}

fn put_component_in_slot(vm: &VM, component: Arc<RefCell<dyn Component>>, slot: SlotId) {
    //TODO: downcast whenever possible
    vm.set_slot_new_foreign(
        "ecs",
        "GenericComponen",
        WrenGenericComponent::new(component),
        slot,
    );
}

impl WrenWorld {
    pub fn new(world: Rc<RefCell<World>>) -> Self {
        Self { world }
    }

    fn get_entities(&self, vm: &VM) {
        let entities = self.world.borrow();
        vm.ensure_slots(2); //one for the list and one temporary
        vm.set_slot_new_list(0);
        for (index, entity) in entities.iter_entities().enumerate() {
            vm.set_slot_double(1, entity as f64);
            vm.insert_in_list(0, index as i32, 1);
        }
    }

    fn get_entity_component(&self, vm: &VM) {
        let entity = vm.get_slot_double(1).unwrap() as u64;
        let component = vm.get_slot_foreign::<WrenComponentId>(2).unwrap();
        if self
            .world
            .borrow()
            .get_entity_component_dyn(component.id, entity)
            .is_none()
        {
            vm.set_slot_null(0);
        } else {
            let object = component::WrenComponent::new(entity, component.id, self.world.clone());
            vm.set_slot_new_foreign("ecs", "Component", object, 0)
                .unwrap();
        }
    }

    fn get_entity_components(&self, vm: &VM) {
        let entity = vm.get_slot_double(1).unwrap() as u64;
        vm.ensure_slots(3);
        vm.set_slot_new_list(0);
        for (idx, component) in self
            .world
            .borrow()
            .get_all_entity_components(entity)
            .enumerate()
        {
            let object = component::WrenComponent::new(
                entity,
                component.gk_as_any().type_id(),
                self.world.clone(),
            );
            vm.set_slot_new_foreign_scratch("ecs", "Component", object, 1, 2)
                .unwrap();
            vm.insert_in_list(0, idx as i32, 1);
        }
    }

    fn get_entity_components_from_ids(&self, vm: &VM) {
        /*let entity = vm.get_slot_double(1).unwrap() as u64;
        let component_count = vm.get_list_count(2);
        vm.ensure_slots(4);
        let mut component_ids = HashSet::new();

        for i in 0..component_count {
            vm.get_list_element(2, i as i32, 3);
            component_ids.insert(vm.get_slot_foreign::<WrenComponentId>(3).unwrap().get_id());
        }

        vm.set_slot_new_list(0);

        let world = self.world.borrow();
        let components = world.get_entity_components(entity).unwrap();
        for (index, component) in components.iter().enumerate() {
            if component_ids.contains(&component.borrow().gk_as_any().type_id()) {
                put_component_in_slot(vm, component.clone(), 3);
                vm.insert_in_list(0, index as i32, 3);
            }
        }*/
    }
}

impl Class for WrenWorld {
    fn initialize(_vm: &VM) -> Self {
        Self {
            world: Rc::new(RefCell::new(World::new())),
        }
    }
}

create_module! {
    class("World") crate::game_kernel::wren_scripting::WrenWorld => wren_world {
        instance(fn "getEntities", 0) get_entities,
        instance(fn "getEntityComponent", 2) get_entity_component,
        instance(fn "getEntityComponents", 1) get_entity_components,
        instance(fn "getCompoentsFromIds", 2) get_entity_components_from_ids
    }

    class("ComponentId") crate::game_kernel::wren_scripting::WrenComponentId => wren_component_id {

    }

    class("GenericComponent") crate::game_kernel::wren_scripting::WrenGenericComponent => wren_generic_component {

    }

    class("Component") crate::game_kernel::wren_scripting::component::WrenComponent => wren_component {
        instance(fn "typeName", 0) get_type_name,
        instance(fn "getField", 1) get_field,
        instance(fn "setField", 2) set_field,
        instance(fn "toString", 0) to_string
    }

    module => ecs
}

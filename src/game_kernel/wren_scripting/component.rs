use std::{
    cell::{Ref, RefCell},
    rc::Rc,
};

use cgmath::Vector3;
use game_kernel_ecs::ecs::{
    component::{Component, ComponentId, ComponentReflect, COMPONENT_FACTORY},
    EntitId, World,
};
use ruwren::{create_module, get_slot_checked, Class, ModuleLibrary, VMConfig, VM};

pub struct WrenComponent {
    //component: Ref<'_, dyn Component>,
    entity: EntitId,
    component: ComponentId,
    type_name: String,
    world: Rc<RefCell<World>>,
}

impl WrenComponent {
    pub fn new(entity: EntitId, component: ComponentId, world: Rc<RefCell<World>>) -> Self {
        let type_name = COMPONENT_FACTORY
            .lock()
            .unwrap()
            .get_name(component)
            .unwrap()
            .to_owned();
        Self {
            entity,
            component,
            type_name,
            world,
        }
    }

    pub fn get_type_name(&self, vm: &VM) {
        vm.set_slot_string(0, &self.type_name);
    }

    pub fn get_field(&self, vm: &VM) {
        let world = self.world.borrow();
        let component = world
            .get_entity_component_dyn(self.component, self.entity)
            .unwrap();

        let name = vm.get_slot_string(1).unwrap();
        let value = component.get_field_any(&name);
        match value {
            None => vm.set_slot_null(0),
            Some(v) if v.is::<Vector3<f32>>() => {
                let v3 = v.downcast_ref::<Vector3<f32>>().unwrap();
                let v3: [f32; 3] = (*v3).into();
                let v3 = v3.map(|v| v as f64);
                vm.set_slot_new_foreign(
                    "math",
                    "Vec3",
                    super::wren_math::WrenVec3::new(v3.into()),
                    0,
                )
                .unwrap();
            }
            Some(f) if f.is::<f32>() => {
                let f = f.downcast_ref::<f32>().unwrap();
                vm.set_slot_double(0, *f as f64);
            }
            Some(f) if f.is::<f64>() => {
                let f = f.downcast_ref::<f64>().unwrap();
                vm.set_slot_double(0, *f);
            }
            Some(s) if s.is::<String>() => {
                let s = s.downcast_ref::<String>().unwrap();
                vm.set_slot_string(0, s);
            }
            _ => todo!(),
        }
    }

    pub fn set_field(&self, vm: &VM) {
        let world = self.world.borrow();
        let mut component = world
            .get_entity_component_dyn_mut(self.component, self.entity)
            .unwrap();

        let name = vm.get_slot_string(1).unwrap();
        let value = component.get_field_any_mut(&name);
        match value {
            None => {}
            Some(v) if v.is::<Vector3<f32>>() => {
                let new_value = vm
                    .get_slot_foreign::<super::wren_math::WrenVec3>(2)
                    .expect("Expected vec3");
                let v3 = v.downcast_mut::<Vector3<f32>>().unwrap();
                *v3 = cgmath::vec3(
                    new_value.inner.x as f32,
                    new_value.inner.y as f32,
                    new_value.inner.z as f32,
                );
            }
            Some(f) if f.is::<f32>() => {
                let new_value = vm.get_slot_double(2).expect("Expected double");
                let f = f.downcast_mut::<f32>().unwrap();
                *f = new_value as f32;
            }
            Some(f) if f.is::<f64>() => {
                let new_value = vm.get_slot_double(2).expect("Expected double");
                let f = f.downcast_mut::<f64>().unwrap();
                *f = new_value;
            }
            Some(s) if s.is::<String>() => {
                let new_value = vm.get_slot_string(2).expect("Expected string");
                let s = s.downcast_mut::<String>().unwrap();
                *s = new_value;
            }
            _ => todo!(),
        }
    }

    pub fn to_string(&self, vm: &VM) {
        self.get_type_name(vm);
    }
}

impl Class for WrenComponent {
    fn initialize(_vm: &VM) -> Self {
        panic!("can't instantiate component");
    }
}

/*create_module! {
    class("Component") crate::game_kernel::wren_scripting::component::WrenComponent => wren_component {
        instance(fn "getField", 1) get_field
    }

    class("ComponentId") crate::game_kernel::wren_scripting::component::WrenComponentId => wren_component_id {
    }

    module => ecs
}*/

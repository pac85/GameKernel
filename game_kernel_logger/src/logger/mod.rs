use colored::*;

pub enum Message<'a> {
    Message(&'a str),
    Warning(&'a str),
    Error(&'a str),
}

pub fn log(msg: Message) {
    match msg {
        Message::Message(s) => {
            println!("[{}] {}", "+".blue(), s);
        }
        Message::Warning(s) => {
            println!("[{}] {}", "-".yellow(), s);
        }
        Message::Error(s) => {
            println!("[{}] {}", "!".red(), s);
        }
    }
}

#[macro_export]
macro_rules! log_msg{
    () => {};
    ($($arg:tt)*) => {
        $crate::logger::log($crate::logger::Message::Message(&format!($($arg)*)));
    };
}

#[macro_export]
macro_rules! log_warn{
    () => {};
    ($($arg:tt)*) => {
        $crate::logger::log($crate::logger::Message::Warning(&format!($($arg)*)));
    };
}

#[macro_export]
macro_rules! log_err{
    () => {};
    ($($arg:tt)*) => {
        $crate::logger::log($crate::logger::Message::Error(&format!($($arg)*)));
    };
}


use game_kernel_ecs;

use game_kernel_ecs::ecs::component::Component;

use proc_macro;

use crate::proc_macro::TokenStream;
use quote::{__private::ext::RepToTokensExt, quote, format_ident};
use syn::{self, Ident, NestedMeta, PathArguments, Type, punctuated::Punctuated, GenericArgument, token::Comma};

#[proc_macro_derive(Component, attributes(arg_default, arg_conv))]
pub fn hello_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_component_macro(&ast)
}

fn impl_component_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl Component for #name {
            fn dyn_clone(&self) -> Box<dyn Component> {
                Box::new(self.clone())
            }
        }
    };
    //panic!("{}", gen);
    gen.into()
}

#version 450

layout(location = 0) out vec4 final;
layout(set = 0, binding = 0) uniform sampler2D hdr;

#include<tone_mapping.h>

void main() {

    ivec2 p_coords = ivec2(gl_FragCoord.xy);
    vec4 ldr = vec4((u2_filmic(texelFetch(hdr, p_coords, 0).xyz)), 1.0);
    final = ldr;
}

#version 450

layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

layout(set = 0, binding = 0) uniform sampler2D hdr;
layout(set = 0, binding = 1) uniform writeonly image2D ldr_output;

#include<tone_mapping.h>

void main()
{
    ivec2 res = textureSize(hdr, 0);
    if(gl_GlobalInvocationID.x > res.x || gl_GlobalInvocationID.y > res.y)
        return;

    ivec2 p_coords = ivec2(gl_GlobalInvocationID.xy);
    vec4 ldr = vec4(gamma_correction(u2_filmic(texelFetch(hdr, p_coords, 0).xyz)), 1.0);
    //vec4 ui = texelFetch(ui, p_coords, 0).xyzw;
    imageStore(ldr_output, p_coords, /*mix(ui, ldr, ui.w)*/ldr);
}

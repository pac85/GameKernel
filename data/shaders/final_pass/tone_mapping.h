vec3 reinhard(vec3 original)
{
    return original / (original+vec3(1.0));
}

vec3 aces_filmic(vec3 original)
{
    float a = 2.51f;
    float b = 0.03f;
    float c = 2.43f;
    float d = 0.59f;
    float e = 0.14f;
    return clamp((original*(a*original+b))/(original*(c*original+d)+e), 0.0, 1.0);
}

vec3 u2_filmic(vec3 x)
{
    float A = 0.22f,
          B = 0.30f,
          C = 0.10f,
          D = 0.20f,
          E = 0.01f,
          F = 0.30;

    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F)) - E/F;
}

vec3 gamma_correction(vec3 original)
{
    return pow(original, vec3(1.0 / 2.2));
}


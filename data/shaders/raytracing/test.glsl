#version 450

#include<rt_common.h>

layout(location = 0) out vec4 final;

layout(set = 0, binding = 0) uniform sampler2D norm_rough;
layout(set = 0, binding = 1) uniform sampler2D in_depth;
layout(set = 0, binding = 2) uniform sampler2D velocity_buffer;
layout(set = 0, binding = 3) uniform sampler2D rt_out;
//layout(set = 0, binding = 4, r11f_g11f_b10f) readonly uniform image2D previous_ssr_frame;
layout(push_constant) uniform ProjectionData
{
    mat4 mv;
    mat4 p;
    //vec3 view_pos;
} projection_data;


#include<common.h>
#include<pbr.h>

vec2 clip_to_uv(vec2 clip)
{
    return clip / 2 - 0.5;
}

float fresnel(vec3 normal, vec3 vdir, float metalness)
{
    float F0 = mix(0.04, 0.9, metalness);
    return fresnel_schlick(max(dot(normal, vdir), 0.0), F0);
}

void main(){
    ivec2 res = textureSize(rt_out, 0);

    ivec2 p_coords = ivec2(gl_FragCoord.xy);
    vec2 uv = vec2(p_coords) / vec2(res);//TODO use real size

    final.xyz = texture(rt_out, uv).xyz;
    return;
    float depth = texelFetch(in_depth, ivec2(gl_FragCoord.xy), 0).x;
    vec3 point = world_space_location(uv, depth);

    vec4 norm_rough_metal = texture(norm_rough, uv);
    vec3 normal = octahedron_decode(norm_rough_metal.xy);
    float roughness = norm_rough_metal.z; 
    float metalness = norm_rough_metal.w; 

    mat4 p = projection_data.p;
    p[0][3] = 0.0;
    p[1][3] = 0.0;
    p[2][3] = -1.0;
    
    //finds direction
    vec3 point_vs = view_space_location(uv, depth);
    vec3 dir_to = normalize(point_vs);
    vec3 normal_vs = mat3(projection_data.mv) * normal;

    mat3 tbn_matrix = tbn(normal_vs);
    vec3 tbn_vdir = (-dir_to) * tbn_matrix;
    vec2 u = vec2(0.0);//hash22(hash1(frame_info.frame_hash) + uv);
    vec3 tbn_normal = sample_ggx_vndf(tbn_vdir, vec2(roughness), u);

    vec3 tbn_reflected = reflect(-tbn_vdir, tbn_normal);

    vec3 refl_vs = tbn_reflected * transpose(tbn_matrix);
    refl_vs = /*refl_vs*/dir_to * mat3(projection_data.mv);
    
    Ray r;
    r.r = refl_vs;
    r.o = point;
    final.xyz = test_scene(r).xyz;

    /*vec2 suv = uv * 2.0 - vec2(1.0);
    suv.x = -suv;
    Ray r;
    r.r = normalize(vec3(suv.xy * vec2(16.0/9.0, 1.0), 1.0));
    r.o = vec3(0.0, 0.0, -1.0);

    r.r = r.r * mat3(vec3(iv[0]), vec3(iv[1]), vec3(iv[2]));
    r.o = -fov_eye.yzw;

    final.xyz = test_scene(r).xyz;*/
}

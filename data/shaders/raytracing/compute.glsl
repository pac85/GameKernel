#version 450

#define TILE_SIZE 8 
#define STACK_SIZE 64

layout(local_size_x = TILE_SIZE, local_size_y = TILE_SIZE, local_size_z = 1) in;

layout(set = 0, binding = 0) uniform sampler2D norm_rough;
layout(set = 0, binding = 1) uniform sampler2D in_depth;
layout(set = 0, binding = 2) uniform sampler2D velocity_buffer;
layout(set = 0, binding = 3) writeonly uniform image2D rt_out;

layout(push_constant) uniform ProjectionData
{
    mat4 mv;
    mat4 p;
    //vec3 view_pos;
} projection_data;

#include<common.h>
#include<pbr.h>
#include<rt_common.h>

layout(set = 1, binding = 0) uniform BVHMeta {
    uint root;
} bvh_meta;

layout(set = 1, binding = 1) buffer BVHTree {
    BVHNode nodes[];
} bvh_tree;

layout(set = 2, binding = 0) buffer VertexBuffer {
    float verts[];
} vbuffer;

layout(set = 2, binding = 1) buffer IndexBuffer {
    uint indices[];
} ibuffer;

shared uint stacks[gl_WorkGroupSize.x*gl_WorkGroupSize.y * STACK_SIZE];

#define stack_init() \
    uint stack_p = STACK_SIZE * gl_LocalInvocationIndex

#define stack_push(v) \
    stacks[stack_p++] = v

#define stack_pop() \
    (stacks[--stack_p])

#define stack_len() \
    (stack_p - (STACK_SIZE * gl_LocalInvocationIndex))

#define stack_empty_or_full()

vec3 test(Ray r) {

    stacks[gl_LocalInvocationIndex] = bvh_meta.root;
    uint cindex = stacks[gl_LocalInvocationIndex];
    BVHNode node = bvh_tree.nodes[cindex];

    return ray_box(r, node.bmin, node.bmax)? vec3(1.0): vec3(0.0);
}

vec3 read_vert(uint i) {
    return vec3(vbuffer.verts[i * 3], vbuffer.verts[i * 3 + 1], vbuffer.verts[i * 3 + 2]);
}

vec3 brute(Ray r) {
    vec4 inted = vec4(1000);
    inted.xyz = vec3(0.0);
    Triangle tt;
    vec4 res;

    vec3 verts[] = {
        vec3(1.0, 1.0, -1.0),
        vec3(-1.0, 1.0, -1.0),
        vec3(-1.0, 1.0, 1.0),
    };

    for(int i = 0; i < 3; i += 3) {
        tt.v[0] = read_vert(ibuffer.indices[i]);
        tt.v[1] = read_vert(ibuffer.indices[i + 1]);
        tt.v[2] = read_vert(ibuffer.indices[i + 2]);

        res = r_triangle(r, tt);
        if (res.x > 0.0 && res.w < inted.w) {
            inted = res;
        }
    }

    return inted.xyz;
}

vec3 traverse_bvh(Ray r) {
    vec4 current = vec4(1000);
    current.xyz = vec3(0.0);
    Triangle tt;

    stack_init();
    
    stack_push(bvh_meta.root);
    uint sl;
    while ((sl = stack_len(), sl > 0 && sl < STACK_SIZE)) {
        uint cindex = stack_pop();
        BVHNode node = bvh_tree.nodes[cindex];
        /*if (node.lchild == NULL_INDEX && node.rchild == NULL_INDEX) { //if leaf
            return vec3(cindex/10, cindex%10, 0.0);
        } 
        else { //internal node
            if(ray_box(r, node.bmin, node.bmax)) {
                if (node.lchild != NULL_INDEX)
                    stack_push(node.lchild);
                if (node.rchild != NULL_INDEX)
                    stack_push(node.rchild);
            }
        }*/

        if(ray_box(r, node.bmin, node.bmax)) {
            if (node.lchild == NULL_INDEX && node.rchild == NULL_INDEX) 
            {
                tt = triangle(
                    read_vert(node.object.x),
                    read_vert(node.object.y),
                    read_vert(node.object.z)
                );
                vec4 r = r_triangle(r, tt);
                if (r.x > 0.0 && r.w < current.w) {
                    current = r;
                }
            }
            if (node.lchild != NULL_INDEX)
                stack_push(node.lchild);
            if (node.rchild != NULL_INDEX)
                stack_push(node.rchild);
        }
    }
    return current.xyz;
}


void main() {
    ivec2 res = imageSize(rt_out);

    if(gl_GlobalInvocationID.x > res.x || gl_GlobalInvocationID.y > res.y)
        return;

    vec2 uv = vec2(gl_GlobalInvocationID.xy) / vec2(res);
    float depth = texelFetch(in_depth, ivec2(gl_GlobalInvocationID.xy), 0).x;
    vec3 point = world_space_location(uv, depth);

    vec4 norm_rough_metal = texture(norm_rough, uv);
    vec3 normal = octahedron_decode(norm_rough_metal.xy);
    float roughness = 0.0;//norm_rough_metal.z; 
    float metalness = norm_rough_metal.w; 

    mat4 p = projection_data.p;
    p[0][3] = 0.0;
    p[1][3] = 0.0;
    p[2][3] = -1.0;
    
    //finds direction
    vec3 point_vs = view_space_location(uv, depth);
    vec3 dir_to = normalize(point_vs);
    vec3 normal_vs = mat3(projection_data.mv) * normal;

    mat3 tbn_matrix = tbn(normal_vs);
    vec3 tbn_vdir = (-dir_to) * tbn_matrix;
    vec2 u = vec2(0.0);//hash22(hash1(frame_info.frame_hash) + uv);
    vec3 tbn_normal = sample_ggx_vndf(tbn_vdir, vec2(roughness), u);

    vec3 tbn_reflected = reflect(-tbn_vdir, tbn_normal);

    vec3 refl_vs = tbn_reflected * transpose(tbn_matrix);
    refl_vs = refl_vs * mat3(projection_data.mv);
    
    Ray r;
    r.r = refl_vs;
    r.o = point;
    /*
     
    refl_vs = dir_to * mat3(projection_data.mv);
    
    Ray r;
    r.r = refl_vs;
    r.o = vec3(
        projection_data.p[0][3],
        projection_data.p[1][3],
        projection_data.p[2][3]
    );
     */
    vec3 final = traverse_bvh(r).xyz;
    /*vec3 final = vec3(0.0);

    BVHNode node = bvh_tree.nodes[bvh_meta.root];
    if(ray_box(r, node.bmin, node.bmax)) {
        final = vec3(1.0);
    }*/

    imageStore(rt_out, ivec2(gl_GlobalInvocationID.xy), vec4(final, 1.0));
}


struct Ray {
    vec3 o, r;
};


struct Plane {
    vec3 o, n;
};


float ray_plane(Ray r, Plane p) {
    float denom = dot(r.r, p.n);
    if (denom < 1e-5) {
        return -1.0;
    }
    float nom = dot(p.o - r.o, p.n);
    
    float t = nom / denom;
    if (t < 0.0) {
        return -1.0;
    }
    
    return t;
}

/*struct Triangle2D {
    vec2 v[3];
};

Triangle2D triangle2d(vec2 v1, vec2 v2, vec2 v3) {
    Triangle2D t;
    t.v[0] = v1;
    t.v[1] = v2;
    t.v[2] = v3;
    return t;
}

float triangle_double_area(Triangle2D t) {
    vec3 v1 = vec3(t.v[1] - t.v[0], 0.0);
    vec3 v2 = vec3(t.v[2] - t.v[0], 0.0);
    return length(cross(v1, v2));
}

vec3 p_triangle2d(vec2 p, Triangle2D t) {
   float a = triangle_double_area(t);
   Triangle2D t1 = triangle2d(t.v[0], t.v[1], p);
   float a1 = triangle_double_area(t1);
   Triangle2D t2 = triangle2d(t.v[1], t.v[2], p);
   float a2 = triangle_double_area(t2);
   Triangle2D t3 = triangle2d(t.v[0], t.v[2], p);
   float a3 = triangle_double_area(t3);
   
   if (a < a1 + a2 + a3 - 1e-6) {
       return vec3(-1.0);
   }
   
   return vec3(a1/a, a2/a, a3/a);
   
}*/

struct Triangle {
    mat3 v;
};

Triangle triangle(vec3 v1, vec3 v2, vec3 v3) {
    Triangle t;
    t.v[0] = v1;
    t.v[1] = v2;
    t.v[2] = v3;
    
    return t;
}

float triangle3d_double_area(Triangle t) {
    vec3 v1 = t.v[1] - t.v[0];
    vec3 v2 = t.v[2] - t.v[0];
    return length(cross(v1, v2));
}

vec3 p_triangle(vec3 p, Triangle t) {
   float a = triangle3d_double_area(t);
   Triangle t1 = triangle(t.v[0], t.v[1], p);
   float a1 = triangle3d_double_area(t1);
   Triangle t2 = triangle(t.v[1], t.v[2], p);
   float a2 = triangle3d_double_area(t2);
   Triangle t3 = triangle(t.v[0], t.v[2], p);
   float a3 = triangle3d_double_area(t3);
   
   if (a < a1 + a2 + a3 - 1e-6) {
       return vec3(-1.0);
   }
   
   return vec3(a1/a, a2/a, a3/a);
   
}

vec3 triangle_normal(Triangle t) {
    vec3 v1 = t.v[1] - t.v[0];
    vec3 v2 = t.v[2] - t.v[0];
    
    return normalize(cross(v1, v2));
}

/*Triangle2D project_triangle(Triangle t, mat3 m) {
    Triangle2D r;
    
    r.v[0] = (t.v[0] * m).xy;
    r.v[1] = (t.v[1] * m).xy;
    r.v[2] = (t.v[2] * m).xy;
    
    return r;
}*/

vec4 r_triangle(Ray r, Triangle t) {
    Plane p;
    p.n = triangle_normal(t);
    if (dot(r.r, p.n) < 0.0) p.n = -p.n;
    p.o = t.v[0];
    
    float pt = ray_plane(r, p);
    if (pt < 0.0) {
        return vec4(-1.0);
    }
    
    vec3 p3d = r.o + r.r * pt;
    /*mat3 b;
    b[0] = p.n;
    b[1] = normalize(cross(r.r, b[0]));
    b[2] = normalize(cross(b[0], b[1]));
    b = transpose(b);
    
    vec2 p2d = (p3d * b).xy;
    
    Triangle2D t2 = project_triangle(t, b);
    
    return vec4(p_triangle2d(p2d, t2), pt);*/
    return vec4(p_triangle(p3d, t), pt);
}

bool ray_box(Ray r, vec3 bmin, vec3 bmax) {
    vec3 vtmin = (bmin - r.o) / r.r;
    vec3 vtmax = (bmax - r.o) / r.r;

    vec3 tmin = min(vtmin, vtmax);
    vec3 tmax = max(vtmin, vtmax);

    float enter = max(tmin.x, max(tmin.y, tmin.z));
    float exit = min(tmax.x, min(tmax.y, tmax.z));

    return exit > 0.0 && exit > enter;
}

#define NULL_INDEX 0xffffffff

struct BVHNode {
    vec3 bmin;
    uint lchild;
    vec3 bmax;
    uint rchild;
    uvec3 object;
    uint parent;
};

vec4 test_scene(Ray r) {
    vec4 inted = vec4(1000);
    inted.xyz = vec3(0.0);
    Triangle tt;
    vec4 res;
    vec3 bar;
    
    
                
                tt.v[0] = vec3(0.378491, 0.393837,  1.418716);
                tt.v[1] = vec3(0.114372, 0.320532,  1.400048);
                tt.v[2] = vec3(0.242106, 0.633369,  1.642215);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.114315, -0.318941,  1.399226);
                tt.v[1] = vec3(0.377848, -0.393098,  1.417728);
                tt.v[2] = vec3(0.315928, -0.556448,  1.678552);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.114372, 0.320532,  1.400048);
                tt.v[1] = vec3(0.378491, 0.393837,  1.418716);
                tt.v[2] = vec3(0.159851, 0.103827,  1.56847);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.377848, -0.393098,  1.417728);
                tt.v[1] = vec3(0.114315, -0.318941,  1.399226);
                tt.v[2] = vec3(0.158959, -0.100956,  1.566819);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.377848, -0.393098,  1.417728);
                tt.v[1] = vec3(0.158959, -0.100956,  1.566819);
                tt.v[2] = vec3(0.462918, -0.278507,  1.610096);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.377848, -0.393098,  1.417728);
                tt.v[1] = vec3(0.462918, -0.278507,  1.610096);
                tt.v[2] = vec3(0.315928, -0.556448,  1.678552);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.242106, 0.633369,  1.642215);
                tt.v[1] = vec3(0.371054, 0.400844,  1.693271);
                tt.v[2] = vec3(0.378491, 0.393837,  1.418716);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.034569, -0.788297,  1.70454);
                tt.v[1] = vec3(-0.273285, -0.249663,  1.783828);
                tt.v[2] = vec3(-0.118257, -0.091305,  1.41636);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[1] = vec3(0.560878, 0.246989,  1.418284);
                tt.v[2] = vec3(0.772171, 0.250629,  1.507034);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.560878, 0.246989,  1.418284);
                tt.v[1] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[2] = vec3(0.386538, 0.308409,  1.397968);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.387254, -0.262416,  1.430353);
                tt.v[1] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[2] = vec3(0.754018, -0.249049,  1.445236);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.389995, -0.816663,  1.569332);
                tt.v[1] = vec3(0.387254, -0.262416,  1.430353);
                tt.v[2] = vec3(0.754018, -0.249049,  1.445236);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.239453, 0.889123,  1.721658);
                tt.v[1] = vec3(0.560878, 0.246989,  1.418284);
                tt.v[2] = vec3(0.249865, 0.560609,  1.531301);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.034569, -0.788297,  1.70454);
                tt.v[1] = vec3(0.260929, -0.553093,  1.515131);
                tt.v[2] = vec3(0.389995, -0.816663,  1.569332);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[1] = vec3(-0.224852, 0.249936,  1.602082);
                tt.v[2] = vec3(0.132351, 0.222894,  1.452129);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.132351, 0.222894,  1.452129);
                tt.v[1] = vec3(0.131121, -0.219586,  1.454948);
                tt.v[2] = vec3(-0.174545, 0.115383,  1.428663);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.984917, 0.369324,  1.698742);
                tt.v[1] = vec3(-0.841543, -0.001748,  1.512551);
                tt.v[2] = vec3(-0.978813, -0.377194,  1.736089);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[1] = vec3(-0.841543, -0.001748,  1.512551);
                tt.v[2] = vec3(-0.984917, 0.369324,  1.698742);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.841543, -0.001748,  1.512551);
                tt.v[1] = vec3(-0.288732, -0.101798,  1.43864);
                tt.v[2] = vec3(-0.978813, -0.377194,  1.736089);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.1875, 0.0,  1.411873);
                tt.v[1] = vec3(-0.140625, 0.0,  1.466561);
                tt.v[2] = vec3(-0.195312, 0.0,  1.458748);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.1875, 0.0,  1.411873);
                tt.v[1] = vec3(-0.140625, 0.0,  1.466561);
                tt.v[2] = vec3(-0.118257, -0.091305,  1.41636);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.140625, 0.0,  1.466561);
                tt.v[1] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[2] = vec3(0.131121, -0.219586,  1.454948);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.118257, -0.091305,  1.41636);
                tt.v[1] = vec3(-0.140625, 0.0,  1.466561);
                tt.v[2] = vec3(0.131121, -0.219586,  1.454948);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.224852, 0.249936,  1.602082);
                tt.v[1] = vec3(0.249865, 0.560609,  1.531301);
                tt.v[2] = vec3(0.06919, 0.414528,  1.511449);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.260929, -0.553093,  1.515131);
                tt.v[1] = vec3(0.034569, -0.788297,  1.70454);
                tt.v[2] = vec3(0.087832, -0.414005,  1.489828);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.387254, -0.262416,  1.430353);
                tt.v[1] = vec3(0.260929, -0.553093,  1.515131);
                tt.v[2] = vec3(0.388532, -0.412973,  1.456214);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.239453, 0.889123,  1.721658);
                tt.v[1] = vec3(0.437234, 0.717688,  1.845992);
                tt.v[2] = vec3(0.772171, 0.250629,  1.507034);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.447829, 0.169166,  1.914262);
                tt.v[1] = vec3(-0.984917, 0.369324,  1.698742);
                tt.v[2] = vec3(-0.978813, -0.377194,  1.736089);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.447829, 0.169166,  1.914262);
                tt.v[1] = vec3(-0.978813, -0.377194,  1.736089);
                tt.v[2] = vec3(-0.273285, -0.249663,  1.783828);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.526019, -0.844113,  2.644174);
                tt.v[1] = vec3(0.964651, -0.345166,  2.024459);
                tt.v[2] = vec3(0.679624, 0.296324,  2.986731);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.900982, 0.465608,  2.056529);
                tt.v[1] = vec3(0.437234, 0.717688,  1.845992);
                tt.v[2] = vec3(0.423212, 0.914063,  2.4557);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.431843, -0.720984,  1.847709);
                tt.v[1] = vec3(0.964651, -0.345166,  2.024459);
                tt.v[2] = vec3(0.526019, -0.844113,  2.644174);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[1] = vec3(0.772171, 0.250629,  1.507034);
                tt.v[2] = vec3(0.437234, 0.717688,  1.845992);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.754018, -0.249049,  1.445236);
                tt.v[1] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[2] = vec3(0.431843, -0.720984,  1.847709);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.526019, -0.844113,  2.644174);
                tt.v[1] = vec3(0.363598, -0.851212,  2.400627);
                tt.v[2] = vec3(0.431843, -0.720984,  1.847709);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.282871, 0.073286,  2.888933);
                tt.v[1] = vec3(0.005499, 0.761512,  2.456012);
                tt.v[2] = vec3(-0.447829, 0.169166,  1.914262);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.061959, 0.696472,  2.583459);
                tt.v[1] = vec3(0.679624, 0.296324,  2.986731);
                tt.v[2] = vec3(0.423212, 0.914063,  2.4557);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.526019, -0.844113,  2.644174);
                tt.v[1] = vec3(-0.282871, 0.073286,  2.888933);
                tt.v[2] = vec3(-0.090076, -0.635407,  2.559018);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.364428, 1.149961,  2.634541);
                tt.v[1] = vec3(0.021499, 1.188219,  2.669896);
                tt.v[2] = vec3(0.466559, 1.291283,  2.676029);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.132351, 0.222894,  1.452129);
                tt.v[1] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[2] = vec3(0.131121, -0.219586,  1.454948);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.364428, 1.149961,  2.634541);
                tt.v[1] = vec3(0.466559, 1.291283,  2.676029);
                tt.v[2] = vec3(0.423212, 0.914063,  2.4557);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.239453, 0.889123,  1.721658);
                tt.v[1] = vec3(0.005499, 0.761512,  2.456012);
                tt.v[2] = vec3(0.423212, 0.914063,  2.4557);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.363598, -0.851212,  2.400627);
                tt.v[1] = vec3(0.445554, -1.213064,  2.638247);
                tt.v[2] = vec3(0.010129, -0.785826,  2.461384);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.034569, -0.788297,  1.70454);
                tt.v[1] = vec3(0.363598, -0.851212,  2.400627);
                tt.v[2] = vec3(0.010129, -0.785826,  2.461384);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.005499, 0.761512,  2.456012);
                tt.v[1] = vec3(0.364428, 1.149961,  2.634541);
                tt.v[2] = vec3(0.423212, 0.914063,  2.4557);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.021499, 1.188219,  2.669896);
                tt.v[1] = vec3(0.364428, 1.149961,  2.634541);
                tt.v[2] = vec3(0.005499, 0.761512,  2.456012);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.301681, -0.983413,  2.563204);
                tt.v[1] = vec3(0.031706, -1.188433,  2.653732);
                tt.v[2] = vec3(0.010129, -0.785826,  2.461384);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.363598, -0.851212,  2.400627);
                tt.v[1] = vec3(0.526019, -0.844113,  2.644174);
                tt.v[2] = vec3(0.445554, -1.213064,  2.638247);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.031706, -1.188433,  2.653732);
                tt.v[1] = vec3(0.445554, -1.213064,  2.638247);
                tt.v[2] = vec3(-0.090076, -0.635407,  2.559018);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.242106, 0.633369,  1.642215);
                tt.v[1] = vec3(0.114372, 0.320532,  1.400048);
                tt.v[2] = vec3(0.020155, 0.484034,  1.631801);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.020155, -0.484034,  1.631801);
                tt.v[1] = vec3(0.114315, -0.318941,  1.399226);
                tt.v[2] = vec3(0.315928, -0.556448,  1.678552);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.020155, 0.484034,  1.631801);
                tt.v[1] = vec3(0.114372, 0.320532,  1.400048);
                tt.v[2] = vec3(0.159851, 0.103827,  1.56847);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.158959, -0.100956,  1.566819);
                tt.v[1] = vec3(0.114315, -0.318941,  1.399226);
                tt.v[2] = vec3(0.020155, -0.484034,  1.631801);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.378491, 0.393837,  1.418716);
                tt.v[1] = vec3(0.371054, 0.400844,  1.693271);
                tt.v[2] = vec3(0.159851, 0.103827,  1.56847);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.224852, 0.249936,  1.602082);
                tt.v[1] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[2] = vec3(-0.984917, 0.369324,  1.698742);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.978813, -0.377194,  1.736089);
                tt.v[1] = vec3(-0.288732, -0.101798,  1.43864);
                tt.v[2] = vec3(-0.273285, -0.249663,  1.783828);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.224852, 0.249936,  1.602082);
                tt.v[1] = vec3(0.239453, 0.889123,  1.721658);
                tt.v[2] = vec3(0.249865, 0.560609,  1.531301);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.772171, 0.250629,  1.507034);
                tt.v[1] = vec3(0.560878, 0.246989,  1.418284);
                tt.v[2] = vec3(0.239453, 0.889123,  1.721658);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.389995, -0.816663,  1.569332);
                tt.v[1] = vec3(0.260929, -0.553093,  1.515131);
                tt.v[2] = vec3(0.387254, -0.262416,  1.430353);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.118257, -0.091305,  1.41636);
                tt.v[1] = vec3(0.131121, -0.219586,  1.454948);
                tt.v[2] = vec3(0.034569, -0.788297,  1.70454);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.447829, 0.169166,  1.914262);
                tt.v[1] = vec3(-0.090076, -0.635407,  2.559018);
                tt.v[2] = vec3(-0.282871, 0.073286,  2.888933);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.131121, -0.219586,  1.454948);
                tt.v[1] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[2] = vec3(0.387254, -0.262416,  1.430353);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[1] = vec3(-0.288732, -0.101798,  1.43864);
                tt.v[2] = vec3(-0.841543, -0.001748,  1.512551);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.1875, 0.0,  1.411873);
                tt.v[1] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[2] = vec3(-0.140625, 0.0,  1.466561);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.118257, -0.091305,  1.41636);
                tt.v[1] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[2] = vec3(-0.1875, 0.0,  1.411873);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.288732, -0.101798,  1.43864);
                tt.v[1] = vec3(-0.174545, 0.115383,  1.428663);
                tt.v[2] = vec3(-0.118257, -0.091305,  1.41636);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.288732, -0.101798,  1.43864);
                tt.v[1] = vec3(-0.118257, -0.091305,  1.41636);
                tt.v[2] = vec3(-0.273285, -0.249663,  1.783828);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.132351, 0.222894,  1.452129);
                tt.v[1] = vec3(-0.224852, 0.249936,  1.602082);
                tt.v[2] = vec3(0.06919, 0.414528,  1.511449);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.087832, -0.414005,  1.489828);
                tt.v[1] = vec3(0.034569, -0.788297,  1.70454);
                tt.v[2] = vec3(0.131121, -0.219586,  1.454948);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.560878, 0.246989,  1.418284);
                tt.v[1] = vec3(0.386538, 0.308409,  1.397968);
                tt.v[2] = vec3(0.249865, 0.560609,  1.531301);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.389995, -0.816663,  1.569332);
                tt.v[1] = vec3(0.754018, -0.249049,  1.445236);
                tt.v[2] = vec3(0.431843, -0.720984,  1.847709);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.034569, -0.788297,  1.70454);
                tt.v[1] = vec3(0.389995, -0.816663,  1.569332);
                tt.v[2] = vec3(0.431843, -0.720984,  1.847709);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.224852, 0.249936,  1.602082);
                tt.v[1] = vec3(-0.984917, 0.369324,  1.698742);
                tt.v[2] = vec3(-0.447829, 0.169166,  1.914262);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[1] = vec3(0.900982, 0.465608,  2.056529);
                tt.v[2] = vec3(0.964651, -0.345166,  2.024459);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.679624, 0.296324,  2.986731);
                tt.v[1] = vec3(-0.061959, 0.696472,  2.583459);
                tt.v[2] = vec3(-0.282871, 0.073286,  2.888933);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.526019, -0.844113,  2.644174);
                tt.v[1] = vec3(0.679624, 0.296324,  2.986731);
                tt.v[2] = vec3(-0.282871, 0.073286,  2.888933);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.964651, -0.345166,  2.024459);
                tt.v[1] = vec3(0.900982, 0.465608,  2.056529);
                tt.v[2] = vec3(0.679624, 0.296324,  2.986731);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.679624, 0.296324,  2.986731);
                tt.v[1] = vec3(0.900982, 0.465608,  2.056529);
                tt.v[2] = vec3(0.423212, 0.914063,  2.4557);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.010129, -0.785826,  2.461384);
                tt.v[1] = vec3(-0.273285, -0.249663,  1.783828);
                tt.v[2] = vec3(0.034569, -0.788297,  1.70454);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.431843, -0.720984,  1.847709);
                tt.v[1] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[2] = vec3(0.964651, -0.345166,  2.024459);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.239453, 0.889123,  1.721658);
                tt.v[1] = vec3(0.423212, 0.914063,  2.4557);
                tt.v[2] = vec3(0.437234, 0.717688,  1.845992);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.431843, -0.720984,  1.847709);
                tt.v[1] = vec3(0.363598, -0.851212,  2.400627);
                tt.v[2] = vec3(0.034569, -0.788297,  1.70454);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.005499, 0.761512,  2.456012);
                tt.v[1] = vec3(-0.224852, 0.249936,  1.602082);
                tt.v[2] = vec3(-0.447829, 0.169166,  1.914262);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.423212, 0.914063,  2.4557);
                tt.v[1] = vec3(0.021499, 1.188219,  2.669896);
                tt.v[2] = vec3(-0.061959, 0.696472,  2.583459);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.282871, 0.073286,  2.888933);
                tt.v[1] = vec3(-0.061959, 0.696472,  2.583459);
                tt.v[2] = vec3(0.005499, 0.761512,  2.456012);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.010129, -0.785826,  2.461384);
                tt.v[1] = vec3(0.445554, -1.213064,  2.638247);
                tt.v[2] = vec3(0.301681, -0.983413,  2.563204);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.301681, -0.983413,  2.563204);
                tt.v[1] = vec3(0.445554, -1.213064,  2.638247);
                tt.v[2] = vec3(0.031706, -1.188433,  2.653732);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.021499, 1.188219,  2.669896);
                tt.v[1] = vec3(0.005499, 0.761512,  2.456012);
                tt.v[2] = vec3(-0.061959, 0.696472,  2.583459);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.090076, -0.635407,  2.559018);
                tt.v[1] = vec3(0.010129, -0.785826,  2.461384);
                tt.v[2] = vec3(0.031706, -1.188433,  2.653732);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.090076, -0.635407,  2.559018);
                tt.v[1] = vec3(0.445554, -1.213064,  2.638247);
                tt.v[2] = vec3(0.526019, -0.844113,  2.644174);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.132351, 0.222894,  1.452129);
                tt.v[1] = vec3(0.386538, 0.308409,  1.397968);
                tt.v[2] = vec3(0.392504, -0.004906,  1.475924);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(-0.447829, 0.169166,  1.914262);
                tt.v[1] = vec3(-0.273285, -0.249663,  1.783828);
                tt.v[2] = vec3(-0.090076, -0.635407,  2.559018);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.392504, -0.004906,  1.475924);
                tt.v[1] = vec3(0.437234, 0.717688,  1.845992);
                tt.v[2] = vec3(0.900982, 0.465608,  2.056529);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.010129, -0.785826,  2.461384);
                tt.v[1] = vec3(-0.090076, -0.635407,  2.559018);
                tt.v[2] = vec3(-0.273285, -0.249663,  1.783828);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.005499, 0.761512,  2.456012);
                tt.v[1] = vec3(0.239453, 0.889123,  1.721658);
                tt.v[2] = vec3(-0.224852, 0.249936,  1.602082);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

                tt.v[0] = vec3(0.423212, 0.914063,  2.4557);
                tt.v[1] = vec3(0.466559, 1.291283,  2.676029);
                tt.v[2] = vec3(0.021499, 1.188219,  2.669896);
        
                res = r_triangle(r, tt);
                if (res.x > 0.0 && res.w < inted.w) {
                    inted = res;
                }
        

        

    
    return inted;
}

#version 450

void main() {
    gl_Position = gl_VertexIndex == 0 ? vec4(-1, -1, 0.5, 1) : (
        gl_VertexIndex == 1 ? vec4(3, -1, 0.5, 1) : vec4(-1, 3, 0.5, 1)
    );
}

#version 450

#extension GL_KHR_shader_subgroup_quad: enable

layout(local_size_x = 256, local_size_y = 1, local_size_z = 1) in;

//layout(set = 0, binding = 0) uniform sampler2D in_depth;
layout(set = 0, binding = 0, rg16) uniform coherent image2D mips[12];
layout(std430, set=0, binding = 1) coherent buffer AtomicBuffer
{
   uint counter;
} atomic_buffer;

shared float intermediate[256];
shared uint group_count;

float reduce_min4(vec4 v) {
    return min(v.x, min(v.y, min(v.z, v.w)));
}

#define REDUCE4 reduce_min4

float min4x4(float v) {
    vec4 vv;
    vv.x = v;
    vv.y = subgroupQuadSwapHorizontal(v);
    vv.z = subgroupQuadSwapVertical(v);
    vv.w = subgroupQuadSwapDiagonal(v);

    return REDUCE4(vv);
}

float load_source_reduce4x4(ivec2 uv, uint mip) {
    //vec4 samples = textureGatherOffset(in_depth, vec2(0), uv);
    vec4 samples;
    /*samples.x = texelFetch(in_depth, uv, int(mip)).x;
    samples.y = texelFetch(in_depth, uv + ivec2(1, 0), int(mip)).x;
    samples.z = texelFetch(in_depth, uv + ivec2(0, 1), int(mip)).x;
    samples.w = texelFetch(in_depth, uv + ivec2(1, 1), int(mip)).x;*/
    samples.x = imageLoad(mips[mip], uv).x;
    samples.y = imageLoad(mips[mip], uv + ivec2(1, 0)).x;
    samples.z = imageLoad(mips[mip], uv + ivec2(0, 1)).x;
    samples.w = imageLoad(mips[mip], uv + ivec2(1, 1)).x;

    return REDUCE4(samples);
}

void store_intermediate(ivec2 uv, float v) {
    intermediate[(uv.x % 16) + (uv.y % 16) * 16] = v;
}

float load_intermediate(ivec2 uv) {
    return intermediate[(uv.x % 16) + (uv.y % 16) * 16];
}

void imageStoreSafe(ivec2 top_size, uint mip, ivec2 p, float v) {
    ivec2 level_size = ivec2(top_size.x >> mip, top_size.y >> mip);
    level_size = max(level_size - 1, ivec2(0));
    p = min(p, level_size);
    imageStore(mips[mip], p, vec4(v));
}

void reduce_1_2(uvec2 invocationID, uvec2 slice, uint n_mips, uint start_mip) {
    vec4 l1; //l1 texels
    ivec2 start_size = imageSize(mips[start_mip]);
    ivec2 top_size = imageSize(mips[0]);

    for(uint y_offset = 0, l_offset = 0; y_offset < 32;  y_offset += 16) {
        for(uint x_offset = 0; x_offset < 32;  x_offset += 16) {
            ivec2 source_uv = ivec2(slice * 32 + invocationID.xy + uvec2(x_offset, y_offset)) * 2;
            ivec2 dest_uv = ivec2(slice * 32 + invocationID.xy + uvec2(x_offset, y_offset));
            source_uv = min(source_uv, start_size - 2);
            l1[l_offset] = load_source_reduce4x4(source_uv, start_mip);
            imageStoreSafe(top_size, start_mip + 1, dest_uv, l1[l_offset]);
            l_offset++;
        }
    }

    if(n_mips <= 1 + start_mip) return;

    for(uint i = 0; i < 4; i++) {
        l1[i] = min4x4(l1[i]);
    }

    if (invocationID.xy % ivec2(2, 2) == ivec2(0, 0)) {
        for(uint y_offset = 0, l_offset = 0; y_offset < 16;  y_offset += 8) {
            for(uint x_offset = 0; x_offset < 16;  x_offset += 8) {
                ivec2 dest_uv = ivec2(slice * 16 + invocationID.xy/2 + uvec2(x_offset, y_offset));
                imageStoreSafe(top_size, start_mip + 2, dest_uv, l1[l_offset]);
                store_intermediate(dest_uv, l1[l_offset]);
                l_offset++;
            }
        }
    }
}

void reduce_step(ivec2 top_size, uvec2 invocationID, uvec2 slice, uint n_mips, uint smip, uint cmip) {
    if(all(lessThan(invocationID, uvec2(32>>cmip)))) {
        //load values for previous level
        float iv = load_intermediate(ivec2(invocationID));
        iv = min4x4(iv);
        if (invocationID.xy % ivec2(2, 2) == ivec2(0, 0)) {
            ivec2 dest_uv = ivec2(slice * (16>>cmip) + invocationID.xy/2);
            imageStoreSafe(top_size, smip, dest_uv, iv/*gl_LocalInvocationIndex/256.0*/);
            store_intermediate(ivec2(invocationID.xy/2), iv);
        }
    }
}

void reduce_4t(uvec2 invocationID, uvec2 slice, uint n_mips, uint smip) {
    ivec2 top_size = imageSize(mips[0]);

    if(1 > n_mips) return;
    reduce_step(top_size, invocationID, slice, n_mips, smip, 1);
    groupMemoryBarrier();
    barrier();
    if(2 > n_mips) return;
    reduce_step(top_size, invocationID, slice, n_mips, smip + 1, 2);
    groupMemoryBarrier();
    barrier();
    if(3 > n_mips) return;
    reduce_step(top_size, invocationID, slice, n_mips, smip + 2, 3);
    groupMemoryBarrier();
    barrier();
    if(4 > n_mips) return;
    reduce_step(top_size, invocationID, slice, n_mips, smip + 3, 4);
}

bool is_not_last_workgroup() {
    if(gl_LocalInvocationIndex == 0) {
        group_count = atomicAdd(atomic_buffer.counter, 1);
    }

    groupMemoryBarrier();
    barrier();
    return group_count /*!=*/< (gl_NumWorkGroups.x * gl_NumWorkGroups.y - 1);
}

void main()
{
    uint n_mips = 10;
    uvec2 invocationID = uvec2(gl_LocalInvocationIndex % 2, (gl_LocalInvocationIndex % 4) / 2);
    invocationID += 2*uvec2((gl_LocalInvocationIndex / 4) % 8, gl_LocalInvocationIndex / 32);
    reduce_1_2(invocationID, gl_WorkGroupID.xy, n_mips, 0);
    groupMemoryBarrier();
    barrier();
    reduce_4t(invocationID, gl_WorkGroupID.xy, n_mips - 2, 3);

    //the image is currently as big as the dispatch size
    //the last workgroup will do the remaining work
    if(is_not_last_workgroup()) return;
    atomic_buffer.counter = 0;

    reduce_1_2(invocationID, uvec2(0, 0), n_mips, 6);
    groupMemoryBarrier();
    barrier();
    reduce_4t(invocationID, uvec2(0, 0), n_mips - 8, 9);
}

#version 450

layout(location = 0) out vec4 final;

layout(set = 0, binding = 0) uniform sampler2D blurred_image;

#include "bloom.h"

void main() {
    ivec2 res = textureSize(blurred_image, 0)*2;

    ivec2 p_coords = ivec2(gl_FragCoord.xy);
    vec2 uv = vec2(p_coords) / vec2(res);
    final.xyz = fetch_tent(blurred_image, res, uv, 0).xyz;
    final.w = 0.016;
}

#ifndef BLOOM_H_
#define BLOOM_H_

#define TENT_RADIUS 3.0

vec3 samples[] = {
    vec3(-2.0, -2.0, 0.03125),
    vec3(0.0, -2.0, 0.0625),
    vec3(2.0, -2.0, 0.03125),
    vec3(-1.0, -1.0, 0.125),
    vec3(1.0, -1.0, 0.125),
    vec3(-2.0, 0.0, 0.0625),
    vec3(0.0, 0.0, 0.125),
    vec3(2.0, 0.0, 0.0625),
    vec3(-1.0, 1.0, 0.125),
    vec3(1.0, 1.0, 0.125),
    vec3(-2.0, 2.0, 0.03125),
    vec3(0.0, 2.0, 0.0625),
    vec3(2.0, 2.0, 0.03125),
};

vec3 samples_tent[] = {
    vec3(-1.0, -1.0, 0.0625),
    vec3(0.0, -1.0, 0.125),
    vec3(1.0, -1.0, 0.0625),
    vec3(-1.0, 0.0, 0.125),
    vec3(0.0, 0.0, 0.25),
    vec3(1.0, 0.0, 0.125),
    vec3(-1.0, 1.0, 0.0625),
    vec3(0.0, 1.0, 0.125),
    vec3(1.0, 1.0, 0.0625),
};

vec4 fetch36(sampler2D im, ivec2 sz, vec2 uv, int level) {
    vec4 r = vec4(0);
    for(int i = 0; i < 13; i++) {
        vec3 s = samples[i];
        r += s.z * textureLod(im, uv + s.xy / vec2(sz), float(level));
    }

    return r;
}

vec4 fetch_tent(sampler2D im, ivec2 sz, vec2 uv, int level) {
    vec4 r = vec4(0);
    for(int i = 0; i < 9; i++) {
        vec3 s = samples_tent[i];
        r += s.z * textureLod(im, uv + TENT_RADIUS * s.xy / vec2(sz), float(level));
    }

    return r;
}


#endif // BLOOM_H_

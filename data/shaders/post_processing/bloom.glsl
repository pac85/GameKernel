#version 450

layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

layout(set = 0, binding = 0) writeonly uniform image2D blurred[12];
layout(set = 0, binding = 1) uniform sampler2D input_image;
layout(set = 0, binding = 2) uniform sampler2D blurred_sampler;

layout(push_constant) uniform MipLevel {
    int mip_level;
} mip_level;

#include "bloom.h"

#ifndef UPSAMPLE
void main() {
    int level = mip_level.mip_level;
    ivec2 sz = ivec2(ceil(vec2(textureSize(input_image, 0)) / vec2((2 << mip_level.mip_level))));
    if(gl_GlobalInvocationID.x >= sz.x || gl_GlobalInvocationID.y >= sz.y) return;
    vec2 uv = vec2(gl_GlobalInvocationID.xy) / vec2(sz);
    uv += 1.0 / vec2(sz*2);
    vec4 filtered_sample;
    if(level == 0) {
        filtered_sample = fetch36(input_image, sz, uv, 0);
        if(any(isnan(filtered_sample))) filtered_sample = vec4(0.0);
    }
    else {
        filtered_sample = fetch36(blurred_sampler, sz * 2, uv, level - 1);
    }

    imageStore(blurred[level], ivec2(gl_GlobalInvocationID.xy), filtered_sample);
}
#else

#define MIP_MERGE_FACTOR 0.7

void main() {
    int level = mip_level.mip_level;
    ivec2 sz = textureSize(blurred_sampler, 0) / (1 << mip_level.mip_level);//;
    if(gl_GlobalInvocationID.x >= sz.x || gl_GlobalInvocationID.y >= sz.y) return;
    vec2 uv = vec2(gl_GlobalInvocationID.xy) / vec2(sz);
    uv += 1.0 / vec2(sz*2);
    vec4 this_sample = textureLod(blurred_sampler, uv, float(level));
    vec4 filtered_sample = fetch_tent(blurred_sampler, sz, uv, level + 1);

    vec4 mixed = (this_sample + filtered_sample) * MIP_MERGE_FACTOR;//mix(this_sample, filtered_sample, 0.5);
    imageStore(blurred[level], ivec2(gl_GlobalInvocationID.xy), mixed);
}
#endif

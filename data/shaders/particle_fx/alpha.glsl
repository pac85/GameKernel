#version 450

layout(location = 0) out vec4 color;

layout(location = 0) in vec2 center_distance;
layout(location = 1) in vec2 uv;
layout(location = 2) in float fade;

layout(push_constant) uniform ProjectionData
{
    mat4 mvp;
    vec4 camera_up_time;
    vec4 camera_right_delta;
};

/*
 *
pub struct ParticleTexture {
    pub altas: super::texture::Texture,
    pub offset: Vector2<f32>,
    pub size: Vector2<f32>,
    pub frames: f32,
    pub fps: f32,
}

pub struct AlphaParticleRendererParams {
    pub life: f32,
    pub sizes: Vec<PropertyCurveNode<f32>>,
    pub texture: ParticleTexture,
}
 * */
struct ParticleTexture {
    vec2 uv_offset,
         size,
         fps;
    uvec2 frames;
};

struct Params {
    float life;
    vec2 size;
    ParticleTexture txt;
};

layout(std430, set = 0, binding = 2) buffer _Params {
    Params p;
} p;

layout(set = 0, binding = 3) uniform sampler2D atlas;

void main() {
    /*float alpha = 1 - dot(center_distance, center_distance);
    alpha = max(alpha, 0.0);

    color = vec4(1.0, 0.52, 0.21, alpha);*/
    color = texture(atlas, uv) * vec4(fade);
}

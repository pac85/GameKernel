#version 450

#include "particles_common.h"

layout(push_constant) uniform ProjectionData
{
    mat4 mvp;
    vec4 camera_up_time;
    vec4 camera_right_delta;
};

layout(std430, set = 0, binding = 0) buffer ParticelStates 
{
    ParticleState states[];
}s;

layout(std430, set = 0, binding = 1) buffer ParticleSystemStates 
{
    ParticleSystemState state;
}ps;

struct ParticleTexture {
    vec2 uv_offset,
         size,
         fps;
    uvec2 frames;
};

struct Params {
    float life;
    vec2 size;
    ParticleTexture txt;
};

layout(std430, set = 0, binding = 2) buffer _Params {
    Params p;
} p;

ParticleState load_particle() {
    return s.states[gl_VertexIndex / 6];
}

vec2 offsets[4] = {
    vec2(-1, 1),
    vec2(1, 1),
    vec2(1, -1),
    vec2(-1, -1),
};

bool is_alive(uint current) {
    uvec2 start_end = ps.state.start_end;
    return within(start_end, current);
}

layout(location = 0) out vec2 center_distance;
layout(location = 1) out vec2 uv;
layout(location = 2) out float fade;

vec2 calculate_uv(float relative_age, vec2 voff) {
    vec2 uv = vec2(0.0);
    uv += p.p.txt.fps * relative_age;
    uvec2 uuv = uvec2(uv);
    uv = vec2(uuv % p.p.txt.frames);
    return (uv + voff) * p.p.txt.size + p.p.txt.uv_offset;
}

int n1mod(int x, int m) {
    if(x < 0) {
        x += m;
    }

    return x % m;
}

void main() {
    int curren_vertex = gl_VertexIndex % 4;
    vec2 ss_offset = offsets[curren_vertex];
    center_distance = ss_offset;
    ss_offset *= p.p.size;
    vec3 vertex_offset = 
        ss_offset.x * camera_right_delta.xyz + 
        ss_offset.y * camera_up_time.xyz;            

    int current_particle = int(gl_VertexIndex / 4) - int(ps.state.start_end.x);
    current_particle = n1mod(current_particle, s.states.length());
    float timestamp = s.states[current_particle].position_spawn_timestamp.w;
    float relative_age = (camera_up_time.w - timestamp) / 1000;
    uv = calculate_uv(relative_age, offsets[curren_vertex] / 2.0 + 0.5);
    fade = 1 - clamp(10.0 * relative_age / p.p.life - 9.0, 0.0, 1.0);
    if(!is_alive(current_particle))
        gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
    else {
        vec4 particle_center = vec4(s.states[current_particle].position_spawn_timestamp.xyz, 1.0);
        vec4 vertex_pos  = particle_center + vec4(vertex_offset, 0.0);

        gl_Position = mvp * vertex_pos;
    }
}

layout(std430)
struct ParticleState {
    vec4 position_spawn_timestamp;
    vec4 velocity;
};

struct ParticleSystemState {
    uvec2 start_end;
};


bool within(uvec2 start_end, uint current) {
    return start_end.x > start_end.y ^^
        (start_end.x <= current && start_end.y > current);
}

#version 450

layout(local_size_x = 256, local_size_y = 1, local_size_z = 1) in;

#include "particles_common.h"

struct SimplePhysicsProperties {
    vec4 accelleration;
};

layout(constant_id = 0) const uint MAX_PARTICLES = 64;

layout(std430, set = 0, binding = 0) buffer ParticelStates 
{
    ParticleState states[];
}s;

layout(std430, set = 0, binding = 1) buffer ParticleSystemStates 
{
    ParticleSystemState state;
}ps;

layout(std430, set = 0, binding = 2) buffer IndirectDrawArgs
{
    uint index_count,
        instance_count,
        first_index,
        vertex_offset,
        first_instance;
}indirect_args;

layout(std430, set = 0, binding = 3) buffer SpawnData
{
    ParticleState new_particles[];
}sd;

layout(std430, set = 0, binding = 4) buffer _SimplePhysicsProperties
{
    SimplePhysicsProperties physics_properties;
}spp;

layout(push_constant) uniform FrameData
{
    float delta_time;
    float time;
    uint n_new_particles;
} frame_data;

void write_indirect_args(uint no_particles) {
    indirect_args.index_count = 6 * min(no_particles, MAX_PARTICLES);
}

bool is_alive(uint current) {
    uvec2 start_end = ps.state.start_end;
    return within(start_end, current);
}

uint unloop_index(uint i) {
    if(i < 0)
        return i + MAX_PARTICLES;
    else 
        return i;
}

#define buff_len(start_end) (unloop_index((start_end).y - (start_end).x))

void add_new_particles(uint current_particle) {
    uint update_end = ps.state.start_end.y + frame_data.n_new_particles;
    uint update_end_mod = update_end % MAX_PARTICLES;

    if(within(uvec2(ps.state.start_end.y, update_end_mod), current_particle)) {
        if(unloop_index(current_particle - ps.state.start_end.y) < frame_data.n_new_particles)
            s.states[current_particle] = sd.new_particles[unloop_index(current_particle - ps.state.start_end.y)];
    }

    if(current_particle == 0)
    {
        //if we've filled the buffer
        if(buff_len(uvec2(ps.state.start_end.x, update_end_mod)) < buff_len(ps.state.start_end) )
        {
            /*if(ps.state.start_end.x < update_end_mod) {
                ps.state.start_end.x = (update_end + MAX_PARTICLES - 1) % MAX_PARTICLES;
            }
            else {
                ps.state.start_end.x = (update_end + 1) % MAX_PARTICLES;
            }*/
            ps.state.start_end.x = (update_end + 1) % MAX_PARTICLES;
        }

        ps.state.start_end.y = update_end_mod;
    }
}

void simulate_particles(uint current_particle) {
    s.states[current_particle].position_spawn_timestamp.xyz += s.states[current_particle].velocity.xyz * frame_data.delta_time / 1000.0;
    s.states[current_particle].velocity += spp.physics_properties.accelleration * frame_data.delta_time / 1000.0;
}

void main() {
    uint current_particle = gl_GlobalInvocationID.x;
    add_new_particles(current_particle);

    //make sure new particles are visible among all threads
    memoryBarrier();
    barrier();

    if(current_particle == 0) {
        write_indirect_args(buff_len(ps.state.start_end));
    }

    if(!is_alive(current_particle)) {
        return;
    }

    simulate_particles(current_particle);
}

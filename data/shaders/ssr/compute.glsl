#version 450

layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

layout(set = 0, binding = 0) uniform sampler2D norm_rough;
layout(set = 0, binding = 1) uniform sampler2D in_depth;
layout(set = 0, binding = 2) uniform sampler2D hdr_color;
layout(set = 0, binding = 3) uniform usampler2D mesh_index_image;
layout(set = 0, binding = 4) uniform sampler2D velocity_buffer;
//layout(set = 0, binding = 4, r11f_g11f_b10f) readonly uniform image2D previous_ssr_frame;
layout(set = 0, binding = 5) uniform sampler2D previous_ssr_frame;
layout(set = 0, binding = 6) writeonly uniform image2D half_res_specular;
layout(set = 0, binding = 7) buffer Res
{
    uvec2 res;
}res;
layout(std430, set = 1, binding = 0) buffer SsrFrameInfo {
    float frame_hash;
} frame_info;

layout(push_constant) uniform ProjectionData
{
    mat4 mv;
    mat4 p;
    //vec3 view_pos;
} projection_data;

#include<common.h>
#include<pbr.h>

vec2 clip_to_uv(vec2 clip)
{
    return clip / 2 - 0.5;
}

bool ray_march(ivec2 res, vec3 o, vec3 d, mat4 pv, float near_plane, float
thickness, float stride, float jitter, const float max_steps, float
max_distance, out vec2 hit)
{
    mat4 clip_to_screen = mat4(
        vec4(res.x / 2,     0,      0, 0),
        vec4(0,         res.y / 2,  0, 0),
        vec4(0,             0,      1, 0),
        vec4(res.x / 2, res.y /2,   0, 1)
    );

    float ray_length = ((o.z + d.z * max_distance) > near_plane) ?
        ((near_plane - o.z) / d.z) * 0.9: 
        max_distance;
    //float ray_length = min(abs((near_plane - o.z) / d.z), max_distance);

    vec3 target = o + d * ray_length;
    hit = vec2(-1, -1);

    //project into screen space
    vec4 H0 = pv * vec4(o, 1.0),
         H1 = pv * vec4(target, 1.0);
    float k0 = 1.0 / H0.w,
          k1 = 1.0 / H1.w;
    vec3 Q0 = o * k0,
         Q1 = target * k1;

    //screen space endpoints
    vec2 P0 = H0.xy * k0,
         P1 = H1.xy * k1;

    P0 = P0 * (res / 2) + ((res / 2));
    P1 = P1 * (res / 2) + ((res / 2));

    float x_max = res.x + 0.5, 
          x_min = 0.5,
          y_max = res.y + 0.5,
          y_min = 0.5,
          alpha = 0;

    if (P1.y > y_max || P1.y < y_min)
        alpha = (P1.y - ((P1.y > y_max) ? y_max : y_min)) / (P1.y - P0.y);
    if ((P1.x > x_max) || (P1.x < x_min))
        alpha = max(alpha, (P1.x - ((P1.x > x_max) ? x_max : x_min)) / (P1.x - P0.x));
    P1 = mix(P1, P0, alpha);
    k1 = mix(k1, k0, alpha);
    Q1 = mix(Q1, Q0, alpha);

    vec2 delta = P1 - P0;
    P1 += vec2((dot(delta, delta) < 0.0001) ? 0.01: 0.0);
    delta = P1 - P0;

    bool permute = false;
    if(abs(delta.x) < abs(delta.y))
    {
        permute = true;
        delta = delta.yx;
        P0 = P0.yx;
        P1 = P1.yx;
    }

    float step_dir = sign(delta.x),
          invdx    = step_dir / delta.x;

    //trach the derivatives of Q and k
    vec3 dQ = (Q1 - Q0) * invdx;
    float dk = (k1 - k0) * invdx;
    vec2 dP = vec2(step_dir, delta.y * invdx);

    dP *= stride; dQ *= stride; dk *= stride;

    P0 += dP * jitter; Q0 += dQ * jitter; k0 += dk * jitter;
    float prev_z_max_estimate = o.z;

    mat4 ipv = inverse(pv);
    //slide ...
    vec3 Q = Q0;
    float k = k0,
          step_count = 0.0,
          end = P1.x * step_dir;
    for(vec2 P = P0;
        ((P.x * step_dir) <= end) && (step_count < max_steps);
        P += dP, Q.z += dQ.z, k += dk, step_count += 1.0)
    {
        //project from homogeneuous to camera space
        hit = permute ? P.yx : P;
        float ray_z_min = prev_z_max_estimate;
        float ray_z_max = (dQ.z * 0.5 + Q.z) / (dk * 0.5 + k);
        prev_z_max_estimate = ray_z_max;
        if(ray_z_min > ray_z_max) swap(ray_z_min, ray_z_max);

        float scene_z_max = texelFetch(in_depth, ivec2(hit * 2), 0).x;
        float ssr_bias = scene_z_max; ssr_bias *= ssr_bias;
        vec2 scene_z_max_h = (ipv * vec4(0, 0, scene_z_max, 1.0)).zw; //TODO optimize
        scene_z_max = scene_z_max_h.x / scene_z_max_h.y;

        float scene_z_min = scene_z_max - thickness;

        float d = ray_z_max - scene_z_max;
        //if(step_count > 150 && d > 0 && d < thickness)
        //if((ray_z_max + ssr_bias >= scene_z_min) && (ray_z_min + ssr_bias <= scene_z_max))
        float zp = abs(ray_z_max - ray_z_min) * 1.5 + 0.5;
        if(abs(-zp - 0.5 + scene_z_max - ray_z_max) < zp) 
            return all(lessThanEqual(abs(hit - (res * 0.5)), (res * 0.5)));
    }
    return false;
    
}

vec4 projectDir(vec3 o, vec3 d, mat4 pv) {
    vec4 off = pv * vec4(o, 1.0);
    //off.xyz / = off.w;
    vec4 dp = pv * vec4(o+d, 1.0);
    //dp.xyz /= dp.w;

    return dp - off;
}

bool ray_march2(ivec2 res, vec3 o, vec3 d, mat4 pv, float near_plane, float thickness, float stride, float jitter, const float max_steps, float max_distance, out vec2 hit) {
    vec4 so = pv * vec4(o, 1.0);
    //so.xyz / = so.w;
    vec4 sd = projectDir(o, normalize(d), pv);

    bool useX = sd.x > sd.y;

    vec4 p = so;
    for(int i = 0; i < 100; i++) {
        p += sd;
        vec3 sp = p.xyz/p.w;
        float scene_z_max = texelFetch(in_depth, ivec2(sp.xy * 2), 0).x;
        if(sp.x < 0 || sp.y < 0 || sp.x > res.x || sp.y > res.y) 
            return false;
        if (abs(scene_z_max - sp.z) < 0.03 && scene_z_max < sp.z) {
            hit = ivec2(sp.xy);
            return true;
        }
    }
    return false;
}

bool ray_march3(ivec2 res, vec3 o, vec3 d, mat4 clip_to_screen, mat4 pv, float near_plane, float thickness, float stride, float jitter, const float max_steps, float max_distance, out vec2 hit) {
    float ray_length = ((o.z + d.z * max_distance) > near_plane) ? 
        (near_plane + o.z) / d.z: 
        max_distance;
    ray_length = 5.0;

    vec4 start = pv * vec4(o, 1.0);
    start.xyzw /= start.w;
    start = clip_to_screen * start;
    vec4 end = pv * vec4(o + ray_length * d, 1.0);
    end.xyzw /= end.z;
    end = clip_to_screen * end;
    //so.xyz / = so.w;
    float deltaFact = 1.0 / length(end.xy-start.xy);

    float f = 0.0;
    for(int i = 0; i < 100; i++) {
        f += deltaFact;
        vec3 p = vec3(0.0);
        p.xy = mix(start.xy, end.xy, f);
        p.z = (start.z*end.z) / mix(end.z, start.z, f);

        float scene_z_max = texelFetch(in_depth, ivec2(p.xy * 2), 0).x;
        if(p.x < 0 || p.y < 0 || p.x > res.x || p.y > res.y) 
            return false;
        if (abs(scene_z_max - p.z) < 0.03 && scene_z_max < p.z) {
            hit = ivec2(p.xy);
            return true;
        }
    }
    return false;
}

bool ray_march4(ivec2 res, vec3 o, vec3 d, mat4 pv, float near_plane, float thickness, float stride, float jitter, const float max_steps, float max_distance, out vec2 hit) {
    vec4 so = pv * vec4(o, 1.0);
    so.xyz /= so.w;
    vec4 sd = projectDir(o, normalize(d), pv);
    sd.xyz /= sd.w;
    sd.z = d.z;
    so.z = o.z;
    //sd.xyz = normalize(sd.xyz);

    float t = 0;
    for(int i = 0; i < 100; i++) {
        t += 0.01;
        vec3 sp = so.xyz + sd.xyz * t;
        vec4 spz = vec4(0.0, 0.0, sp.z, 1.0);
        spz = pv * spz;
        sp.z = spz.z/spz.w;
        float scene_z_max = texelFetch(in_depth, ivec2(sp.xy * 2), 0).x;
        if(sp.x < 0 || sp.y < 0 || sp.x > res.x || sp.y > res.y) 
            return false;
        if (abs(scene_z_max - sp.z) < 0.03 && scene_z_max < sp.z) {
            hit = ivec2(sp.xy);
            return true;
        }
    }
    return false;
}

bool stupid_ray_march(ivec2 res, vec3 o, vec3 d, mat4 pv, float near_plane, float thickness, float stride, float jitter, const float max_steps, float max_distance, out vec2 hit) {
    vec4 p = vec4(o + d, 1.0);
    float scene_z_max = 0.1;

    mat4 clip_to_screen = mat4(
        vec4(res.x / 2,     0,      0, 0),
        vec4(0,         res.y / 2,  0, 0),
        vec4(0,             0,      1, 0),
        vec4(res.x / 2, res.y /2,   0, 1)
    );

    for (int i = 0; i < 100; i++) {
        p.xyz += d * 1.1;
        vec4 sp = pv * p;
        sp.xyz /= sp.w;
        sp.w = 1;
        sp = clip_to_screen * sp;
        scene_z_max = texelFetch(in_depth, ivec2(sp.xy * 2), 0).x;
        if (sp.x < 0 || sp.y < 0 || sp.x > res.x || sp.y > res.y || abs(0.03 + scene_z_max - sp.z) < 0.03) {
            hit = ivec2(sp.xy);
            return true;
        }
    }
    return false;
}

float fresnel(vec3 normal, vec3 vdir, float metalness)
{
    float F0 = mix(0.04, 0.9, metalness);
    return fresnel_schlick(max(dot(normal, vdir), 0.0), F0);
}

bool is_same_object(vec2 reprojected_uv, vec2 current_uv) {
    uvec4 id1 = texture(mesh_index_image, current_uv);
    uvec4 id2 = texture(mesh_index_image, reprojected_uv);

    return id1.x == id2.x;
}

bool is_reprojection_valid(vec3 current_pos, vec3 current_normal, float reprojected_depth, vec2 reprojected_uv, vec2 current_uv) {
    const float PLANE_DISTANCE_THRESHOLD = 5.1;
    const float NORMAL_DISTANCE_THRESHOLD = 0.44;
    vec3 reprojected_pos = world_space_location(reprojected_uv, reprojected_depth);
    vec3 to_current = current_pos - reprojected_pos;
    float dist_to_plane = abs(dot(to_current, current_normal));


    vec4 norm_rough_metal = texture(norm_rough, reprojected_uv);
    vec3 reprojected_normal = octahedron_decode(norm_rough_metal.xy);
    /*if (pow(abs(dot(current_normal, reprojected_normal)), 2) < NORMAL_DISTANCE_THRESHOLD) {
        return false;
    }*/
    return dist_to_plane < PLANE_DISTANCE_THRESHOLD && is_same_object(reprojected_uv, current_uv);
}

struct Sphere
{
    vec3 center;
    float rad;
    vec3 color;
    vec3 EmissionColor;
};
   
struct Ray
{
    vec3 origin;
    vec3 direction;
};


Ray cray(vec3 o, vec3 d) {
  Ray t;
  t.origin = o;
  t.direction = d;
  return t;
}

Sphere csphere(vec3 center, float rad) {
    Sphere r;
    r.center  = center;
    r.rad = rad;
    return r;
}

bool RCsphere(Ray ray,  Sphere sphere)
{
    vec3 op = sphere.center - ray.origin;
    float t ,eps = 1e-3;
    float b = dot(op , ray.direction);
    float det = b * b - dot(op,op) + sphere.rad*sphere.rad;
    if(det < 0.0){return false;} else det = sqrt(det);
    float d = (t = b - det) > eps ? t : ((t = b + det) > eps ? t : 0.0);
    return d == 0;
}

void main()
{
    ivec2 buffer_res = imageSize(half_res_specular);
    ivec2 res = ivec2(res.res.x / 2, res.res.y / 2);

    if(gl_GlobalInvocationID.x > res.x || gl_GlobalInvocationID.y > res.y)
        return;

    vec2 clip_space_uv = vec2(gl_GlobalInvocationID.xy) / vec2(res);
    vec2 uv = vec2(gl_GlobalInvocationID.xy) / vec2(buffer_res);
    //float depth = texture(in_depth, uv).x;
    float depth = texelFetch(in_depth, ivec2(gl_GlobalInvocationID.xy) * 2, 0).x;
    vec3 point = world_space_location(clip_space_uv, depth);

    float depthx = texelFetch(in_depth, ivec2(gl_GlobalInvocationID.xy) * 2 + ivec2(1.0, 0.0), 0).x;
    float depthy = texelFetch(in_depth, ivec2(gl_GlobalInvocationID.xy) * 2 + ivec2(0.0, 1.0), 0).x;

    vec4 norm_rough_metal = texture(norm_rough, uv);
    vec3 normal = octahedron_decode(norm_rough_metal.xy);
    float roughness = norm_rough_metal.z; 
    float metalness = norm_rough_metal.w; 

    mat4 p = projection_data.p;
    p[0][3] = 0.0;
    p[1][3] = 0.0;
    p[2][3] = -1.0;
    
    //finds direction
    vec3 point_vs = view_space_location(clip_space_uv, depth);
    vec3 point_vsx = view_space_location(clip_space_uv, depthx);
    vec3 point_vsy = view_space_location(clip_space_uv, depthy);
    vec3 dir_to = normalize(point_vs);
    vec3 normal_vs = mat3(projection_data.mv) * normal;

    mat3 tbn_matrix = tbn(normal_vs);
    vec3 tbn_vdir = (-dir_to) * tbn_matrix;
    vec2 u = hash22(hash1(frame_info.frame_hash) + uv);
    vec3 tbn_normal = sample_ggx_vndf(tbn_vdir, vec2(roughness), u);

    vec3 tbn_reflected = reflect(-tbn_vdir, tbn_normal);

    vec3 refl_vs = tbn_reflected * transpose(tbn_matrix);

    float near_plane = 0.1;
    /*vec2 cnp = (inverse(p) * vec4(0, 0, near_plane, 1.0)).zw;
    near_plane = cnp.x / cnp.y;*/

    //calculates a matrix that goes from NDC to screen coords
    vec4 t = vec4(0);
    vec4 sc = vec4(0.0, 30.0, 0.0, 1.0);
    sc = projection_data.mv * sc;
    vec2 hitp;
    bool hit = ray_march(res, point_vs.xyz, refl_vs.xyz,
        p, near_plane, 1.5, 4.0, 0, 64, 500.0, hitp);
    
    vec2 cpos = hitp/buffer_res;
    vec2 velocity_vector = texture(velocity_buffer, cpos).xy * 2 - 1;
    velocity_vector *= vec2(res) / vec2(buffer_res);
    vec2 puv = cpos - velocity_vector;
    puv = clamp(puv, 0, 1);
    vec3 reflected_color;
    if(hit && cpos.x > 0 && cpos.x < 1.0 && cpos.y > 0 && cpos.x < 1.0)
    {
        reflected_color = texture(hdr_color, puv).xyz *
            fresnel(normal_vs, -dir_to, metalness);
    }   
    else 
    {
        reflected_color = vec3(0);
    }

    velocity_vector = texture(velocity_buffer, uv).xy * 2 - 1;
    vec2 sample_jitter = vec2(0.0);//2 * u - 1.0;
    ivec2 screen_space_velovity = ivec2(sample_jitter + (velocity_vector * buffer_res));
    vec2 reprojected_uv = uv - velocity_vector + 0.5 / vec2(res);
    float factor = mix(1.0, 0.1, sqrt(min(2*roughness, 1.0)));
    if (reprojected_uv.x > 1.0 || 
            reprojected_uv.y > 1.0 || 
            reprojected_uv.x < 0 || 
            reprojected_uv.y < 0) {

        factor = 1.0;
    }
    float repr_depth = texture(in_depth, reprojected_uv).x;
    if(!is_reprojection_valid(point, normal, repr_depth, reprojected_uv - 0.5 / vec2(res), uv - 0.5 / vec2(res))) {
        factor = 1.0;
    }
    reflected_color = clamp(reflected_color, vec3(0.0), vec3(1.0));
    //vec3 previous_frame = imageLoad(previous_ssr_frame, reprojected_uv).xyz;
    vec3 previous_frame = texture(previous_ssr_frame, uv - velocity_vector + 0.5 / vec2(buffer_res)).xyz;
    previous_frame = clamp(previous_frame, vec3(0.0), vec3(1.0));
    vec3 mixed = mix(previous_frame, reflected_color, factor);
    imageStore(half_res_specular, ivec2(gl_GlobalInvocationID.xy), mixed.xyzz);
}

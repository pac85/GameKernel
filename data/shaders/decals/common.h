#ifndef DECAL_COMMON_H_
#define DECAL_COMMON_H_

struct Decal {
    uvec4 textures; //albedo normals metalness_roughness_alpha emission
    mat4 world_matrix;
};

struct DecalsInfo {
    uint n_decals;
};

vec3 decal_texture_coordinates(vec3 ws, mat4 mat) {
    vec4 r = inverse(mat) * vec4(ws, 1.0);
    r.xyz /= r.w;
    r.xy = (r.xy+1)/2.0;

    return r.xyz;
}

vec3 calculate_view_vector(mat4 view_matrix, vec3 pworld_space) {
    vec4 camera = inverse(view_matrix)*vec4(0.0, 0.0, 0.0, 1.0);
    camera.xyz /= camera.w; //not necessary
    return camera.xyz - pworld_space;
}

#endif // DECAL_COMMON_H_

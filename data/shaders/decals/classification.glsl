#version 450
//#define FAKE_DERIVATIVES
#ifndef FAKE_DERIVATIVES
#extension GL_KHR_shader_subgroup_quad: enable
#endif

#define NO_PROJ
#define COMPUTE_DERIVATIVES
#include<common.h>
#include<normal_mapping.h>

#define TSIZE 8
layout(local_size_x = TSIZE*TSIZE, local_size_y = 1, local_size_z = 1) in;

#include "common.h"

layout(std430, set = 0, binding = 0) buffer DecalsBuffer
{
    Decal decals[];
} decals;

layout(std430, set = 0, binding = 1) buffer DecalsInfoBuffer
{
    DecalsInfo info;
} decals_info;

layout(set = 0, binding = 2) uniform sampler2D albedo_textures[MAX_DECALS];

layout(set = 0, binding = 3) uniform sampler2D normal_maps[MAX_DECALS];

layout(set = 0, binding = 4) uniform sampler2D metalness_roughness[MAX_DECALS];

layout(set = 0, binding = 5) uniform sampler2D emission_textures[MAX_DECALS];

//tile bucket and framebuffers

layout(std430, set = 1, binding = 0) buffer BitBucketsBuffer
{
    uint buckets[];
} bitbuckets;

layout(set = 1, binding = 1) uniform sampler2D depth_buffer;

layout(set = 1, binding = 2, rgba8) uniform image2D norm_rough_met;

layout(set = 1, binding = 3) uniform sampler2D reduced_depth_sampler;

layout(set = 1, binding = 4) buffer Res
{
    uvec2 res;
}res;

layout(push_constant) uniform ProjectionData
{
    mat4 mv;
    mat4 p;
    //vec3 view_pos;
} projection_data;

const uint NBUCKETS = MAX_DECALS/32;
shared uint tile_buckets[/*NBUCKETS*/128];

void append_decal(uint index) {
    atomicOr(tile_buckets[index / 32], 1 << (index % 32));
}

vec3 transform_v3(mat4 m, vec3 iaabb) {
    return (m * vec4(iaabb, 1.0)).xyz;
}

mat2x3 transform_aabb(mat4 m, mat2x3 iaabb) {
    mat2x3 aabb = {
        transform_v3(m, iaabb[0]),
        transform_v3(m, iaabb[1])
    };
    mat2x3 minmax;
    vec3 n = vec3(aabb[0].xy, aabb[1].z);
    minmax[0] = n;
    minmax[1] = n;
    /*minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);*/
    n = vec3(aabb[0].x, aabb[1].y, aabb[0].z);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = vec3(aabb[0].x, aabb[1].y, aabb[1].z);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);

    n = vec3(aabb[1].x, aabb[0].y, aabb[0].z);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = vec3(aabb[1].x, aabb[0].y, aabb[1].z);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = vec3(aabb[1].x, aabb[1].y, aabb[0].z);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = aabb[1];
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);

    return minmax;
}

bool has_influence(Decal decal, mat2x3 tile_aabb) {
    //TODO inverse is expensive, pass inverted matrices from CPU
    mat2x3 xformed = transform_aabb(inverse(decal.world_matrix), tile_aabb);
    //aabb test with unit aabb
    mat2x3 unit_aabb = {vec3(-1.0), vec3(1.0)};
    return all(lessThan(xformed[0], unit_aabb[1])) && all(greaterThan(xformed[1], unit_aabb[0]));
}

vec3 world_space_location(vec2 uv, float depth)
{
    //converts from normalized range to -1.0..1.0
    vec4 clip_space = vec4(1.0);
    clip_space.xy = uv*2.0-1.0;
    clip_space.z = depth;

    mat4 p = projection_data.p;
    p[0][3] = 0.0;
    p[1][3] = 0.0;
    p[2][3] = -1.0;
    vec4 homogenous = inverse(p)*clip_space;
    homogenous /= homogenous.w;

    vec4 world_space = inverse(projection_data.mv) * homogenous;
    return world_space.xyz;
}

vec3 world_space_location(ivec2 ctile, float depth, ivec2 ntiles)
{
    ivec2 resolution = ntiles*ivec2(TSIZE);
    ivec2 vpntilesres = ivec2(ceil(vec2(res.res)/vec2(TSIZE))*float(TSIZE));
    ivec2 sntiles = vpntilesres * ntiles / resolution;
    vec2 uv = vec2(ctile)/vec2(sntiles);

    return world_space_location(uv, depth);
}

vec2 depth_minmax(uvec2 ctile)
{
    return texelFetch(reduced_depth_sampler, ivec2(ctile), 0).xy;
}

mat2x3 froxel_worldspace_aabb(ivec2 ctile, vec2 depth_minmax, ivec2 ntiles) {
    mat2x3 minmax;
    vec3 n = world_space_location(ctile, depth_minmax.x, ntiles);
    minmax[0] = n;
    minmax[1] = n;
    n = world_space_location(ctile+ivec2(0, 1), depth_minmax.x, ntiles);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = world_space_location(ctile+ivec2(1, 0), depth_minmax.x, ntiles);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = world_space_location(ctile+ivec2(1, 1), depth_minmax.x, ntiles);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);

    n = world_space_location(ctile, depth_minmax.y, ntiles);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = world_space_location(ctile+ivec2(0, 1), depth_minmax.y, ntiles);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = world_space_location(ctile+ivec2(1, 0), depth_minmax.y, ntiles);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);
    n = world_space_location(ctile+ivec2(1, 1), depth_minmax.y, ntiles);
    minmax[0] = min(minmax[0], n);
    minmax[1] = max(minmax[1], n);

    return minmax;
}

uvec2 remap_8x8() {
    uvec2 invocationID = uvec2(gl_LocalInvocationIndex % 2, (gl_LocalInvocationIndex % 4) / 2);
    invocationID += 2*uvec2((gl_LocalInvocationIndex / 4) % 4, gl_LocalInvocationIndex / 16);
    return invocationID;
}

//can't pass a readable image2D as parameter because parameters don't
//havea layout qualifier (format)
void blend_decals(sampler2D depth_buffer, /*image2D norm_rough_met, */uvec2 ctile) {
    uvec2 uv_pixels = ctile * uvec2(TSIZE) + /*gl_LocalInvocationID.xy*/ remap_8x8();
    vec2 uv = vec2(uv_pixels) / vec2(res.res);
    float depth = texelFetch(depth_buffer, ivec2(uv_pixels), 0).x;
    vec3 ws = world_space_location(uv, depth);

    vec4 normal_rough_met_result;
    float res_alpha = 1.0;

    //needed for normal mapping
    vec4 source_norm_rough_met = imageLoad(norm_rough_met, ivec2(uv_pixels));
    vec3 decoded_source_normal = octahedron_decode(source_norm_rough_met.xy);
    vec3 view_vector = calculate_view_vector(projection_data.mv, ws);

    uint nbuckets = uint(ceil(float(decals_info.info.n_decals) / 32.0));
    nbuckets = min(nbuckets, MAX_DECALS/32);
    for(int bucket_index = 0; bucket_index < nbuckets; bucket_index++) {
        uint bucket = tile_buckets[bucket_index];
        while(bucket != 0) {
            int bit_index = findLSB(bucket);
            bucket ^= 1 << bit_index;
            uint decal_index = bit_index + bucket_index * 32;
            Decal decal = decals.decals[decal_index];

            vec3 decal_uv = decal_texture_coordinates(ws, decal.world_matrix);

            vec4 metal_rough_alpha_sample = texture(metalness_roughness[decal.textures.z], decal_uv.xy);
            vec2 normal_map_sample = octahedron_encode(perturb_normal(decoded_source_normal, view_vector, decal_uv.xy, normal_maps[decal.textures.y]));
            //
            //perform blending
            vec4 norm_rough_met_sample = vec4(normal_map_sample.xy, metal_rough_alpha_sample.yx);
            normal_rough_met_result = mix(normal_rough_met_result, norm_rough_met_sample, res_alpha);
            if(abs(decal_uv.z) <= 1.0) {
                res_alpha *= (1.0 - metal_rough_alpha_sample.z);
            }
            //TODO exit early if all threads of the workgroup are below an alpha threshold
        }
    }

    vec4 norm_rough_met_sample = source_norm_rough_met;
    normal_rough_met_result = mix(normal_rough_met_result, norm_rough_met_sample, res_alpha);

    imageStore(norm_rough_met, ivec2(uv_pixels), normal_rough_met_result);
    //if((tile_buckets[gl_LocalInvocationID.x / 32] >> gl_LocalInvocationID.x % 32) % 2 == 1) {
    //if(tile_buckets[gl_LocalInvocationID.x / 16] != 0) {
    /*if(bucket != 0) {
        imageStore(norm_rough_met, ivec2(uv_pixels), vec4(0.0));
    }*/
}

void debug_out(uvec2 ctile, ivec2 ntiles, vec4 v) {
    vec2 depth_minmax = depth_minmax(ctile / 2);
    //world space aabb for the tile
    mat2x3 froxel_aabb = froxel_worldspace_aabb(ivec2(ctile), depth_minmax, ntiles);
    uvec2 uv_pixels = ctile * uvec2(TSIZE) + gl_LocalInvocationID.xy;
    imageStore(norm_rough_met, ivec2(uv_pixels), v);
}

void clean_buckets() {
    uint nbuckets = uint(ceil(float(decals_info.info.n_decals) / 32.0));
    uint nloops = uint(ceil(float(nbuckets) / float(TSIZE*TSIZE))) * TSIZE*TSIZE;
    for(int i = 0; i < nloops; i+= TSIZE*TSIZE) {
        uint index = i + gl_LocalInvocationIndex;
        tile_buckets[index] = 0;
    }
}

void main() {
    //HACK vulkano doesn't allow unused descriptors :(
    if(1==0) {
        textureSize(albedo_textures[0], 0);
        textureSize(emission_textures[0], 0);
    }
    clean_buckets();
    groupMemoryBarrier();
    barrier();

    ivec2 ntiles = ivec2(gl_NumWorkGroups);
    uvec2 ctile = ivec2(gl_WorkGroupID.xy);
    uint linear_tile_index = ctile.y + ctile.x * ntiles.y;
    vec2 depth_minmax = depth_minmax(ctile / 2);
    //world space aabb for the tile
    mat2x3 froxel_aabb = froxel_worldspace_aabb(ivec2(ctile), depth_minmax, ntiles);

    uint i = 0;
    uint nloops = uint(ceil(float(decals_info.info.n_decals) / float(TSIZE*TSIZE))) * TSIZE*TSIZE;

    for(;i <  nloops; i+= TSIZE*TSIZE) {
        uint index = i + gl_LocalInvocationIndex;
        if(index < decals_info.info.n_decals) {
            Decal decal = decals.decals[index];
            if(has_influence(decal, froxel_aabb)) {
                append_decal(index);
            }
        }
    }
    //debug_out(ctile, ntiles, has_influence(decals.decals[0], froxel_aabb)? vec4(1.0):vec4(vec3(0.0), 1.0)); if(1==1) return;
    //debug_out(ctile, ntiles, vec4(froxel_aabb[1]/100.0, 1.0)); if(1==1) return;

    groupMemoryBarrier();
    barrier();

    //export buckets to main memory
    uint nbuckets = uint(ceil(float(decals_info.info.n_decals) / 32.0));
    nloops = uint(ceil(float(nbuckets) / float(TSIZE*TSIZE))) * TSIZE*TSIZE;
    i = 0;
    for(;i < nloops; i+= TSIZE*TSIZE) {
        uint offset = linear_tile_index * NBUCKETS;
        uint index = i + gl_LocalInvocationIndex;
        if(index < nbuckets) {
           bitbuckets.buckets[offset + index] = tile_buckets[index];
        }
    }

    //apply to normals and metalness
    blend_decals(depth_buffer, ctile);
}

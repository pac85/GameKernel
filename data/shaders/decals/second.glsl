#extension GL_KHR_shader_subgroup_quad: enable

#define TSIZE 8
#define MAX_DECALS 4096 //TODO move to rust

#include "common.h"

layout(std430, set = DECALS_DESCRIPTOR_SET, binding = 0) buffer DecalsBuffer
{
    Decal decals[];
} decals;

layout(std430, set = DECALS_DESCRIPTOR_SET, binding = 1) buffer DecalsInfoBuffer
{
    DecalsInfo info;
} decals_info;

layout(set = DECALS_DESCRIPTOR_SET, binding = 2) uniform sampler2D albedo_textures[MAX_DECALS];

layout(set = DECALS_DESCRIPTOR_SET, binding = 3) uniform sampler2D metalness_roughness[MAX_DECALS];

layout(set = DECALS_DESCRIPTOR_SET, binding = 4) uniform sampler2D emission_textures[MAX_DECALS];

//tile bucket and framebuffers

layout(std430, set = DECALS_DESCRIPTOR_SET + 1, binding = 0) buffer BitBucketsBuffer
{
    uint buckets[];
} bitbuckets;

const uint NBUCKETS = MAX_DECALS/32;

int linear_tile_index(vec2 uv, vec2 res) {
    ivec2 ntiles = ivec2(ceil(res.x/float(TSIZE)), ceil(res.y/float(TSIZE)));
    ivec2 ctile = ivec2(int(uv.x*ntiles.x), int(uv.y*ntiles.y));
    return ctile.y + ctile.x * ntiles.y;
}

//can't pass a readable image2D as parameter because parameters don't
//havea layout qualifier (format)
mat3 blend_decals(vec3 ws, vec2 res, vec3 source_albedo, float source_metalness) {
    uvec2 uv_pixels = uvec2(gl_FragCoord.xy);
    vec2 uv = vec2(uv_pixels) / res;

    vec4 albedo_result = vec4(1.0);
    float metalness_result = 0.0;
    vec3 emission_result = vec3(0.0);

    int tile_offset = linear_tile_index(uv, res) * int(NBUCKETS);

    /*vec4 albedo_alpha_sample = texture(albedo_textures[0], uv);
    emission_result += texture(emission_textures[0], uv).xyz;
    return mat2x3(albedo_alpha_sample.xyz, emission_result + vec3(0.0*bitbuckets.buckets[0]+decals.decals[0].world_matrix[0][0]+float(decals_info.info.n_decals)));*/
    uint nbuckets = uint(ceil(float(decals_info.info.n_decals) / 32.0));
    nbuckets = min(nbuckets, MAX_DECALS/32);
    for(int bucket_index = 0; bucket_index < nbuckets; bucket_index++) {
        uint bucket = bitbuckets.buckets[tile_offset + bucket_index];
        while(bucket != 0) {
            int bit_index = findLSB(bucket);
            bucket ^= 1 << bit_index;
            uint decal_index = bit_index + bucket_index * 32;
            Decal decal = decals.decals[decal_index];

            vec3 decal_uv = decal_texture_coordinates(ws, decal.world_matrix);

            vec4 albedo_alpha_sample = texture(albedo_textures[decal.textures.x], decal_uv.xy);
            float metalness_sample = texture(metalness_roughness[decal.textures.z], decal_uv.xy).x;
            emission_result += texture(emission_textures[decal.textures.w], decal_uv.xy).xyz;

            //perform blending
            albedo_result.xyz = mix(albedo_result.xyz, albedo_alpha_sample.xyz, albedo_result.w);
            metalness_result = mix(metalness_result, metalness_sample, albedo_result.w);

            if(abs(decal_uv.z) <= 1.0) {
                albedo_result.w *= (1.0 - albedo_alpha_sample.w);
            }
        }
    }

    albedo_result.xyz  = mix(albedo_result.xyz, source_albedo, albedo_result.w);
    metalness_result  = mix(metalness_result, source_metalness, albedo_result.w);

    return mat3(albedo_result.xyz, emission_result, vec3(metalness_result));
}

#version 450

#define DECALS_DESCRIPTOR_SET 3

#include "../decals/second.glsl"

layout(location = 0) in vec3 normal;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 world_space;

layout(location = 0) out vec3 color;
layout(location = 1) out vec3 final;

layout(set = 0, binding = 0) uniform sampler2D diffuse;
layout(set = 0, binding = 1) uniform sampler2D specular;
layout(set = 0, binding = 2) uniform sampler2D half_res_specular;

layout(set = 1, binding = 0) uniform sampler2D albedo;
layout(set = 1, binding = 1) uniform sampler2D metalness;
layout(set = 1, binding = 2) uniform sampler2D emissive;

void main()
{
    ivec2 p_coords = ivec2(gl_FragCoord.xy);
    vec3 albedo_color = texture(albedo, vec2(uv.x, uv.y)).xyz;

    vec2 res = vec2(textureSize(diffuse, 0));
    float metalness = texture(metalness, vec2(uv.x, uv.y)).x;
    mat3 decal_vals = blend_decals(world_space, res, albedo_color, metalness);
    albedo_color = decal_vals[0];
    metalness = decal_vals[2].x;

    vec3 spec_color = mix(vec3(1.0), albedo_color, metalness);
    vec3 hrspec = texture(half_res_specular, vec2(p_coords) / res).xyz;
    color = albedo_color*texelFetch(diffuse, p_coords, 0).xyz + spec_color*texelFetch(specular, p_coords, 0).xyz;

    vec3 emissive_color = texture(emissive, vec2(uv.x, uv.y)).xyz + decal_vals[1];

    final = color + spec_color*hrspec + emissive_color;
}

#version 450

layout(location = 0) in vec3 normal;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec3 color;
layout(location = 1) out vec3 final;

layout(set = 0, binding = 0) uniform sampler2D diffuse;
layout(set = 0, binding = 1) uniform sampler2D specular;
layout(set = 0, binding = 2) uniform sampler2D half_res_specular;

layout(set = 1, binding = 0) uniform sampler2D albedo;
layout(set = 1, binding = 1) uniform sampler2D metalness;
layout(set = 1, binding = 2) uniform sampler2D emissive;

void main()
{
    ivec2 p_coords = ivec2(gl_FragCoord.xy);
    vec3 albedo_color = texture(albedo, vec2(uv.x, uv.y)).xyz;
    float roughness = texture(metalness, vec2(uv.x, uv.y)).x;
    vec3 spec_color = mix(vec3(1.0), albedo_color, roughness);
    vec3 hrspec = texture(half_res_specular, vec2(p_coords) / vec2(textureSize(diffuse, 0))).xyz;
    color = albedo_color*texelFetch(diffuse, p_coords, 0).xyz + spec_color*texelFetch(specular, p_coords, 0).xyz;

    vec3 emissive_color = texture(emissive, vec2(uv.x, uv.y)).xyz;

    final = color + spec_color*hrspec + emissive_color;
}

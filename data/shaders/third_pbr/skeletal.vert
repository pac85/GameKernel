#version 450

#include<skeletal.h>

layout(push_constant) uniform ProjectionData
{
    mat4 mvp;
    mat3x4 normal_matrix;
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in ivec4 affecting_bones_indices;
layout(location = 4) in vec4 weights;

layout(location = 0) out vec3 o_norm;
layout(location = 1) out vec2 o_uv;

layout(set = 2, binding = 0) buffer Bones {
    Bone bones[];
} bones;

void main() {
    vec4 new_vertex = weights.x * transform_by_bone(vec4(position, 1.0), (bones.bones[affecting_bones_indices.x])) + 
                      weights.y * transform_by_bone(vec4(position, 1.0), (bones.bones[affecting_bones_indices.y])) +
                      weights.z * transform_by_bone(vec4(position, 1.0), (bones.bones[affecting_bones_indices.z])) +
                      weights.w * transform_by_bone(vec4(position, 1.0), (bones.bones[affecting_bones_indices.w]));

    o_norm = mat3(normal_matrix) * normal;
    o_norm =         (weights.x * transform_normal_by_bone(vec4(o_norm, 0.0), (bones.bones[affecting_bones_indices.x])) + 
                      weights.y * transform_normal_by_bone(vec4(o_norm, 0.0), (bones.bones[affecting_bones_indices.y])) +
                      weights.z * transform_normal_by_bone(vec4(o_norm, 0.0), (bones.bones[affecting_bones_indices.z])) +
                      weights.w * transform_normal_by_bone(vec4(o_norm, 0.0), (bones.bones[affecting_bones_indices.w]))).xyz;

    o_uv = uv;

    gl_Position = mvp * vec4(new_vertex.xyz, 1.0);
}

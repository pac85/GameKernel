#version 450

layout(push_constant) uniform ProjectionData
{
    mat4 model;
    mat3x4 normal_matrix;
};

layout(std430, set = 5, binding = 0) buffer ViewProjection
{
    mat4 view,
         projection;
    uvec2 res;
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

layout(location = 0) out vec3 o_norm;
layout(location = 1) out vec2 o_uv;
layout(location = 2) out vec3 o_world_space;

void main()
{
    o_norm = mat3(normal_matrix) * normal;
    o_uv = uv;
    gl_Position = (projection * view * model) * vec4(position, 1.0);
    o_world_space = (model * vec4(position, 1.0)).xyz;
}

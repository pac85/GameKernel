#version 450

layout(push_constant) uniform ProjectionData
{
    mat4 mvp;
    mat3x4 normal_matrix;
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

layout(location = 0) out vec3 o_norm;
layout(location = 1) out vec2 o_uv;

void main()
{
    o_norm = mat3(normal_matrix) * normal;
    o_uv = uv;
    gl_Position = mvp*vec4(position, 1.0);
}

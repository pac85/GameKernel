#version 450

layout(location = 0) in vec3 position;

layout(push_constant) uniform ProjectionData
{
    mat4 mvp;
};

void main()
{
    gl_Position = mvp*vec4(position, 1.0);
}

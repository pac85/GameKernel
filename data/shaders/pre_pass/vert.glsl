#version 450

layout(push_constant) uniform ProjectionData
{
    mat4 model;
    mat3x4 normal_matrix;
    uvec4 prev_mvp_index;
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

layout(location = 0) out vec3 o_norm;
layout(location = 1) out vec2 o_uv;
layout(location = 2) out vec3 o_v;
layout(location = 3) out vec3 o_mpos;

layout(set = 1, binding = 0) buffer Projection
{
    mat4 view, 
         projection;
    uvec2 res;
}projection;

layout(set = 1, binding = 1) buffer PrevMvps 
{
    mat4 mvp;
}prev;

void main()
{
    mat4 mvp = projection.projection * projection.view * model;
    o_norm = normalize(mat3(normal_matrix) * normal);
    o_uv = uv;
    gl_Position = mvp * vec4(position, 1.0);

    o_mpos = position;

    vec4 camera = inverse(mvp)*vec4(0.0, 0.0, 0.0, 1.0);
    camera.xyz /= camera.w; //not necessary
    o_v = camera.xyz - position;
}

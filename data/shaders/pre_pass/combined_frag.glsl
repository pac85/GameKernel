#version 450

#define NO_PROJ
#include<common.h>
#include<normal_mapping.h>

layout(location = 0) in vec3 normal;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 view;
layout(location = 3) in vec3 mpos;

/*layout(set = 0, binding = 0) uniform sampler2D diffuse;
layout(set = 0, binding = 1) uniform sampler2D specular;*/
layout(set = 0, binding = 0) uniform sampler2D normal_map;
layout(set = 0, binding = 1) uniform sampler2D metalness_roughness_map;

layout(location = 0) out vec4 color;
layout(location = 1) out uint mesh_index;
layout(location = 2) out vec2 velocity;

layout(set = 1, binding = 0) buffer Projection
{
    mat4 view, 
         projection;
    uvec2 res;
}projection;

layout(set = 1, binding = 1) buffer PrevMvps 
{
    mat4 mvp[];
}prev;

layout(push_constant) uniform ProjectionData
{
    mat4 model;
    mat3x4 normal_matrix;
    uvec4 prev_mvp_index;
};

void main()
{
    mat4 mvp = projection.projection * projection.view * model;
    //color.xyz = (normal)/2.0+0.5;
    color.xy = octahedron_encode(perturb_normal(normal, view, uv, normal_map));
    //color.xyz = normal/2.0+0.5;
    vec2 met_rough = texture(metalness_roughness_map, uv).xy;
    float roughness = met_rough.y;
    float metalness = met_rough.x;
    color.zw = vec2(roughness, metalness * 4);
    
    vec4 cpos = mvp * vec4(mpos, 1.0);
    cpos.xyz /= cpos.w;
    vec4 prev_spos = prev.mvp[prev_mvp_index.x] * vec4(mpos, 1.0);
    prev_spos.xyz /= prev_spos.w;
    /*vec2 svelocity = gl_FragCoord.xy / vec2(projection.res) - (prev_spos.xy / 2 + 0.5);
    velocity = svelocity * (32767/65535.0) + (32767.0/65535.0);*/

    /*ivec2 svel = ivec2(gl_FragCoord.xy) - ivec2((prev_spos.xy * 0.5 + 0.5) * projection.res);
    vec2 snvel = svel / vec2(projection.res);
    //velocity = snvel * (32767/65535.0) + (32767.0/65535.0);
    velocity = snvel * 0.5 + 0.5;*/

    vec2 ndcvelocity = cpos.xy - prev_spos.xy;
    velocity = ndcvelocity * 0.25 + 0.5;
    mesh_index = prev_mvp_index.y;
}

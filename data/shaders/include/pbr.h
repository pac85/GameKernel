
//#define PI 3.141592653589793

float square(float a)
{
    return a*a;
}

float normal_distribution_ggx(vec3 normal, vec3 halfway, float roughness) 
{
    float a2 = square(square(roughness));
    float dotnh = max(dot(normal, halfway), 0.0);
    float dotnh2 = square(dotnh);

    return a2 / (PI * square(dotnh2 * (a2 - 1.0) + 1.0));
}

float k_direct(float roughness)
{
    return square(roughness + 1) / 8;
}

float k_ibl(float roughness)
{
    return square(roughness) / 2;
}

float geometry_schlick_ggx(float dotnv, float k)
{
    return dotnv / (dotnv * (1.0 - k) + k);
}

float geometry_smith(vec3 normal, vec3 vdir, vec3 point_to_light, float k)
{
    float dotnv = max(dot(normal, vdir), 0.0);
    float dotnl = max(dot(normal, point_to_light), 0.0);

    return geometry_schlick_ggx(dotnv, k) *
           geometry_schlick_ggx(dotnl, k);
}

float fresnel_schlick(float cos_theta, float F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cos_theta, 5.0);
}

float hash1(float seed) {
    return fract(sin(seed += 0.1)*43758.5453123);
}


vec2 hash2(float seed) {
    return fract(sin(vec2(seed+=0.1,seed+=0.1))*vec2(43758.5453123,22578.1459123));
}

vec2 hash22(vec2 uv) {
    return hash2(uv.x + hash1(uv.y));
}

// http://jcgt.org/published/0007/04/01/paper.pdf by Eric Heitz
// Input Ve: view direction
// Input alpha_x, alpha_y: roughness parameters
// Input U1, U2: uniform random numbers
// Output Ne: normal sampled with PDF D_Ve(Ne) = G1(Ve) * max(0, dot(Ve, Ne)) * D(Ne) / Ve.z
vec3 sample_ggx_vndf(vec3 ve, vec2 alpha, vec2  U) {
    vec3 Vh = normalize(vec3(alpha.x * ve.x, alpha.y * ve.y, ve.z));
    float lensq = length(Vh.xy);

    vec3 T1 = lensq > 0.0 ? vec3(-Vh.y, Vh.x, 0.0) * inversesqrt(lensq) : vec3(1.0, 0.0, 0.0);
    vec3 T2 = cross(Vh, T1);

    float r = sqrt(U.x);
    float phi = 2.0 * PI * U.y;
    float t1 = r * cos(phi);
    float t2 = r * sin(phi);
    float s = 0.5 * (1.0 + Vh.z);
    t2 = (1.0 - s) * sqrt(1.0 - t1 * t1) + s * t2;

    vec3 Nh = t1 * T1 + t2 * T2 + sqrt(max(0.0, 1.0 - t1 * t1 - t2 * t2)) * Vh;

    vec3 Ne = normalize(vec3(alpha.x * Nh.x, alpha.y * Nh.y, max(0.0, Nh.z)));
    return Ne;
}

mat3 tbn(vec3 n) {
    vec3 u;
    if(abs(n.z) > 0.0) {
        u = vec3(1.0, -1.0, 1.0) * vec3(0.0, n.zy) / length(n.yz);
    }
    else {
        u = vec3(1.0, -1.0, 1.0) * vec3(n.yx, 0.0) / length(n.xy);
    }

    mat3 TBN;

    TBN[0] = u;
    TBN[1] = cross(n, u);
    TBN[2] = n;

    return TBN;

}

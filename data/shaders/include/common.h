#define PI 3.141592653589793

//hack
#ifndef NO_PROJ

vec3 view_space_location(vec2 uv, float depth) {
    //converts from normalized range to -1.0..1.0
    vec4 clip_space = vec4(1.0);
    clip_space.xy = uv*2.0-1.0;
    clip_space.z = depth;

    mat4 p = projection_data.p;
    p[0][3] = 0.0;
    p[1][3] = 0.0;
    p[2][3] = -1.0;
    vec4 homogenous = inverse(p)*clip_space;
    return homogenous.xyz / homogenous.w;
}

vec3 world_space_location(vec2 uv, float depth)
{
    //converts from normalized range to -1.0..1.0
    vec4 clip_space = vec4(1.0);
    clip_space.xy = uv*2.0-1.0;
    clip_space.z = depth;

    mat4 p = projection_data.p;
    p[0][3] = 0.0;
    p[1][3] = 0.0;
    p[2][3] = -1.0;
    vec4 homogenous = inverse(p)*clip_space;
    homogenous /= homogenous.w;
    
    vec4 world_space = inverse(projection_data.mv) * homogenous;
    return world_space.xyz;
}

vec3 get_view_pos()
{
    vec3 pos;
    pos.x = projection_data.p[0][3];
    pos.y = projection_data.p[1][3];
    pos.z = projection_data.p[2][3];
    return pos;
}

#endif

void swap(inout float a, inout float b)
{
    float t = a;
    a = b;
    b = t;
}

//https://knarkowicz.wordpress.com/2014/04/16/octahedron-normal-vector-encoding/

/*vec2 oct_wrap(vec2 v) {
    return ( vec2(1.0) - abs(v) ) * vec2(v.x >= 0.0? 1.0: -1.0, v.y >= 0.0? 1.0: -1.0);
}

vec2 octahedron_encode(vec3 n) {
    n /= abs(n.x) + abs(n.y) + abs(n.z);
    n.xy = n.z >= 0.0 ? n.xy: oct_wrap(n.xy);
    n.xy = n.xy * 0.5 + 0.5;
    return n.xy;
}

vec3 octahedron_decode(vec2 f) {
    f = f * 2.0 - 1.0;

    // https://twitter.com/Stubbesaurus/status/937994790553227264
    vec3 n = vec3(f.x, f.y, 1.0 - abs(f.x) - abs(f.y));
    float t = clamp(-n.z, 0.0, 1.0);
    n.xy += vec2(n.x >= 0.0? -t: t, n.y >= 0.0? -t: t);
    return normalize(n);
}*/

//http://jcgt.org/published/0003/02/01/paper.pdf

vec2 signNotZero(vec2 v) {
    return vec2((v.x >= 0.0) ? +1.0 : -1.0, (v.y >= 0.0) ? +1.0 : -1.0);
}

//Assume normalized input.  Output is on [-1, 1] for each component.
vec2 octahedron_encode(in vec3 v) {
    //Project the sphere onto the octahedron, and then onto the xy plane
    vec2 p = v.xy*(1.0 / (abs(v.x) + abs(v.y) + abs(v.z)));
    //Reflect the folds of the lower hemisphere over the diagonals
    vec2 r = (v.z <= 0.0) ? ((1.0 - abs(p.yx))*signNotZero(p)) : p;
    return r * 0.5 + 0.5;
}

vec3 octahedron_decode(vec2 e) {
    e = e * 2.0 - 1.0;
    vec3 v = vec3(e.xy, 1.0 - abs(e.x) - abs(e.y));
    if (v.z < 0) 
        v.xy = (1.0 - abs(v.yx))*signNotZero(v.xy);
    return normalize(v);
}

vec3 cosWeightedRandomHemisphereDirection(vec2 rv2, const vec3 n ) {
	vec3  uu = normalize( cross( n, vec3(0.0,1.0,1.0) ) );
	vec3  vv = normalize( cross( uu, n ) );
	
	float ra = sqrt(rv2.y);
	float rx = ra*cos(6.2831*rv2.x); 
	float ry = ra*sin(6.2831*rv2.x);
	float rz = sqrt( 1.0-rv2.y );
	vec3  rr = vec3( rx*uu + ry*vv + rz*n );
    
    //return normalize(n + (hash3()*vec3(2.0) - vec3(1.0)));
    return normalize( rr );
}



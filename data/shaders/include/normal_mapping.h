vec2 fakeswap(vec2 v) {return vec2(0.0);}
vec3 fakeswap(vec3 v) {return vec3(0.0);}

#ifdef FAKE_DERIVATIVES
#define subgroupQuadSwapHorizontal fakeswap
#define subgroupQuadSwapVertical fakeswap
#endif
#ifdef COMPUTE_DERIVATIVES
#define DERIVATIVE_X_LANE_SIGN ((gl_LocalInvocationIndex & 1) == 0 ? 1.0 : -1.0)
#define DERIVATIVE_Y_LANE_SIGN ((gl_LocalInvocationIndex & 2) == 0 ? 1.0 : -1.0)

vec3 compute_dFdx(vec3 v) {
    vec3 vd = subgroupQuadSwapHorizontal(v);
    return (v - vd) * DERIVATIVE_X_LANE_SIGN;
}

vec3 compute_dFdy(vec3 v) {
    vec3 vd = subgroupQuadSwapVertical(v);
    return (v - vd) * DERIVATIVE_Y_LANE_SIGN;
}

vec2 compute_dFdx(vec2 v) {
    vec2 vd = subgroupQuadSwapHorizontal(v);
    return (v - vd) * DERIVATIVE_X_LANE_SIGN;
}

vec2 compute_dFdy(vec2 v) {
    vec2 vd = subgroupQuadSwapVertical(v);
    return (v - vd) * DERIVATIVE_Y_LANE_SIGN;
}

#define dFdx compute_dFdx
#define dFdy compute_dFdy
#endif

//http://www.thetenthplanet.de/archives/1180
mat3 cotangent_frame( vec3 N, vec3 p, vec2 uv ) {
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx( p );
    vec3 dp2 = dFdy( p );
    vec2 duv1 = dFdx( uv );
    vec2 duv2 = dFdy( uv );

    // solve the linear system
    vec3 dp2perp = cross( dp2, N );
    vec3 dp1perp = cross( N, dp1 );
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    // construct a scale-invariant frame
    float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
    return mat3( T * invmax, B * invmax, N );
}

#define WITH_NORMALMAP_UNSIGNED

vec3 perturb_normal( vec3 N, vec3 V, vec2 texcoord, sampler2D normal_map ) {
    // assume N, the interpolated vertex normal and
    // V, the view vector (vertex to eye)
    vec3 map = texture( normal_map, texcoord ).xyz;
#ifdef WITH_NORMALMAP_UNSIGNED
    map = (map + 1./255.) * 255./127. - 128./127.;
#endif
#ifdef WITH_NORMALMAP_2CHANNEL
    map.z = sqrt( 1. - dot( map.xy, map.xy ) );
#endif
#ifdef WITH_NORMALMAP_GREEN_UP
    map.y = -map.y;
#endif

    mat3 TBN = cotangent_frame( N, -V, texcoord );
    return normalize( TBN * map);
}

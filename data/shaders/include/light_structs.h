#define TO_RAD(x) (3.1415*x/180)

struct Tile
{
    int offset;
    int point_spot_area_probe_count;            //each 8 bits
};

struct Item
{
    int point_spot_offset, area_probe_offset;   //each 16 bits
};


layout(std430)
struct DirectionalLight
{
    vec3 color;
    vec3 direction;
    float angle;
    uint shadows;
    uint n_cascades;
    float cascade_scale;
};

layout(std430)
struct PointLight
{
    vec4 color, position;
    float size, influence_radius;
    uint shadows;
};

layout(std430)
struct SpotLight
{
    vec3 color, position;
    vec3 direction;
    float size, cone_length, cone_angle;
    uint shadows;
};

layout(std430)
struct AreaLight
{
    vec3 color, position;
    vec3 normal, tangent;
    vec2 size;
    float influence_radius;
    bool double_sided;
    uint  shadows;
};

layout(std430)
struct ReflectionProbe
{
    vec3 position;
    vec2 offsets[6];
    float size;
};

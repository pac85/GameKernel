vec3 rotate_vec3( vec4 quat, vec3 vec )
{
    return vec + 2.0 * cross( quat.xyz,  cross( quat.xyz, vec ) + quat.w * vec);
}

vec4 rotate_by_quat(vec4 q, vec4 v) {
    return vec4(rotate_vec3(q, v.xyz), v.w);
}

struct Bone {
    vec4 scale;
    vec4 rotation;
    vec4 offset;
    mat4 inverse_bind_matrix;
};

vec4 transform_by_mat4(mat4 m, vec4 v) {
    vec4 h = m * v;
    return h/* / h.w*/;
}

vec4 transform_by_bone(vec4 x, Bone bone) {
    return rotate_by_quat(bone.rotation, bone.scale * transform_by_mat4(bone.inverse_bind_matrix, x)) + bone.offset;
    //return bone.inverse_bind_matrix * rotate_by_quat(x + bone.offset, bone.rotation);
}

vec4 transform_normal_by_bone(vec4 x, Bone bone) {
    return rotate_by_quat(bone.rotation, transform_by_mat4(bone.inverse_bind_matrix, x));
    //return bone.inverse_bind_matrix * rotate_by_quat(x + bone.offset, bone.rotation);
}

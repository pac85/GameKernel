Game Kernel
=======

About 
-----------

This project is a game engine made for fun written in rust that uses vulkan.
Features include:

* Deferred lighting renderer with tiling for light culling
* shadow mapping
* PBR and screen space reflections 
* GPU particles
* Software (GPGPU) raytracing
* Binaural audio and reverberation
* ECS based

Example project
-----------

You can find an example minimal first person shooter [here](https://gitlab.com/pac85/game-kernel-test-game)

Screenshots
-----------

![editor](screenshots/ed.png)
![game](screenshots/game.png)

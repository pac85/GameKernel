use game_kernel_utils;
use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::{punctuated::Punctuated, token::Comma, GenericArgument, PathArguments};

#[proc_macro_derive(MetaStruct, attributes(arg_default, arg_conv))]
pub fn hello_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_component_macro(&ast)
}

fn inner_path_arg(p: PathArguments) -> Punctuated<GenericArgument, Comma> {
    match p {
        PathArguments::AngleBracketed(a) => a.args,
        _ => panic!("Arc without arguments"),
    }
}

fn extract_arc_inner(path: &syn::Path) -> Option<Punctuated<GenericArgument, Comma>> {
    let segs: Vec<_> = path.segments.clone().into_iter().collect();
    segs.last()
        .and_then(|syn::PathSegment { ident, arguments }| {
            if format!("{ident}").as_str() == "Arc" {
                Some(inner_path_arg(arguments.clone()))
            } else {
                None
            }
        })
}

fn has_attribute(f: &syn::Field, name: &str) -> bool {
    f.attrs.iter().any(|attr| {
        attr.path
            .get_ident()
            .map(|id| format!("{id}") == name)
            .unwrap_or(false)
    })
}

fn impl_component_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let fields = match ast.data {
        syn::Data::Struct(syn::DataStruct {
            fields: syn::Fields::Named(ref fields_named),
            ..
        }) => &fields_named.named,
        _ => todo!(),
    };
    let fields: Vec<_> = fields.iter().collect();

    let creation_tokens = fields.iter().map(|f| {
        let arg_default = has_attribute(f, "arg_default");
        let arg_conv = f.attrs.iter().find(|attr| attr.path.get_ident().map(|id| format!("{id}") == "arg_conv").unwrap_or(false)).map(|attr| {
            if let Ok(syn::Meta::List(al)) = attr.parse_meta() {
                let mut al = al.nested.iter();
                let conv_f = al.next().expect("arg_default expects a conversion function name");
                match conv_f {
                    syn::NestedMeta::Meta(syn::Meta::Path(conv_f_path)) => {
                        conv_f_path.clone()
                    },
                    _ => panic!("")
                }
            } else {
                panic!("");
            }
        });
        let ident = f.ident.as_ref().unwrap();
        let ty = &f.ty;

        if arg_default {
            return quote!{
                #ident: Default::default()
            };
        }
        match (ty, arg_conv) {
            (_, Some(conv_f)) => {
                //if let syn::Meta::List(l) = f.attrs.parse
                quote! {
                    #ident: {
                        component_catch!({#conv_f(<Arc<_> as AsRef<_>>::as_ref(&parse_component_arg_infer!(args, stringify!(#ident))))}).or_else(|_| {
                            component_catch!({parse_component_arg!(args, stringify!(#ident), #ty).as_ref().clone()})
                        })?
                    }
                }
            },
            //TODO use if let guards when stabilized
            (syn::Type::Path(ts), _) if extract_arc_inner(&ts.path).is_some() => {
                let inner_ty = extract_arc_inner(&ts.path);
                quote! {
                #ident: parse_component_arg!(args, stringify!(#ident), #inner_ty).clone()
                }
            },
            _ => quote! {
                #ident: parse_component_arg!(args, stringify!(#ident), #ty).as_ref().clone()
            },
        }
    });

    let is_pub = |f: &&&syn::Field| match f.vis {
        syn::Visibility::Public(_) => true,
        _ => false,
    };
    let get_field_tokens = fields.iter().filter(is_pub).filter_map(|f| {
        let name = f.ident.as_ref()?;

        let t = quote! {
            stringify!(#name) => Some(&self.#name as &dyn Any)
        };

        Some(t)
    });

    let get_field_mut_tokens = fields.iter().filter(is_pub).filter_map(|f| {
        let name = f.ident.as_ref()?;

        let t = quote! {
            stringify!(#name) => Some(&mut self.#name as &mut dyn Any)
        };

        Some(t)
    });

    let get_public_fields_token = fields.iter().filter(is_pub).filter_map(|f| {
        let name = f.ident.as_ref()?;

        let t = quote! {
            stringify!(#name)
        };

        Some(t)
    });

    let get_fields_token = fields.iter().filter_map(|f| {
        let name = f.ident.as_ref()?;

        let t = quote! {
            stringify!(#name)
        };

        Some(t)
    });

    let get_fields_types_static_tokens = fields.iter().map(|f| {
        let ident = f.ident.as_ref().unwrap();
        let ty = &f.ty;
        let p = is_pub(&f);

        quote!{
            map.insert(stringify!(#ident).to_owned(), #name::new_field(TypeId::of::<#ty>(), std::any::type_name::<#ty>(), ordering, #p));
            ordering += 1;
        }
    });

    let get_meta_field_tokens = fields.iter().filter_map(|f| {
        if has_attribute(f, "meta_field") {
            Some(quote! {
                stringify!(#name) => Some(&self.#name as &dyn MetaStruct)
            })
        } else {
            None
        }
    });

    let get_meta_field_tokens_mut = fields.iter().filter_map(|f| {
        if has_attribute(f, "meta_field") {
            Some(quote! {
                stringify!(#name) => Some(&mut self.#name as &mut dyn MetaStruct)
            })
        } else {
            None
        }
    });

    let is_meta_field_tokens = fields.iter().filter_map(|f| {
        if has_attribute(f, "meta_field") {
            Some(quote! {
                stringify!(#name) => true
            })
        } else {
            None
        }
    });

    let dummy_const = format_ident!("_IMPL_COMPONENT_FOR_{}", name);
    let gen = quote! {
        const #dummy_const: () = {
            use std::any::TypeId;
            use std::collections::HashMap;
            use std::any::Any;
            use std::sync::Arc;
            use std::boxed::Box;

            impl MetaStruct for #name {
                fn get_name() -> String where Self: Sized{
                    stringify!(#name).to_owned()
                }

                fn get_name_dyn(&self) -> String where Self: Sized{
                    stringify!(#name).to_owned()
                }

                fn get_builder() -> fn(args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>)-> Result<std::boxed::Box<dyn MetaStruct>, MetaStructCreationError>
                {
                    use std::convert::AsRef;
                    |args|{
                        //#name::component_new(args)
                        Ok(Box::new(#name{
                            #(#creation_tokens),*
                        }))
                    }
                }

                fn get_field_any(&self, name: &str) -> Option<&dyn Any> {
                    match name {
                        #(#get_field_tokens,)*
                        _ => None,
                    }
                }

                fn get_field_any_mut(&mut self, name: &str) -> Option<&mut dyn Any> {
                    match name {
                        #(#get_field_mut_tokens,)*
                        _ => None,
                    }
                }

                fn get_fields(&self) -> &'static [&'static str] {
                    &[#(#get_fields_token),*]
                }

                fn get_public_fields(&self) -> &'static [&'static str] {
                    &[#(#get_public_fields_token),*]
                }

                fn get_fields_types_static() -> HashMap<String, Field> {
                    let mut map = HashMap::new();
                    let mut ordering = 0;
                    #(#get_fields_types_static_tokens);*
                    map
                }

                fn get_meta_field(&self, name: &str) -> Option<&dyn MetaStruct> {
                    match name {
                        #(#get_meta_field_tokens,)*
                        _ => None,
                    }
                }

                fn get_meta_field_mut(&mut self, name: &str) -> Option<&mut dyn MetaStruct> {
                    match name {
                        #(#get_meta_field_tokens_mut,)*
                        _ => None,
                    }
                }

                fn is_field_meta(name: &str) -> bool {
                    match name {
                        #(#is_meta_field_tokens,)*
                        _ => false,
                    }
                }

                fn into_any(self: Box<Self>) -> Box<dyn Any>
                {
                    self
                }

                fn gk_as_any(&self) -> &dyn Any
                {
                    self
                }

                fn gk_as_any_mut(&mut self) -> &mut dyn Any
                {
                    self
                }

                fn dyn_clone(&self) -> Box<dyn MetaStruct> {
                    Box::new(self.clone())
                }
            }
        };
    };
    //panic!("{}", gen);
    gen.into()
}

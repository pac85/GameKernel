use std::{
    any::{Any, TypeId},
    collections::HashMap,
    fmt::Display,
    sync::Arc,
};

#[derive(Clone, Debug)]
pub enum MetaStructCreationError {
    NoSuchMetaStruct,
    MissingArgument(String),
    WrongArgument(String),
    MismatechedArgumentType(String),
}

impl Display for MetaStructCreationError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub struct Field {
    pub type_id: TypeId,
    pub type_name: &'static str,
    pub ordering: u32,
    pub is_public: bool,
}

impl Field {
    pub fn new(type_id: TypeId, type_name: &'static str, ordering: u32, is_public: bool) -> Self {
        Self {
            type_id,
            type_name,
            ordering,
            is_public,
        }
    }
}

pub trait MetaStruct: Any {
    fn get_name() -> String
    where
        Self: Sized;
    fn get_name_dyn(&self) -> String;
    fn get_builder() -> fn(
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn MetaStruct>, MetaStructCreationError>
    where
        Self: Sized;
    fn get_field_any(&self, name: &str) -> Option<&dyn Any>;
    fn get_field_any_mut(&mut self, name: &str) -> Option<&mut dyn Any>;
    fn get_fields(&self) -> &'static [&'static str];
    fn get_public_fields(&self) -> &'static [&'static str];
    fn get_fields_types_static() -> HashMap<String, Field>
    where
        Self: Sized;
    fn get_meta_field(&self, name: &str) -> Option<&dyn MetaStruct>;
    fn get_meta_field_mut(&mut self, name: &str) -> Option<&mut dyn MetaStruct>;
    fn is_field_meta(name: &str) -> bool
    where
        Self: Sized;
    fn into_any(self: Box<Self>) -> Box<dyn Any>;
    fn gk_as_any(&self) -> &dyn Any;
    fn gk_as_any_mut(&mut self) -> &mut dyn Any;

    fn dyn_clone(&self) -> Box<dyn MetaStruct>;

    fn new_field(type_id: TypeId, type_name: &'static str, ordering: u32, is_public: bool) -> Field
    where
        Self: Sized,
    {
        Field {
            type_id,
            type_name,
            ordering,
            is_public,
        }
    }
}

impl Clone for Box<dyn MetaStruct> {
    fn clone(&self) -> Self {
        self.dyn_clone()
    }
}

pub trait MetaStructReflect {
    fn get_field<T: 'static>(&self, name: &str) -> Option<&T>;
    fn get_field_mut<T: 'static>(&mut self, name: &str) -> Option<&mut T>;
}

impl<C: MetaStruct> MetaStructReflect for C {
    fn get_field<T: 'static>(&self, name: &str) -> Option<&T> {
        self.get_field_any(name).and_then(|f| f.downcast_ref())
    }

    fn get_field_mut<T: 'static>(&mut self, name: &str) -> Option<&mut T> {
        self.get_field_any_mut(name).and_then(|f| f.downcast_mut())
    }
}

impl MetaStructReflect for dyn MetaStruct {
    fn get_field<T: 'static>(&self, name: &str) -> Option<&T> {
        self.get_field_any(name).and_then(|f| f.downcast_ref())
    }

    fn get_field_mut<T: 'static>(&mut self, name: &str) -> Option<&mut T> {
        self.get_field_any_mut(name).and_then(|f| f.downcast_mut())
    }
}

pub struct MetaStructFactory {
    components_map: HashMap<
        String,
        fn(
            args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
        ) -> Result<Box<dyn MetaStruct>, MetaStructCreationError>,
    >,
    ids_map: HashMap<TypeId, String>,
    rids_map: HashMap<String, TypeId>,
    fields_maps: HashMap<String, HashMap<String, Field>>,
}

impl MetaStructFactory {
    pub fn new() -> Self {
        Self {
            components_map: HashMap::new(),
            ids_map: HashMap::new(),
            rids_map: HashMap::new(),
            fields_maps: HashMap::new(),
        }
    }

    pub fn register<T>(&mut self)
    where
        T: MetaStruct,
    {
        self.components_map.insert(T::get_name(), T::get_builder());
        self.ids_map.insert(TypeId::of::<T>(), T::get_name());
        self.rids_map.insert(T::get_name(), TypeId::of::<T>());
        self.fields_maps
            .insert(T::get_name(), T::get_fields_types_static());
    }

    pub fn instantiate(
        &mut self,
        name: &str,
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn MetaStruct>, MetaStructCreationError> {
        match self.components_map.get(name) {
            Some(builder_fn) => builder_fn(args),
            None => Err(MetaStructCreationError::NoSuchMetaStruct),
        }
    }

    pub fn all_names<'a>(&'a self) -> impl Iterator<Item = String> + 'a {
        self.components_map.iter().map(|(n, _)| n.clone())
    }

    pub fn get_fields(&self, component_name: &str) -> Option<&HashMap<String, Field>> {
        self.fields_maps.get(component_name)
    }

    pub fn get_name(&self, id: TypeId) -> Option<&String> {
        self.ids_map.get(&id)
    }

    pub fn get_id(&self, name: &str) -> Option<TypeId> {
        self.rids_map.get(name).cloned()
    }
}

use std::sync::Mutex;

lazy_static::lazy_static! {
    pub static ref META_STRUCT_FACTORY: Mutex<MetaStructFactory> = Mutex::new(MetaStructFactory::new());
}

#[macro_export]
macro_rules! parse_component_arg {
    ($args:expr, $name:expr, $arg_type:ty) => {
        ($args
            .get($name)
            .ok_or(MetaStructCreationError::MissingArgument($name.to_owned()))?
            .clone()
            .downcast::<$arg_type>()
            .map_err(|_| MetaStructCreationError::MismatechedArgumentType($name.to_owned()))?)
    };
}

#[macro_export]
macro_rules! parse_component_arg_infer {
    ($args:expr, $name:expr) => {
        ($args
            .get($name)
            .ok_or(MetaStructCreationError::MissingArgument($name.to_owned()))?
            .clone()
            .downcast()
            .map_err(|_| MetaStructCreationError::MismatechedArgumentType($name.to_owned()))?)
    };
}

#[macro_export]
macro_rules! component_catch {
    ($in:block) => {
        (|| -> Result<_, game_kernel_ecs::ecs::component::MetaStructCreationError> { Ok($in) })()
    };
}

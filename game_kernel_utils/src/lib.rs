#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub mod hierarchy;
pub mod factory;

use std::collections::{HashMap, HashSet};
#[derive(Clone)]
pub struct AutoIndexMap<T> {
    data: Vec<Option<T>>,
    current_index: u64,
}

pub type KeyType = u64;
impl<T> AutoIndexMap<T> {
    pub fn new() -> Self {
        Self {
            data: Vec::new(),
            current_index: 0,
        }
    }

    pub fn insert(&mut self, v: T) -> u64 {
        self.data.push(Some(v));
        self.data.len() as u64 - 1
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = (KeyType, &'a T)> {
        self.data
            .iter()
            .enumerate()
            .map(|(k, v)| (k as KeyType, v))
            .filter(|(_, i)| i.is_some())
            .map(|(i, e)| (i, e.as_ref().unwrap()))
    }

    pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item = (KeyType, &'a mut T)> {
        self.data
            .iter_mut()
            .enumerate()
            .map(|(k, v)| (k as KeyType, v))
            .filter(|(_, i)| i.is_some())
            .map(|(i, e)| (i, e.as_mut().unwrap()))
    }

    pub fn get(&self, k: KeyType) -> Option<&T> {
        self.data.get(k as usize)?.as_ref()
    }

    pub fn get_mut(&mut self, k: KeyType) -> Option<&mut T> {
        self.data.get_mut(k as usize)?.as_mut()
    }

    pub fn is_empty(&self) -> bool {
        self.data.is_empty() || self.data.iter().find(|e| e.is_some()).is_none()
    }

    pub fn len(&self) -> usize {
        self.data.iter().filter(|e| e.is_some()).count()
    }

    pub fn remove(&mut self, k: KeyType) -> Option<T> {
        self.data.get_mut(k as usize)?.take()
    }
}

impl<T: Eq + PartialEq> AutoIndexMap<T> {
    pub fn contains(&self, x: &T) -> bool {
        self.iter().find(|(i, e)| *e == x).is_some()
    }
}

use std::ops::{Index, Deref};

impl<T> Index<&KeyType> for AutoIndexMap<T> {
    type Output = T;

    fn index(&self, index: &KeyType) -> &T {
        &self.data[*index as usize].as_ref().unwrap()
    }
}

pub trait MapAbstract<K, V> {
    fn get<Q: Sized>(&self, k: &Q) -> Option<&V>
    where
        Self: Sized;

    fn get_mut<Q: Sized>(&mut self, k: &Q) -> Option<&mut V>
    where
        Self: Sized;
}

impl<K, V> MapAbstract<K, V> for HashMap<K, V> {
    fn get<Q: Sized>(&self, k: &Q) -> Option<&V>
    where
        Self: Sized,
    {
        self.get(k)
    }

    fn get_mut<Q: Sized>(&mut self, k: &Q) -> Option<&mut V>
    where
        Self: Sized,
    {
        self.get_mut(k)
    }
}

#[test]
fn test_auto_index_map() {
    let mut aim = AutoIndexMap::new();
    let k1 = aim.insert(1);
    let k2 = aim.insert(2);
    let k3 = aim.insert(3);

    assert!(aim.iter().zip([1, 2, 3]).all(|((i, a), b)| *a == b && b == i + 1));
    aim.remove(k2);
    assert!(aim.iter().zip([1, 3]).all(|((i, a), b)| *a == b && b == i + 1));
    aim.insert(4);
    assert!(aim.iter().zip([1, 3, 4]).all(|((i, a), b)| *a == b && b == i + 1));
}

use std::marker::PhantomData;

pub struct MappedMap<K, V, N, M, F, FM>
where
    M: MapAbstract<K, V>,
    F: Fn(&V) -> &N,
    FM: Fn(&mut V) -> &mut N,
{
    mapped: M,
    f: F,
    fm: FM,
    p1: PhantomData<K>,
    p2: PhantomData<V>,
    p3: PhantomData<N>,
}

impl<K, V, N, M, F, FM> MappedMap<K, V, N, M, F, FM>
where
    M: MapAbstract<K, V>,
    F: Fn(&V) -> &N,
    FM: Fn(&mut V) -> &mut N,
{
    fn new(mapped: M, f: F, fm: FM) -> Self {
        Self {
            mapped,
            f,
            fm,
            p1: PhantomData,
            p2: PhantomData,
            p3: PhantomData,
        }
    }

    fn get<Q: Sized>(&self, k: &Q) -> Option<&N> {
        Some((self.f)(self.mapped.get(k)?)) //doh
    }

    fn get_mut<Q: Sized>(&mut self, k: &Q) -> Option<&mut N> {
        Some((self.fm)(self.mapped.get_mut(k)?)) //doh
    }
}

impl<K, V, N, M, F, FM> MapAbstract<K, N> for MappedMap<K, V, N, M, F, FM>
where
    M: MapAbstract<K, V>,
    F: Fn(&V) -> &N,
    FM: Fn(&mut V) -> &mut N,
{
    fn get<Q: Sized>(&self, k: &Q) -> Option<&N>
    where
        Self: Sized,
    {
        self.get(k)
    }

    fn get_mut<Q: Sized>(&mut self, k: &Q) -> Option<&mut N>
    where
        Self: Sized,
    {
        self.get_mut(k)
    }
}

pub trait MapMap<K, V> {
    fn map_map<N, F, FM, M>(self, f: F, fm: FM) -> MappedMap<K, V, N, Self, F, FM>
    where
        F: Fn(&V) -> &N,
        FM: Fn(&mut V) -> &mut N,
        Self: MapAbstract<K, V>,
        Self: Sized,
    {
        MappedMap::new(self, f, fm)
    }
}

impl<K, V> MapMap<K, V> for HashMap<K, V> {}

pub struct SparseSet<T> {
    sparse: Vec<Option<usize>>,
    dense: Vec<(usize, T)>,
}

impl<T> SparseSet<T> {
    pub fn new() -> Self {
        Self {
            sparse: vec![],
            dense: vec![],
        }
    }

    pub fn insert(&mut self, k: usize, v: T) {
        if k >= self.sparse.len() {
            self.sparse.resize(k + 1, None);
        }

        self.sparse[k] = Some(self.dense.len());
        self.dense.push((k, v));
    }

    pub fn get(&self, k: usize) -> Option<&T> {
        self.dense.get((*self.sparse.get(k)?)?).map(|(_, v)| v)
    }

    pub fn get_mut(&mut self, k: usize) -> Option<&mut T> {
        self.dense.get_mut((*self.sparse.get(k)?)?).map(|(_, v)| v)
    }

    pub fn remove(&mut self, k: usize) -> Option<T> {
        let dk = (*self.sparse.get(k)?)?;
        self.sparse[k] = None;

        let (_, ov) = self.dense.swap_remove(dk);

        //Will be none if we remove the last element
        if let Some((ok, _)) = self.dense.get(dk) {
            self.sparse[*ok] = Some(dk);
        }

        Some(ov)
    }

    pub fn iter(&self) -> impl Iterator<Item = Option<&T>> {
        self.sparse.iter().map(move |v| self.dense.get((*v)?).map(|(_, v)| v))
    }

    pub fn len(&self) -> usize {
        self.sparse.len()
    }

    pub fn clear(&mut self) {
        self.sparse.clear();
        self.dense.clear();
    }
}

#[test]
fn test_sparse_set() {
    let mut ss = SparseSet::new();
    ss.insert(1, 1);
    ss.insert(2, 2);
    ss.insert(3, 3);

    assert_eq!(ss.get(0), None);
    assert_eq!(ss.get(1), Some(&1));
    assert_eq!(ss.get(2), Some(&2));
    assert_eq!(ss.get(3), Some(&3));

    ss.remove(1);

    assert_eq!(ss.get(0), None);
    assert_eq!(ss.get(1), None);
    assert_eq!(ss.get(2), Some(&2));
    assert_eq!(ss.get(3), Some(&3));

    ss.remove(2);

    assert_eq!(ss.get(0), None);
    assert_eq!(ss.get(1), None);
    assert_eq!(ss.get(2), None);
}

#[test]
fn sparse_set_delete_non_existing() {
    let mut ss = SparseSet::new();
    ss.insert(1, 1);
    ss.insert(2, 2);

    assert_eq!(ss.remove(3), None);
}

pub struct Framestamps {
    frame_stamps: Vec<Option<u64>>,
    frame_counter: u64,
}

impl Framestamps {
    pub fn new() -> Self {
        Self {
            frame_stamps: vec![],
            frame_counter: 0,
        }
    }

    pub fn set_last_seen(&mut self, index: KeyType) {
        let index = index as usize;
        if self.frame_stamps.len() <= index {
            self.frame_stamps.resize(index + 1, None);
        }

        self.frame_stamps[index] = Some(self.frame_counter);
    }

    pub fn is_element_old(&self, index: KeyType) -> bool {
        let e = self.frame_stamps.get(index as usize);
        match e {
            Some(Some(v)) if *v < self.frame_counter => true,
            _ => false,
        }
    }

    pub fn element_removed(&mut self, index: KeyType) {
        self.frame_stamps[index as usize] = None;
    }

    pub fn next_frame(&mut self) {
        self.frame_counter += 1;
    }
}

pub struct HashFramestamps<T: std::hash::Hash + Eq> {
    frame_stamps: HashMap<T, u64>,
    frame_counter: u64,
}

impl<T: std::hash::Hash + Eq> HashFramestamps<T> {
    pub fn new() -> Self {
        Self {
            frame_stamps: HashMap::new(),
            frame_counter: 0,
        }
    }

    pub fn set_last_seen(&mut self, index: T) {
        self.frame_stamps.insert(index, self.frame_counter);
    }

    pub fn is_element_old(&self, index: &T) -> bool {
        let e = self.frame_stamps.get(index);
        match e {
            Some(v) if *v < self.frame_counter => true,
            _ => false,
        }
    }

    pub fn element_removed(&mut self, index: &T) {
        self.frame_stamps.remove(index);
    }

    pub fn next_frame(&mut self) {
        self.frame_counter += 1;
    }
}

use cgmath;
use cgmath::{Matrix3, Matrix4, /*BaseFloat, InnerSpace, SquareMatrix*/};

/*pub trait MatOps<T> {
    fn transponse(&self) -> Matrix3<T>;
    fn inverse(&self) -> Matrix3<T>;
}

fn mat3_transponse<T: Copy>(a: &Matrix3<T>) -> Matrix3<T> {
    Matrix3::new(a[0][0], a[1][0], a[2][0],
                 a[0][1], a[1][1], a[2][1],
                 a[0][2], a[1][2], a[2][2],)
}

*/
pub fn mat4_transponse<T: Copy>(a: &Matrix4<T>) -> Matrix4<T> {
    Matrix4::new(a[0][0], a[1][0], a[2][0], a[3][0],
                 a[0][1], a[1][1], a[2][1], a[3][1],
                 a[0][2], a[1][2], a[2][2], a[3][2],
                 a[0][3], a[1][3], a[2][3], a[3][3],)
}
/*
impl<T: BaseFloat> MatOps<T> for Matrix3<T> {
    fn transponse(&self) -> Matrix3<T> {
        mat3_transponse(self)
    }

    fn inverse(&self) -> Matrix3<T> {
        let mt = Matrix3::new(self[0][0], self[1][0], self[2][0],
                     self[0][1], self[1][1], self[2][1],
                     self[0][2], self[1][2], self[2][2],);
        let det = mt.x.cross(mt.y).dot(mt.z);
        let adj = Matrix3::from_cols(mt.y.cross(mt.z), mt.z.cross(mt.x), mt.x.cross(mt.y));
        adj/det
    }
}*/

pub fn submat3<T: Copy>(a: Matrix4<T>) -> Matrix3<T> {
    Matrix3::new(a[0][0], a[0][1], a[0][2], 
                 a[1][0], a[1][1], a[1][2],
                 a[2][0], a[2][1], a[2][2],)
}

pub fn mat3_extend(a: Matrix3<f32>) -> [[f32; 4]; 3] {
    [[a[0][0], a[0][1], a[0][2], 0.0],
    [a[1][0], a[1][1], a[1][2], 0.0],
    [a[2][0], a[2][1], a[2][2], 0.0],]
}

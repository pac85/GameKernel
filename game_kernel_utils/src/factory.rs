use std::collections::HashMap;
use std::sync::Arc;
use std::any::*;

#[derive(Clone, Debug)]
pub enum CreationError {
    NoRegisteredConstructor(String),
    MissingArgument(String),
    WrongArgument(String),
    MismatechedArgumentType(String),
}

pub trait Buildable<T> {
    fn get_builder() -> FactoryConstructor<T> 
    where
        Self: Sized;
    fn get_name() -> String 
    where
        Self: Sized;
}

pub type FactoryConstructor<T> = fn(
    args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
) -> Result<T, CreationError>;

pub struct Factory<T> {
    constructors: HashMap<String, FactoryConstructor<T>>,

    ids_map: HashMap<TypeId, String>,
}

impl<T> Factory<T> 
where
    T: 'static
{
    pub fn new() -> Self {
        Self {
            constructors: HashMap::new(),
            ids_map: HashMap::new(),
        }
    }

    /*pub fn register<T>(&mut self, constructor: EmitterBuilder)
    where
        T: Emitter,
    {
        self.constructors
            .insert(std::any::type_name::<T>().to_owned(), constructor);
    }*/

    pub fn register<B>(&mut self)
    where
        B: Buildable<T>,
    {
        self.constructors.insert(B::get_name(), B::get_builder());
        self.ids_map.insert(TypeId::of::<T>(), B::get_name());
    }

    pub fn instantiate(
        &self,
        name: &str,
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<T, CreationError> {
        match self.constructors.get(name) {
            Some(builder_fn) => builder_fn(args),
            None => Err(CreationError::NoRegisteredConstructor(name.to_owned())),
        }
    }
}

#[macro_export]
macro_rules! get_dynamic_arg {
    ($args:expr, $name:expr, $arg_type:ty) => {
        ($args
            .get($name)
            .ok_or(CreationError::MissingArgument($name.to_owned()))?
            .clone()
            .downcast::<$arg_type>()
            .map_err(|_| CreationError::MismatechedArgumentType($name.to_owned()))?)
    };
}

